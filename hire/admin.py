from django.contrib import admin

# Register your models here.
from hire.models import HiredJobsContract, ContractStatus, MileStoneStatus, JobsMilestone, TimesheetStatus, TimeWork, \
    WeeklyTimeEntry

admin.site.register(ContractStatus)
admin.site.register(MileStoneStatus)
admin.site.register(JobsMilestone)
admin.site.register(HiredJobsContract)
admin.site.register(TimesheetStatus)
admin.site.register(TimeWork)
admin.site.register(WeeklyTimeEntry)
