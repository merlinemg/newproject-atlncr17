from django.conf.urls import url
from django.contrib.auth.decorators import login_required

from freelancer.views import FreelancerProfile, FreelancerProfileView, JobDetailsView, FreelancerJobList, \
    JoblistPaginations, FreelancerView

urlpatterns = [
    url(r'freelancer-profile/$', login_required(FreelancerProfile.as_view()), name='freelancerprofile'),
    url(r'freelancer-profile-view/$', login_required(FreelancerProfileView.as_view()), name='freelancerprofileview'),
    url(r'jobs-list/$', login_required(FreelancerJobList.as_view()), name='freelancerjoblist'),
    url(r'^freelancer_job_details/(?P<slug>[-\w]+)/$', login_required(JobDetailsView.as_view()), name='jobdetailsview'),
    url(r'paginations-jobs-freelancer/$', JoblistPaginations.as_view(), name='jobspaginationfreelancer'),
    url(r'profile_view/(?P<slug>[-\w]+)/$', FreelancerView.as_view(), name='profileview'),
]