from django import forms

from accounts.models import Users
from site_administration.models import Role_Permission, Permission


class RolePermissionform(forms.ModelForm):
    # users = forms.MultipleChoiceField(User.objects.all(), widget=forms.CheckboxSelectMultiple)
    role = forms.CharField(label="role", max_length=50,
                           widget=forms.TextInput(
                               attrs={'name': 'role','class':"filter form-control"}))

    #permission =forms.MultipleChoiceField(Permission.objects.all(), widget=forms.CheckboxSelectMultiple)formsname="duallistbox_demo2" class="demo2"

    class Meta:
        model = Role_Permission
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(RolePermissionform, self).__init__(*args, **kwargs)

        for field in self.fields:
            self.fields['permission'].widget.attrs['class'] = 'demo2'


    def clean_role(self):
        role = self.cleaned_data.get('role')

        if role and Role_Permission.objects.filter(role=role).count():
            raise forms.ValidationError('Role must be unique.')
        if self.cleaned_data["role"].strip() == '':
            raise forms.ValidationError("Role name is required.")
        return self.cleaned_data["role"]

    def clean_permission(self):
        print self.cleaned_data.get('permission'),"SSSSSSSS"
        if not self.cleaned_data.get('permission'):
            raise forms.ValidationError("Permission is required.")
        return self.cleaned_data["permission"]

    def save(self):
        form = super(RolePermissionform, self).save()

        self.object = form.save()
        self.object = form.save()
        # form.save()
        return form

class RolePermissionUpdateform(forms.ModelForm):
    # users = forms.MultipleChoiceField(User.objects.all(), widget=forms.CheckboxSelectMultiple)
    role = forms.CharField(label="role", max_length=50,
                           widget=forms.TextInput(
                               attrs={'name': 'role','class':"filter form-control"}))

    class Meta:
        model = Role_Permission
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(RolePermissionUpdateform, self).__init__(*args, **kwargs)

        for field in self.fields:
            self.fields['permission'].widget.attrs['class'] = 'demo2'

    def clean_role(self):
        role = self.cleaned_data.get('role')

        if role and Role_Permission.objects.filter(role=role).exclude(role=role).count():
            raise forms.ValidationError('Role must be unique.')
        if self.cleaned_data["role"].strip() == '':
            raise forms.ValidationError("Role name is required.")
        return self.cleaned_data["role"]

    def clean_permission(self):

        if not self.cleaned_data.get('permission'):
            raise forms.ValidationError("Permission is required.")
        return self.cleaned_data["permission"]

    def save(self):
        form = super(RolePermissionUpdateform, self).save()
        for permission_obj in self.cleaned_data.get('permission'):
            print permission_obj.id
            form.permission.add(permission_obj)
        form.save()  # form.save()
        # form.save()
        return form