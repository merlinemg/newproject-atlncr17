import base64
import json

import datetime
import threading

from django.core import serializers
from django.core.files.base import ContentFile
from django.core.mail import send_mail
from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.
from django.template.loader import render_to_string
from django.utils.crypto import get_random_string
from django.views.generic import ListView
from rolepermissions.mixins import HasPermissionsMixin
from django.views.generic import TemplateView
from cities_light.models import Country, Region, City

from accounts.models import Users, User_Type
from freelancer.models import WorkCategories, Skills, WorkSubCategories, Certifications
from freelancer_company.forms import Freelancer_Company_ProfileForm
from freelancer_company.mixins import CompanyMixin
from freelancer_company.models import Freelancer_Company_Profile, Freelancer_company_Portfolio, \
    Freelancer_company_Certifications, Freelancer_company_HourlyRate, Freelancer_company_LocationDetails, \
    Invited_Freelancer_Company_User, FreelancerCompanyPortfolioimages


class FreelancerCompany(HasPermissionsMixin, CompanyMixin, TemplateView):
    required_permission = 'add_Freelancer_company'
    template_name = 'freelancer/freelancer_company.html'

    def post(self, request, *args, **kwargs):
        method = request.POST['method']
        if method == 'Save_freelancer_company_profile':
            if request.user.invite_type == 'OWNER':
                 self.check_exist_freelancer_company(request.user)


            user = self.check_freelancer_company_profile()
            form = Freelancer_Company_ProfileForm(request.POST, instance=user)
            payload = ''
            if form.is_valid():
                print "succes"
                if user != None:
                    self.delete_existing_skill()
                    self.delete_existing_subcategory()
                    self.delete_existing_workcategory()
                form.save(self.request.user, self.get_selected_skill(request.POST.getlist('skills')),
                          self.get_selected_subcategory(request.POST.getlist('subcategory')),
                          self.get_selected_workcategory(request.POST.getlist('workcategory'))
                          )
                hourly_html = render_to_string('freelancer/ajax/freelancer_company_profile_details.html',
                                                {'request': self.request})
                payload = {'status': 'sucess', 'message': "success", 'html': hourly_html}
                return HttpResponse(json.dumps(payload), content_type='application/json', )

            else:

                self.message = "Validation failed."
                self.data = form.errors
                self.success = False
                payload = {'status': 'failed', 'message': self.message, 'data': self.data}
                return HttpResponse(json.dumps(payload), content_type='application/json', )
        if method == 'freelancer_company_image_save':
            # print request.FILES['freelancer_image'],"cccccccccccccccccccccccccccccch"
            if request.user.invite_type == 'OWNER':
                 self.check_exist_freelancer_company(request.user)

            freelancer_object, freelancer_objects = Freelancer_Company_Profile.objects.get_or_create(
                user=self.Get_user())

            if freelancer_object and request.POST['company_image']:

                format, imgstr = request.POST['company_image'].split(';base64,')
                ext = format.split('/')[-1]
                date_str = datetime.datetime.utcnow()
                data = ContentFile(base64.b64decode(imgstr), name=str(request.user.first_name) + date_str.strftime(
                    "%Y_%m_%d_%H_%M_%S_%f") + '.' + ext)

                freelancer_object.logo = data
                freelancer_object.save()
                option_html = render_to_string('freelancer/ajax/freelancer_company_logo.html',
                                               {'object': freelancer_object.logo.url,
                                                'csrf_token_value': request.COOKIES['csrftoken'], request: request}
                                               )
            payload = {'status': 'success',
                       'message': 'Successfully Updated',
                       'data': freelancer_object.logo.url, 'html': option_html}
            return HttpResponse(json.dumps(payload), content_type='application/json', )
        if method == 'save_frelancer_company_portfolio':
            data = json.loads(request.POST['data'])
            if data:
                request.user.get_freelancer_company_intance().user.freelancer_company_portfolio_user.all().delete()

                for portfolio in data['portfolio']:
                    if portfolio['project_name']:
                        if request.user.invite_type == 'OWNER':
                          self.check_exist_freelancer_company(request.user)
                        # print request.user.get_freelancer_company_intance().user, "hhhhhhhhhhhhhh"


                        freelancer_object = Freelancer_company_Portfolio.objects.create(
                            user=request.user.get_freelancer_company_intance().user,
                            project_name=portfolio[
                                'project_name'],
                            description=portfolio[
                                'description'],
                            project_url=portfolio[
                                'project_url']
                            )

                        if portfolio['work_category']:
                            freelancer_object.work_category = WorkCategories.objects.get(id=portfolio['work_category'])
                            freelancer_object.save()

                        if portfolio['skills']:

                            for skill in Skills.objects.filter(pk__in=portfolio['skills']):
                                freelancer_object.skills.add(skill)
                        if portfolio['sub_category']:
                            for sub_category in WorkSubCategories.objects.filter(
                                    pk__in=portfolio['sub_category']):
                                freelancer_object.sub_category.add(sub_category)
                        print portfolio['project_name'],"fffffffffff"
                        freelancer_object, freelancer_objects = Freelancer_company_Portfolio.objects.get_or_create(
                            user=request.user,
                            project_name=portfolio['project_name']
                            )

                        if portfolio['upload']:
                            for image in portfolio['upload']:
                                upload = FreelancerCompanyPortfolioimages()
                                format, imgstr = image.split(';base64,')
                                ext = format.split('/')[-1]
                                date_str = datetime.datetime.utcnow()
                                datas = ContentFile(base64.b64decode(imgstr),
                                                    name=str(request.user.first_name) + date_str.strftime(
                                                        "%Y_%m_%d_%H_%M_%S_%f") + '.' + ext)
                                upload.portfolio_image = datas
                                upload.save()
                                upload.portfolio_id.add(freelancer_object)

                for certification in data['certification']:
                    if certification['certificate_name']:
                        self.check_exist_freelancer_company(request.user)
                        # print request.user.get_freelancer_company_intance().user, "hhhhhhhhhhhhhh"
                        request.user.get_freelancer_company_intance().user.freelancer_company_cerifications_user.all().delete()

                        getcertification = Freelancer_company_Certifications.objects.create(
                            user=request.user.get_freelancer_company_intance().user,
                            certificate_name=certification[
                                'certificate_name'],
                            issued_by=certification[
                                'issuedby']
                            )
                        if certification['start_date'] != '':
                            start = datetime.datetime.strptime(str(certification['start_date']), "%m/%d/%Y")
                            getcertification.valid_from = start
                            getcertification.save()

                        if certification['end_date'] != '':
                            end_date = datetime.datetime.strptime(str(certification['end_date']), "%m/%d/%Y")
                            getcertification.end_to = end_date
                            getcertification.save()

            payload = {'status': 'sucess', 'message': "success", 'data': "sss"}
            return HttpResponse(json.dumps(payload),
                                content_type='application/json',
                                )
        if method == "change_hourly_rate_company":
            if request.POST['rate']:
                actual_rate = (float(request.POST['rate']) - float(request.POST['rate'])*10/100)
                print actual_rate, "actual_rate"
                payload = {'status': 'sucess', 'message': "success", 'data': float(actual_rate)}
                return HttpResponse(json.dumps(payload),
                                    content_type='application/json',
                                    )
        if method == "get_country":
            leads_as_json = serializers.serialize('json', Country.objects.all())
            return HttpResponse(leads_as_json,
                                content_type='application/json',
                                )
        if method == "get_state":
            if request.POST['country']:
                leads_as_json = serializers.serialize('json', Region.objects.filter(country=request.POST['country']))
                return HttpResponse(leads_as_json,
                                    content_type='application/json',
                                    )
        if method == "get_city":
            if request.POST['state']:
                leads_as_json = serializers.serialize('json', City.objects.filter(region=request.POST['state']))
                return HttpResponse(leads_as_json,
                                    content_type='application/json',
                                    )

        if method == "save_frelancer_company_hourly":
            data = json.loads(request.POST['data'])

            if data:
                # print data, "dddddddddddddd"
                for hourly in data['hourly']:

                    if hourly['hourly_rate'] != '':
                        self.check_exist_freelancer_company(request.user)
                        # print request.user.get_freelancer_company_intance().user, "hhhhhhhhhhhhhh"
                        hourly_object, hourly_objects = Freelancer_company_HourlyRate.objects.get_or_create(
                            user=self.Get_user())

                        if hourly_object and hourly['hourly_rate']:
                            hourly_object.hourly_rate = hourly['hourly_rate']
                            hourly_object.actual_rate = (float(hourly['hourly_rate']) - float(hourly['hourly_rate'])*10/100)
                            hourly_object.save()

                for location in data['location']:
                    #if location['country'] != '0' or location['state'] != '0' or location['city'] != '0' or \
                                    #location['zip'] != '' or location['address'] != '':
                    self.check_exist_freelancer_company(request.user)
                    #print request.user.get_freelancer_company_intance().user, "hhhhhhhhhhhhhh"
                    location_object, location_objects = Freelancer_company_LocationDetails.objects.get_or_create(
                        user=self.Get_user())
                    if location_object:
                        # print location['country'], "dddddddddddddddddd"
                        if location['country'] != '0' and location['country'] != '':
                            location_object.country = Country.objects.get(id=location['country'])
                        else:
                            location_object.country = None

                        if location['state'] != '0' and location['state'] != '':
                            location_object.state = Region.objects.get(id=location['state'])
                        else:
                            location_object.state = None
                        if location['city'] != '0' and location['city'] != '':
                            location_object.city = City.objects.get(id=location['city'])
                        else:
                            location_object.city = None

                        location_object.zip_code = location['zip']
                        location_object.address = location['address']
                        location_object.save()
                hourly_html = render_to_string('freelancer/ajax/freelancer_company_hourlyrate.html', {'request': self.request})
                profile_html = render_to_string('freelancer/ajax/freelancer_company_profile_details.html',{'request': self.request})
                payload = {'status': 'sucess', 'message': "success", 'html': hourly_html, 'profile_html': profile_html}

                return HttpResponse(json.dumps(payload),
                                    content_type='application/json',
                                    )


class FreelancerCompanyView(HasPermissionsMixin, CompanyMixin, TemplateView):
    template_name = 'freelancer/freelancer_company_view.html'
    required_permission = 'view_Freelancer_company'

    def post(self, request, *args, **kwargs):
        method = request.POST['method']
        if method == 'Save_freelancer_company_portfolio':
            # print "f"
            if request.POST['project_name'] != '' :
                freelancer_object = Freelancer_company_Portfolio.objects.create(
                    user=request.user.get_freelancer_company_intance().user,
                    project_name=request.POST['project_name'],
                    description=request.POST['description'],
                    project_url=request.POST['project_url'],

                    )
                if request.POST['work_category'] != '':
                    freelancer_object.work_category = WorkCategories.objects.get(
                        id=request.POST['work_category'])
                if request.POST.getlist('skills') != '':

                    for skill in Skills.objects.filter(pk__in=request.POST.getlist('skills')):
                        freelancer_object.skills.add(skill)
                if request.POST.getlist('sub_category') != '':
                    for sub_category in WorkSubCategories.objects.filter(
                            pk__in=request.POST.getlist('sub_category')):
                        freelancer_object.sub_category.add(sub_category)
                user_html = render_to_string('freelancer/ajax/freelancer_company_portfolio_list.html',
                                             {'request': self.request})
                payload = {'status': 'success',
                           'message': 'Successfully Updated',
                           'count': request.user.get_freelancer_company_intance().user.freelancer_company_portfolio_user.count(), 'html': user_html}
            else:
                payload = {'status': 'error',
                           'message': 'Successfully Updated',
                           'data': 'g'}

            return HttpResponse(json.dumps(payload), content_type='application/json', )
        if method == 'Save_freelancer_company_certification':
            # print "ffffffffffffffffffffffffffff"

            if request.POST['certificate_name'] != '':
                getcertification = Freelancer_company_Certifications.objects.create(
                    user=request.user.get_freelancer_company_intance().user,
                    certificate_name=request.POST['certificate_name'],
                    issued_by=request.POST['issuedby']
                    )
                if request.POST['start_date'] != '':
                    start = datetime.datetime.strptime(str(request.POST['start_date']), "%m/%d/%Y")
                    getcertification.valid_from = start
                    getcertification.save()

                if request.POST['end_date'] != '':
                    end_date = datetime.datetime.strptime(str(request.POST['end_date']), "%m/%d/%Y")
                    getcertification.end_to = end_date
                    getcertification.save()
                user_html = render_to_string('freelancer/ajax/freelancer_company_certification_list.html',
                                             {'request': self.request})
                payload = {'status': 'success',
                           'message': 'Successfully Updated',
                           'count': request.user.get_freelancer_company_intance().user.freelancer_company_cerifications_user.count(),
                           'html': user_html}

            else:
                payload = {'status': 'error',
                           'message': 'Successfully Updated',
                           'data': 'g'}

            return HttpResponse(json.dumps(payload), content_type='application/json', )
        if method == 'DeletedDetails':
            if request.POST['datatitle'] == 'cerifications':
                instance = Freelancer_company_Certifications.objects.get(id=request.POST['dataid'])
                instance.delete()
                user_html = render_to_string('freelancer/ajax/freelancer_company_certification_list.html',
                                             {'request': self.request})
                payload = {'status': 'success',
                           'message': 'Successfully Updated',
                           'count': request.user.get_freelancer_company_intance().user.freelancer_company_cerifications_user.count(),
                           'html': user_html}
                return HttpResponse(json.dumps(payload), content_type='application/json', )

            if request.POST['datatitle'] == 'portfolio':
                # print request.POST['dataid'], "bbbbbbbbbbbbbbbb"
                instance = Freelancer_company_Portfolio.objects.get(id=request.POST['dataid'])
                instance.delete()
                user_html = render_to_string('freelancer/ajax/freelancer_company_portfolio_list.html',
                                             {'request': self.request})
                payload = {'status': 'success',
                           'message': 'Successfully Updated',
                           'count': request.user.get_freelancer_company_intance().user.freelancer_company_portfolio_user.count(),
                           'html': user_html}
                return HttpResponse(json.dumps(payload), content_type='application/json', )
        if method == 'EditDetails':
            payload = {'status': 'success', 'data': ''}
            if request.POST['datatitle'] == 'cerifications':
                option_html = render_to_string('freelancer/ajax/edit_freelencer_certifications.html',
                                               {'object': Freelancer_company_Certifications.objects.get(
                                                   id=request.POST['id']),
                                                'csrf_token_value': request.COOKIES['csrftoken'], request: request}
                                               )
                payload = {'status': 'success', 'html': option_html}

                return HttpResponse(json.dumps(payload), content_type='application/json', )

            if request.POST['datatitle'] == 'portfolio':
                option_html = render_to_string('freelancer/ajax/edit_freelencer_portfolio.html',
                                               {'object': Freelancer_company_Portfolio.objects.get(
                                                   id=request.POST['id']),
                                                'csrf_token_value': request.COOKIES['csrftoken'], request: request}
                                               )
                payload = {'status': 'success', 'html': option_html}

                return HttpResponse(json.dumps(payload), content_type='application/json', )
        if method == 'Update_freelancer_portfolio':

            portfolio_object, portfolio_objects = Freelancer_company_Portfolio.objects.get_or_create(
                user=request.user.get_freelancer_company_intance().user, id=request.POST['id'])
            if portfolio_object:
                if request.POST['project_name'] != '':
                    portfolio_object.project_name = request.POST['project_name']
                    portfolio_object.description = request.POST['description']
                    portfolio_object.project_url = request.POST['project_url']

                    portfolio_object.save()
                if request.POST['work_category'] != '':
                    portfolio_object.work_category = WorkCategories.objects.get(
                        id=request.POST['work_category'])
                    portfolio_object.save()

                if request.POST.getlist('skills') != '':

                    for skill in Skills.objects.filter(pk__in=request.POST.getlist('skills')):
                        portfolio_object.skills.add(skill)
                if request.POST.getlist('sub_category') != '':
                    for sub_category in WorkSubCategories.objects.filter(
                            pk__in=request.POST.getlist('sub_category')):
                        portfolio_object.sub_category.add(sub_category)
                user_html = render_to_string('freelancer/ajax/freelancer_company_portfolio_list.html',
                                             {'request': self.request})
                payload = {'status': 'success',
                           'message': 'Successfully Updated',
                           'count': request.user.get_freelancer_company_intance().user.freelancer_company_portfolio_user.count(),
                           'html': user_html}
                return HttpResponse(json.dumps(payload), content_type='application/json', )
        if method == 'Update_freelancer_certification':
            certification_object, certification_objects = Freelancer_company_Certifications.objects.get_or_create(
                user=request.user.get_freelancer_company_intance().user, id=request.POST['id'])
            if certification_object:
                if request.POST['certificate_name'] != '':
                    certification_object.certificate_name = request.POST['certificate_name']
                    certification_object.issued_by = request.POST['issuedby']
                    certification_object.save()
                if request.POST['start_date'] != '':
                    start = datetime.datetime.strptime(str(request.POST['start_date']), "%m/%d/%Y")
                    certification_object.valid_from = start
                    certification_object.save()

                if request.POST['end_date'] != '':
                    end_date = datetime.datetime.strptime(str(request.POST['end_date']), "%m/%d/%Y")
                    certification_object.end_to = end_date
                    certification_object.save()
                user_html = render_to_string('freelancer/ajax/freelancer_company_certification_list.html',
                                             {'request': self.request})
                payload = {'status': 'success',
                           'message': 'Successfully Updated',
                           'count': request.user.get_freelancer_company_intance().user.freelancer_company_cerifications_user.count(),
                           'html': user_html}
                return HttpResponse(json.dumps(payload), content_type='application/json', )

        if method == 'invite_user':
            if request.POST['email'] and request.POST['role_type']:
                user = self.check_user_exist(request.POST['email'])
                if user is None:
                    client_object, clientr_objects = Users.objects.get_or_create(
                                                                                 email=request.POST['email'],
                                                                                 is_active=False,
                                                                                 token=get_random_string(length=32),
                                                                                 user_type=self.get_user_type('FREELANCER'),invite_type=self.get_user_type('FREELANCER'))
                    if client_object:
                        # print client_object.pk, "jjj"
                        self.activate_users_accounts(client_object, request.META['HTTP_HOST'],'new')

                        invite_object, invite_objects = Invited_Freelancer_Company_User.objects.get_or_create(
                            user=client_object,
                            role_type=request.POST['role_type'],
                            freelancer_company=request.user.get_freelancer_company_intance())
                    payload = {'status': 'success', 'message': "success", 'data': "jkjk"}
                    return HttpResponse(json.dumps(payload), content_type='application/json', )
                else:
                    print "exist"

                    client_object, clientr_objects = Users.objects.get_or_create(
                                                                                 email=request.POST['email']
                                                                                 )
                    users = self.check_user_exist_invited_company(request.POST['email'])
                    client_user = self.client_user(request.POST['email'])

                    if users is None and not user.is_superuser and user.invite_type.type != 'OWNER':
                        if client_user.client_company.user.email == request.user.get_freelancer_company_intance().user.email:

                            invite_object, invite_objects = Invited_Freelancer_Company_User.objects.get_or_create(
                                user=client_object,
                                role_type=request.POST['role_type'],
                                freelancer_company=request.user.get_freelancer_company_intance())

                            if client_object.invite_type.type == 'CLIENT':
                                client_object.invite_type  =User_Type.objects.get(type='BOTH')
                                client_object.save()
                            else:
                                client_object.invite_type =User_Type.objects.get(type='FREELANCER')
                                client_object.save()
                            self.activate_users_accounts(client_object, request.META['HTTP_HOST'],'existing')

                            invite_object, invite_objects = Invited_Freelancer_Company_User.objects.get_or_create(user=client_object,role_type=request.POST['role_type'],
                                freelancer_company=request.user.get_freelancer_company_intance())
                            payload = {'status': 'success', 'message': "success", 'data': "jkjk"}
                            return HttpResponse(json.dumps(payload), content_type='application/json', )
                        else:
                            payload = {'status': 'failed', 'message': "This user can't be added as a team member", 'data': "jkjk"}
                            return HttpResponse(json.dumps(payload), content_type='application/json', )

                    else:
                        payload = {'status': 'failed', 'message': "This user can't be added as a team member", 'data': "jkjk"}
                        return HttpResponse(json.dumps(payload), content_type='application/json', )
            else:
              payload = {'status': 'failed', 'message': "This user can't be added as a team member", 'data': "jkjk"}
              return HttpResponse(json.dumps(payload), content_type='application/json', )
        if method == 'ChangeRole':
            object =Invited_Freelancer_Company_User.objects.get(id=request.POST['roleid'])

            msg_html = render_to_string('freelancer/ajax/changeRole.html',
                                        {'csrf_token_value': request.COOKIES['csrftoken'],'object':object})
            payload = {'status': 'success', 'message': "success", 'html': msg_html}
            return HttpResponse(json.dumps(payload), content_type='application/json', )

        if method == 'ChangeDectivation':
            object, objects  =Users.objects.get_or_create(email=request.POST['roleid'])
            print object
            if request.POST['rolestatus'] == 'True' or request.POST['rolestatus'] == 'true':
                status = False
                type = "Activate"
            else:
                status = True
                type = "Deactivate"
            print status
            object.is_active = status
            object.save()



            payload = {'status': 'success', 'message': "success",'data':status,'type':type}
            return HttpResponse(json.dumps(payload), content_type='application/json', )
        if method == 'update_invite_user':
            object =Invited_Freelancer_Company_User.objects.get(id=request.POST['roleid'])
            object.role_type =request.POST['role_type']
            object.save()


            payload = {'status': 'success', 'message': "success", }
            return HttpResponse(json.dumps(payload), content_type='application/json', )
        if method == 'Get_freelancer_company_hourlypopup':
            user_html = render_to_string('freelancer/ajax/get_freelancer_company_hourlypopup.html',
                                         {'request': self.request})
            payload = {'status': 'success',
                       'message': 'Successfully Updated',
                        'html': user_html}
            return HttpResponse(json.dumps(payload), content_type='application/json', )
        if method == 'Get_freelancer_company_profile_popup':
            user_html = render_to_string('freelancer/ajax/get_freelancer_company_profile_popup.html',
                                         {'request': self.request,'csrf_token_value': request.COOKIES['csrftoken']})
            payload = {'status': 'success',
                       'message': 'Successfully Updated',
                        'html': user_html}
            return HttpResponse(json.dumps(payload), content_type='application/json', )




    def activate_users_accounts(self, client_object, site,type):
        token = client_object.token
        msg_plain = "Atlancer Freelancer Company Account Activation"
        if type == 'new':
           msg_html = render_to_string('registration/activate_company_user.html',
                                    {'site': site, 'token': token, 'firstname': client_object.email})
        else:
            msg_html = render_to_string('registration/activate_company_existing_user.html',
                                        {'site': site, 'token': token, 'firstname': client_object.email,'account':'Freelancer'})

        def Email():
            send_mail(
                msg_plain,
                msg_plain,
                'atlancer@sender.com',
                [client_object.email],
                html_message=msg_html,
            )
        send_user_mail = threading.Thread(name='Email', target=Email)
        send_user_mail.start()

    def get_paginated_queryset(self, request, page_by, sort_by=None):
        if page_by:
            page_by = int(page_by)
        else:
            page_by = 1
        if sort_by:
            page_size = int(sort_by)
        else:
            page_size = self.get_paginate_by(request.user.get_freelancer_company_intance.invited_freelancer_company_id.all())

        paginator, page, queryset, is_paginated = self.paginate_queryset(request.user.get_freelancer_company_intance.invited_freelancer_company_id(), page_size)
        try:
            queryset = paginator.page(page_by).object_list
        except:
            queryset = queryset

        user_table = render_to_string('freelancer/ajax/listing_invited_user.html',
                                      {'all_invited': queryset, 'paginator': paginator, 'page': page,
                                       'is_paginated': is_paginated, 'request': request,'page_obj': paginator.page(page_by)})

        # paginator_html = render_to_string('ajax/paginator_html.html',
        #                                   {'page_obj': paginator.page(page_by)})

        context = {'user_table': user_table, 'paginator_html': user_table}
        return context
class InvitedUserList(ListView):
    paginate_by = 6



    def post(self, request, *args, **kwargs):
        response_data = {}
        # option_html = render_to_string('freelancer/ajax/listing_invited_user.html')

        page_by = request.POST.get('page_by')
        search = request.POST.get('search')
        # print page_by,"ssssssssssssssssssssssssssssssssssssssssssssssss"

        paginator_response = self.get_paginated_queryset(request, page_by,search)
        # response_data['count'] = self.request.user.get_freelancer_company_intance().invited_freelancer_company_id.filter(
        #     user__is_active=True).count()
        response_data['count'] = self.request.user.get_freelancer_company_intance().invited_freelancer_company_id.count()
        # print paginator_response

        response_data['paginator_html'] = paginator_response['paginator_html']
        return HttpResponse(json.dumps(response_data), content_type="application/json")

    def get_paginated_queryset(self, request, page_by,search):
        if page_by:
            page_by = int(page_by)
        else:
            page_by = 1
        # print search,"hsearchsearchsearch"
        if search =='':
            page_size = self.get_paginate_by(
                request.user.get_freelancer_company_intance().invited_freelancer_company_id.all())
            size =self.paginate_queryset(request.user.get_freelancer_company_intance().invited_freelancer_company_id.all().exclude(user=self.request.user), page_size)
        else:
            page_size = self.get_paginate_by(
                request.user.get_freelancer_company_intance().invited_freelancer_company_id.filter(Q(user__first_name__contains=search) | Q(user__last_name__contains=search)| Q(user__email__contains=search)).exclude(user=self.request.user))
            size = self.paginate_queryset(
                request.user.get_freelancer_company_intance().invited_freelancer_company_id.filter(Q(user__first_name__contains=search) | Q(user__last_name__contains=search)| Q(user__email__contains=search)).exclude(user=self.request.user), page_size)

        paginator, page, queryset, is_paginated = size
        try:
            queryset = paginator.page(page_by).object_list
        except:
            queryset = queryset


        user_table = render_to_string('freelancer/ajax/listing_invited_user.html',
                                      {'all_invited': queryset, 'paginator': paginator, 'page': page,
                                       'is_paginated': is_paginated,'type':'Freenlancers', 'request': request,'page_obj': paginator.page(page_by)})

        # paginator_html = render_to_string('ajax/paginator_html.html',
        #                                   {'page_obj': paginator.page(page_by)})

        context = {'user_table': user_table, 'paginator_html': user_table}
        return context


