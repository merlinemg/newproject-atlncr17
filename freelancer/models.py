from __future__ import unicode_literals

from PIL import Image
from django.db import models

# Create your models here.
# accounts.models import Users
from newproject import settings
from cities_light.models import Country, Region, City
from imagekit.models import ImageSpecField

from pilkit.processors import ResizeToFill




class WorkCategories(models.Model):
    """
    Freelancer Work Categories model.
    """
    category_title = models.CharField(max_length=255)
    sorting_order = models.IntegerField(blank=True, null=True)

    def __unicode__(self):
        return self.category_title

    class Meta:
        verbose_name = "Work Category"
        verbose_name_plural = "Work Categories"


class WorkSubCategories(models.Model):
    """
    Freelancer Work SubCategories model
    """
    work_category = models.ForeignKey(WorkCategories, related_name='work_sub_categories',
                                      verbose_name="Work Categories")
    sub_category_title = models.CharField(('Sub category title'), max_length=255)

    def __unicode__(self):
        return self.sub_category_title

    class Meta:
        verbose_name = "Work Sub Category"
        verbose_name_plural = "Work Sub Categories"


class Skills(models.Model):
    name = models.CharField(max_length=500)
    category = models.ManyToManyField(WorkCategories, related_name='work_category_skils')

    def __unicode__(self):
        return self.name


class Freelancer_Profile(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='user_detail')
    profile = models.ImageField(upload_to='freelancer/profile', blank=True, null=True)
    title = models.CharField(max_length=500, blank=True, null=True)
    about = models.TextField(max_length=12000, blank=True, null=True)
    workcategory = models.ForeignKey(WorkCategories, related_name='work_category', blank=True, null=True)
    subcategory = models.ManyToManyField(WorkSubCategories, related_name='work_sub_category', blank=True, null=True)
    skills = models.ManyToManyField(Skills, related_name='skills', blank=True, null=True)

    def __unicode__(self):
        return self.user.email


class ExperienceLevel(models.Model):
    """
    Freelancer Experience Level model
    """
    user = models.OneToOneField(settings.AUTH_USER_MODEL, related_name='experience_level',
                                verbose_name="user")
    exp_level_type = models.CharField('Experience Level title', max_length=255)

    def __unicode__(self):
        return self.user.email

    class Meta:
        verbose_name = "Experience Level"
        verbose_name_plural = "Experience Levels"


class EmploymentHistory(models.Model):
    """
    Freelancer Employment History
    """
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='employment_history',
                             verbose_name="user")
    company_name = models.CharField(max_length=500)

    role = models.CharField(max_length=255, blank=True, null=True)
    start_date = models.DateField(blank=True, null=True)
    end_date = models.DateField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)

    def __unicode__(self):
        return self.user.get_full_name()

    class Meta:
        verbose_name = "Employment History"
        verbose_name_plural = "Employment Histories"


class FreelancerPortfolio(models.Model):
    """
    Freelancer Portfolio model
    """
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='portfolio',
                             verbose_name="user")
    project_name = models.CharField(max_length=255)
    description = models.TextField(blank=True, null=True)
    project_url = models.URLField(blank=True, null=True)
    work_category = models.ForeignKey(WorkCategories, related_name='portfolio_work_categories',
                                      verbose_name="Work Categories", blank=True, null=True)
    sub_category = models.ManyToManyField(WorkSubCategories, verbose_name='Work Sub Category',
                                          related_name='portfolio_sub_category', blank=True, null=True)
    skills = models.ManyToManyField(Skills, verbose_name='Skills', related_name='portfolio_skills', blank=True,
                                    null=True)

    def __unicode__(self):
        return self.user.get_full_name()

    class Meta:
        verbose_name = "Freelancer portfolio"
        verbose_name_plural = "Freelancer portfolios"


class FreelancerPortfolioimages(models.Model):
    portfolio_id = models.ManyToManyField(FreelancerPortfolio, verbose_name='Portfoilio id',
                                          related_name='portfoilid_id')
    portfolio_image = models.ImageField(upload_to='freelancer/portfolio_image')
    thumb_portfolio_image =  ImageSpecField(source='portfolio_image',
                                           processors=[ResizeToFill(150, 150)],
                                           format='JPEG',
                                           options={'quality': 60})

    # def save(self, force_insert=False, force_update=False):
    #
    #     super(FreelancerPortfolioimages, self).save(force_insert, force_update)
    #
    #     if self.id is not None:
    #         previous = FreelancerPortfolioimages.objects.get(id=self.id)
    #         if self.thumb_portfolio_image and self.thumb_portfolio_image != previous.thumb_portfolio_image:
    #             image = Image.open(self.thumb_portfolio_image.path)
    #             image = image.resize((113, 113), Image.ANTIALIAS)
    #             image.save(self.logo.thumb_portfolio_image)





class Certifications(models.Model):
    """
     Freelancer Certifications model
    """
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='cerifications',
                             verbose_name="user")
    certificate_name = models.CharField(max_length=255)
    issued_by = models.CharField(max_length=255, blank=True, null=True)
    valid_from = models.DateField(blank=True, null=True)
    end_to = models.DateField(blank=True, null=True)

    def __unicode__(self):
        return self.user.get_full_name()

    class Meta:
        verbose_name = "Certifications"
        verbose_name_plural = "Certifications"


class HourlyRate(models.Model):
    """
    Freelancer Hourly Rate model
    """
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='freelancer_hourly_user')
    hourly_rate = models.DecimalField(max_digits=7, decimal_places=2, blank=True, null=True)
    actual_rate = models.DecimalField(max_digits=7, decimal_places=2, blank=True, null=True)

    def __unicode__(self):
        return self.user.email

    class Meta:
        verbose_name = "Hourly rate"
        verbose_name_plural = "Hourly rates"


class LocationDetails(models.Model):
    """
    Freelancer Location Details model
    """
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='freelancer_location_user')
    country = models.ForeignKey(Country, max_length=255, related_name='country', blank=True, null=True)
    state = models.ForeignKey(Region, max_length=255, related_name='region', blank=True, null=True)
    city = models.ForeignKey(City, max_length=255, related_name='city', blank=True, null=True)

    address = models.CharField(max_length=100, blank=True, null=True)
    zip_code = models.CharField(max_length=5, blank=True, null=True)

    def __unicode__(self):
        return self.user.email

    class Meta:
        verbose_name = "Location Details"
        verbose_name_plural = "Location Details"



