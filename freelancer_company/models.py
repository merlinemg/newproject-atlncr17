from __future__ import unicode_literals

from cities_light.models import Country, Region, City
from django.db import models

# Create your models here.
from accounts.models import BaseTimestamp
from freelancer.models import WorkCategories, WorkSubCategories, Skills
from newproject import settings


class Freelancer_Company_Profile(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='freelancer_company_user')
    company_name = models.CharField(max_length=500)
    description = models.TextField(blank=True, null=True)
    logo = models.ImageField(upload_to='freelancer/company', blank=True, null=True)
    workcategory = models.ManyToManyField(WorkCategories, related_name='freelancer_company_work_category', blank=True,
                                     null=True)
    subcategory = models.ManyToManyField(WorkSubCategories, related_name='freelancer_company_work_sub_category',
                                         blank=True, null=True)
    skills = models.ManyToManyField(Skills, related_name='freelancer_company_skills', blank=True, null=True)

    def __unicode__(self):
        return self.user.email

    class Meta:
        verbose_name = "Freelancer Company Profile"
        verbose_name_plural = "Freelancer Company Profile"


class Freelancer_company_Portfolio(models.Model):
    """
    Freelancer Portfolio model
    """
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='freelancer_company_portfolio_user',
                             verbose_name="user")
    project_name = models.CharField(max_length=255)
    description = models.TextField()
    project_url = models.URLField()
    work_category = models.ForeignKey(WorkCategories, related_name='freelancer_company_portfolio_work_categories',
                                      verbose_name="Work Categories", blank=True, null=True)
    sub_category = models.ManyToManyField(WorkSubCategories, verbose_name='Work Sub Category',
                                          related_name='freelancer_company_portfolio_sub_category', blank=True,
                                          null=True)
    skills = models.ManyToManyField(Skills, verbose_name='Skills', related_name='freelancer_company_portfolio_skills',
                                    blank=True, null=True)

    def __unicode__(self):
        return self.user.get_full_name()

    class Meta:
        verbose_name = "Freelancer Company  Portfolio"
        verbose_name_plural = "Freelancer Company  Portfolios"


class Freelancer_company_Certifications(models.Model):
    """
     Freelancer Certifications model
    """
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='freelancer_company_cerifications_user',
                             verbose_name="user")
    certificate_name = models.CharField(max_length=255)
    issued_by = models.CharField(max_length=255, blank=True, null=True)
    valid_from = models.DateField(blank=True, null=True)
    end_to = models.DateField(blank=True, null=True)

    def __unicode__(self):
        return self.user.get_full_name()

    class Meta:
        verbose_name = "Freelancer Company Certifications"
        verbose_name_plural = "Freelancer Company Certifications"


class Freelancer_company_HourlyRate(models.Model):
    """
    Freelancer Hourly Rate model
    """
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='freelancer_company_hourly_user')
    hourly_rate = models.DecimalField(max_digits=7, decimal_places=2, blank=True, null=True)
    actual_rate = models.DecimalField(max_digits=7, decimal_places=2, blank=True, null=True)

    def __unicode__(self):
        return self.user.email

    class Meta:
        verbose_name = "Freelancer Company Hourly rate"
        verbose_name_plural = "Freelancer Company Hourly rates"


class Freelancer_company_LocationDetails(models.Model):
    """
    Freelancer Location Details model
    """
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='freelancer_company_location_user')
    country = models.ForeignKey(Country, max_length=255, related_name='freelancer_company_country', blank=True,
                                null=True)
    state = models.ForeignKey(Region, max_length=255, related_name='freelancer_company_region', blank=True, null=True)
    city = models.ForeignKey(City, max_length=255, related_name='freelancer_company_city', blank=True, null=True)

    address = models.CharField(max_length=100, blank=True, null=True)
    zip_code = models.CharField(max_length=5, blank=True, null=True)

    def __unicode__(self):
        return self.user.email

    class Meta:
        verbose_name = "Freelancer Company Location Details"
        verbose_name_plural = "Freelancer Company Location Details"


class Invited_Freelancer_Company_User(BaseTimestamp):
    ACCOUNT_TYPES = (
        ('Admin', '1'),
        ('Staff', '0'),

    )
    user = models.OneToOneField(settings.AUTH_USER_MODEL, related_name='invited_freelancer_company_user')
    role_type = models.CharField(max_length=10,
                                 choices=ACCOUNT_TYPES,
                                 default='admin', verbose_name="Role Type")
    freelancer_company = models.ForeignKey(Freelancer_Company_Profile, related_name='invited_freelancer_company_id')
    # modified_date = models.DateTimeField(verbose_name='Modified Date', auto_now=True, blank=True, null=True)
    # created_date = models.DateTimeField(verbose_name='Create Date', auto_now_add=True, blank=True, null=True)

    def __unicode__(self):
        return self.user.email

    class Meta:
        verbose_name = "Invited Freelancer Company User"
        verbose_name_plural = "Invited Freelancer Company Users"
class FreelancerCompanyPortfolioimages(models.Model):
    portfolio_id = models.ManyToManyField(Freelancer_company_Portfolio, verbose_name='Portfoilio id',
                                          related_name='freelancer_company_portfoilid_id')
    portfolio_image = models.ImageField(upload_to='freelancercompany/portfolio_image')
