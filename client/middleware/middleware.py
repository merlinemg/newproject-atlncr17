from datetime import datetime

from django.utils.deprecation import MiddlewareMixin

from accounts.models import Users
from client.models import ClientJobs
from client.views import _thread_locals


job_details_url = '/create-profile/'

class ClientLastVisitjob(MiddlewareMixin):

    def process_request(self, request):
        request_url = request.path
        if request.user:
            _thread_locals.current_user=request.user


        job_url = '/client/client_job_details/'
        if len(request_url.split('/')) == 5 and job_url.split('/')[1] in request_url.split('/') and job_url.split('/')[2] in request_url.split('/'):
            print "slug ===","", request_url.split('/')[3]
            jobdetails = ClientJobs.objects.get(slug=request_url.split('/')[3])

            if request.user == jobdetails.user:

                jobdetails.last_view_date=datetime.now()
                jobdetails.save()
