import base64
import datetime
import json

from django.core import serializers
from django.core.files.base import ContentFile
from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render
from django.template.loader import render_to_string
from django.template.response import TemplateResponse
from django.urls import reverse
from django.views.generic import ListView
from django.views.generic import TemplateView
from rolepermissions.mixins import HasPermissionsMixin
from rolepermissions.roles import assign_role
from rolepermissions.roles import remove_role

# Create your views here.
from accounts.models import Users
from client.forms import ClientForm, PostJobForm, InviteForm
from client.mixins import ClientMixin
from client.models import Client_Profile, ClientJobs
from client_company.models import Client_Company_Profile

from freelancer.models import WorkCategories, Skills, WorkSubCategories
from hire.models import HiredJobsContract
from site_administration.models import VerificationJobPost
from threading import local

_thread_locals = local()


class ClientProfile(HasPermissionsMixin, ClientMixin, TemplateView):
    required_permission = 'View_Dashboard_Client'
    template_name = 'client/client-profile.html'

    def get(self, request):

        # all_jobs = self.get_my_jobs()

        template_name = 'client/client-profile.html'

        joblist = self.get_all_JobList()

        context = {'joblist': joblist.filter(user=request.user).order_by("-id")[:2]}
        return TemplateResponse(request, template_name, context)

    def post(self, request, *args, **kwargs):
        method = request.POST['method']
        if method == 'client_image_save':
            # print request.FILES['freelancer_image'],"cccccccccccccccccccccccccccccch"
            client_object, clientr_objects = Client_Profile.objects.get_or_create(user=request.user)
            if client_object:
                format, imgstr = request.POST['freelancer_image'].split(';base64,')
                ext = format.split('/')[-1]
                date_str = datetime.datetime.utcnow()
                data = ContentFile(base64.b64decode(imgstr), name=str(request.user.first_name) + date_str.strftime(
                    "%Y_%m_%d_%H_%M_%S_%f") + '.' + ext)
                client_object.profile = data
                client_object.save()
                option_html = render_to_string('client/ajax/client_profile_image.html',
                                               {'object': client_object.profile.url,
                                                'csrf_token_value': request.COOKIES['csrftoken'], request: request}
                                               )

            payload = {'status': 'sucess', 'message': "success", 'data': client_object.profile.url, 'html': option_html}
            return HttpResponse(json.dumps(payload),
                                content_type='application/json',
                                )
        if method == 'Save_User_Details':
            print "user"
            user = Users.objects.get(email=request.user)
            form = ClientForm(request.POST)
            if form.is_valid():
                user_object, user_objects = Users.objects.get_or_create(email=request.user)
                if user_object:
                    user_object.first_name = request.POST['first_name']
                    user_object.last_name = request.POST['last_name']
                    user_object.save()

                payload = {'status': 'success',
                           'message': 'Successfully Updated',
                           'data': 'g'}
                return HttpResponse(json.dumps(payload), content_type='application/json', )
            else:
                self.message = "Validation failed."
                self.data = form.errors
                self.success = False
                payload = {'status': 'failed', 'message': self.message, 'data': self.data}
                return HttpResponse(json.dumps(payload), content_type='application/json', )
        if method == 'Cancel_image':
            if request.POST['type'] == 'profile':
                try:
                    freelancer_object = Client_Profile.objects.get(user=request.user)
                    if freelancer_object.profile:
                        pic = freelancer_object.profile.url
                    else:
                        pic = ""
                except:
                    pic = ""

                option_html = render_to_string('client/ajax/client_profile_image.html',
                                               {'object': pic,
                                                'csrf_token_value': request.COOKIES['csrftoken'], request: request}
                                               )
                payload = {'status': 'sucess', 'message': "success",
                           'html': option_html}
                return HttpResponse(json.dumps(payload), content_type='application/json', )
            else:
                try:
                    freelancer_object = Client_Company_Profile.objects.get(
                        user=request.user.get_client_company_intance().user)
                    if freelancer_object.logo:
                        pic = freelancer_object.logo.url
                    else:
                        pic = ""
                except:
                    pic = ""
                option_html = render_to_string('client/ajax/client_company_logo.html',
                                               {'object': pic,
                                                'csrf_token_value': request.COOKIES['csrftoken'], request: request}
                                               )
                payload = {'status': 'sucess', 'message': "success",
                           'html': option_html}
                return HttpResponse(json.dumps(payload), content_type='application/json', )
        if method == 'Delete_image':
            if request.POST['type'] == 'profile':
                freelancer_object = Client_Profile.objects.get(user=request.user)
                freelancer_object.profile.delete()
                # freelancer_object.delete()

                option_html = render_to_string('client/ajax/client_profile_image.html',
                                               {'object': '',
                                                'csrf_token_value': request.COOKIES['csrftoken'], request: request}
                                               )
                payload = {'status': 'sucess', 'message': "success",
                           'html': option_html}
                return HttpResponse(json.dumps(payload), content_type='application/json', )
            if request.POST['type'] == 'logo':
                freelancer_object = Client_Company_Profile.objects.get(
                    user=request.user.get_client_company_intance().user)
                freelancer_object.logo.delete()
                # freelancer_object.delete()
                option_html = render_to_string('client/ajax/client_company_logo.html',
                                               {'object': '',
                                                'csrf_token_value': request.COOKIES['csrftoken'], request: request}
                                               )
                payload = {'status': 'sucess', 'message': "success",
                           'html': option_html}
                return HttpResponse(json.dumps(payload), content_type='application/json', )
        if method == 'get_worksubcategorys':

            # if len(request.POST.getlist('workid[]')) > 0:
            #     subid = self.get_sub_id(request.POST.getlist('workid[]'))
            # else:
            #     subid = self.get_sub_id(request.POST['workid'])
            if len(request.POST.getlist('workid[]')) > 0:
                subid = request.POST.getlist('workid[]')
            else:
                subid = [request.POST['workid']]

            leads_as_json = serializers.serialize('json', WorkSubCategories.objects.filter(
                work_category__in=subid))
            return HttpResponse(leads_as_json,
                                content_type='application/json',
                                )
        if method == 'get_workcategory':
            leads_as_json = serializers.serialize('json', WorkCategories.objects.all())
            return HttpResponse(leads_as_json,
                                content_type='application/json',
                                )
        if method == 'get_skillsoptions':
            # if len(request.POST.getlist('subid[]')) > 0:
            #     subid = self.get_sub_id(request.POST.getlist('subid[]'))
            # else:
            #     subid = self.get_sub_id(request.POST['subid'])
            if len(request.POST.getlist('subid[]')) > 0:
                subid = self.get_sub_id(request.POST.getlist('subid[]'))
            else:
                subid = [request.POST['subid']]

            getskills = Skills.objects.all()
            leads_as_json = serializers.serialize('json', Skills.objects.filter(
                category__in=subid))
            return HttpResponse(leads_as_json,
                                content_type='application/json',
                                )


class ClientJobPost(ClientMixin, TemplateView):
    template_name = 'client/client_job_posting.html'

    def get(self, request,**kwargs):

        # all_jobs = self.get_my_jobs()

        template_name = 'client/client_job_posting.html'
        Payment_type = self.get_all_Payment_type()
        job_status = self.get_all_job_status()
        duration = self.get_all_duration()
        get_previous_data = self.get_all_previous_data().order_by('-id')
        try:
            jobdetails = ClientJobs.objects.get(slug=kwargs.get('slug'))
        except:

            jobdetails =None


        context = {'Payment_type': Payment_type, 'jobstatus': job_status,'duration': duration, 'get_previous_data': get_previous_data,'jobdetails':jobdetails}
        return TemplateResponse(request, template_name, context)

    def post(self, request, *args, **kwargs):
        if request.is_ajax():
            response_data = {}
            if request.POST['method'] == 'Post_job':
                form = PostJobForm(request.POST)

                if form.is_valid():
                    if request.POST['errors_valid'] == '1':

                        job_category = self.get_category(request.POST['job_main_category'])
                        job_sub_category = self.get_selected_sub_catrories(request.POST.getlist('job_sub_category'))
                        skills = self.get_selected_job_skill(request.POST.getlist('job_skills'))
                        try:
                            if request.user.invite_type.type == request.user.get_client_company_intance().user.invite_type.type == 'OWNER':

                                created_by = "Created by " + request.user.get_client_company_intance().company_name + " Super Admin Member " + request.user.get_client_company_intance().user.first_name + " " + request.user.get_client_company_intance().user.last_name
                            else:

                                created_by = "Created by " + request.user.get_client_company_intance().company_name + " " + request.user.invited_client_company_user.role_type + " Member " + request.user.get_client_company_intance().user.first_name + " " + request.user.get_client_company_intance().user.last_name
                        except:
                            created_by = "Created by " + request.user.first_name + " " + request.user.last_name
                        verification = VerificationJobPost.objects.all().order_by('-id')
                        message = "Your job has successfully posted"
                        if request.POST['job_status'] == '1':
                            if verification[0].verification_status == 'verification_off':

                                job_status = self.Get_status(request.POST['job_status'])
                                status = 0
                            else:
                                job_status = self.Get_status(request.POST['job_status'])
                                status = 1
                                message = "Your job has been successfully posted for the verification"
                        else:
                            job_status = self.Get_status(request.POST['job_status'])
                            status = 0
                        payment_type =self.get_payment(request.POST['payment_type'])
                        duration =self.get_duration(request.POST['duration'])
                        date =(datetime.date.today() + datetime.timedelta(1 * 365 / 12)).isoformat()


                        if request.POST['job_close_date'] =='':
                            date =date
                        else:
                            date = datetime.datetime.strptime(str(request.POST['job_close_date']), "%m/%d/%Y")

                        Client_Profile.objects.get_or_create(user=request.user)
                        job_id = form.save(request.user,duration,payment_type,request.POST['budget'],request.POST['hourly_max'],request.POST['hourly_min'],job_status, created_by, job_category, job_sub_category, skills,date,status)
                        payload = {'status': 'success', 'type':'1','message': message, 'data': "k"}
                        return HttpResponse(json.dumps(payload), content_type='application/json', )
                    else:
                        payload = {'status': 'success', 'type':'0','message': "Your job has successfully posted", 'data': "k"}
                        return HttpResponse(json.dumps(payload), content_type='application/json')


                else:
                    self.message = "Validation failed."
                    self.data = form.errors
                    self.success = False

                    payload = {'status': 'failed','type':'0', 'message': self.message, 'data': self.data}
                    return HttpResponse(json.dumps(payload), content_type='application/json', )
            if request.POST['method'] == 'Edit_job':
                job =self.get_current_job(request.POST['id'])
                form = PostJobForm(request.POST,instance=job)

                if form.is_valid():
                    if request.POST['errors_valid'] == '1':
                        hire_details =self.get_all_hire_single(job.id)
                        if hire_details.count() == 0:

                            job_category = self.get_category(request.POST['job_main_category'])
                            job_sub_category = self.get_selected_sub_catrories(request.POST.getlist('job_sub_category'))
                            skills = self.get_selected_job_skill(request.POST.getlist('job_skills'))


                            message = "Your job has successfully Updated"

                            job_status = self.Get_status(request.POST['job_status'])

                            payment_type =self.get_payment(request.POST['payment_type'])
                            duration =self.get_duration(request.POST['duration'])
                            date =(datetime.date.today() + datetime.timedelta(1 * 365 / 12)).isoformat()


                            if request.POST['job_close_date'] =='':
                                date =date
                            else:
                                date = datetime.datetime.strptime(str(request.POST['job_close_date']), "%m/%d/%Y")

                            created_by =job.created_by
                            status = job.verification_status
                            job_id = form.save(request.user,duration,payment_type,request.POST['budget'],request.POST['hourly_max'],request.POST['hourly_min'],job_status, created_by, job_category, job_sub_category, skills,date,status)
                            payload = {'status': 'success', 'type':'1','message': message, 'data': "k"}
                            return HttpResponse(json.dumps(payload), content_type='application/json', )
                        else:
                            payload = {'status': 'failed','type':'1', 'message':"You are not allowed to edit this job"}
                            return HttpResponse(json.dumps(payload), content_type='application/json', )

                    else:
                        payload = {'status': 'success', 'type':'0','message': "error", 'data': "k"}
                        return HttpResponse(json.dumps(payload), content_type='application/json')


                else:
                    self.message = "Validation failed."
                    self.data = form.errors
                    self.success = False

                    payload = {'status': 'failed','type':'0', 'message': self.message, 'data': self.data}
                    return HttpResponse(json.dumps(payload), content_type='application/json', )
            if request.POST['method'] == 'client_Get_Job':
                jobid = request.POST['jobid']
                get_previous_data = self.get_all_previous_data()

                if jobid != '0':
                    job = self.get_current_job(jobid)
                    form = PostJobForm(instance=job)
                else:
                    job = "0"
                    form = PostJobForm()

                html = render_to_string('client/ajax/get_job_details.html',
                                        {'csrf_token_value': request.COOKIES['csrftoken'], 'form': form, 'job': job,
                                         'job_status' : self.get_all_job_status(),'duration' : self.get_all_duration(),'Payment_type': self.get_all_Payment_type(),'get_previous_data':get_previous_data })
                payload = {'status': 'success', 'message': "Successfully Posted", 'data': html}
                return HttpResponse(json.dumps(payload), content_type='application/json', )


class SerachFreelancer(ClientMixin, TemplateView):
    template_name = 'client/search_freelancers.html'

    def get(self, request):
        # all_jobs = self.get_my_jobs()

        template_name = 'client/search_freelancers.html'
        workcategory = self.get_all_WorkCategory()
        subcategory = self.get_all_SubWorkCategory()
        countries = self.get_all_Country()
        skills = self.get_all_Skills()
        # user = request.user.email
        freelancerlist = self.get_all_Freelancerlist()

        context = {'workcategory': workcategory, 'key_words': self.get_freelancer_search_query_list(),
                   'subcategory': subcategory, 'countries': countries, 'skills': skills,
                   'freelancerlist': freelancerlist.exclude(user=request.user)}
        return TemplateResponse(request, template_name, context)

    def post(self, request, *args, **kwargs):
        try:

            user = request.user.get_client_company_intance().user

        except:
            user = request.user
        if request.POST['method'] == "Search_autocomplete":
            search = request.POST['search']

            if search:
                search = search.split(",")

                freelancerlist = self.get_all_Freelancerlist()
                all_freelancers = freelancerlist.filter(Q(subcategory__sub_category_title__icontains=search) | Q(
                    subcategory__sub_category_title__in=search) | Q(workcategory__category_title__icontains=search) | Q(
                    workcategory__category_title__in=search) |
                                                        Q(user__first_name__icontains=search) | Q(
                    user__first_name__in=search) | Q(
                    title__icontains=search) | Q(title__in=search) | Q(skills__name__in=search) | Q(
                    skills__name__contains=search) | Q(
                    skills__name__icontains=search)).distinct()
            else:
                all_freelancers = self.get_all_Freelancerlist()

            list_html = render_to_string('client/ajax/freelancers_list.html',
                                         {'all_freelancers': all_freelancers.exclude(user=request.user)})
            response_data = {'list_html': list_html}
            return HttpResponse(json.dumps(response_data), content_type="application/json")
        if request.POST['method'] == "Invite_Freelancer":
            freelancerlist = self.get_all_Freelancerlist()
            details = freelancerlist.get(id=request.POST['id'])
            jobdetails = ClientJobs.objects.all()

            # & Q(job_close_date__gte=today)
            today = datetime.datetime.today()


            jobdetails = ClientJobs.objects.filter(
                Q(job_status__status='Publish') &  Q(verification_status='0') & Q(repost_status='0') & Q(user=self.request.user) ).order_by("-id")

            html = render_to_string('client/ajax/invite_freelancer.html',
                                    {'details': details, 'jobdetails': jobdetails,
                                     'csrf_token_value': request.COOKIES['csrftoken']})
            response_data = {'html': html}
            return HttpResponse(json.dumps(response_data), content_type="application/json")
        if request.POST['method'] == "Save_Invite_Job":
            form = InviteForm(request.POST)
            if form.is_valid():

                job = self.get_job(request.POST['job'])

                freelancer =self.get_freelancer_user(request.POST['freelancer_id'])
                if not self.if_job_invitation(self.get_freelancer_user(request.POST['freelancer_id']), job):
                    if not self.if_job_applied(self.get_freelancer_user(request.POST['freelancer_id']), job):
                        if job.payment_type.id == 2:
                            if (job.budget != None and job.budget != 0) :
                                form.save(freelancer, job, request.POST['message'])

                                payload_message = {'created_by': request.user.id, 'type': 'FREELANCER',
                                                   'user_id': freelancer.user.id,
                                                   'title': " has invited a  job -" + job.job_title,
                                                   'description': "job title is " + job.job_title,  "url":reverse('freelancerjoblist')}
                                payload = {'status': 'success',
                                           'message': 'Successfully Updated',
                                           'data': job.job_title,
                                           'fid': self.get_freelancer_user(request.POST['freelancer_id']).user.id,
                                           'note': payload_message}
                                return HttpResponse(json.dumps(payload), content_type='application/json', )


                            else:
                                self.message = "Validation failed."
                                self.data = {'job': ["Budget not available for this job"]}
                                self.success = False
                                payload = {'status': 'failed', 'message': self.message, 'data': self.data}
                                return HttpResponse(json.dumps(payload), content_type='application/json', )
                        elif  job.payment_type.id == 1:
                                try:
                                    if (freelancer.user.freelancer_hourly_user.get().hourly_rate !='' and freelancer.user.freelancer_hourly_user.get().hourly_rate !=None )  :
                                            form.save(freelancer, job, request.POST['message'])

                                            payload_message = {'created_by': request.user.id,'type':'FREELANCER',
                                                       'user_id':freelancer.user.id,
                                                       'title': " has invited a  job -"+job.job_title,'description':"job title is "+job.job_title}
                                            payload = {'status': 'success',
                                                       'message': 'Successfully Updated',
                                                       'data': job.job_title,'fid':self.get_freelancer_user(request.POST['freelancer_id']).user.id,'note':payload_message}
                                            return HttpResponse(json.dumps(payload), content_type='application/json', )
                                    else:
                                            self.message = "Validation failed."
                                            self.data = {'job': ["Hourly Rate not available for this user"]}
                                            self.success = False
                                            payload = {'status': 'failed', 'message': self.message, 'data': self.data}
                                            return HttpResponse(json.dumps(payload), content_type='application/json', )
                                except:

                                    self.message = "Validation failed."
                                    self.data = {'job': ["Hourly Rate not available for this user"]}
                                    self.success = False
                                    payload = {'status': 'failed', 'message': self.message, 'data': self.data}
                                    return HttpResponse(json.dumps(payload), content_type='application/json', )


                                    # if ((job.hourly_max != None and job.hourly_min != None) and (
                            #             job.hourly_max != 0 and job.hourly_min != 0) and (
                            #         job.hourly_max != '' and job.hourly_min != '')):


                        # if (job.budget != None and job.budget != 0) or (
                        #         (job.hourly_max != None and job.hourly_min != None) and (
                        #             job.hourly_max != 0 and job.hourly_min != 0) and (
                        #         job.hourly_max != '' and job.hourly_min != '')):
                        #     if (freelancer.user.freelancer_hourly_user.get().hourly_rate !='' and freelancer.user.freelancer_hourly_user.get().hourly_rate !=None ) or (job.budget != None and job.budget != 0) :
                        #         form.save(freelancer, job, request.POST['message'])
                        #
                        #         payload_message = {'created_by': request.user.id,'type':'FREELANCER',
                        #                    'user_id':freelancer.user.id,
                        #                    'title': " has invited a  job -"+job.job_title,'description':"job title is "+job.job_title}
                        #         payload = {'status': 'success',
                        #                    'message': 'Successfully Updated',
                        #                    'data': job.job_title,'fid':self.get_freelancer_user(request.POST['freelancer_id']).user.id,'note':payload_message}
                        #         return HttpResponse(json.dumps(payload), content_type='application/json', )
                        #     else:
                        #         self.message = "Validation failed."
                        #         self.data = {'job': ["Hourly Rate not available for this user"]}
                        #         self.success = False
                        #         payload = {'status': 'failed', 'message': self.message, 'data': self.data}
                        #         return HttpResponse(json.dumps(payload), content_type='application/json', )
                        #
                        #
                        # else:
                        #     self.message = "Validation failed."
                        #     self.data = {'job': ["Budget not available for this job"]}
                        #     self.success = False
                        #     payload = {'status': 'failed', 'message': self.message, 'data': self.data}
                        #     return HttpResponse(json.dumps(payload), content_type='application/json', )

                    else:
                        self.message = "Validation failed."
                        self.data = {'job': ["Freelancer already applied for this job"]}
                        self.success = False
                        payload = {'status': 'failed', 'message': self.message, 'data': self.data}
                        return HttpResponse(json.dumps(payload), content_type='application/json', )

                else:
                    self.message = "Validation failed."
                    self.data = {'job': ["You are already invited for this job"]}
                    self.success = False
                    payload = {'status': 'failed', 'message': self.message, 'data': self.data}
                    return HttpResponse(json.dumps(payload), content_type='application/json', )

            else:

                self.message = "Validation failed."
                self.data = form.errors
                self.success = False
                payload = {'status': 'failed', 'message': self.message, 'data': self.data}
                return HttpResponse(json.dumps(payload), content_type='application/json', )
        if request.POST['method']  =='Get_html_Subcategory':
            workid = request.POST['workid']
            workid = workid.split(",")
            html = render_to_string('client/ajax/filter_subcategory.html',
                                         {'type':'0','subcategory': WorkSubCategories.objects.filter(work_category__in=workid)})
            html_popup = render_to_string('client/ajax/filter_subcategory.html',
                                    {'type':'1','subcategory': WorkSubCategories.objects.filter(work_category__in=workid)})

            response_data = {'status': 'success','sub_category_html':html,'sub_category_pophtml':html_popup}
            return HttpResponse(json.dumps(response_data), content_type="application/json")



class PaginationView(ClientMixin, ListView):
    paginate_by = 10

    def post(self, request, *args, **kwargs):
        response_data = {}
        if request.POST.get('method') == 'Get_Work_Category':
            page_by = request.POST.get('page_by')
            search = request.POST.get('search')

            paginator_response = self.get_paginated_queryset_1(request, page_by, search)
            response_data['paginator_html'] = paginator_response['paginator_html']

            return HttpResponse(json.dumps(response_data), content_type="application/json")
        if request.POST.get('method') == 'Get_Work_SubCategory':
            page_by = request.POST.get('page_by')
            search = request.POST.get('search')

            paginator_response = self.get_paginated_queryset_subcategory(request, page_by, search)
            response_data['paginator_html'] = paginator_response['paginator_html']

            return HttpResponse(json.dumps(response_data), content_type="application/json")
            # if request.POST.get('method') == 'Search_autocomplete':
            #
            #     page_by = request.POST.get('page_by')
            #     search = request.POST.get('search')
            #     freelancerlist = self.get_all_Freelancerlist()
            #     if search == '':
            #         freelancerlist = freelancerlist.exclude(user=request.user)
            #     else:
            #         search = search.split(",")
            #
            #         freelancerlist = freelancerlist.exclude(user=request.user)
            #         freelancerlist = freelancerlist.filter(
            #             Q(subcategory__sub_category_title__icontains=search) | Q(
            #                 subcategory__sub_category_title__in=search) | Q(
            #                 workcategory__category_title__icontains=search) | Q(workcategory__category_title__in=search) |
            #             Q(user__first_name__icontains=search) | Q(user__first_name__in=search) | Q(
            #                 title__icontains=search) | Q(title__in=search) | Q(skills__name__in=search) | Q(
            #                 skills__name__contains=search) | Q(
            #                 skills__name__icontains=search)).distinct()
            #
            #     paginator_response = self.get_paginated_queryset_search(request, page_by, search, freelancerlist)
            #     response_data['list_html'] = paginator_response['paginator_html']
            #
            #     return HttpResponse(json.dumps(response_data), content_type="application/json")

    def get_paginated_queryset_1(self, request, page_by, search):

        if page_by:
            page_by = int(page_by)
        else:
            page_by = 1

        if search == '':
            page_size = self.get_paginate_by(WorkCategories.objects.all().order_by('sorting_order'))
            size = self.paginate_queryset(WorkCategories.objects.all().order_by('sorting_order'), page_size)
        else:
            page_size = self.get_paginate_by(
                WorkCategories.objects.filter(category_title__contains=search).order_by('sorting_order'))
            size = self.paginate_queryset(
                WorkCategories.objects.filter(category_title__contains=search).order_by('sorting_order'), page_size)

        paginator, page, queryset, is_paginated = size
        try:
            queryset = paginator.page(page_by).object_list
        except:
            queryset = queryset

        user_table = render_to_string('client/ajax/listing_workcategory.html',
                                      {'queryset': queryset, 'paginator': paginator, 'page': page,
                                       'is_paginated': is_paginated, 'type': 'Client', 'request': request,
                                       'page_obj': paginator.page(page_by)})

        # paginator_html = render_to_string('ajax/paginator_html.html',
        #                                   {'page_obj': paginator.page(page_by)})

        context = {'user_table': user_table, 'paginator_html': user_table}
        return context

    def get_paginated_queryset_subcategory(self, request, page_by, search):

        if page_by:
            page_by = int(page_by)
        else:
            page_by = 1

        if search == '':
            page_size = self.get_paginate_by(WorkSubCategories.objects.all())
            size = self.paginate_queryset(WorkSubCategories.objects.all(), page_size)
        else:

            page_size = self.get_paginate_by(
                WorkSubCategories.objects.filter(sub_category_title__contains=search))
            size = self.paginate_queryset(
                WorkSubCategories.objects.filter(sub_category_title__contains=search), page_size)

        paginator, page, queryset, is_paginated = size
        try:
            queryset = paginator.page(page_by).object_list
        except:
            queryset = queryset

        user_table = render_to_string('client/ajax/listing_worksubcategory.html',
                                      {'queryset': queryset, 'paginator': paginator, 'page': page,
                                       'is_paginated': is_paginated, 'type': 'Client', 'request': request,
                                       'page_obj': paginator.page(page_by)})

        # paginator_html = render_to_string('ajax/paginator_html.html',
        #                                   {'page_obj': paginator.page(page_by)})

        context = {'user_table': user_table, 'paginator_html': user_table}
        return context

    def get_paginated_queryset_search(self, request, page_by, search, freelancerlist):

        paginate_by = 3
        if page_by:
            page_by = int(page_by)
        else:
            page_by = 1
        page_size = self.get_paginate_by(freelancerlist)
        size = self.paginate_queryset(freelancerlist, page_size)

        # if search == '':
        #     page_size = self.get_paginate_by(freelancerlist)
        #     size = self.paginate_queryset(freelancerlist, page_size)
        # else:
        #     page_size = self.get_paginate_by(
        #         WorkSubCategories.objects.filter(sub_category_title__contains=search))
        #     size = self.paginate_queryset(
        #         WorkSubCategories.objects.filter(sub_category_title__contains=search), page_size)

        paginator, page, queryset, is_paginated = size
        try:
            queryset = paginator.page(page_by).object_list
        except:
            queryset = queryset

        user_table = render_to_string('client/ajax/freelancers_list.html',
                                      {'queryset': queryset, 'paginator': paginator, 'page': page,
                                       'is_paginated': is_paginated, 'type': 'Client', 'request': request,
                                       'page_obj': paginator.page(page_by)})

        # paginator_html = render_to_string('ajax/paginator_html.html',
        #                                   {'page_obj': paginator.page(page_by)})

        context = {'user_table': user_table, 'paginator_html': user_table}
        return context


class FreelancerPaginationView(ClientMixin, ListView):
    paginate_by =25

    def post(self, request, *args, **kwargs):
        response_data = {}

        if request.POST.get('method') == 'Search_autocomplete':
            page_by = request.POST.get('page_by')
            search = request.POST.get('search')
            hourly_max = request.POST.get('hourlymax')
            hourly_min = request.POST.get('hourlymin')
            workcategory = request.POST.get('workcategory')
            subcategory = request.POST.get('subcategory')
            country = request.POST.getlist('country[]')
            skills = request.POST.getlist('skills[]')
            freelancerlist = self.get_search_freelancer(search, hourly_max, hourly_min, workcategory, subcategory,
                                                        country, skills)

            paginator_response = self.get_paginated_queryset_search(request, page_by, search, freelancerlist)
            response_data['list_html'] = paginator_response['paginator_html']
            response_data['count'] = paginator_response['count']

            return HttpResponse(json.dumps(response_data), content_type="application/json")

    def get_paginated_queryset_search(self, request, page_by, search, freelancerlist):

        if page_by:
            page_by = int(page_by)
        else:
            page_by = 1
        page_size = self.get_paginate_by(freelancerlist)
        size = self.paginate_queryset(freelancerlist, page_size)

        # if search == '':
        #     page_size = self.get_paginate_by(freelancerlist)
        #     size = self.paginate_queryset(freelancerlist, page_size)
        # else:
        #     page_size = self.get_paginate_by(
        #         WorkSubCategories.objects.filter(sub_category_title__contains=search))
        #     size = self.paginate_queryset(
        #         WorkSubCategories.objects.filter(sub_category_title__contains=search), page_size)

        paginator, page, queryset, is_paginated = size
        try:
            queryset = paginator.page(page_by).object_list
        except:
            queryset = queryset


        user_table = render_to_string('client/ajax/freelancers_list.html',
                                      {'queryset': queryset, 'paginator': paginator, 'page': page,
                                       'is_paginated': is_paginated, 'type': 'Client', 'request': request,
                                       'page_obj': paginator.page(page_by)})

        # paginator_html = render_to_string('ajax/paginator_html.html',
        #                                   {'page_obj': paginator.page(page_by)})

        context = {'count': freelancerlist.count(), 'paginator_html': user_table}
        return context


class MyJobs(ClientMixin, TemplateView):
    template_name = 'client/my_joblist.html'


    def get(self, request,**kwargs):
        # all_jobs = self.get_my_jobs()

        template_name = 'client/my_joblist.html'
        if request.user.is_authenticated():
            extended_template = 'inner_page_base.html'
        else:
            extended_template = 'base.html'



        workcategory = self.get_all_WorkCategory()
        subcategory = self.get_all_SubWorkCategory()
        countries = self.get_all_Country()
        payment = self.get_all_Payment_type()
        skills = self.get_all_Skills()
        job_status = self.get_all_job_status()
        # user = request.user.email
        freelancerlist = self.get_all_JobList()

        context = {'extended_template': extended_template,'key_words_search': self.get_job_search_query_list(),'workcategory': workcategory,'job_status': job_status,'payment': payment, 'key_words': self.get_freelancer_search_query_list(),
                   'subcategory': subcategory, 'countries': countries, 'skills': skills,
                   'freelancerlist': freelancerlist, 'type': 'Client'}
        return TemplateResponse(request, template_name, context)
class FindJobs(ClientMixin, TemplateView):
    template_name = 'client/find_joblist.html'

    def get(self, request):
        # all_jobs = self.get_my_jobs()

        template_name = 'client/find_joblist.html'
        workcategory = self.get_all_WorkCategory()
        subcategory = self.get_all_SubWorkCategory()
        countries = self.get_all_Country()
        payment = self.get_all_Payment_type()
        skills = self.get_all_Skills()
        job_status = self.get_all_job_status()
        # user = request.user.email
        freelancerlist = self.get_all_JobList()

        context = {'workcategory': workcategory,'job_status': job_status,'payment': payment, 'key_words': self.get_freelancer_search_query_list(),
                   'subcategory': subcategory, 'countries': countries, 'skills': skills,
                   'freelancerlist': freelancerlist, 'type': 'Client'}
        return TemplateResponse(request, template_name, context)


class JoblistPaginationView(ClientMixin, ListView):
    paginate_by = 5

    def post(self, request, *args, **kwargs):
        response_data = {}

        if request.POST.get('method') == 'Search_job':
            page_by = request.POST.get('page_by')
            serach_all = request.POST.get('search')
            search = request.POST.get('search_category')
            search_status = request.POST.get('search_status')
            search_type = request.POST.get('search_type')
            search_experiance = request.POST.get('search_experiance')
            search_hourlymin = request.POST.get('search_hourlymin')
            search_hourlymax = request.POST.get('search_hourlymax')
            joblist = self.get_searchJob_list(serach_all ,search_status, search, search_type, search_experiance, search_hourlymin,
                                              search_hourlymax)

            paginator_response = self.get_paginated_queryset_searchjob(request, page_by, search, joblist)
            response_data['list_html'] = paginator_response['paginator_html']
            response_data['count'] = paginator_response['count']

            return HttpResponse(json.dumps(response_data), content_type="application/json")

    def get_paginated_queryset_searchjob(self, request, page_by, search, joblist):

        if page_by:
            page_by = int(page_by)
        else:
            page_by = 1
        page_size = self.get_paginate_by(joblist)
        size = self.paginate_queryset(joblist, page_size)

        # if search == '':
        #     page_size = self.get_paginate_by(freelancerlist)
        #     size = self.paginate_queryset(freelancerlist, page_size)
        # else:
        #     page_size = self.get_paginate_by(
        #         WorkSubCategories.objects.filter(sub_category_title__contains=search))
        #     size = self.paginate_queryset(
        #         WorkSubCategories.objects.filter(sub_category_title__contains=search), page_size)

        paginator, page, queryset, is_paginated = size
        try:
            queryset = paginator.page(page_by).object_list
        except:
            queryset = queryset

        user_table = render_to_string('client/ajax/job_list.html',
                                      {'queryset': queryset, 'count': joblist.count(), 'paginator': paginator,
                                       'page': page,
                                       'is_paginated': is_paginated, 'type': 'Client', 'request': request,
                                       'page_obj': paginator.page(page_by)})

        # paginator_html = render_to_string('ajax/paginator_html.html',
        #                                   {'page_obj': paginator.page(page_by)})

        context = {'count': joblist.count(),'paginator_html': user_table}
        return context


class JobDetails(ClientMixin,TemplateView):
    template_name = 'client/job_details.html'

    def get_context_data(self, **kwargs):
        context = super(JobDetails, self).get_context_data(**kwargs)

        context['jobdetails'] = self.get_current_job(kwargs.get('slug'))
        return context
    def post(self,request, *args, **kwargs):

        if request.POST.get('method') == 'Hire_freelancer':
            if request.user.active_account.type == 'CLIENT':


                jobdetails =self.get_current_job(kwargs.get('slug'))


                if jobdetails.payment_type !=None:



                    freelancer = self.is_user(request.POST.get('details'))

                    hire,hires = HiredJobsContract.objects.get_or_create(client_profile=self.is_client_profile(request.user),freelancer_profile=self.is_freelancer_profile(freelancer),client_job=jobdetails)
                    hire.job_title =jobdetails.job_title
                    hire.job_description = jobdetails.job_description
                    hire.status = self.get_hire_status('1')
                    if jobdetails.payment_type.id == 1:
                        hire.payment_type = self.get_payment(jobdetails.payment_type.id)
                        # hire.hourly_max = jobdetails.hourly_max
                        # hire.hourly_min = jobdetails.hourly_min
                        hire.budget = freelancer.freelancer_hourly_user.get().hourly_rate

                    if jobdetails.payment_type.id == 2:
                        hire.payment_type = self.get_payment(jobdetails.payment_type.id)
                        hire.budget = jobdetails.budget

                    hire.created_by = request.user
                    #hire.repost_status = 0
                    hire.save()
                    #print hire.slug,"dffffffffffffffff"
                    # print hire.modified_date, "dffffffffffffffff"
                    payload = {'status': 'success', 'urlLink': reverse('hireviews', kwargs={'slug': hire.slug}),'type': '0', 'message': "You have hired this freelancer", 'data': "k"}
                    return HttpResponse(json.dumps(payload), content_type='application/json')


                else:
                    payload = {'status': 'failed',
                               'type': '0', 'message': "Please add payment type and amount", 'data': "k"}
                    return HttpResponse(json.dumps(payload), content_type='application/json')

            else:

                payload = {'status': 'failed',
                           'type': '0', 'message': "Pls login as client", 'data': "k"}
                return HttpResponse(json.dumps(payload), content_type='application/json')



class JobInvitedPaginationView(ClientMixin, ListView):
    paginate_by = 5

    def post(self, request, *args, **kwargs):
        response_data = {}

        if request.POST.get('method') == 'Get_InvitedList':
            page_by = request.POST.get('page_by')

            invitelist = self.Get_invited_freelancerList(request.POST.get('jobid'),request.POST.get('type'))
            paginator_response = self.get_paginated_queryset_invitedlist(request, page_by, invitelist,request.POST.get('type'),request.POST.get('jobid'))
            response_data['list_html'] = paginator_response['paginator_html']

            return HttpResponse(json.dumps(response_data), content_type="application/json")

    def get_paginated_queryset_invitedlist(self, request, page_by, invitelist,jobtype,jobid):

        if page_by:
            page_by = int(page_by)
        else:
            page_by = 1
        page_size = self.get_paginate_by(invitelist)
        size = self.paginate_queryset(invitelist, page_size)

        # if search == '':
        #     page_size = self.get_paginate_by(freelancerlist)
        #     size = self.paginate_queryset(freelancerlist, page_size)
        # else:
        #     page_size = self.get_paginate_by(
        #         WorkSubCategories.objects.filter(sub_category_title__contains=search))
        #     size = self.paginate_queryset(
        #         WorkSubCategories.objects.filter(sub_category_title__contains=search), page_size)

        paginator, page, queryset, is_paginated = size
        try:
            queryset = paginator.page(page_by).object_list
        except:
            queryset = queryset

        user_table = render_to_string('client/ajax/job_invitedfreelancer_list.html',
                                      {'queryset': queryset, 'count': invitelist.count(), 'paginator': paginator,
                                       'page': page,
                                       'is_paginated': is_paginated, 'type': 'Client','jobid': self.get_job(jobid),'jobtype':jobtype, 'request': request,
                                       'page_obj': paginator.page(page_by)})

        # paginator_html = render_to_string('ajax/paginator_html.html',
        #                                   {'page_obj': paginator.page(page_by)})

        context = {'user_table': user_table, 'paginator_html': user_table}
        return context
class EditJob(ClientMixin,TemplateView):
    template_name = 'client/edit_job_details.html'
    def get(self, request,**kwargs):
        jobdetails = ClientJobs.objects.get(slug=kwargs.get('slug'))
        Payment_type = self.get_all_Payment_type()
        job_status = self.get_all_job_status()
        duration = self.get_all_duration()
        template_name = 'client/edit_job_details.html'
        context = {'jobdetails':jobdetails,'Payment_type':Payment_type,'job_status':job_status,'duration':duration}
        return TemplateResponse(request, template_name, context)
