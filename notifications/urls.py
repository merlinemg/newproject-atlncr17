from django.conf.urls import url
from django.contrib.auth.decorators import login_required

from notifications.views import NotificationCount, NotificationView, NotificationsListView

urlpatterns = [

    url(r'check-count$', login_required(NotificationCount.as_view()), name='checkcount'),
    url(r'notification$', login_required(NotificationView.as_view()), name='notificationview'),
    url(r'notification-list$', login_required(NotificationsListView.as_view()), name='notificationlist'),

]