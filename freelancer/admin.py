from django.contrib import admin

# Register your models here.
from freelancer.models import Freelancer_Profile, WorkCategories, WorkSubCategories, Skills, HourlyRate, LocationDetails, \
    EmploymentHistory, FreelancerPortfolio, Certifications, ExperienceLevel, FreelancerPortfolioimages

admin.site.register(WorkCategories)

admin.site.register(WorkSubCategories)
admin.site.register(Skills)
admin.site.register(HourlyRate)
admin.site.register(LocationDetails)
admin.site.register(EmploymentHistory)
admin.site.register(FreelancerPortfolio)
admin.site.register(Certifications)
admin.site.register(ExperienceLevel)
admin.site.register(FreelancerPortfolioimages)
admin.site.register(Freelancer_Profile)