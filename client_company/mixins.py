from accounts.models import Users, User_Type
from client_company.models import Client_Company_Profile, Invited_Client_Company_User
from freelancer_company.models import Invited_Freelancer_Company_User


class ClientCompanyMixin(object):

    def check_client_company_profile(self, user):
        try:

            data = Client_Company_Profile.objects.get(user=user)

        except:
            data = None
        return data
    def Get_user_client(self):
        try:

            user = self.request.user.get_client_company_intance().user

        except:
            user = self.request.user
        return user

    def check_user_exist(self, user):
        try:

            data = Users.objects.get(email=user)

        except:
            data = None
        return data
    def check_user_exist_invited_company(self, user):
        try:

            data = Invited_Client_Company_User.objects.get(user__email=user)

        except:
            data = None
        return data

    def get_user_type(self, type):
        """
        get selected skills.
        :return:
        """
        print type,"mmmmmmmmmmmmmm"
        types = User_Type.objects.get(type=type)
        return types
    def freelancer_user(self, user):
        try:

            data = Invited_Freelancer_Company_User.objects.get(user__email=user)

        except:
            data = None
        return data
    def check_exist_client_company(self, user):
        try:

            data = Client_Company_Profile.objects.get_or_create(user=user)

        except:
            data = None
        return data
