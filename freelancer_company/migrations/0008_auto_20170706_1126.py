# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-07-06 11:26
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('freelancer_company', '0007_auto_20170706_1116'),
    ]

    operations = [
        migrations.AlterField(
            model_name='freelancer_company_hourlyrate',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='freelancer_company_hourly_user', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='freelancer_company_locationdetails',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='freelancer_company_location_user', to=settings.AUTH_USER_MODEL),
        ),
    ]
