import random

from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver
from django.utils.crypto import get_random_string
from django.utils.text import slugify


from client.models import ClientJobs, JobInvitation


@receiver(post_save, sender=ClientJobs)
def Client_Job_Id(sender, instance, **kwargs):

    if not instance.slug:
        unique_id = get_random_string(length=15)
        instance.slug = slugify(instance.job_title) + "-" + unique_id
        instance.save()

    if not instance.job_number:
        lower = 100 * 1000
        upper = lower * 1000
        random_num = random.randint(lower, upper)
        instance.job_number = random_num
        instance.save()
    # if not kwargs['created']:
    #    # instance.modified_by = _thread_locals.current_user
    return sender

@receiver(post_save, sender=JobInvitation)
def JobInvitationmodified(sender, instance, **kwargs):

    # if not kwargs['created']:
    #     instance.modified_by = Users.objects.get(email="test@gmail.com")
    #     instance.save()
    # else:
    #     instance.created_by = _thread_locals.current_user
    #     instance.save()



    return sender


