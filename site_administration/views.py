import json
import logging
from django.contrib.auth import authenticate, login
from django.contrib.auth.views import deprecate_current_app
from django.contrib.sites.shortcuts import get_current_site
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect, resolve_url

# Create your views here.
from django.template.loader import render_to_string
from django.template.response import TemplateResponse
from django.utils import timezone
from django.utils.http import is_safe_url
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.debug import sensitive_post_parameters
from django.views.generic import FormView
from django.views.generic import ListView
from django.views.generic import TemplateView
from django.views.generic import UpdateView

from accounts.forms import LoginForm
from accounts.models import Users
from site_administration.forms import RolePermissionform, RolePermissionUpdateform
from site_administration.models import Permission, Role_Permission
from rolepermissions.roles import assign_role
from rolepermissions.roles import remove_role


logger = logging.getLogger(__name__)
from django.contrib.auth import (
    REDIRECT_FIELD_NAME, get_user_model, login as auth_login,
    logout as auth_logout, update_session_auth_hash, )
from rolepermissions.mixins import HasPermissionsMixin


def index(request):
    return HttpResponse("Hello, world. You're at")


@deprecate_current_app
@sensitive_post_parameters()
@csrf_protect
@never_cache
def siteAdminView(request, template_name='admin/admin_login.html',
                  redirect_field_name='/site_administration/Adminclientlist/',
                  authentication_form=LoginForm,
                  extra_context=None):
    """    Displays the login form and handles the login action.    """
    if request.method == 'GET' and request.user.is_authenticated():
        if request.user.user_type == 'Site_admin' and request.user.is_superuser:
            return redirect('/site_administration/admincreaterolelist')
        else:
            return redirect('/site_administration/Adminclientlist')

    redirect_to = request.POST.get(redirect_field_name,
                                   request.GET.get(redirect_field_name, ''))
    if request.method == "POST":
        form = LoginForm(request, data=request.POST)
        # print request.POST
        # print "values"
        if form.is_valid():

            # print request.POST
            # print "valuessssssssssss"
            if not is_safe_url(url=redirect_to, host=request.get_host()):
                redirect_to = resolve_url('/site_administration/Adminclientlist')
            user_obj = form.get_user()
            if user_obj.user_type == 'Site_admin' or user_obj.is_superuser:
                auth_login(request, form.get_user())
                user = Users.objects.get(email=user_obj.email)
                if user_obj.is_superuser and user_obj.user_type == 'Site_admin':
                    remove_role(user,'superadmin')
                    assign_role(user, 'superadmin')
                    return redirect('/site_administration/admincreaterolelist')
                else:
                    remove_role(user, 'admin')
                    assign_role(user, 'admin')
                    return redirect('/site_administration/Adminclientlist')
            else:
                form.add_error(None, "Not a site admin.")

        else:

            # print "Failed"
            logger.debug("Login failed from " + request.META.get('REMOTE_ADDR'))
    else:
        # print request.method
        # print request.POST
        # print "get"
        form = LoginForm(request)
    current_site = get_current_site(request)
    context = {'form': form, redirect_field_name: redirect_to, 'site': current_site,
               'site_name': current_site.name, }
    print context
    if extra_context is not None:
        context.update(extra_context)
    return TemplateResponse(request, template_name, context)


class AdminDashboard(HasPermissionsMixin, TemplateView):
    required_permission = 'Admin_Client_List'
    template_name = 'admin/clientlist.html'


class AdmincreateList(HasPermissionsMixin, ListView):
    required_permission = 'Admin_Role_list'
    template_name = 'admin/rolelist.html'
    model = Role_Permission
    paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super(AdmincreateList, self).get_context_data(**kwargs)

        context['rolelist'] = Role_Permission.objects.all()

        return context


class AdmincreateRole(TemplateView):
    template_name = 'admin/addrole.html'
    model = Role_Permission
    paginate_by = 10
    form_class = RolePermissionform

    def get_context_data(self, **kwargs):
        context = super(AdmincreateRole, self).get_context_data(**kwargs)

        context['permissionlist'] = Permission.objects.all()

        return context

    def post(self, request, *args, **kwargs):
        # print request.POST
        # request.POST.get('permission',request.POST.get('duallistbox_demo2_helper2'))

        form = RolePermissionform(request.POST)

        if form.is_valid():
            # print request.POST
            form.save()

            payload = {'status': 'success', 'message': 'Successfully Created',
                       'data': 'g'}
            context = super(AdmincreateRole, self).get_context_data(**kwargs)
        else:
            self.message = "Validation failed."
            self.data = form.errors
            self.success = False

            payload = {'status': 'failed', 'message': self.message, 'data': self.data}

            context = {'form': form, 'permissionlist': Permission.objects.all()}
        if self.request.is_ajax():
            return HttpResponse(json.dumps(payload),
                                content_type='application/json',
                                )
        return TemplateResponse(request, 'admin/addrole.html', context)

        # return render(request, 'admin/addrole.html', context)


class AdminClientList(HasPermissionsMixin, ListView):
    required_permission = 'Admin_Client_List'
    template_name = 'admin/clientlist.html'
    model = Users
    paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super(AdminClientList, self).get_context_data(**kwargs)

        context['clientlist'] = Users.objects.filter(user_type__type='CLIENT')

        return context


class AdminFreelancerList(HasPermissionsMixin, ListView):
    required_permission = 'Admin_Freelancer_List'
    template_name = 'admin/freelancerlist.html'
    model = Users
    paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super(AdminFreelancerList, self).get_context_data(**kwargs)

        context['freelancerlist'] = Users.objects.filter(user_type__type='FREELANCER')

        return context


def logout(request, next_page=None,
           template_name='registration/logged_out.html',
           redirect_field_name='/site_administration',
           current_app=None, extra_context=None):
    """
    Logs out the user and displays 'You are logged out' message.
    """
    print request.user
    if request.user.is_authenticated():

        auth_logout(request)

        if next_page is not None:
            next_page = resolve_url(next_page)

        if (redirect_field_name in request.POST or
                    redirect_field_name in request.GET):
            next_page = request.POST.get(redirect_field_name,
                                         request.GET.get(redirect_field_name))
            # Security check -- don't allow redirection to a different host.
            if not is_safe_url(url=next_page, host=request.get_host()):
                next_page = request.path

        if next_page:
            # Redirect to this page until the session has been cleared.
            return HttpResponseRedirect(next_page)

        current_site = get_current_site(request)
        context = {
            'site': current_site,
            'site_name': current_site.name,
            'title': _('Logged out')
        }
        if extra_context is not None:
            context.update(extra_context)

        if current_app is not None:
            request.current_app = current_app

        return TemplateResponse(request, template_name, context)
    else:
        if request.user.is_superuser and request.user_type == 'Site_admin':
            return redirect('/site_administration/admincreaterolelist')
        else:
            return redirect('/site_administration/admindashboard')


class EditRole(UpdateView):
    template_name = 'admin/editrole.html'
    model = Role_Permission
    form_class = RolePermissionUpdateform
    success_url = '.'
    context_object_name = 'my_model'

    def get_context_data(self, **kwargs):
        context = super(EditRole, self).get_context_data(**kwargs)
        selected_permission = []
        all_permission = Permission.objects.all()
        for permission in Role_Permission.objects.get(role=self.object.role).permission.all():
            # ['list'] = permission
            selected_permission.append(permission)
            all_permission = all_permission.exclude(name=permission.name)
        context['all_permissions'] = all_permission
        context['selected_permissions'] = selected_permission
        return context

    def post(self, request, *args, **kwargs):
        print request.POST

        if self.request.is_ajax():
            selected_permission = []
            if self.request.POST.get('method') =='get':
                all_permission = Permission.objects.all()
                for permission in Role_Permission.objects.get(role=self.request.POST.get('rolename')).permission.all():
                    # ['list'] = permission
                    selected_permission.append(permission)
                    all_permission = all_permission.exclude(name=permission.name)
                all_permissions = all_permission
                selected_permissions = selected_permission

                option_html = render_to_string('admin/ajax/selected_role_options.html',
                                               {'object_list': selected_permissions})
                payload = {'status': 'success', 'data': option_html,'count':len(selected_permission)}
            else:
               form = RolePermissionUpdateform(request.POST, instance=self.get_object())
               if form.is_valid():
                   print form.cleaned_data['permission'],"jjjjjjjjjjjjjjjjjjjjj"
                   form.save()



                   payload = {'status': 'success', 'message': 'Successfully Created'}

               else:
                   print form.errors
                   payload = {'status': 'failed', 'data': form.errors }


            return HttpResponse(json.dumps(payload),
                                content_type='application/json',
                                )

        return super(EditRole, self).post(request, *args, **kwargs)
