# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-07-14 08:58
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('freelancer', '0016_auto_20170714_0856'),
    ]

    operations = [
        migrations.AlterField(
            model_name='employmenthistory',
            name='description',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='employmenthistory',
            name='role',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name='freelancerportfolio',
            name='description',
            field=models.TextField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='freelancerportfolio',
            name='project_url',
            field=models.URLField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='freelancerportfolio',
            name='skills',
            field=models.ManyToManyField(blank=True, null=True, related_name='portfolio_skills', to='freelancer.Skills', verbose_name='Skills'),
        ),
        migrations.AlterField(
            model_name='freelancerportfolio',
            name='sub_category',
            field=models.ManyToManyField(blank=True, null=True, related_name='portfolio_sub_category', to='freelancer.WorkSubCategories', verbose_name='Work Sub Category'),
        ),
        migrations.AlterField(
            model_name='freelancerportfolio',
            name='work_category',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='portfolio_work_categories', to='freelancer.WorkCategories', verbose_name='Work Categories'),
        ),
    ]
