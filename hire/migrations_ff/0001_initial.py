# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-10-17 05:46
from __future__ import unicode_literals

from django.conf import settings
import django.core.validators
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('client', '0043_auto_20171007_1017'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('freelancer', '0019_auto_20171006_1343'),
    ]

    operations = [
        migrations.CreateModel(
            name='HiredJobsContract',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('slug', models.SlugField(auto_created=True, editable=False, max_length=255, unique=True)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('contract_number', models.CharField(default=0, max_length=20, verbose_name='Job Contract Number')),
                ('job_description', models.TextField(validators=[django.core.validators.MinLengthValidator(0)], verbose_name='Hire Job Description')),
                ('amount', models.IntegerField(default=0, verbose_name='Hire Amount')),
                ('company', models.CharField(default='', max_length=255, null=True, verbose_name='Company Name')),
                ('status', models.CharField(choices=[('DRAFT', 'Draft'), ('PENDING', 'Pending'), ('HOLD', 'Hold'), ('APPROVED', 'Approved'), ('CANCEL', 'Cancel')], default='DRAFT', max_length=10, verbose_name='contract status')),
                ('approved_date', models.DateTimeField(blank=True, null=True, verbose_name='Date')),
                ('client_job', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='hire_jobs', to='client.ClientJobs', verbose_name='Client Job')),
                ('client_profile', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='hire_client', to='client.Client_Profile', verbose_name='Hired Client')),
                ('created_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to=settings.AUTH_USER_MODEL)),
                ('freelancer_profile', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='hire_freelancer', to='freelancer.Freelancer_Profile', verbose_name='Hired Freelancer')),
                ('modified_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to=settings.AUTH_USER_MODEL)),
                ('payment_type', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='hire_payment_type', to='client.PaymentType', verbose_name='Payment Type')),
            ],
            options={
                'verbose_name': 'HireJob',
                'verbose_name_plural': 'HireJobs',
            },
        ),
    ]
