from django.contrib import admin

# Register your models here.
from site_administration.models import Permission,  Role_Permission, VerificationJobPost

admin.site.register(Permission)

admin.site.register(Role_Permission)
admin.site.register(VerificationJobPost)
