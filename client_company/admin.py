from django.contrib import admin

# Register your models here.
from client_company.models import Client_Company_Profile, Invited_Client_Company_User

admin.site.register(Client_Company_Profile)
admin.site.register(Invited_Client_Company_User)