import StringIO
import base64
import json

import datetime
from base64 import b64decode

from PIL import Image
from django.contrib.auth.decorators import login_required
from django.core import serializers
from django.core.files.base import ContentFile
from django.http import HttpResponse
from django.shortcuts import render, redirect

# Create your views here.
from django.template.loader import render_to_string
from django.template.response import TemplateResponse
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import ListView
from django.views.generic import TemplateView
from rolepermissions.mixins import HasPermissionsMixin

from accounts.models import User_Type, Users
from client.forms import ApplyJobForm, ApplyJobCancelForm
from client.mixins import ClientMixin
from client.models import ClientJobs, JobInvitation, InvitationStatus, ApplyJob
from freelancer.forms import FreelancerProfileForm, HourlyRateForm, LocationDetailsForm
from freelancer.mixins import MainMixin
from freelancer.models import Skills, WorkCategories, WorkSubCategories, EmploymentHistory, FreelancerPortfolio, \
    Certifications, ExperienceLevel, Freelancer_Profile, HourlyRate, LocationDetails, FreelancerPortfolioimages
from cities_light.models import Country, Region, City

from freelancer_company.models import Freelancer_Company_Profile
from hire.models import HiredJobsContract
from notifications.models import Notifications


class FreelancerProfile(HasPermissionsMixin, MainMixin, TemplateView):
    required_permission = 'View_Dashboard_Freelancer'

    #  @csrf_exempt
    @method_decorator(login_required)
    # template_name = 'freelancer/freelancer-profile.html'

    def get(self, request, *args, **kwargs):

        user = self.check_freelancer_profile(request.user)
        introduceform = FreelancerProfileForm(instance=user)
        userhourly = self.check_hourly_rate(request.user)
        hourlyrateform = HourlyRateForm(instance=userhourly)
        userlocation = self.check_location(request.user)
        locationform = LocationDetailsForm(instance=userlocation)
        template_name = 'freelancer/freelancer-profile.html'
        context = {'introduceform': introduceform, 'hourlyrateform': hourlyrateform, 'locationform': locationform}
        return TemplateResponse(request, template_name, context)

    def post(self, request, *args, **kwargs):
        user = request.user

        response_data = {}





        method = request.POST['method']

        user = self.check_freelancer_profile(request.user)
        introduceform = FreelancerProfileForm(instance=user)
        userhourly = self.check_hourly_rate(request.user)
        hourlyrateform = HourlyRateForm(instance=userhourly)
        userlocation = self.check_location(request.user)
        locationform = LocationDetailsForm(instance=userlocation)

        if method == 'get_skills':

            getskills = Skills.objects.all()
            leads_as_json = serializers.serialize('json', Skills.objects.all())
            return HttpResponse(leads_as_json,
                                content_type='application/json',
                                )
        if method == 'get_skillsoptions':
            if len(request.POST.getlist('subid[]')) > 0:
                subid = self.get_sub_id(request.POST.getlist('subid[]'))
            else:
                subid = [request.POST['subid']]
            print request.META.get('HTTP_AUTHORIZATION'),"nbnbn"

            if request.META.get('HTTP_AUTHORIZATION') == request.user.token:
               leads_as_jsons = serializers.serialize('json', Skills.objects.filter(
                category__in=subid))
               leads_as_json = {'status': 'success', 'message': "Suceess", 'data': leads_as_jsons}

            else:
                leads_as_json = {'status': 'failed', 'message': "Erorr", 'data': ''}

            return HttpResponse(json.dumps(leads_as_json),
                                content_type='application/json',
                                )


        if method == 'get_workcategory':


            if request.META.get('HTTP_AUTHORIZATION') == request.user.token:

                leads_as_jsons = serializers.serialize('json', WorkCategories.objects.all())

                leads_as_json = {'status': 'success', 'message': "Suceess", 'data': leads_as_jsons}
            else:
                leads_as_json = {'status': 'failed', 'message': "Erorr", 'data': ''}


            return HttpResponse(json.dumps(leads_as_json),
                                content_type='application/json',
                                )
        if method == 'get_worksubcategory':
            if request.META.get('HTTP_AUTHORIZATION') == request.user.token:
               leads_as_jsons = serializers.serialize('json', WorkSubCategories.objects.all())
               leads_as_json = {'status': 'success', 'message': "Suceess", 'data': leads_as_jsons}

            else:
                leads_as_json = {'status': 'failed', 'message': "Erorr", 'data': ''}

            return HttpResponse(json.dumps(leads_as_json),
                                content_type='application/json',
                                )
        if method == 'get_worksubcategorys':

            if len(request.POST.getlist('workid[]')) > 0:
                subid = request.POST.getlist('workid[]')
            else:
                subid = [request.POST['workid']]
            print subid, "hjh"
            if request.META.get('HTTP_AUTHORIZATION') == request.user.token:
               leads_as_jsons = serializers.serialize('json', WorkSubCategories.objects.filter(
                work_category__in=subid))
               leads_as_json = {'status': 'success', 'message': "Suceess", 'data': leads_as_jsons}

            else:
                leads_as_json = {'status': 'failed', 'message': "Erorr", 'data': ''}

            return HttpResponse(json.dumps(leads_as_json),
                                content_type='application/json',
                                )


        if method == 'save_hourlyrate':
            user = self.check_hourly_rate(request.user)
            # if user != None:
            #     form = HourlyRateForm(request.POST, instance=user)
            # else:
            #     form = HourlyRateForm(request.POST)
            form = HourlyRateForm(request.POST, instance=user)

            if form.is_valid():
                form.save(self.request.user)

                payload = {'status': 'sucess', 'message': "success", 'data': "sss"}
            else:
                self.message = "Validation failed."
                self.data = form.errors
                self.success = False
                payload = {'status': 'failed', 'message': self.message, 'data': self.data}

            return HttpResponse(json.dumps(payload),
                                content_type='application/json',
                                )
        if method == 'save_loctiondetails':
            user = self.check_location(request.user)
            # if user != None:
            #     form = HourlyRateForm(request.POST, instance=user)
            # else:
            #     form = HourlyRateForm(request.POST)
            form = LocationDetailsForm(request.POST, instance=user)

            if form.is_valid():
                form.save(self.request.user)

                payload = {'status': 'sucess', 'message': "success", 'data': "sss"}
            else:
                self.message = "Validation failed."
                self.data = form.errors
                self.success = False
                payload = {'status': 'failed', 'message': self.message, 'data': self.data}

            return HttpResponse(json.dumps(payload),
                                content_type='application/json',
                                )

        if method == 'freelancer_image_save':
            # print request.FILES['freelancer_image'],"cccccccccccccccccccccccccccccch"
            freelancer_object, freelancer_objects = Freelancer_Profile.objects.get_or_create(user=request.user)

            if freelancer_object:
                print freelancer_object, "ddddddddddd"
                format, imgstr = request.POST['freelancer_image'].split(';base64,')
                ext = format.split('/')[-1]
                date_str = datetime.datetime.utcnow()
                data = ContentFile(base64.b64decode(imgstr), name=str(request.user.first_name) + date_str.strftime(
                    "%Y_%m_%d_%H_%M_%S_%f") + '.' + ext)

                freelancer_object.profile = data
                freelancer_object.save()
            # if freelancer_objects != False :
            #     print freelancer_objects, "ddddddddddds"
            #     freelancer_objects.profile =request.FILES['freelancer_image']
            #     freelancer_objects.save()
            option_html = render_to_string('freelancer/ajax/freelancer_profile_image.html',
                                           {'object': freelancer_object.profile.url,
                                            'csrf_token_value': request.COOKIES['csrftoken'], request: request}
                                           )
            payload = {'status': 'sucess', 'message': "success", 'data': freelancer_object.profile.url,
                       'html': option_html}
            return HttpResponse(json.dumps(payload),
                                content_type='application/json',
                                )

        if method == "change_hourly_rate_freelancer":
            if request.POST['rate']:
                if request.META.get('HTTP_AUTHORIZATION') == request.user.token:
                    actual_rate = (float(request.POST['rate']) - float(request.POST['rate'])*10/100)

                    payload = {'status': 'sucess', 'message': "success", 'data': float(actual_rate)}
                else:
                    payload = {'status': 'failed', 'message': "Erorr", 'data': ''}
                return HttpResponse(json.dumps(payload),
                                    content_type='application/json',
                                    )

        if method == 'save_experiances':

            data = json.loads(request.POST['data'],)
            #print data,"xxxxxxxxxxxx"

            if data:

                experienceLevel, experienceLevels = ExperienceLevel.objects.get_or_create(user=request.user)
                experienceLevel.exp_level_type = request.POST['experiance']
                experienceLevel.save()

                print data['employment'];
                request.user.employment_history.all().delete()
                for employment in data['employment']:

                    if employment['company_name']:

                        getemploymentHistory = EmploymentHistory.objects.create(user=request.user,
                                                                                company_name=employment[
                                                                                    'company_name'],
                                                                                role=employment['role'],
                                                                                description=employment[
                                                                                    'description'])

                        if employment['start_date']:
                            start_dates = datetime.datetime.strptime(str(employment['start_date']), "%m/%d/%Y")
                            getemploymentHistory.start_date = start_dates
                            getemploymentHistory.save()

                        if employment['end_date']:
                            end_dates = datetime.datetime.strptime(str(employment['end_date']), "%m/%d/%Y")

                            getemploymentHistory.end_date = end_dates
                            getemploymentHistory.save()

                FreelancerPortfolioimages.objects.filter(portfolio_id__user=request.user).delete()

                request.user.portfolio.all().delete()

                for portfolio in data['portfolio']:
                    #print "dffdf",portfolio['upload']

                    if portfolio['project_name']:

                        freelancer_object = FreelancerPortfolio.objects.create(user=request.user,
                                                                               project_name=portfolio['project_name'],
                                                                               description=portfolio['description']
                                                                               )
                        if portfolio['work_category']:
                            freelancer_object.work_category = WorkCategories.objects.get(
                                id=portfolio['work_category'])
                            freelancer_object.save()
                        if portfolio['project_url'] != '' or portfolio['project_url'] != '0':
                            freelancer_object.project_url = portfolio['project_url']
                            freelancer_object.save()
                        if portfolio['skills']:
                            for skill in Skills.objects.filter(pk__in=portfolio['skills']):
                                freelancer_object.skills.add(skill)
                        if portfolio['sub_category']:
                            for sub_category in WorkSubCategories.objects.filter(
                                    pk__in=portfolio['sub_category']):
                                freelancer_object.sub_category.add(sub_category)
                        # if portfolio['filess']:
                        #     print"d"
                        #     portfolio_idss = FreelancerPortfolio.objects.get(id=freelancer_object.id)
                        #     print portfolio_idss,"jjjjjjj"
                        #     portfolio_id =FreelancerPortfolioimages()
                        #     portfolio_id.portfolio_id=portfolio_idss
                        #     portfolio_id.portfolio_image.add(portfolio['filess'])
                        #     portfolio_id.save()
                        freelancer_object,freelancer_objects = FreelancerPortfolio.objects.get_or_create(user=request.user,
                                                                               project_name=portfolio['project_name'],
                                                                               )

                        if portfolio['upload']:
                            for image in portfolio['upload']:
                                upload =FreelancerPortfolioimages()
                                format, imgstr = image.split(';base64,')
                                ext = format.split('/')[-1]
                                date_str = datetime.datetime.utcnow()
                                datas = ContentFile(base64.b64decode(imgstr),
                                                   name=str(request.user.first_name) + date_str.strftime(
                                                       "%Y_%m_%d_%H_%M_%S_%f") + '.' + ext)
                                upload.portfolio_image = datas
                                upload.save()
                                upload.portfolio_id.add(freelancer_object)



                request.user.cerifications.all().delete()
                for certification in data['certification']:
                    if certification['certificate_name']:

                        getcertification = Certifications.objects.create(user=request.user,
                                                                         certificate_name=certification[
                                                                             'certificate_name'],
                                                                         issued_by=certification['issuedby']
                                                                         )
                        if certification['start_date'] != '':
                            start = datetime.datetime.strptime(str(certification['start_date']), "%m/%d/%Y")
                            getcertification.valid_from = start
                            getcertification.save()

                        if certification['end_date'] != '':
                            end_date = datetime.datetime.strptime(str(certification['end_date']), "%m/%d/%Y")
                            getcertification.end_to = end_date
                            getcertification.save()

            payload = {'status': 'sucess', 'message': "success", 'data': "sss"}
            return HttpResponse(json.dumps(payload),
                                content_type='application/json',
                                )
        if method == "save_frelancer_hourly":
            if request.META.get('HTTP_AUTHORIZATION') == request.user.token:

                data = json.loads(request.POST['data'])

                if data:

                    for hourly in data['hourly']:

                        if hourly['hourly_rate'] != '':

                            hourly_object, hourly_objects = HourlyRate.objects.get_or_create(user=request.user)

                            if hourly_object and hourly['hourly_rate']:
                                hourly_object.hourly_rate = hourly['hourly_rate']
                                hourly_object.actual_rate = (float(hourly['hourly_rate']) - float(hourly['hourly_rate'])*10/100)
                                hourly_object.save()

                    for location in data['location']:
                        # if location['country'] != '0' or  location['state'] != '0' or  location['city'] != '0' or location['zip'] != '' or location['address'] != '':
                        location_object, location_objects = LocationDetails.objects.get_or_create(
                            user=request.user)
                        if location_object:
                            print location['country'], "dddddddddddddddddd"
                            if location['country'] != '0' and location['country'] != '':
                                location_object.country = Country.objects.get(id=location['country'])
                            else:
                                location_object.country = None

                            if location['state'] != '0' and location['state'] != '':
                                location_object.state = Region.objects.get(id=location['state'])
                            else:
                                location_object.state = None
                            if location['city'] != '0' and location['city'] != '':
                                location_object.city = City.objects.get(id=location['city'])
                            else:
                                location_object.city = None

                            location_object.zip_code = location['zip']
                            location_object.address = location['address']
                            location_object.save()
                    hourly_html = render_to_string('freelancer/ajax/freelancer_hourlyrate.html', {'request': self.request})
                    profile_html = render_to_string('freelancer/ajax/freelancer_profile_details.html',
                                                    {'request': self.request})
                    payload = {'status': 'sucess', 'message': "success", 'html': hourly_html, 'profile_html': profile_html}
                    return HttpResponse(json.dumps(payload),
                                        content_type='application/json',
                                        )

            else:
               payload = {'status': 'failed', 'message': "Erorr", 'data': ''}
               return HttpResponse(json.dumps(payload),
                                content_type='application/json',
                                )
        # if method == 'Save_freelancer_profile':
        #     user = self.check_freelancer_profile(request.user)
        #     form = FreelancerProfileForm(request.POST, instance=user)
        #     payload=''
        #     if form.is_valid():
        #         print "succes"
        #         if user != None:
        #             self.delete_existing_skill(request.user)
        #             self.delete_existing_subcategory(request.user)
        #         form.save(self.request.user, self.get_selected_skill(request.POST.getlist('skills')),
        #                   self.get_selected_subcategory(request.POST.getlist('subcategory')))
        #         payload = {'status': 'success',
        #                    'message': 'Successfully Updated',
        #                    'data': 'g'}
        #         return HttpResponse(json.dumps(payload), content_type='application/json', )

        if method == 'Save_freelancer_profile':
            user = self.check_freelancer_profile(request.user)
            form = FreelancerProfileForm(request.POST, instance=user)
            payload = ''
            if form.is_valid():
                print request.META.get('HTTP_AUTHORIZATION'),"succes"
                if request.META.get('HTTP_AUTHORIZATION') == request.user.token:
                    if user != None:
                        self.delete_existing_skill(request.user)
                        self.delete_existing_subcategory(request.user)
                    form.save(self.request.user, self.get_selected_skill(request.POST.getlist('skills')),
                              self.get_selected_subcategory(request.POST.getlist('subcategory')))
                    user_html = render_to_string('freelancer/ajax/freelancer_profile_details.html',
                                                 {'request': self.request})

                    payload = {'status': 'success',
                               'message': 'Successfully Updated',
                               'data': 'g', 'html': user_html}

                    return HttpResponse(json.dumps(payload), content_type='application/json', )
                else:
                    payload = {'status': 'failed', 'message': "Erorr", 'data': ''}
                    return HttpResponse(json.dumps(payload),
                                        content_type='application/json',
                                        )
            else:
                # print "not valid profile"
                # print form.errors
                self.message = "Validation failed."
                self.data = form.errors
                self.success = False
                payload = {'status': 'failed', 'message': self.message, 'data': self.data}
                return HttpResponse(json.dumps(payload), content_type='application/json', )
        if method == 'save':
            user = self.check_freelancer_profile(request.user)
            form = FreelancerProfileForm(request.POST, instance=user)

            if form.is_valid():

                # skills_objs = Skills.objects.filter(pk__in=request.POST.getlist('skills'))
                # print "valid profile",skills_objs
                if user != None:
                    self.delete_existing_skill(request.user)
                    self.delete_existing_subcategory(request.user)
                form.save(self.request.user, self.get_selected_skill(request.POST.getlist('skills')),
                          self.get_selected_subcategory(request.POST.getlist('subcategory')))
            else:
                print "not valid profile"
                print form.errors

        context = {'introduceform': introduceform, 'hourlyrateform': hourlyrateform, 'locationform': locationform}

        template_name = 'freelancer/freelancer-profile.html'

        return TemplateResponse(request, template_name, context)


class FreelancerProfileView(MainMixin, TemplateView):
    template_name = 'freelancer/freelancer_profile_view.html'

    def post(self, request, *args, **kwargs):
        method = request.POST['method']
        if method == 'Save_freelancer_portfolio':
            if request.META.get('HTTP_AUTHORIZATION') == request.user.token:
                if request.POST['project_name'] != '':
                    freelancer_object = FreelancerPortfolio.objects.create(user=request.user,
                                                                           project_name=request.POST['project_name'],
                                                                           description=request.POST['description'],
                                                                           project_url=request.POST['project_url']

                                                                           )
                    if request.POST['work_category']:
                        freelancer_object.work_category = WorkCategories.objects.get(
                            id=request.POST['work_category'])
                        freelancer_object.save()

                    for skill in Skills.objects.filter(pk__in=request.POST.getlist('skills')):
                        freelancer_object.skills.add(skill)
                    for sub_category in WorkSubCategories.objects.filter(
                            pk__in=request.POST.getlist('sub_category')):
                        freelancer_object.sub_category.add(sub_category)
                    user_html = render_to_string('freelancer/ajax/freelancer_portfolio_list.html',
                                                 {'request': self.request})
                    payload = {'status': 'success',
                               'message': 'Successfully Updated',
                               'count': request.user.portfolio.count(), 'html': user_html}

                else:
                    payload = {'status': 'error',
                               'message': 'Successfully Updated',
                              'data': 'g'}
            else:
                payload = {'status': 'failed', 'message': "Erorr", 'data': ''}


            return HttpResponse(json.dumps(payload), content_type='application/json', )
        if method == 'Save_freelancer_employment':
            if request.META.get('HTTP_AUTHORIZATION') == request.user.token:
                if request.POST['company_name'] != '':
                    getemploymentHistory = EmploymentHistory.objects.create(user=request.user,
                                                                            company_name=request.POST
                                                                            ['company_name'],
                                                                            role=request.POST['role'],
                                                                            description=request.POST[
                                                                                'description'])

                    if request.POST['start_date'] != '':
                        start_dates = datetime.datetime.strptime(str(request.POST['start_date']), "%m/%d/%Y")
                        getemploymentHistory.start_date = start_dates
                        getemploymentHistory.save()

                    if request.POST['end_date']:
                        end_dates = datetime.datetime.strptime(str(request.POST['end_date']), "%m/%d/%Y")

                        getemploymentHistory.end_date = end_dates
                        getemploymentHistory.save()

                    user_html = render_to_string('freelancer/ajax/freelancer_employment_list.html',
                                                 {'request': self.request})
                    payload = {'status': 'success',
                               'message': 'Successfully Updated',
                               'count': request.user.employment_history.count(), 'html': user_html}
                else:
                    payload = {'status': 'error',
                               'message': 'Successfully Updated',
                               'data': 'g'}
            else:
                payload = {'status': 'failed', 'message': "Erorr", 'data': ''}
            return HttpResponse(json.dumps(payload), content_type='application/json', )
        if method == 'DeletedDetails':
            if request.META.get('HTTP_AUTHORIZATION') == request.user.token:
                if request.POST['datatitle'] == 'cerifications':
                    instance = Certifications.objects.get(id=request.POST['dataid'])
                    instance.delete()

                    user_html = render_to_string('freelancer/ajax/freelancer_certifications_list.html',
                                                 {'request': self.request})
                    payload = {'status': 'success',
                               'message': 'Successfully Updated',
                               'count': request.user.cerifications.count(), 'html': user_html}
                    return HttpResponse(json.dumps(payload), content_type='application/json', )

                if request.POST['datatitle'] == 'portfolio':
                    instance = FreelancerPortfolio.objects.get(id=request.POST['dataid'])
                    instance.delete()
                    user_html = render_to_string('freelancer/ajax/freelancer_portfolio_list.html',
                                                 {'request': self.request})
                    payload = {'status': 'success',
                               'message': 'Successfully Updated',
                               'count': request.user.portfolio.count(), 'html': user_html}

                    return HttpResponse(json.dumps(payload), content_type='application/json', )

                if request.POST['datatitle'] == 'employment':
                    instance = EmploymentHistory.objects.get(id=request.POST['dataid'])
                    instance.delete()
                    user_html = render_to_string('freelancer/ajax/freelancer_employment_list.html',
                                                 {'request': self.request})
                    payload = {'status': 'success',
                               'message': 'Successfully Updated',
                               'count': request.user.employment_history.count(), 'html': user_html}

                    return HttpResponse(json.dumps(payload), content_type='application/json', )
                if request.POST['datatitle'] == 'portfolioimage':
                    instance = FreelancerPortfolioimages.objects.get(id=request.POST['dataid'])
                    instance.portfolio_image.delete()

                    instance.delete()

                    html = render_to_string('freelancer/ajax/freelancer_portfolio_list.html',
                                                 {'request': self.request})
                    payload = {'status': 'success',
                               'message': 'Successfully Updated',
                               'count': request.user.portfolio.count(), 'html': html}

                    return HttpResponse(json.dumps(payload), content_type='application/json', )

            else:
                payload = {'status': 'failed', 'message': "Erorr", 'data': ''}
                return HttpResponse(json.dumps(payload), content_type='application/json', )

        if method == 'Save_freelancer_certification':
            if request.META.get('HTTP_AUTHORIZATION') == request.user.token:

                if request.POST['certificate_name'] != '':
                    getcertification = Certifications.objects.create(user=request.user,
                                                                     certificate_name=request.POST['certificate_name'],
                                                                     issued_by=request.POST['issuedby']
                                                                     )
                    if request.POST['start_date'] != '':
                        start = datetime.datetime.strptime(str(request.POST['start_date']), "%m/%d/%Y")
                        getcertification.valid_from = start
                        getcertification.save()

                    if request.POST['end_date'] != '':
                        end_date = datetime.datetime.strptime(str(request.POST['end_date']), "%m/%d/%Y")
                        getcertification.end_to = end_date
                        getcertification.save()
                    user_html = render_to_string('freelancer/ajax/freelancer_certifications_list.html',
                                                 {'request': self.request})
                    payload = {'status': 'success',
                               'message': 'Successfully Updated',
                               'count': request.user.cerifications.count(), 'html': user_html}
                else:
                    payload = {'status': 'error',
                               'message': 'Successfully Updated',
                               'data': 'g'}
            else:
                payload = {'status': 'failed', 'message': "Erorr", 'data': ''}

            return HttpResponse(json.dumps(payload), content_type='application/json', )
        if method == 'EditDetails':
            payload = {'status': 'success', 'data': ''}
            if request.META.get('HTTP_AUTHORIZATION') == request.user.token:
                if request.POST['datatitle'] == 'cerifications':
                    option_html = render_to_string('freelancer/ajax/edit_freelencer_certifications.html',
                                                   {'object': Certifications.objects.get(id=request.POST['id']),
                                                    'csrf_token_value': request.COOKIES['csrftoken'], request: request}
                                                   )
                    payload = {'status': 'success', 'html': option_html}

                    return HttpResponse(json.dumps(payload), content_type='application/json', )

                if request.POST['datatitle'] == 'portfolio':
                    option_html = render_to_string('freelancer/ajax/edit_freelencer_portfolio.html',
                                                   {'object': FreelancerPortfolio.objects.get(id=request.POST['id']),
                                                    'csrf_token_value': request.COOKIES['csrftoken'], request: request}
                                                   )
                    payload = {'status': 'success', 'html': option_html}

                    return HttpResponse(json.dumps(payload), content_type='application/json', )

                if request.POST['datatitle'] == 'employment':
                    option_html = render_to_string('freelancer/ajax/edit_freelencer_employment.html',
                                                   {'object': EmploymentHistory.objects.get(id=request.POST['id']),
                                                    'csrf_token_value': request.COOKIES['csrftoken'], request: request}
                                                   )
                    payload = {'status': 'success', 'html': option_html}

                    return HttpResponse(json.dumps(payload), content_type='application/json', )
            else:
                payload = {'status': 'failed', 'message': "Erorr", 'data': ''}

                return HttpResponse(json.dumps(payload), content_type='application/json', )

        if method == 'Update_freelancer_portfolio':
            if request.META.get('HTTP_AUTHORIZATION') == request.user.token:

                portfolio_object, portfolio_objects = FreelancerPortfolio.objects.get_or_create(
                    user=request.user, id=request.POST['id'])
                if portfolio_object:
                    if request.POST['project_name'] != '':
                        portfolio_object.project_name = request.POST['project_name']
                        portfolio_object.description = request.POST['description']
                        portfolio_object.project_url = request.POST['project_url']

                        portfolio_object.save()
                    if request.POST['work_category']:
                        portfolio_object.work_category = WorkCategories.objects.get(
                            id=request.POST['work_category'])
                        portfolio_object.save()

                    if request.POST.getlist('skills') != '':

                        for skill in Skills.objects.filter(pk__in=request.POST.getlist('skills')):
                            portfolio_object.skills.add(skill)
                    if request.POST.getlist('sub_category') != '':
                        for sub_category in WorkSubCategories.objects.filter(
                                pk__in=request.POST.getlist('sub_category')):
                            portfolio_object.sub_category.add(sub_category)
                    user_html = render_to_string('freelancer/ajax/freelancer_portfolio_list.html',
                                                 {'request': self.request})
                    payload = {'status': 'success',
                               'message': 'Successfully Updated',
                               'count': request.user.portfolio.count(), 'html': user_html}

                    return HttpResponse(json.dumps(payload), content_type='application/json', )
            else:
                payload = {'status': 'failed', 'message': "Erorr", 'data': ''}

                return HttpResponse(json.dumps(payload), content_type='application/json', )

        if method == 'Update_freelancer_employment':
            if request.META.get('HTTP_AUTHORIZATION') == request.user.token:
                employment_object, employment_objects = EmploymentHistory.objects.get_or_create(
                    user=request.user, id=request.POST['id'])
                if employment_object:
                    if request.POST['company_name'] != '':
                        employment_object.company_name = request.POST['company_name']
                        employment_object.role = request.POST['role']
                        employment_object.description = request.POST['description']
                        employment_object.save()
                    print request.POST['start_date'],"jj"
                    if request.POST['start_date'] != '':
                        start = datetime.datetime.strptime(str(request.POST['start_date']), "%m/%d/%Y")
                        employment_object.start_date = start
                        employment_object.save()

                    if request.POST['end_date'] != '':
                        end_date = datetime.datetime.strptime(str(request.POST['end_date']), "%m/%d/%Y")
                        employment_object.end_date = end_date
                        employment_object.save()

                    user_html = render_to_string('freelancer/ajax/freelancer_employment_list.html',
                                                 {'request': self.request})
                    payload = {'status': 'success',
                               'message': 'Successfully Updated',
                               'count': request.user.employment_history.count(), 'html': user_html}
                    return HttpResponse(json.dumps(payload), content_type='application/json', )
            else:
                payload = {'status': 'failed', 'message': "Erorr", 'data': ''}

                return HttpResponse(json.dumps(payload), content_type='application/json', )

        if method == 'Update_freelancer_certification':
            if request.META.get('HTTP_AUTHORIZATION') == request.user.token:
                certification_object, certification_objects = Certifications.objects.get_or_create(
                    user=request.user, id=request.POST['id'])
                if certification_object:
                    if request.POST['certificate_name'] != '':
                        certification_object.certificate_name = request.POST['certificate_name']
                        certification_object.issued_by = request.POST['issuedby']
                        certification_object.save()
                    if request.POST['start_date'] != '':
                        start = datetime.datetime.strptime(str(request.POST['start_date']), "%m/%d/%Y")
                        certification_object.valid_from = start
                        certification_object.save()

                    if request.POST['end_date'] != '':
                        end_date = datetime.datetime.strptime(str(request.POST['end_date']), "%m/%d/%Y")
                        certification_object.end_to = end_date
                        certification_object.save()
                    user_html = render_to_string('freelancer/ajax/freelancer_certifications_list.html',
                                                 {'request': self.request})
                    payload = {'status': 'success',
                               'message': 'Successfully Updated',
                               'count': request.user.cerifications.count(), 'html': user_html}
                    return HttpResponse(json.dumps(payload), content_type='application/json', )
            else:
                payload = {'status': 'failed', 'message': "Erorr", 'data': ''}
                return HttpResponse(json.dumps(payload), content_type='application/json', )
        if method == 'Cancel_image':
            if request.POST['type'] == 'profile':
                try:
                    freelancer_object, freelancer_objects = Freelancer_Profile.objects.get_or_create(user=request.user)
                    if freelancer_object.profile:
                        pic = freelancer_object.profile.url
                    else:
                        pic = ""
                except:
                    pic = ""

                option_html = render_to_string('freelancer/ajax/freelancer_profile_image.html',
                                               {'object': pic,
                                                'csrf_token_value': request.COOKIES['csrftoken'], request: request}
                                               )
                payload = {'status': 'sucess', 'message': "success",
                           'html': option_html}
                return HttpResponse(json.dumps(payload), content_type='application/json', )
            else:
                try:
                    freelancer_object = Freelancer_Company_Profile.objects.get(user=request.user)
                    if freelancer_object.logo:
                        pic = freelancer_object.logo.url
                    else:
                        pic = ""
                except:
                    pic = ""
                option_html = render_to_string('freelancer/ajax/freelancer_company_logo.html',
                                               {'object': pic,
                                                'csrf_token_value': request.COOKIES['csrftoken'], request: request}
                                               )
                payload = {'status': 'sucess', 'message': "success",
                           'html': option_html}
                return HttpResponse(json.dumps(payload), content_type='application/json', )
        if method == 'Delete_image':
            if request.POST['type'] == 'profile':
                freelancer_object = Freelancer_Profile.objects.get(user=request.user)
                freelancer_object.profile.delete()
                # freelancer_object.delete()
                option_html = render_to_string('freelancer/ajax/freelancer_profile_image.html',
                                               {'object': '',
                                                'csrf_token_value': request.COOKIES['csrftoken'], request: request}
                                               )
                payload = {'status': 'sucess', 'message': "success",
                           'html': option_html}
                return HttpResponse(json.dumps(payload), content_type='application/json', )
            if request.POST['type'] == 'logo':
                freelancer_object = Freelancer_Company_Profile.objects.get(user=request.user)
                freelancer_object.logo.delete()
                # freelancer_object.delete()
                option_html = render_to_string('freelancer/ajax/freelancer_company_logo.html',
                                               {'object': '',
                                                'csrf_token_value': request.COOKIES['csrftoken'], request: request}
                                               )
                payload = {'status': 'sucess', 'message': "success",
                           'html': option_html}
                return HttpResponse(json.dumps(payload), content_type='application/json', )
        if method == 'Get_freelancer_hourlypopup':
            if request.META.get('HTTP_AUTHORIZATION') == request.user.token:
                user_html = render_to_string('freelancer/ajax/get_freelancer_hourlypopup.html',
                                             {'request': self.request})
                payload = {'status': 'success',
                           'message': 'Successfully Updated',
                           'html': user_html}
                return HttpResponse(json.dumps(payload), content_type='application/json', )
            else:
                payload = {'status': 'failed', 'message': "Erorr", 'data': ''}
                return HttpResponse(json.dumps(payload), content_type='application/json', )
        if method == 'Get_freelancer_profile_popup':
            if request.META.get('HTTP_AUTHORIZATION') == request.user.token:
                user_html = render_to_string('freelancer/ajax/get_freelancer_profile_popup.html',
                                             {'request': self.request, 'csrf_token_value': request.COOKIES['csrftoken']})
                payload = {'status': 'success',
                           'message': 'Successfully Updated',
                           'html': user_html}
                return HttpResponse(json.dumps(payload), content_type='application/json', )
            else:
                payload = {'status': 'failed', 'message': "Erorr", 'data': ''}
                return HttpResponse(json.dumps(payload), content_type='application/json', )
        if method == 'Update_portfolio_image':
            print json.loads(request.POST.get('existing_files')) ,"jjkjk"

            thumbnail_list = []
            if request.POST.get('existing_files'):
                for d in json.loads(request.POST.get('existing_files')):
                    thumbnail_list.append(d)
            if request.FILES:


                for files in request.FILES:

                    portfolio_ids = FreelancerPortfolioimages()

                    portfolio_ids.portfolio_image=request.FILES[files]

                    portfolio_ids.save()
                    portfolio_ids.portfolio_id.add(self.get_freelancer_protfolio(request.POST['portfolio_id']))
                    portfolio_ids.save()

                    thumbnail_list.append(portfolio_ids.id)
                    # portfolioimages =FreelancerPortfolioimages.objects.create(portfolio_id=self.get_freelancer_protfolio(request.POST['portfolio_id']))
                    # portfolioimages.portfolio_image =request.FILES[files]
                    # portfolioimages.save()
            if  request.POST.get('existing_files'):


                allimages = self.get_all_portfolioimages(request.POST['portfolio_id'])

                #for i in request.POST.getlist('existing_files[]'):
                #print allimages.exclude(id__in=[request.POST.get('existing_files')])
                # thumbnail_list = []
                # for d in json.loads(request.POST.get('existing_files')):
                #     thumbnail_list.append(d)

                allimagess=allimages.exclude(id__in=thumbnail_list)
                #allimages.portfolio_image.delete()

                allimagess.delete()




                # if request.POST['existing_files'] !=[]:
                #   allimages.exclude(id__in=request.POST['existing_files'])
                # for images in allimages:
                #     if images.id not in request.POST['existing_files']:
                #         portfolioimages = self.get_portfolioimages(images.id)
                #         portfolioimages.portfolio_image.delete()
                #         portfolioimages.delete()





               # allimages.delete()
                # for images in allimages:
                #     print images.id,"dfdfdf"
                #     if images.id not in request.POST['existing_files']:
                #         portfolioimages = self.get_portfolioimages(images.id)
                #         portfolioimages.portfolio_image.delete()

            message = "Please login as freelancer to apply job"
            html = render_to_string('freelancer/ajax/freelancer_portfolio_image_list.html',{'portfoilio':allimages,'id':request.POST['portfolio_id']})

            payload = {'status': 'success',
                       'message': message,'html':html}
            return HttpResponse(json.dumps(payload), content_type='application/json', )





class FindJobs(ClientMixin, TemplateView):
    template_name = 'client/my_joblist.html'

    def get(self, request):
        # all_jobs = self.get_my_jobs()

        template_name = 'client/my_joblist.html'
        workcategory = self.get_all_WorkCategory()
        subcategory = self.get_all_SubWorkCategory()
        countries = self.get_all_Country()
        skills = self.get_all_Skills()
        # user = request.user.email
        freelancerlist = self.get_all_Freelancerlist()

        context = {'workcategory': workcategory, 'key_words': self.get_freelancer_search_query_list(),
                   'subcategory': subcategory, 'countries': countries, 'skills': skills,
                   'freelancerlist': freelancerlist, 'type': 'Freelancer'}
        return TemplateResponse(request, template_name, context)


class JobDetailsView(ClientMixin, TemplateView):
    template_name = 'freelancer/freelancer_job_detailview.html'

    def get_context_data(self, **kwargs):
        context = super(JobDetailsView, self).get_context_data(**kwargs)

        context['jobdetails'] = self.get_current_job(kwargs.get('slug'))
        context['job_cancel'] = self.if_job_applied_cancel(self.request.user, context['jobdetails'])

        context['apply_jobdetails'] = self.if_job_applied(self.is_freelancer_profile(self.request.user), context['jobdetails'])

        return context

    def post(self, request, *args, **kwargs):
        method = request.POST['method']
        job = self.get_current_job(kwargs.get('slug'))
        is_user = self.if_job_applied(self.is_freelancer_profile(self.request.user), job)
        is_job_posted = self.if_job_posted(self.request.user, job.id)
        is_job_invited = self.is_job_invited(self.is_freelancer_profile(self.request.user), job.id)
        try:
            if is_job_invited.status !=None:
                status = is_job_invited.status.status
            else:
                status =None
        except:
            status =''
        print "c", is_job_invited

        if method == 'ApplyJob':

            form = ApplyJobForm(request.POST, request.FILES)
            freelancer =self.is_freelancer_profile(request.user)
            if request.user.is_authenticated():

                if request.user.active_account.type == "FREELANCER":
                    if is_job_posted == '':

                        if is_job_invited == '' or status != None:
                            if  is_job_invited == ''  or status != 'Declined':
                                if form.is_valid():
                                    if is_user == '':
                                        if job.payment_type.type == 'per hour':
                                            #print "per hour"
                                            if (job.hourly_max != None and job.hourly_min != None) and (
                                                        job.hourly_max != 0 and job.hourly_min != '') and (
                                                    job.hourly_max != '' and job.hourly_min != ''):
                                                #print "fd"
                                                if freelancer.user.freelancer_hourly_user.get().hourly_rate != '' and freelancer.user.freelancer_hourly_user.get().hourly_rate != None:
                                                    current_month = datetime.datetime.now()
                                                    if self.get_no_applied_job(freelancer, current_month) < 101:
                                                        form.save(freelancer, job)
                                                        html = render_to_string(
                                                            'freelancer/ajax/get_updated_quote.html',
                                                            {'apply_jobdetails': self.if_job_applied(
                                                                self.is_freelancer_profile(request.user), job),
                                                             'csrf_token_value': request.COOKIES['csrftoken']})
                                                        payload_message = {'created_by': request.user.id,
                                                                           'type': 'CLIENT',
                                                                           'user_id': job.user.id,
                                                                           'title': " has Quote Applied a  job -" + job.job_title,
                                                                           'description': "job title is " + job.job_title,
                                                                           "url": reverse('jobdetails',
                                                                                          kwargs={'slug': job.slug})}

                                                        message = "You are successfully applied"
                                                        payload = {'status': 'success',
                                                                   'message': message, 'type': '1', 'html': html,
                                                                   'fid': job.user.id, 'data': job.job_title,
                                                                   'note': payload_message}
                                                        return HttpResponse(json.dumps(payload),
                                                                            content_type='application/json', )
                                                    else:
                                                        message = "Limit exceed this month"
                                                        payload = {'status': 'failed', 'type': 'exist',
                                                                   'message': message}
                                                        return HttpResponse(json.dumps(payload),
                                                                            content_type='application/json', )

                                                else:
                                                    message = "You haven't updated Hourly Rate"
                                                    payload = {'status': 'failed', 'type': 'exist',
                                                               'message': message}
                                                    return HttpResponse(json.dumps(payload),
                                                                        content_type='application/json', )
                                            else:

                                                message = "Hourly Rate not available for this job"
                                                payload = {'status': 'failed', 'type': 'exist',
                                                           'message': message}
                                                return HttpResponse(json.dumps(payload),
                                                                    content_type='application/json', )

                                        else:
                                            #print "per job"
                                            if (job.budget != None and job.budget != 0) :
                                                current_month = datetime.datetime.now()
                                                if self.get_no_applied_job(freelancer, current_month) < 101:
                                                    form.save(freelancer, job)
                                                    html = render_to_string(
                                                        'freelancer/ajax/get_updated_quote.html',
                                                        {'apply_jobdetails': self.if_job_applied(
                                                            self.is_freelancer_profile(request.user), job),
                                                            'csrf_token_value': request.COOKIES['csrftoken']})
                                                    payload_message = {'created_by': request.user.id,
                                                                       'type': 'CLIENT',
                                                                       'user_id': job.user.id,
                                                                       'title': " has Quote Applied a  job -" + job.job_title,
                                                                       'description': "job title is " + job.job_title,
                                                                       "url": reverse('jobdetails',
                                                                                      kwargs={'slug': job.slug})}

                                                    message = "You are successfully applied"
                                                    payload = {'status': 'success',
                                                               'message': message, 'type': '1', 'html': html,
                                                               'fid': job.user.id, 'data': job.job_title,
                                                               'note': payload_message}
                                                    return HttpResponse(json.dumps(payload),
                                                                        content_type='application/json', )
                                                else:
                                                    message = "Limit exceed this month"
                                                    payload = {'status': 'failed', 'type': 'exist',
                                                               'message': message}
                                                    return HttpResponse(json.dumps(payload),
                                                                        content_type='application/json', )

                                            else:
                                                message = "Budget not available for this job"
                                                payload = {'status': 'failed', 'type': 'exist',
                                                           'message': message}
                                                return HttpResponse(json.dumps(payload),
                                                                    content_type='application/json', )

                                    else:
                                        message = "You are already applied for this Job"
                                        payload = {'status': 'failed', 'type': 'exist',
                                                   'message': message}
                                        return HttpResponse(json.dumps(payload), content_type='application/json', )
                                else:

                                    payload = {'status': 'failed', 'type': 'Validations',
                                               'data': form.errors}
                                    return HttpResponse(json.dumps(payload), content_type='application/json', )
                            else:
                                message = "You are not allowed to  apply for this Job (declined invitation)"
                                payload = {'status': 'failed', 'type': 'exist',
                                           'message': message}
                                return HttpResponse(json.dumps(payload), content_type='application/json', )
                        else:
                            message = "You are not allowed to  apply for this Job (declined invitation)"
                            payload = {'status': 'failed', 'type': 'exist',
                                       'message': message}
                            return HttpResponse(json.dumps(payload), content_type='application/json', )




                    else:
                        message = "You are not allowed to  apply for this Job"
                        payload = {'status': 'failed', 'type': 'exist',
                                   'message': message}
                        return HttpResponse(json.dumps(payload), content_type='application/json', )

                else:
                    message = "Please login as freelancer to apply job"
                    payload = {'status': 'success',
                               'message': message}
                    return HttpResponse(json.dumps(payload), content_type='application/json', )

            else:
                return redirect('/accounts/login/')
        if method == 'UpdateQuote':
            applyjob = self.get_Applyjob(request.POST['applyid'])
            form = ApplyJobForm(request.POST, request.FILES, instance=applyjob)

            if request.user.is_authenticated():
                if request.user.active_account.type == "FREELANCER":
                    if form.is_valid():
                        hire_details = self.get_all_hire(job.id,request.user.id)
                        if hire_details.count() == 0:

                           form.save(self.is_freelancer_profile(request.user), job)
                           message = "Updated successfully"
                           payload = {'status': 'success',
                                      'message': message, 'type': '0'}
                           Notifications.objects.get_or_create(user=hire_details.job.user,
                                                               title='has quoate updated a  job ' + hire_details.job.job_title,
                                                               created_by=Users.objects.get(id=request.user.id),
                                                               status='0',
                                                               url=reverse('jobdetails', kwargs={'slug': hire_details.job.slug}),
                                                               type=User_Type.objects.get(type='CLIENT'))

                        else:
                            message = "Couldn't Update"
                            payload = {'status': 'failed',
                                       'message': message, 'type': '0'}



                        return HttpResponse(json.dumps(payload), content_type='application/json', )

                    else:
                        payload = {'status': 'failed', 'type': 'Validations',
                                   'data': form.errors}
                        return HttpResponse(json.dumps(payload), content_type='application/json', )
                else:
                    message = "Please login as freelancer to apply job"
                    payload = {'status': 'success',
                               'message': message}
                    return HttpResponse(json.dumps(payload), content_type='application/json', )

            else:
                return redirect('/accounts/login/')

        if method == 'CancelQuote':
            form = ApplyJobCancelForm(request.POST)
            if request.user.is_authenticated():
                if request.user.active_account.type == "FREELANCER":
                    if form.is_valid():
                        hire = HiredJobsContract.objects.get(client_job=job)
                        print hire.status.status,"fddfdf"
                        if hire.status.status != 'Approved':
                            form.save(request.user, job)
                            self.update_status_cancel(self.is_freelancer_profile(request.user), job,'Cancelled')
                            html = render_to_string('freelancer/ajax/get_withdrawn_msg.html',
                                                    {'job_cancel': self.if_job_applied_cancel(request.user, job),
                                                     'csrf_token_value': request.COOKIES['csrftoken']})

                            message = "You are Cancelled your application"
                            payload = {'status': 'success',
                                       'message': message, 'type': '0', 'html': html}
                            return HttpResponse(json.dumps(payload), content_type='application/json', )
                        else:
                            payload = {'status': 'failed', 'type': '0',
                                       'message': "Contract approved"}
                            return HttpResponse(json.dumps(payload), content_type='application/json', )
                    else:
                        payload = {'status': 'failed', 'type': 'Validations',
                                   'data': form.errors}
                        return HttpResponse(json.dumps(payload), content_type='application/json', )
                else:
                    message = "Please login as freelancer to apply job"
                    payload = {'status': 'success',
                               'message': message}
                    return HttpResponse(json.dumps(payload), content_type='application/json', )

            else:
                return redirect('/accounts/login/')


class FreelancerJobList(MainMixin,ClientMixin, TemplateView):

    def get(self, request):
        # all_jobs = self.get_my_jobs()

        template_name = 'freelancer/freelancer_job_list.html'
        if request.user.is_authenticated():
            extended_template = 'inner_page_base.html'
        else:
            extended_template = 'base.html'

        workcategory = self.get_all_WorkCategory()
        subcategory = self.get_all_SubWorkCategory()
        countries = self.get_all_Country()
        payment = self.get_all_Payment_type()
        skills = self.get_all_Skills()
        job_status = self.get_all_job_status()
        # user = request.user.email
        freelancerlist = self.get_all_JobList()

        context = {'extended_template': extended_template,'key_words_search': self.get_job_search_query_list(),'workcategory': workcategory,'job_status': job_status,'payment': payment, 'key_words': self.get_freelancer_search_query_list(),
                   'subcategory': subcategory, 'countries': countries, 'skills': skills,
                   'freelancerlist': freelancerlist, 'type': 'Client'}
        return TemplateResponse(request, template_name, context)
    def post(self, request, *args, **kwargs):
        response_data = {}
        if request.POST.get('method') == 'Accept_or_reject':

            if request.POST.get('title') == 'Accept':
                job =JobInvitation.objects.get(id =request.POST.get('id'))
                job.status =InvitationStatus.objects.get(status='Accepted')
                job.save()
                Notifications.objects.get_or_create(user=job.job.user,title='has accepted a  job '+ job.job.job_title,created_by=Users.objects.get(id=request.user.id),status='0',url=reverse('jobdetails', kwargs={'slug': job.job.slug}),type=User_Type.objects.get(type='CLIENT'))
            else:
                job = JobInvitation.objects.get(id=request.POST.get('id'))
                job.status = InvitationStatus.objects.get(status='Declined')
                job.save()
                Notifications.objects.get_or_create(user=job.job.user,title='has Declined a  job '+ job.job.job_title,created_by=Users.objects.get(id=request.user.id),status='0',url=reverse('jobdetails', kwargs={'slug': job.job.slug}),type=User_Type.objects.get(type='CLIENT'))
        payload = {'status': 'success',
                   'message': "success" }
        return HttpResponse(json.dumps(payload), content_type='application/json', )





class JoblistPaginations(MainMixin,ListView):
    paginate_by = 5

    def post(self, request, *args, **kwargs):
        response_data = {}
        if request.POST.get('method') == 'Search_job':

            page_by = request.POST.get('page_by')
            type = request.POST.get('type')

            serach_all = request.POST.get('search')
            search = request.POST.get('search_category')
            search_status = request.POST.get('search_status')
            search_type = request.POST.get('search_type')
            search_experiance = request.POST.get('search_experiance')
            search_hourlymin = request.POST.get('search_hourlymin')
            search_hourlymax = request.POST.get('search_hourlymax')
            joblist = self.get_searchJob_list(type,serach_all ,search_status, search, search_type, search_experiance, search_hourlymin,
                                              search_hourlymax)
            quote_cancelled = self.get_quotes_cancel_status()
            quote_apply = self.get_quotes_apply_status()
            quote_cancel_array =self.get_quote_cancel_array(quote_cancelled)
            quote_apply_array = self.get_quote_apply_array(quote_apply)

            paginator_response = self.get_paginated_queryset_joblist(request, page_by, joblist,quote_cancel_array,quote_apply_array,type)
            response_data['list_html'] = paginator_response['paginator_html']
            response_data['count'] = paginator_response['count']
            response_data['type'] = type

            return HttpResponse(json.dumps(response_data), content_type="application/json")

    def get_paginated_queryset_joblist(self, request, page_by, joblist,quote_cancel_array,quote_apply_array,type):

        if page_by:
            page_by = int(page_by)
        else:
            page_by = 1
        page_size = self.get_paginate_by(joblist)
        size = self.paginate_queryset(joblist, page_size)

        # if search == '':
        #     page_size = self.get_paginate_by(freelancerlist)
        #     size = self.paginate_queryset(freelancerlist, page_size)
        # else:
        #     page_size = self.get_paginate_by(
        #         WorkSubCategories.objects.filter(sub_category_title__contains=search))
        #     size = self.paginate_queryset(
        #         WorkSubCategories.objects.filter(sub_category_title__contains=search), page_size)

        paginator, page, queryset, is_paginated = size
        try:
            queryset = paginator.page(page_by).object_list
        except:
            queryset = queryset

        user_table = render_to_string('freelancer/ajax/job_list.html',
                                      {'queryset': queryset,'listtype': type,'quote_cancel_array': quote_cancel_array,'quote_apply_array': quote_apply_array, 'count': joblist.count(), 'paginator': paginator,
                                       'page': page,
                                       'is_paginated': is_paginated, 'type': 'Client', 'request': request,
                                       'page_obj': paginator.page(page_by)})

        # paginator_html = render_to_string('ajax/paginator_html.html',
        #                                   {'page_obj': paginator.page(page_by)})

        context = {'count': joblist.count(), 'paginator_html': user_table}
        return context
class FreelancerView(MainMixin,TemplateView):
    template_name = 'freelancer/profile_view.html'
    def get_context_data(self, **kwargs):
        context = super(FreelancerView, self).get_context_data(**kwargs)

        context['user'] = self.get_current_user(kwargs.get('slug'))
        return context
