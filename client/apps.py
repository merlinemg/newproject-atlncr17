from __future__ import unicode_literals

from django.apps import AppConfig
from django.db.models.signals import post_migrate



def define_status(sender, **kwargs):
    Status = sender.get_model("JobStatus")

    store_corporate,create = Status.objects.get_or_create(status='Publish')

    store_corporates, creates = Status.objects.get_or_create(status='Unpublish')
    store_corporates, creates = Status.objects.get_or_create(status='Draft')
    paymenttype = sender.get_model("PaymentType")

    type, types = paymenttype.objects.get_or_create(type='per hour')
    type, types = paymenttype.objects.get_or_create(type='fixed price')
    invtnstatus = sender.get_model("InvitationStatus")
    type, types = invtnstatus.objects.get_or_create(status='Accepted')
    type, types = invtnstatus.objects.get_or_create(status='Declined')
    type, types = invtnstatus.objects.get_or_create(status='Withdrawn')
    type, types = invtnstatus.objects.get_or_create(status='Cancelled')

def define_duration(sender, **kwargs):
    Durations = sender.get_model("Duration")
    duration, durations = Durations.objects.get_or_create(date_range="Less than One day")
    duration, durations = Durations.objects.get_or_create(date_range="Less than Two day")
    duration, durations = Durations.objects.get_or_create(date_range="Less than Three day")
    duration, durations = Durations.objects.get_or_create(date_range="Less than One Week")
    duration, durations = Durations.objects.get_or_create(date_range="Less than Two Week")
    duration, durations = Durations.objects.get_or_create(date_range="Less than One Month")
    duration, durations = Durations.objects.get_or_create(date_range="Between 1-3 Months")
    duration, durations = Durations.objects.get_or_create(date_range="Between 3-6 Months")
    duration, durations = Durations.objects.get_or_create(date_range="More than 6 Months")




    print sender
class ClientConfig(AppConfig):
    name = 'client'
    def ready(self):
        from client import signals
        post_migrate.connect(define_status, sender=self)
        post_migrate.connect(define_duration, sender=self)