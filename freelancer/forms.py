from django import forms

from freelancer.models import Freelancer_Profile, Skills, HourlyRate, LocationDetails


class FreelancerProfileForm(forms.ModelForm):
    title = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Enter Your Title','maxlength':25}),
                            error_messages={'required': 'Title is required'})
    #about = forms.CharField(widget=forms.Textarea(attrs={'placeholder': 'About Me', 'cols': '2', 'rows': '2'})
                           #)

    # workcategory = forms.CharField(widget=forms.TextInput(attrs={'id': 'myAutocompletecategory'}),
    # error_messages={'required': 'Work Category is required'})
    # subcategory = forms.CharField(widget=forms.TextInput(attrs={'id': 'myAutocompletesub'}),
    # error_messages={'required': 'Sub Category is required'})
    # skills = forms.CharField(widget=forms.TextInput(attrs={'id': 'myAutocomplete', 'cols': '2', 'rows': '2'}),
    # error_messages={'required': 'Skills is required'})

    class Meta:
        model = Freelancer_Profile
        fields = ['title', 'about', 'workcategory', 'subcategory', 'skills']

    def __init__(self, *args, **kwargs):
        super(FreelancerProfileForm, self).__init__(*args, **kwargs)

        for field in self.fields:
            # self.fields['skills'].widget.attrs['id'] = 'myAutocomplete'
            self.fields['skills'].widget.attrs['class'] = 'skills '
            self.fields['workcategory'].widget.attrs['class'] = 'workcategory '
            self.fields['workcategory'].widget.attrs['onchange'] = 'getsubcategory(this)'
            self.fields['subcategory'].widget.attrs['class'] = 'subcategory '
            self.fields['about'].widget.attrs['placeholder'] = 'About Me '
            # self.fields['workcategory'].error_messages['required'] = 'Work Category  is required'

    def clean_title(self):
        if self.cleaned_data["title"].strip() == '':
            raise forms.ValidationError("Title is required.")
        return self.cleaned_data["title"]

    # def clean_about(self):
    #     if self.cleaned_data["about"].strip() == '':
    #         raise forms.ValidationError("About is required.")
    #     return self.cleaned_data["about"]

    # def clean_workcategory(self):
    #     if not self.cleaned_data["workcategory"]:
    #         raise forms.ValidationError("Workcategory is required.")
    #     return self.cleaned_data["workcategory"]

    # def clean_subcategory(self):
    #     if not self.cleaned_data["subcategory"]:
    #         raise forms.ValidationError("Subcategory is required.")
    #     return self.cleaned_data["subcategory"]
    #
    # def clean_skills(self):
    #     if not self.cleaned_data["skills"]:
    #         raise forms.ValidationError("Skills is required.")
    #     return self.cleaned_data["skills"]

    def save(self, user, skills, subcategory, commit=True):
        form = super(FreelancerProfileForm, self).save(commit=False)

        # form.workcategory = values['workcategory']
        # form.subcategory = values['subcategory']


        if commit:
            form.user = user
            form.save()
            try:
                form.skills.add(skills)
                form.subcategory.add(subcategory)

            except:
                for skill in skills:
                    form.skills.add(skill)
                for subcategorys in subcategory:
                    # form.subcategory.remove(subcategorys)
                    form.subcategory.add(subcategorys)

                    # for skill in skills:
                    #
                    #     form.skills.add(skill)
                    # for subcategorys in subcategory:
                    #     form.subcategory.remove(subcategorys)
                    #     form.subcategory.add(subcategorys)

                    # form.skills = self.cleaned_data["skills"]
        return form


class HourlyRateForm(forms.ModelForm):
    hourly_rate = forms.RegexField(widget=forms.TextInput(attrs={'placeholder': 'Hourly Rate'}),
                                   regex=r'^[0-9]\d*(\.\d+)?$',
                                   error_message=(
                                       "Enter a valid rate."))
    actual_rate = forms.RegexField(widget=forms.TextInput(attrs={'placeholder': 'Actual Rate '}),
                                   regex=r'^[0-9]\d*(\.\d+)?$',
                                   error_message=(
                                       "Enter a valid rate."))

    class Meta:
        model = HourlyRate
        fields = ['hourly_rate', 'actual_rate']

    def clean_hourly_rate(self):
        if not self.cleaned_data["hourly_rate"]:
            raise forms.ValidationError("hourly rate is required.")
        return self.cleaned_data["hourly_rate"]

    def clean_actual_rate(self):
        if not self.cleaned_data["actual_rate"]:
            raise forms.ValidationError("actual rate is required.")
        return self.cleaned_data["actual_rate"]

    def save(self, user, commit=True):
        form = super(HourlyRateForm, self).save(commit=False)
        form.user = user
        form.save()
        # form.save()
        return form


class LocationDetailsForm(forms.ModelForm):
    zip_code = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'ZIP Code '}),
                               error_messages={'required': 'zip_code is required'})
    address = forms.CharField(widget=forms.Textarea(attrs={'placeholder': 'Address', 'cols': '2', 'rows': '2'}),
                              error_messages={'required': 'Address is required'})

    class Meta:
        model = LocationDetails
        fields = ['country', 'state', 'city', 'address', 'zip_code']

    def __init__(self, *args, **kwargs):
        super(LocationDetailsForm, self).__init__(*args, **kwargs)

        for field in self.fields:
            # self.fields['skills'].widget.attrs['id'] = 'myAutocomplete'
            self.fields['country'].empty_label = "Select Country"
            self.fields['state'].empty_label = "Select State"
            self.fields['city'].empty_label = "Select City"

    def save(self, user, commit=True):
        form = super(LocationDetailsForm, self).save(commit=False)
        form.user = user
        form.save()
        # form.save()
        return form
