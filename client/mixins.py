from django.db.models import Q
import datetime
from accounts.models import Users, User_Type
from client.models import PaymentType, ClientJobs, JobInvitation, ApplyJob, JobStatus, Duration, ApplyJobCancel, \
    InvitationStatus, Client_Profile
from freelancer.models import WorkCategories, WorkSubCategories, Skills, Freelancer_Profile, FreelancerPortfolioimages
from cities_light.models import Country

from hire.models import HiredJobsContract, ContractStatus, MileStoneStatus, JobsMilestone, TimesheetStatus
from notifications.models import Notifications


class ClientMixin(object):
    def get_category(self, job_main_category):
        """
        Get all  categories .
        :return:categories object
        """
        if job_main_category != '':
            job_main_category = WorkCategories.objects.get(pk=job_main_category)
        else:
            job_main_category = None
        return job_main_category

    def get_selected_sub_catrories(self, sub_categ_list):
        """
        get selected Sub category.
        :return:
        """
        if sub_categ_list != '':
            catogs = WorkSubCategories.objects.filter(id__in=sub_categ_list)
        else:
            catogs = None
        return catogs

    def get_selected_job_skill(self, skill_list):
        """
        get selected skills.
        :return:
        """
        if skill_list != '':
            skills = Skills.objects.filter(id__in=skill_list)
        else:
            skills = None

        return skills

    def get_all_Payment_type(self):
        """
        get selected skills.
        :return:
        """
        payment = PaymentType.objects.all()
        return payment
    def get_user_type(self,type):
        """
        get selected skills.
        :return:
        """
        types =User_Type.objects.get(type=type)
        return types

    def get_sub_id(self, total):
        id = []

        if len(total) > 0:
            for datas in total:
                id.append(int(datas))
        else:
            id.append(total)

        return id

    def get_all_previous_data(self):
        try:

            user = self.request.user.get_client_company_intance().user

        except:
            user = self.request.user
        data = ClientJobs.objects.filter(user=self.request.user)
        return data

    # def get_current_job(self, id):
    #     """
    #     Get Current Job .
    #     :return:job object
    #     """
    #     try:
    #         try:
    #             job = ClientJobs.objects.get(id=id)
    #         except:
    #             job = ClientJobs.objects.get(slug=id)
    #     except:
    #         job = []
    #     print job,"dfdfdfdf"
    #     return job

    def get_all_WorkCategory(self):
        category = WorkCategories.objects.all()
        return category

    def get_all_SubWorkCategory(self):
        category = WorkSubCategories.objects.all()
        return category

    def get_all_Country(self):
        Countrys = Country.objects.all()
        return Countrys

    def get_all_Skills(self):
        skills = Skills.objects.all()
        return skills

    def Get_invited_freelancerList(self, job,type):

       if type == 'Applied':
           list = ApplyJob.objects.filter(job__id=job)

       elif type == 'Invitaion':
           list = JobInvitation.objects.filter(job__id=job)
       else:
           list = HiredJobsContract.objects.filter(client_job__id=job)
       return list

    # def get_all_job_status(self):
    #     status = JobStatus.objects.all()
    #     return status



    def get_all_Freelancerlist(self):
        category = Freelancer_Profile.objects.filter(user__is_active=True).order_by("-id")
        return category
    def get_payment(self,type):
        if type !='':
           category = PaymentType.objects.get(id=type)
        else:
            category=None

        return category

    def get_duration(self,type):
        if type !='':
           category = Duration.objects.get(id=type)
        else:
            category=None
        return category
    def get_all_duration(self):
        durationlist = Duration.objects.all()
        return durationlist
    def Get_status(self,status):
        category = JobStatus.objects.get(id=status)
        return category

    def get_all_job_status(self):
        status = JobStatus.objects.all()
        return status

    def get_all_JobList(self):
        try:

            user = self.request.user.get_client_company_intance().user

        except:
            user = self.request.user
        try:
            if self.request.user.active_account.type == 'CLIENT':
                jobs = ClientJobs.objects.filter(user=self.request.user).order_by('-id')
            else:

                jobs = ClientJobs.objects.filter(job_status__status='Publish',verification_status='0',repost_status='0',job_close_date__gte=datetime.datetime.today()).exclude(user=self.request.user).order_by('-id')
                #jobs = ClientJobs.objects.filter(job_status__status='Publish',verification_status='0',repost_status='0').order_by('-id')
        except:
              jobs = ClientJobs.objects.filter(job_status__status='Publish',verification_status='0',repost_status='0',job_close_date__gte=datetime.datetime.today()).order_by('-id')

        return jobs

    def get_job(self, id):
        job = ClientJobs.objects.get(id=id)
        return job

    def get_Applyjob(self, id):
        if id:
            job = ApplyJob.objects.get(id=id)
        else:
            job = None
        return job

    def if_job_applied_cancel(self, user, job_details):
        """
        Get ApplyJob
        :return: objects of ApplyJob
        """
        try:
            is_user = ApplyJobCancel.objects.get(user=user, job=job_details)
        except:
            is_user = ''
        return is_user

    def get_freelancer_user(self, user):
        users = Freelancer_Profile.objects.get(user__email=user)

        return users
    def get_job_list(self):

        jobs = ClientJobs.objects.filter(job_status__status='Publish',verification_status='0',repost_status='0').order_by('-id')

        return jobs

    def if_job_invitation(self, freelancers_users, job_details):
        """
        Get JobInvitation objects .
        :return:JobInvitation objects
        """
        is_invitation = JobInvitation.objects.select_related().filter(user=freelancers_users, job=job_details).exists()
        return is_invitation

    def get_search_freelancer(self, search, hourly_max, hourly_min, workcategory, subcategory, country, skills):
        freelancerlist = self.get_all_Freelancerlist()

        freelancerlist = freelancerlist.exclude(user=self.request.user)
        if search and hourly_max == '' and hourly_min == '' and workcategory == '' and subcategory == '' and skills == [] and country == []:
            search = search.split(",")

            freelancerlist = freelancerlist.filter(Q(
                user__freelancer_location_user__country__name__in=search) | Q(
                subcategory__sub_category_title__icontains=search) | Q(
                subcategory__sub_category_title__in=search) | Q(
                workcategory__category_title__icontains=search) | Q(workcategory__category_title__in=search) |
                                                   Q(user__first_name__icontains=search) | Q(
                user__first_name__in=search) | Q(user__last_name__icontains=search) | Q(
                user__last_name__in=search) | Q(
                title__icontains=search) | Q(title__in=search) | Q(skills__name__in=search) | Q(
                skills__name__contains=search) | Q(
                skills__name__icontains=search)).distinct()
        elif search == '' and hourly_max == '' and hourly_min == '' and workcategory and subcategory == '' and skills == [] and country == []:

            workcategory = filter(None, workcategory.split(","))
            freelancerlist = freelancerlist.filter(Q(
                workcategory__id__icontains=workcategory) | Q(
                workcategory__id__in=workcategory)).distinct()

        elif search == '' and hourly_max == '' and hourly_min == '' and workcategory == '' and subcategory and skills == [] and country == []:
            subcategory = filter(None, subcategory.split(","))
            freelancerlist = freelancerlist.filter(Q(
                subcategory__id__icontains=subcategory) | Q(
                subcategory__id__in=subcategory)).distinct()


        elif search == '' and hourly_max == '' and hourly_min == '' and workcategory == '' and subcategory == '' and skills and country == []:
            # skills = skills.split(",")
            freelancerlist = freelancerlist.filter(Q(skills__name__in=skills)).distinct()


        elif search == '' and hourly_max == '' and hourly_min == '' and workcategory == '' and subcategory == '' and skills == [] and country:
            # skills = skills.split(",")
            freelancerlist = freelancerlist.filter(
                Q(user__freelancer_location_user__country__name__in=country)).distinct()


        elif search == '' and hourly_max and hourly_min == '' and workcategory == '' and subcategory == '' and skills == [] and country == []:

            freelancerlist = freelancerlist.filter(
                Q(user__freelancer_hourly_user__hourly_rate__lte=hourly_max)).distinct()
        elif search == '' and hourly_max == '' and hourly_min and workcategory == '' and subcategory == '' and skills == [] and country == []:

            freelancerlist = freelancerlist.filter(
                Q(user__freelancer_hourly_user__hourly_rate__gte=hourly_min)).distinct()
        elif search == '' and hourly_max == '' and hourly_min == '' and workcategory and subcategory and skills == [] and country == []:
            subcategory = filter(None, subcategory.split(","))
            workcategory = filter(None, workcategory.split(","))
            freelancerlist = freelancerlist.filter((Q(
                subcategory__id__icontains=subcategory) | Q(
                subcategory__id__in=subcategory)) & (Q(
                workcategory__id__icontains=workcategory) | Q(
                workcategory__id__in=workcategory))).distinct()
        elif search == '' and hourly_max == '' and hourly_min == '' and workcategory and subcategory == '' and skills and country ==[]:

            workcategory = filter(None, workcategory.split(","))
            freelancerlist = freelancerlist.filter(Q(skills__name__in=skills) & (Q(
                workcategory__id__icontains=workcategory) | Q(
                workcategory__id__in=workcategory))).distinct()

        elif search == '' and hourly_max == '' and hourly_min == '' and workcategory and subcategory == '' and skills == [] and country:

            workcategory = filter(None, workcategory.split(","))
            freelancerlist = freelancerlist.filter((Q(
                workcategory__id__icontains=workcategory) | Q(
                workcategory__id__in=workcategory)) & Q(
                user__freelancer_location_user__country__name__in=country)).distinct()

        elif search == '' and hourly_max == '' and hourly_min == '' and workcategory == '' and subcategory and skills and country == []:

            subcategory = filter(None, subcategory.split(","))
            freelancerlist = freelancerlist.filter((Q(
                subcategory__id__icontains=subcategory) | Q(
                subcategory__id__in=subcategory)) & Q(skills__name__in=skills)).distinct()

        elif search == '' and hourly_max == '' and hourly_min == '' and workcategory == '' and subcategory == '' and skills and country:

            subcategory = subcategory.split(",")
            freelancerlist = freelancerlist.filter(
                Q(user__freelancer_location_user__country__name__in=country) & Q(
                    skills__name__in=skills)).distinct()
            print freelancerlist, "fffffff"

        elif search == '' and hourly_max == '' and hourly_min == '' and workcategory == '' and subcategory and skills == [] and country:

            subcategory = filter(None, subcategory.split(","))
            freelancerlist = freelancerlist.filter((Q(
                subcategory__id__icontains=subcategory) | Q(
                subcategory__id__in=subcategory)) & Q(
                user__freelancer_location_user__country__name__in=country)).distinct()

        elif search == '' and hourly_max == '' and hourly_min == '' and workcategory and subcategory and skills and country == []:
            workcategory = filter(None, workcategory.split(","))
            subcategory = filter(None, subcategory.split(","))
            freelancerlist = freelancerlist.filter((Q(
                subcategory__id__icontains=subcategory) | Q(
                subcategory__id__in=subcategory)) & Q(skills__name__in=skills) & (Q(
                workcategory__id__icontains=workcategory) | Q(
                workcategory__id__in=workcategory))).distinct()

        elif search == '' and hourly_max == '' and hourly_min == '' and workcategory and subcategory and skills == [] and country:
            workcategory = filter(None, workcategory.split(","))
            subcategory = filter(None, subcategory.split(","))
            freelancerlist = freelancerlist.filter((Q(
                subcategory__id__icontains=subcategory) | Q(
                subcategory__id__in=subcategory)) & Q(
                user__freelancer_location_user__country__name__in=country) & (Q(
                workcategory__id__icontains=workcategory) | Q(
                workcategory__id__in=workcategory))).distinct()

        elif search == '' and hourly_max == '' and hourly_min == '' and workcategory and subcategory == '' and skills and country:
            workcategory = filter(None, workcategory.split(","))
            subcategory = subcategory.split(",")
            freelancerlist = freelancerlist.filter(
                Q(skills__name__in=skills) & Q(user__freelancer_location_user__country__name__in=country) & (Q(
                    workcategory__id__icontains=workcategory) | Q(
                    workcategory__id__in=workcategory))).distinct()

        elif search == '' and hourly_max == '' and hourly_min == '' and workcategory == '' and subcategory and skills and country:
            workcategory = workcategory.split(",")
            subcategory = filter(None, subcategory.split(","))
            freelancerlist = freelancerlist.filter(
                Q(skills__name__in=skills) & Q(user__freelancer_location_user__country__name__in=country) & (Q(
                    subcategory__id__icontains=subcategory) | Q(
                    subcategory__id__in=subcategory))).distinct()

        elif search == '' and hourly_max == '' and hourly_min == '' and workcategory and subcategory and skills and country:
            workcategory = filter(None, workcategory.split(","))
            subcategory = filter(None, subcategory.split(","))
            freelancerlist = freelancerlist.filter(Q(skills__name__in=skills) & (Q(
                workcategory__id__icontains=workcategory) | Q(
                workcategory__id__in=workcategory)) & Q(
                user__freelancer_location_user__country__name__in=country) & (Q(
                subcategory__id__icontains=subcategory) | Q(
                subcategory__id__in=subcategory))).distinct()
            print freelancerlist, "dddddddddddd"

        elif search == '' and hourly_max == '' and hourly_min and workcategory and subcategory == '' and skills == [] and country == []:
            workcategory = filter(None, workcategory.split(","))
            subcategory = subcategory.split(",")
            freelancerlist = freelancerlist.filter(Q(user__freelancer_hourly_user__hourly_rate__gte=hourly_min) & (Q(
                workcategory__id__icontains=workcategory) | Q(
                workcategory__id__in=workcategory))).distinct()

        elif search == '' and hourly_max == '' and hourly_min and workcategory == '' and subcategory and skills == [] and country == []:
            workcategory = workcategory.split(",")
            subcategory = filter(None, subcategory.split(","))
            freelancerlist = freelancerlist.filter(Q(user__freelancer_hourly_user__hourly_rate__gte=hourly_min) & (Q(
                subcategory__id__icontains=subcategory) | Q(
                subcategory__id__in=subcategory))).distinct()

        elif search == '' and hourly_max == '' and hourly_min and workcategory == '' and subcategory == '' and skills and country == []:
            workcategory = workcategory.split(",")
            subcategory = subcategory.split(",")
            freelancerlist = freelancerlist.filter(
                Q(user__freelancer_hourly_user__hourly_rate__gte=hourly_min) & Q(skills__name__in=skills)).distinct()

        elif search == '' and hourly_max == '' and hourly_min and workcategory == '' and subcategory == '' and skills == [] and country:
            workcategory = workcategory.split(",")
            subcategory = subcategory.split(",")
            freelancerlist = freelancerlist.filter(Q(user__freelancer_hourly_user__hourly_rate__gte=hourly_min) & Q(
                user__freelancer_location_user__country__name__icontains=country)).distinct()
            print freelancerlist, "hourlymin"
        elif search == '' and hourly_max and hourly_min == '' and workcategory and subcategory == '' and skills == [] and country == []:
            workcategory = filter(None, workcategory.split(","))
            subcategory = subcategory.split(",")
            freelancerlist = freelancerlist.filter(Q(user__freelancer_hourly_user__hourly_rate__lte=hourly_max) & (Q(
                workcategory__id__icontains=workcategory) | Q(
                workcategory__id__in=workcategory))).distinct()

        elif search == '' and hourly_max and hourly_min == '' and workcategory == '' and subcategory and skills == [] and country == []:
            workcategory = workcategory.split(",")
            subcategory = filter(None, subcategory.split(","))
            freelancerlist = freelancerlist.filter(Q(user__freelancer_hourly_user__hourly_rate__lte=hourly_max) & (Q(
                subcategory__id__icontains=subcategory) | Q(
                subcategory__id__in=subcategory))).distinct()

        elif search == '' and hourly_max and hourly_min == '' and workcategory == '' and subcategory == '' and skills and country == []:
            workcategory = workcategory.split(",")
            subcategory = subcategory.split(",")
            freelancerlist = freelancerlist.filter(
                Q(user__freelancer_hourly_user__hourly_rate__lte=hourly_max) & Q(skills__name__in=skills)).distinct()

        elif search == '' and hourly_max and hourly_min == '' and workcategory == '' and subcategory == '' and skills == [] and country:
            workcategory = workcategory.split(",")
            subcategory = subcategory.split(",")
            freelancerlist = freelancerlist.filter(Q(user__freelancer_hourly_user__hourly_rate__lte=hourly_max) & Q(
                user__freelancer_location_user__country__name__in=country)).distinct()
            print freelancerlist, "hourlymax sec"
        elif search == '' and hourly_max == '' and hourly_min and workcategory and subcategory == '' and skills == [] and country:
            workcategory = filter(None, workcategory.split(","))
            subcategory = subcategory.split(",")
            freelancerlist = freelancerlist.filter(Q(user__freelancer_hourly_user__hourly_rate__gte=hourly_min) & Q(
                user__freelancer_location_user__country__name__in=country) & (Q(
                workcategory__id__icontains=workcategory) | Q(
                workcategory__id__in=workcategory))).distinct()

        elif search == '' and hourly_max == '' and hourly_min and workcategory and subcategory == '' and skills and country == []:
            workcategory = filter(None, workcategory.split(","))
            subcategory = subcategory.split(",")
            freelancerlist = freelancerlist.filter(
                Q(user__freelancer_hourly_user__hourly_rate__gte=hourly_min) & Q(skills__name__in=skills) & (Q(
                    workcategory__id__icontains=workcategory) | Q(
                    workcategory__id__in=workcategory))).distinct()

        elif search == '' and hourly_max == '' and hourly_min and workcategory and subcategory and skills == [] and country == []:
            workcategory = filter(None, workcategory.split(","))
            subcategory = filter(None, subcategory.split(","))
            freelancerlist = freelancerlist.filter(Q(user__freelancer_hourly_user__hourly_rate__gte=hourly_min) & (Q(
                subcategory__id__icontains=subcategory) | Q(
                subcategory__id__in=subcategory)) & (Q(
                workcategory__id__icontains=workcategory) | Q(
                workcategory__id__in=workcategory))).distinct()

        elif search == '' and hourly_max == '' and hourly_min and workcategory == '' and subcategory and skills and country == []:
            workcategory = workcategory.split(",")
            subcategory = filter(None, subcategory.split(","))
            freelancerlist = freelancerlist.filter(Q(user__freelancer_hourly_user__hourly_rate__gte=hourly_min) & (Q(
                subcategory__id__icontains=subcategory) | Q(
                subcategory__id__in=subcategory)) & Q(skills__name__in=skills)).distinct()

        elif search == '' and hourly_max == '' and hourly_min and workcategory == '' and subcategory and skills == [] and country:
            workcategory = workcategory.split(",")
            subcategory = filter(None, subcategory.split(","))
            freelancerlist = freelancerlist.filter(Q(user__freelancer_hourly_user__hourly_rate__gte=hourly_min) & (Q(
                subcategory__id__icontains=subcategory) | Q(
                subcategory__id__in=subcategory)) & Q(
                user__freelancer_location_user__country__name__in=country)).distinct()

        elif search == '' and hourly_max == '' and hourly_min and workcategory == '' and subcategory == '' and skills and country:
            workcategory = workcategory.split(",")
            subcategory = subcategory.split(",")
            freelancerlist = freelancerlist.filter(
                Q(user__freelancer_hourly_user__hourly_rate__gte=hourly_min) & Q(skills__name__in=skills) & Q(
                    user__freelancer_location_user__country__name__in=country)).distinct()
            print freelancerlist, "cc"
        elif search == '' and hourly_max and hourly_min == '' and workcategory and subcategory == '' and skills == [] and country:
            workcategory = filter(None, workcategory.split(","))
            subcategory = subcategory.split(",")
            freelancerlist = freelancerlist.filter(Q(user__freelancer_hourly_user__hourly_rate__lte=hourly_max) & Q(
                user__freelancer_location_user__country__name__in=country) & (Q(
                workcategory__id__icontains=workcategory) | Q(
                workcategory__id__in=workcategory))).distinct()

        elif search == '' and hourly_max and hourly_min == '' and workcategory and subcategory == '' and skills and country == []:
            workcategory = filter(None, workcategory.split(","))
            subcategory = subcategory.split(",")
            freelancerlist = freelancerlist.filter(
                Q(user__freelancer_hourly_user__hourly_rate__lte=hourly_max) & Q(skills__name__in=skills) & (Q(
                    workcategory__id__icontains=workcategory) | Q(
                    workcategory__id__in=workcategory))).distinct()

        elif search == '' and hourly_max and hourly_min == '' and workcategory and subcategory and skills == [] and country == []:
            workcategory = filter(None, workcategory.split(","))
            subcategory = filter(None, subcategory.split(","))
            freelancerlist = freelancerlist.filter(
                Q(user__freelancer_hourly_user__hourly_rate__lte=hourly_max) & (Q(
                    subcategory__id__icontains=subcategory) | Q(
                    subcategory__id__in=subcategory)) & (Q(
                    workcategory__id__icontains=workcategory) | Q(
                    workcategory__id__in=workcategory))).distinct()

        elif search == '' and hourly_max and hourly_min == '' and workcategory == '' and subcategory and skills and country == []:
            workcategory = workcategory.split(",")
            subcategory = filter(None, subcategory.split(","))
            freelancerlist = freelancerlist.filter(
                Q(user__freelancer_hourly_user__hourly_rate__lte=hourly_max) & (Q(
                    subcategory__id__icontains=subcategory) | Q(
                    subcategory__id__in=subcategory)) & Q(skills__name__in=skills)).distinct()

        elif search == '' and hourly_max and hourly_min == '' and workcategory == '' and subcategory and skills == [] and country:
            workcategory = workcategory.split(",")
            subcategory = filter(None, subcategory.split(","))
            freelancerlist = freelancerlist.filter(
                Q(user__freelancer_hourly_user__hourly_rate__lte=hourly_max) & (Q(
                    subcategory__id__icontains=subcategory) | Q(
                    subcategory__id__in=subcategory)) & Q(
                    user__freelancer_location_user__country__name__in=country)).distinct()
        elif search == '' and hourly_max and hourly_min == '' and workcategory == '' and subcategory == '' and skills and country:
            workcategory = workcategory.split(",")
            subcategory = subcategory.split(",")
            freelancerlist = freelancerlist.filter(
                Q(user__freelancer_hourly_user__hourly_rate__lte=hourly_max) & Q(skills__name__in=skills) & Q(
                    user__freelancer_location_user__country__name__in=country)).distinct()
            print "net"

        elif search == '' and hourly_max and hourly_min == '' and workcategory == '' and subcategory and skills and country:
            workcategory = workcategory.split(",")
            subcategory = filter(None, subcategory.split(","))
            freelancerlist = freelancerlist.filter(
                Q(user__freelancer_hourly_user__hourly_rate__lte=hourly_max) & Q(skills__name__in=skills) & Q(
                    user__freelancer_location_user__country__name__in=country) & (Q(
                    subcategory__id__icontains=subcategory) | Q(
                    subcategory__id__in=subcategory))).distinct()

        elif search == '' and hourly_max and hourly_min == '' and workcategory and subcategory == '' and skills and country:
            workcategory = filter(None, workcategory.split(","))
            subcategory = subcategory.split(",")
            freelancerlist = freelancerlist.filter(
                Q(user__freelancer_hourly_user__hourly_rate__lte=hourly_max) & Q(skills__name__in=skills) & Q(
                    user__freelancer_location_user__country__name__in=country) & (Q(
                    workcategory__id__icontains=workcategory) | Q(
                    workcategory__id__in=workcategory))).distinct()

        elif search == '' and hourly_max and hourly_min == '' and workcategory and subcategory and skills and country == []:
            workcategory = filter(None, workcategory.split(","))
            subcategory = filter(None, subcategory.split(","))
            freelancerlist = freelancerlist.filter(
                Q(user__freelancer_hourly_user__hourly_rate__lte=hourly_max) & Q(skills__name__in=skills) & (Q(
                    subcategory__id__icontains=subcategory) | Q(
                    subcategory__id__in=subcategory)) & (Q(
                    workcategory__id__icontains=workcategory) | Q(
                    workcategory__id__in=workcategory))).distinct()

        elif search == '' and hourly_max and hourly_min == '' and workcategory and subcategory and skills == [] and country:
            workcategory = filter(None, workcategory.split(","))
            subcategory = filter(None, subcategory.split(","))
            freelancerlist = freelancerlist.filter(
                Q(user__freelancer_hourly_user__hourly_rate__lte=hourly_max) & Q(
                    user__freelancer_location_user__country__name__in=country) & (Q(
                    subcategory__id__icontains=subcategory) | Q(
                    subcategory__id__in=subcategory)) & (Q(
                    workcategory__id__icontains=workcategory) | Q(
                    workcategory__id__in=workcategory))).distinct()

        elif search == '' and hourly_max and hourly_min == '' and workcategory and subcategory and skills and country:
            workcategory = filter(None, workcategory.split(","))
            subcategory = filter(None, subcategory.split(","))
            freelancerlist = freelancerlist.filter(
                Q(user__freelancer_hourly_user__hourly_rate__lte=hourly_max) & Q(skills__name__in=skills) & Q(
                    user__freelancer_location_user__country__name__in=country) & (Q(
                    subcategory__id__icontains=subcategory) | Q(
                    subcategory__id__in=subcategory)) & (Q(
                    workcategory__id__icontains=workcategory) | Q(
                    workcategory__id__in=workcategory))).distinct()
            print freelancerlist, "cchnect"
        elif search == '' and hourly_max == '' and hourly_min and workcategory == '' and subcategory and skills and country:
            workcategory = workcategory.split(",")
            subcategory = filter(None, subcategory.split(","))
            freelancerlist = freelancerlist.filter(
                Q(user__freelancer_hourly_user__hourly_rate__gte=hourly_min) & Q(skills__name__in=skills) & Q(
                    user__freelancer_location_user__country__name__in=country) & (Q(
                    subcategory__id__icontains=subcategory) | Q(
                    subcategory__id__in=subcategory))).distinct()
        elif search == '' and hourly_max == '' and hourly_min and workcategory and subcategory == '' and skills and country:
            workcategory = filter(None, workcategory.split(","))
            subcategory = subcategory.split(",")
            freelancerlist = freelancerlist.filter(
                Q(user__freelancer_hourly_user__hourly_rate__gte=hourly_min) & Q(skills__name__in=skills) & Q(
                    user__freelancer_location_user__country__name__in=country) & (Q(
                    workcategory__id__icontains=workcategory) | Q(
                    workcategory__id__in=workcategory))).distinct()
        elif search == '' and hourly_max == '' and hourly_min and workcategory and subcategory and skills and country == []:
            workcategory = filter(None, workcategory.split(","))
            subcategory = filter(None, subcategory.split(","))
            freelancerlist = freelancerlist.filter(
                Q(user__freelancer_hourly_user__hourly_rate__gte=hourly_min) & Q(skills__name__in=skills) & (Q(
                    subcategory__id__icontains=subcategory) | Q(
                    subcategory__id__in=subcategory)) & (Q(
                    workcategory__id__icontains=workcategory) | Q(
                    workcategory__id__in=workcategory))).distinct()
        elif search == '' and hourly_max == '' and hourly_min and workcategory and subcategory and skills == [] and country:
            workcategory = filter(None, workcategory.split(","))
            subcategory = filter(None, subcategory.split(","))
            freelancerlist = freelancerlist.filter(
                Q(user__freelancer_hourly_user__hourly_rate__gte=hourly_min) & Q(
                    user__freelancer_location_user__country__name__in=country) & (Q(
                    subcategory__id__icontains=subcategory) | Q(
                    subcategory__id__in=subcategory)) & (Q(
                    workcategory__id__icontains=workcategory) | Q(
                    workcategory__id__in=workcategory))).distinct()
        elif search == '' and hourly_max == '' and hourly_min and workcategory and subcategory and skills and country:
            workcategory = filter(None, workcategory.split(","))
            subcategory = filter(None, subcategory.split(","))
            freelancerlist = freelancerlist.filter(
                Q(user__freelancer_hourly_user__hourly_rate__gte=hourly_min) & Q(skills__name__in=skills) & Q(
                    user__freelancer_location_user__country__name__in=country) & (Q(
                    subcategory__id__icontains=subcategory) | Q(
                    subcategory__id__in=subcategory)) & (Q(
                    workcategory__id__icontains=workcategory) | Q(
                    workcategory__id__in=workcategory))).distinct()
            print"jjjnext"
        elif search == '' and hourly_max and hourly_min and workcategory and subcategory and skills and country:
            workcategory = filter(None, workcategory.split(","))
            subcategory = filter(None, subcategory.split(","))
            freelancerlist = freelancerlist.filter(
                Q(user__freelancer_hourly_user__hourly_rate__range=(hourly_min, hourly_max)) & Q(
                    skills__name__in=skills) & Q(
                    user__freelancer_location_user__country__name__in=country) & (Q(
                    subcategory__id__icontains=subcategory) | Q(
                    subcategory__id__in=subcategory)) & (Q(
                    workcategory__id__icontains=workcategory) | Q(
                    workcategory__id__in=workcategory))).distinct()
        elif search == '' and hourly_max and hourly_min and workcategory and subcategory and skills == [] and country:
            workcategory = filter(None, workcategory.split(","))
            subcategory = filter(None, subcategory.split(","))
            freelancerlist = freelancerlist.filter(
                Q(user__freelancer_hourly_user__hourly_rate__range=(hourly_min, hourly_max)) & Q(
                    user__freelancer_location_user__country__name__in=country) & (Q(
                    subcategory__id__icontains=subcategory) | Q(
                    subcategory__id__in=subcategory)) & (Q(
                    workcategory__id__icontains=workcategory) | Q(
                    workcategory__id__in=workcategory))).distinct()

        elif search == '' and hourly_max and hourly_min and workcategory and subcategory and skills and country == []:
            workcategory = filter(None, workcategory.split(","))
            subcategory = filter(None, subcategory.split(","))
            freelancerlist = freelancerlist.filter(
                Q(user__freelancer_hourly_user__hourly_rate__range=(hourly_min, hourly_max)) & Q(
                    skills__name__in=skills) & (Q(
                    subcategory__id__icontains=subcategory) | Q(
                    subcategory__id__in=subcategory)) & (Q(
                    workcategory__id__icontains=workcategory) | Q(
                    workcategory__id__in=workcategory))).distinct()

        elif search == '' and hourly_max and hourly_min and workcategory and subcategory == '' and skills and country:
            workcategory = filter(None, workcategory.split(","))
            subcategory = subcategory.split(",")
            freelancerlist = freelancerlist.filter(
                Q(user__freelancer_hourly_user__hourly_rate__range=(hourly_min, hourly_max)) & Q(
                    skills__name__in=skills) & Q(
                    user__freelancer_location_user__country__name__in=country) & (Q(
                    workcategory__id__icontains=workcategory) | Q(
                    workcategory__id__in=workcategory))).distinct()

        elif search == '' and hourly_max and hourly_min and workcategory == '' and subcategory and skills and country:

            subcategory = filter(None, subcategory.split(","))
            freelancerlist = freelancerlist.filter(
                Q(user__freelancer_hourly_user__hourly_rate__range=(hourly_min, hourly_max)) & Q(
                    skills__name__in=skills) & Q(
                    user__freelancer_location_user__country__name__in=country) & (Q(
                    subcategory__id__icontains=subcategory) | Q(
                    subcategory__id__in=subcategory))).distinct()

        elif search == '' and hourly_max and hourly_min and workcategory == '' and subcategory == '' and skills and country:
            freelancerlist = freelancerlist.filter(
                Q(user__freelancer_hourly_user__hourly_rate__range=(hourly_min, hourly_max)) & Q(
                    skills__name__in=skills) & Q(
                    user__freelancer_location_user__country__name__in=country)).distinct()

        elif search == '' and hourly_max and hourly_min and workcategory == '' and subcategory and skills == [] and country:

            subcategory = filter(None, subcategory.split(","))
            freelancerlist = freelancerlist.filter(
                Q(user__freelancer_hourly_user__hourly_rate__range=(hourly_min, hourly_max)) & Q(
                    user__freelancer_location_user__country__name__in=country) & (Q(
                    subcategory__id__icontains=subcategory) | Q(
                    subcategory__id__in=subcategory))).distinct()

        elif search == '' and hourly_max and hourly_min and workcategory == '' and subcategory and skills and country == []:

            subcategory = filter(None, subcategory.split(","))
            freelancerlist = freelancerlist.filter(
                Q(user__freelancer_hourly_user__hourly_rate__range=(hourly_min, hourly_max)) & Q(
                    skills__name__in=skills) & (Q(
                    subcategory__id__icontains=subcategory) | Q(
                    subcategory__id__in=subcategory))).distinct()

        elif search == '' and hourly_max and hourly_min and workcategory and subcategory == '' and skills == [] and country:
            workcategory = filter(None, workcategory.split(","))
            subcategory = subcategory.split(",")
            freelancerlist = freelancerlist.filter(
                Q(user__freelancer_hourly_user__hourly_rate__range=(hourly_min, hourly_max)) & Q(
                    user__freelancer_location_user__country__name__in=country) & (Q(
                    workcategory__id__icontains=workcategory) | Q(
                    workcategory__id__in=workcategory))).distinct()

        elif search == '' and hourly_max and hourly_min and workcategory and subcategory == '' and skills and country == []:
            workcategory = filter(None, workcategory.split(","))

            freelancerlist = freelancerlist.filter(
                Q(user__freelancer_hourly_user__hourly_rate__range=(hourly_min, hourly_max)) & Q(
                    skills__name__in=skills) & (Q(
                    workcategory__id__icontains=workcategory) | Q(
                    workcategory__id__in=workcategory))).distinct()
        elif search == '' and hourly_max and hourly_min and workcategory == '' and subcategory == '' and skills == [] and country:
            workcategory = filter(None, workcategory.split(","))

            freelancerlist = freelancerlist.filter(
                Q(user__freelancer_hourly_user__hourly_rate__range=(hourly_min, hourly_max)) & Q(
                    user__freelancer_location_user__country__name__in=country)).distinct()

        elif search == '' and hourly_max and hourly_min and workcategory == '' and subcategory == '' and skills and country == []:
            workcategory = workcategory.split(",")

            freelancerlist = freelancerlist.filter(
                Q(user__freelancer_hourly_user__hourly_rate__range=(hourly_min, hourly_max)) & Q(
                    skills__name__in=skills)).distinct()

        elif search == '' and hourly_max and hourly_min and workcategory == '' and subcategory and skills == [] and country == []:
            subcategory = filter(None, subcategory.split(","))

            freelancerlist = freelancerlist.filter(
                Q(user__freelancer_hourly_user__hourly_rate__range=(hourly_min, hourly_max)) & (Q(
                    subcategory__id__icontains=subcategory) | Q(
                    subcategory__id__in=subcategory))).distinct()

        elif search == '' and hourly_max and hourly_min and workcategory and subcategory == '' and skills == [] and country == []:
            workcategory = filter(None, workcategory.split(","))

            freelancerlist = freelancerlist.filter(
                Q(user__freelancer_hourly_user__hourly_rate__range=(hourly_min, hourly_max)) & (Q(
                    workcategory__id__icontains=workcategory) | Q(
                    workcategory__id__in=workcategory))).distinct()

        elif search == '' and hourly_max and hourly_min and workcategory=='' and subcategory == '' and skills == [] and country == []:


            freelancerlist = freelancerlist.filter(
                Q(user__freelancer_hourly_user__hourly_rate__range=(hourly_min, hourly_max))).distinct()


        return freelancerlist

    def get_searchJob_list(self, serach_all,search_status, search, search_type, search_experiance, search_hourlymin,
                           search_hourlymax):
        joblist = self.get_all_JobList()


        if serach_all  and search_status == '' and search=='' and search_type == '' and search_experiance == '' and search_hourlymin == '' and search_hourlymax == '':
            serach_all = serach_all.split(",")
            joblist = joblist.filter(Q(job_main_category__category_title__icontains=serach_all) | Q(
                job_main_category__category_title__in=serach_all)| Q(job_sub_category__sub_category_title__icontains=serach_all) | Q(
                job_sub_category__sub_category_title__in=serach_all) | Q(job_skills__name__icontains=serach_all) | Q(
                job_skills__name__in=serach_all) | Q(job_title__icontains=serach_all) | Q(
                job_title__in=serach_all)| Q(job_description__icontains=serach_all) | Q(
                job_description__in=serach_all)).distinct()




        #
        # if serach_all==''  and search_status == '' and search and search_type == '' and search_experiance == '' and search_hourlymin == '' and search_hourlymax == '':
        #     search = search.split(",")
        #     joblist = joblist.filter(Q(job_main_category__category_title__icontains=search) | Q(
        #         job_main_category__category_title__in=search)).distinct()
        #
        #


        elif serach_all == '' and search_status == '' and search and search_type == '' and search_experiance == '' and search_hourlymin == '' and search_hourlymax == '':
            search = filter(None, search.split(","))
            joblist = joblist.filter(Q(job_main_category__id__icontains=search) | Q(
                job_main_category__id__in=search)).distinct()




        elif serach_all==''  and search_status and search and search_type == '' and search_experiance == '' and search_hourlymin == '' and search_hourlymax == '':

            search = filter(None, search.split(","))
            search_status = search_status.split(",")

            joblist = joblist.filter((Q(
                job_main_category__id__icontains=search) | Q(
                job_main_category__id__in=search)) & Q(job_status__status__in=search_status)).distinct()

        elif serach_all==''  and search_status and search == '' and search_type == '' and search_experiance == '' and search_hourlymin == '' and search_hourlymax == '':

            search_status = search_status.split(",")

            joblist = joblist.filter(Q(job_status__status__in=search_status)).distinct()
        elif serach_all==''  and search_status == '' and search == '' and search_type and search_experiance == '' and search_hourlymin == '' and search_hourlymax == '':

            search_type = search_type.split(",")

            joblist = joblist.filter(
                Q(payment_type__type__icontains=search_type) | Q(payment_type__type__in=search_type)).distinct()
        elif serach_all==''  and search_status and search == '' and search_type and search_experiance == '' and search_hourlymin == '' and search_hourlymax == '':

            search_type = search_type.split(",")
            search_status = search_status.split(",")
            joblist = joblist.filter(
                Q(job_status__status__in=search_status) & Q(payment_type__type__in=search_type)).distinct()

        elif serach_all==''  and search_status == '' and search and search_type and search_experiance == '' and search_hourlymin == '' and search_hourlymax == '':

            search_type = search_type.split(",")
            search = filter(None, search.split(","))
            joblist = joblist.filter((Q(job_main_category__id__icontains=search) | Q(
                job_main_category__id__in=search)) & Q(payment_type__type__in=search_type)).distinct()

        elif serach_all==''  and search_status == '' and search == '' and search_type == '' and search_experiance and search_hourlymin == '' and search_hourlymax == '':

            search_experiance = search_experiance.split(",")
            joblist = joblist.filter(Q(experiance_level__in=search_experiance)).distinct()

        elif serach_all==''  and search_status and search == '' and search_type == '' and search_experiance and search_hourlymin == '' and search_hourlymax == '':
            search_status = search_status.split(",")
            search_experiance = search_experiance.split(",")
            joblist = joblist.filter(
                Q(experiance_level__in=search_experiance) & Q(job_status__status__in=search_status)).distinct()

        elif serach_all==''  and search_status == '' and search and search_type == '' and search_experiance and search_hourlymin == '' and search_hourlymax == '':
            search = filter(None, search.split(","))
            search_experiance = search_experiance.split(",")
            joblist = joblist.filter(Q(experiance_level__in=search_experiance) & (
                Q(job_main_category__id__icontains=search) | Q(
                    job_main_category__id__in=search))).distinct()


        elif serach_all==''  and search_status == '' and search == '' and search_type and search_experiance and search_hourlymin == '' and search_hourlymax == '':
            search_type = search_type.split(",")
            search_experiance = search_experiance.split(",")
            joblist = joblist.filter(
                Q(experiance_level__in=search_experiance) & Q(payment_type__type__in=search_type)).distinct()


        elif serach_all==''  and search_status and search and search_type == '' and search_experiance and search_hourlymin == '' and search_hourlymax == '':
            search_status = search_status.split(",")
            search = filter(None, search.split(","))
            search_experiance = search_experiance.split(",")
            joblist = joblist.filter((Q(job_main_category__id__icontains=search) | Q(
                job_main_category__id__in=search)) & Q(experiance_level__in=search_experiance) & Q(
                job_status__status__in=search_status)).distinct()


        elif serach_all==''  and search_status == '' and search and search_type and search_experiance and search_hourlymin == '' and search_hourlymax == '':
            search_type = search_type.split(",")
            search = search.split(",")
            search_experiance = search_experiance.split(",")
            joblist = joblist.filter((Q(job_main_category__id__icontains=search) | Q(
                job_main_category__id__in=search)) & Q(experiance_level__in=search_experiance) & Q(
                payment_type__type__in=search_type)).distinct()


        elif serach_all==''  and search_status and search == '' and search_type and search_experiance and search_hourlymin == '' and search_hourlymax == '':
            search_type = search_type.split(",")
            search_status = search_status.split(",")
            search_experiance = search_experiance.split(",")
            joblist = joblist.filter(Q(experiance_level__in=search_experiance) & Q(
                payment_type__type__in=search_type) & Q(job_status__status__in=search_status)).distinct()


        elif serach_all==''  and search_status and search and search_type and search_experiance == '' and search_hourlymin == '' and search_hourlymax == '':
            search = filter(None, search.split(","))
            search_status = search_status.split(",")
            search_type = search_type.split(",")
            print search_status,"ghgh"

            joblist = joblist.filter((Q(
                job_main_category__id__icontains=search) | Q(
                job_main_category__id__in=search)) & Q(job_status__status__in=search_status) & Q(
                payment_type__type__in=search_type)).distinct()


        elif serach_all==''  and search_status == '' and search == '' and search_type == '' and search_experiance == '' and search_hourlymin and search_hourlymax:

            joblist = joblist.filter(Q(budget__range=(search_hourlymin, search_hourlymax)) | (Q(hourly_min__gte=search_hourlymin) & Q(hourly_max__lte=search_hourlymax))).distinct()


        # elif search_status == '' and search == '' and search_type == '' and search_experiance == '' and search_hourlymin and search_hourlymax:
        #
        #     joblist = joblist.filter(Q(amount__range=(search_hourlymin, search_hourlymax))).distinct()

        elif serach_all==''  and search_status and search == '' and search_type == '' and search_experiance == '' and search_hourlymin and search_hourlymax:
            search_status = search_status.split(",")
            joblist = joblist.filter(
                Q(job_status__status__in=search_status) & (Q(budget__range=(search_hourlymin, search_hourlymax)) | (Q(hourly_min__gte=search_hourlymin) & Q(hourly_max__lte=search_hourlymax)))).distinct()
        elif serach_all==''  and search_status == '' and search and search_type == '' and search_experiance == '' and search_hourlymin and search_hourlymax:
            search = filter(None, search.split(","))
            joblist = joblist.filter((Q(
                job_main_category__id__icontains=search) | Q(
                job_main_category__id__in=search)) & (Q(budget__range=(search_hourlymin, search_hourlymax)) | (Q(hourly_min__gte=search_hourlymin) & Q(hourly_max__lte=search_hourlymax)))).distinct()
        elif serach_all==''  and search_status == '' and search == '' and search_type and search_experiance == '' and search_hourlymin and search_hourlymax:
            search_type = search_type.split(",")
            joblist = joblist.filter(Q(payment_type__type__in=search_type) & (Q(budget__range=(search_hourlymin, search_hourlymax)) | (Q(hourly_min__gte=search_hourlymin) & Q(hourly_max__lte=search_hourlymax)))).distinct()
        elif serach_all==''  and  search_status == '' and search == '' and search_type == '' and search_experiance and search_hourlymin and search_hourlymax:
            search_experiance = search_experiance.split(",")
            joblist = joblist.filter(Q(experiance_level__in=search_experiance) & (Q(budget__range=(search_hourlymin, search_hourlymax)) | (Q(hourly_min__gte=search_hourlymin) & Q(hourly_max__lte=search_hourlymax)))).distinct()

        elif serach_all==''  and search_status and search and search_type == '' and search_experiance == '' and search_hourlymin and search_hourlymax:
            search_status = search_status.split(",")
            search = filter(None, search.split(","))
            joblist = joblist.filter(Q(job_status__status__in=search_status) & (Q(
                job_main_category__id__icontains=search) | Q(
                job_main_category__id__in=search)) & (Q(budget__range=(search_hourlymin, search_hourlymax)) | (Q(hourly_min__gte=search_hourlymin) & Q(hourly_max__lte=search_hourlymax)))).distinct()

        elif serach_all==''  and search_status == '' and search and search_type and search_experiance == '' and search_hourlymin and search_hourlymax:
            search_type = search_type.split(",")
            search = filter(None, search.split(","))
            joblist = joblist.filter(Q(payment_type__type__in=search_type) & (Q(
                job_main_category__id__icontains=search) | Q(
                job_main_category__id__in=search)) & (Q(budget__range=(search_hourlymin, search_hourlymax)) | (Q(hourly_min__gte=search_hourlymin) & Q(hourly_max__lte=search_hourlymax)))).distinct()

        elif serach_all==''  and search_status == '' and search == '' and search_type and search_experiance and search_hourlymin and search_hourlymax:
            search_type = search_type.split(",")
            search_experiance = search_experiance.split(",")
            joblist = joblist.filter(
                Q(payment_type__type__in=search_type) & Q(experiance_level__in=search_experiance) & (Q(budget__range=(search_hourlymin, search_hourlymax)) | (Q(hourly_min__gte=search_hourlymin) & Q(hourly_max__lte=search_hourlymax)))).distinct()

        elif serach_all==''  and search_status and search == '' and search_type and search_experiance == '' and search_hourlymin and search_hourlymax:
            search_status = search_status.split(",")
            search_type = search_type.split(",")
            joblist = joblist.filter(Q(payment_type__type__in=search_type) & Q(job_status__status__in=search_status) & (Q(budget__range=(search_hourlymin, search_hourlymax)) | (Q(hourly_min__gte=search_hourlymin) & Q(hourly_max__lte=search_hourlymax)))).distinct()

        elif serach_all==''  and search_status == '' and search and search_type == '' and search_experiance and search_hourlymin and search_hourlymax:

            search = filter(None, search.split(","))
            search_experiance = search_experiance.split(",")
            joblist = joblist.filter(Q(experiance_level__in=search_experiance) & (Q(
                job_main_category__id__icontains=search) | Q(
                job_main_category__id__in=search)) & (Q(budget__range=(search_hourlymin, search_hourlymax)) | (Q(hourly_min__gte=search_hourlymin) & Q(hourly_max__lte=search_hourlymax)))).distinct()


        elif serach_all==''  and search_status == '' and search == '' and search_type == '' and search_experiance == '' and search_hourlymin and search_hourlymax == '':

            joblist = joblist.filter(Q(budget__gte=search_hourlymin) | Q(hourly_min__gte=search_hourlymin)).distinct()


        elif serach_all==''  and search_status and search == '' and search_type == '' and search_experiance == '' and search_hourlymin and search_hourlymax == '':
            search_status = search_status.split(",")
            joblist = joblist.filter(
                Q(job_status__status__in=search_status) & (Q(budget__gte=search_hourlymin) | Q(hourly_min__gte=search_hourlymin))).distinct()

        elif serach_all==''  and search_status == '' and search and search_type == '' and search_experiance == '' and search_hourlymin and search_hourlymax == '':
            search = filter(None, search.split(","))
            joblist = joblist.filter((Q(
                job_main_category__id__icontains=search) | Q(
                job_main_category__id__in=search)) & (Q(budget__gte=search_hourlymin) | Q(hourly_min__gte=search_hourlymin))).distinct()

        elif serach_all==''  and search_status == '' and search == '' and search_type and search_experiance == '' and search_hourlymin and search_hourlymax == '':
            search_type = search_type.split(",")
            joblist = joblist.filter(Q(payment_type__type__in=search_type) & (Q(budget__gte=search_hourlymin) | Q(hourly_min__gte=search_hourlymin))).distinct()

        elif serach_all==''  and search_status == '' and search == '' and search_type == '' and search_experiance and search_hourlymin and search_hourlymax == '':
            search_experiance = search_experiance.split(",")
            joblist = joblist.filter(
                Q(experiance_level__in=search_experiance) & (Q(budget__gte=search_hourlymin) | Q(hourly_min__gte=search_hourlymin))).distinct()

        elif serach_all==''  and search_status and search and search_type == '' and search_experiance == '' and search_hourlymin and search_hourlymax == '':
            search_status = search_status.split(",")
            search = filter(None, search.split(","))
            joblist = joblist.filter(Q(job_status__status__in=search_status) & (Q(
                job_main_category__id__icontains=search) | Q(
                job_main_category__id__in=search)) & (Q(budget__gte=search_hourlymin) | Q(hourly_min__gte=search_hourlymin))).distinct()


        elif serach_all==''  and search_status == '' and search and search_type and search_experiance == '' and search_hourlymin and search_hourlymax == '':
            search_type = search_type.split(",")
            search = filter(None, search.split(","))
            joblist = joblist.filter(Q(payment_type__type__in=search_type) & (Q(
                job_main_category__id__icontains=search) | Q(
                job_main_category__id__in=search)) & (Q(budget__gte=search_hourlymin) | Q(hourly_min__gte=search_hourlymin))).distinct()

        elif serach_all==''  and search_status == '' and search == '' and search_type and search_experiance and search_hourlymin and search_hourlymax == '':
            search_type = search_type.split(",")
            search_experiance = search_experiance.split(",")
            joblist = joblist.filter(
                Q(payment_type__type__in=search_type) & Q(experiance_level__in=search_experiance) & (Q(budget__gte=search_hourlymin) | Q(hourly_min__gte=search_hourlymin))).distinct()


        elif serach_all==''  and search_status and search == '' and search_type and search_experiance == '' and search_hourlymin and search_hourlymax == '':
            search_status = search_status.split(",")
            search_type = search_type.split(",")
            joblist = joblist.filter(Q(payment_type__type__in=search_type) & Q(job_status__status__in=search_status) & (Q(budget__gte=search_hourlymin) | Q(hourly_min__gte=search_hourlymin))).distinct()

        elif serach_all==''  and search_status == '' and search and search_type == '' and search_experiance and search_hourlymin and search_hourlymax == '':
            search = filter(None, search.split(","))
            search_experiance = search_experiance.split(",")
            joblist = joblist.filter(Q(experiance_level__in=search_experiance) & (Q(
                job_main_category__id__icontains=search) | Q(
                job_main_category__id__in=search)) & (Q(budget__gte=search_hourlymin) | Q(hourly_min__gte=search_hourlymin))).distinct()

        elif serach_all==''  and search_status and search and search_type == '' and search_experiance and search_hourlymin and search_hourlymax == '':
            search = filter(None, search.split(","))
            search_status = search_status.split(",")
            search_experiance = search_experiance.split(",")
            joblist = joblist.filter(Q(experiance_level__in=search_experiance) & (Q(
                job_main_category__id__icontains=search) | Q(
                job_main_category__id__in=search)) & Q(job_status__status__in=search_status) & (Q(budget__gte=search_hourlymin) | Q(hourly_min__gte=search_hourlymin))).distinct()

        elif serach_all==''  and search_status == '' and search and search_type and search_experiance and search_hourlymin and search_hourlymax == '':
            search = filter(None, search.split(","))
            search_type = search_type.split(",")
            search_experiance = search_experiance.split(",")
            joblist = joblist.filter(Q(experiance_level__in=search_experiance) & (Q(
                job_main_category__id__icontains=search) | Q(
                job_main_category__id__in=search)) & Q(payment_type__type__in=search_type) &  (Q(budget__gte=search_hourlymin) | Q(hourly_min__gte=search_hourlymin))).distinct()




        elif serach_all==''  and search_status and search == '' and search_type and search_experiance and search_hourlymin and search_hourlymax == '':
            search_status = search_status.split(",")
            search_type = search_type.split(",")
            search_experiance = search_experiance.split(",")
            joblist = joblist.filter(
                Q(experiance_level__in=search_experiance) & Q(job_status__status__in=search_status) & Q(
                    payment_type__type__in=search_type) &  (Q(budget__gte=search_hourlymin) | Q(hourly_min__gte=search_hourlymin))).distinct()




        elif serach_all==''  and search_status and search and search_type and search_experiance == '' and search_hourlymin and search_hourlymax == '':
            search_status = search_status.split(",")
            search_type = search_type.split(",")
            search = filter(None, search.split(","))
            joblist = joblist.filter((Q(
                job_main_category__id__icontains=search) | Q(
                job_main_category__id__in=search)) & Q(job_status__status__in=search_status) & Q(
                payment_type__type__in=search_type) &  (Q(budget__gte=search_hourlymin) | Q(hourly_min__gte=search_hourlymin))).distinct()




        elif serach_all==''  and search_status and search and search_type and search_experiance and search_hourlymin and search_hourlymax == '':
            search_status = search_status.split(",")
            search_type = search_type.split(",")
            search = filter(None, search.split(","))
            search_experiance = search_experiance.split(",")
            joblist = joblist.filter((Q(
                job_main_category__id__icontains=search) | Q(
                job_main_category__id__in=search)) & Q(experiance_level__in=search_experiance) & Q(
                job_status__status__in=search_status) & Q(payment_type__type__in=search_type) & (Q(budget__gte=search_hourlymin) | Q(hourly_min__gte=search_hourlymin))).distinct()

        elif serach_all==''  and search_status == '' and search == '' and search_type == '' and search_experiance == '' and search_hourlymin == '' and search_hourlymax:


            joblist = joblist.filter( (Q(budget__lte=search_hourlymax) & Q(hourly_max__lte=search_hourlymax))).distinct()


        elif serach_all==''  and search_status and search == '' and search_type == '' and search_experiance == '' and search_hourlymin == '' and search_hourlymax:
            search_status = search_status.split(",")
            joblist = joblist.filter(
                Q(job_status__status__in=search_status) & (Q(budget__lte=search_hourlymax) & Q(hourly_max__lte=search_hourlymax))).distinct()

        elif serach_all==''  and search_status == '' and search and search_type == '' and search_experiance == '' and search_hourlymin == '' and search_hourlymax:
            search = filter(None, search.split(","))
            joblist = joblist.filter((Q(
                job_main_category__id__icontains=search) | Q(
                job_main_category__id__in=search)) & (Q(budget__lte=search_hourlymax) & Q(hourly_max__lte=search_hourlymax))).distinct()
            print(joblist), "hx";
        elif serach_all==''  and search_status == '' and search == '' and search_type and search_experiance == '' and search_hourlymin == '' and search_hourlymax:
            search_type = search_type.split(",")
            joblist = joblist.filter(
                Q(payment_type__type__in=search_type) & (Q(budget__lte=search_hourlymax) & Q(hourly_max__lte=search_hourlymax))).distinct()

        elif serach_all==''  and search_status == '' and search == '' and search_type == '' and search_experiance and search_hourlymin == '' and search_hourlymax:
            search_experiance = search_experiance.split(",")
            joblist = joblist.filter(
                Q(experiance_level__in=search_experiance) & (Q(budget__lte=search_hourlymax) & Q(hourly_max__lte=search_hourlymax))).distinct()

        elif serach_all==''  and search_status and search and search_type == '' and search_experiance == '' and search_hourlymin == '' and search_hourlymax:
            search_status = search_status.split(",")
            search = filter(None, search.split(","))
            joblist = joblist.filter(Q(job_status__status__in=search_status) & (Q(
                job_main_category__id__icontains=search) | Q(
                job_main_category__id__in=search)) & (Q(budget__lte=search_hourlymax) & Q(hourly_max__lte=search_hourlymax))).distinct()

        elif serach_all==''  and search_status == '' and search and search_type and search_experiance == '' and search_hourlymin == '' and search_hourlymax:

            search_type = search_type.split(",")
            search = filter(None, search.split(","))
            joblist = joblist.filter(Q(payment_type__type__in=search_type) & (Q(
                job_main_category__id__icontains=search) | Q(
                job_main_category__id__in=search)) & (Q(budget__lte=search_hourlymax) & Q(hourly_max__lte=search_hourlymax))).distinct()

        elif serach_all==''  and search_status == '' and search == '' and search_type and search_experiance and search_hourlymin == '' and search_hourlymax:
            search_type = search_type.split(",")
            search_experiance = search_experiance.split(",")
            joblist = joblist.filter(
                Q(payment_type__type__in=search_type) & Q(experiance_level__in=search_experiance) & (Q(budget__lte=search_hourlymax) & Q(hourly_max__lte=search_hourlymax))).distinct()

        elif serach_all==''  and search_status and search == '' and search_type and search_experiance == '' and search_hourlymin == '' and search_hourlymax:
            search_status = search_status.split(",")
            search_type = search_type.split(",")
            joblist = joblist.filter(Q(payment_type__type__in=search_type) & Q(job_status__status__in=search_status) & (Q(budget__lte=search_hourlymax) & Q(hourly_max__lte=search_hourlymax))).distinct()


        elif serach_all==''  and search_status == '' and search and search_type == '' and search_experiance and search_hourlymin == '' and search_hourlymax:
            search = filter(None, search.split(","))
            search_experiance = search_experiance.split(",")
            joblist = joblist.filter(Q(experiance_level__in=search_experiance) & (Q(
                job_main_category__id__icontains=search) | Q(
                job_main_category__id__in=search)) & (Q(budget__lte=search_hourlymax) & Q(hourly_max__lte=search_hourlymax))).distinct()



        elif serach_all==''  and search_status and search and search_type == '' and search_experiance and search_hourlymin == '' and search_hourlymax:
            search = filter(None, search.split(","))
            search_status = search_status.split(",")
            search_experiance = search_experiance.split(",")
            joblist = joblist.filter(Q(experiance_level__in=search_experiance) & (Q(
                job_main_category__id__icontains=search) | Q(
                job_main_category__id__in=search)) & Q(job_status__status__in=search_status) & (Q(budget__lte=search_hourlymax) & Q(hourly_max__lte=search_hourlymax))).distinct()




        elif serach_all==''  and search_status == '' and search and search_type and search_experiance and search_hourlymin == '' and search_hourlymax:
            search = filter(None, search.split(","))
            search_type = search_type.split(",")
            search_experiance = search_experiance.split(",")
            joblist = joblist.filter(Q(experiance_level__in=search_experiance) & (Q(
                job_main_category__category_title__icontains=search) | Q(
                job_main_category__category_title__in=search)) & Q(payment_type__type__in=search_type) & (Q(budget__lte=search_hourlymax) & Q(hourly_max__lte=search_hourlymax))).distinct()




        elif serach_all==''  and search_status and search == '' and search_type and search_experiance and search_hourlymin == '' and search_hourlymax:
            search_status = search_status.split(",")
            search_type = search_type.split(",")
            search_experiance = search_experiance.split(",")
            joblist = joblist.filter(Q(experiance_level__in=search_experiance) & Q(job_status__status__in=search_status) & Q(
                payment_type__type__in=search_type) & (Q(budget__lte=search_hourlymax) & Q(hourly_max__lte=search_hourlymax))).distinct()




        elif serach_all==''  and search_status and search and search_type and search_experiance == '' and search_hourlymin == '' and search_hourlymax:
            search_status = search_status.split(",")
            search_type = search_type.split(",")
            search = filter(None, search.split(","))
            joblist = joblist.filter((Q(
                job_main_category__id__icontains=search) | Q(
                job_main_category__id__in=search)) & Q(job_status__status__in=search_status) & Q(
                payment_type__type__in=search_type) & (Q(budget__lte=search_hourlymax) & Q(hourly_max__lte=search_hourlymax))).distinct()




        elif serach_all==''  and search_status and search and search_type and search_experiance and search_hourlymin == '' and search_hourlymax:
            search_status = search_status.split(",")
            search_type = search_type.split(",")
            search = filter(None, search.split(","))
            search_experiance = search_experiance.split(",")
            joblist = joblist.filter((Q(
                job_main_category__id__icontains=search) | Q(
                job_main_category__id__in=search)) & Q(experiance_level__in=search_experiance) & Q(
                job_status__status__in=search_status) & Q(payment_type__type__in=search_type) & (Q(budget__lte=search_hourlymax) & Q(hourly_max__lte=search_hourlymax))).distinct()
            print joblist, "fffffff"



        elif serach_all==''  and search_status and search == '' and search_type and search_experiance and search_hourlymin == '' and search_hourlymax:
            search_status = search_status.split(",")
            search_experiance = search_experiance.split(",")
            joblist = joblist.filter(Q(experiance_level__in=search_experiance) & Q(
                job_status__status__in=search_status) & (Q(budget__lte=search_hourlymax) & Q(hourly_max__lte=search_hourlymax))).distinct()




        elif serach_all==''  and search_status and search and search_type and search_experiance and search_hourlymin and search_hourlymax:
            search = filter(None, search.split(","))
            search_status = search_status.split(",")
            search_type = search_type.split(",")
            search_experiance = search_experiance.split(",")

            joblist = joblist.filter((Q(
                job_main_category__id__icontains=search) | Q(
                job_main_category__id__in=search)) & Q(experiance_level__in=search_experiance) & Q(
                job_status__status__in=search_status) & Q(
                payment_type__type__in=search_type) & ((Q(budget__range=(search_hourlymin, search_hourlymax)))| (Q(hourly_max__lte=search_hourlymax) & Q(hourly_min__gte=search_hourlymin)))).distinct()
            print joblist, "fshibif"


        elif serach_all==''  and search_status and search and search_type and search_experiance and search_hourlymin == '' and search_hourlymax == '':
            search = filter(None, search.split(","))
            search_status = search_status.split(",")
            search_type = search_type.split(",")
            search_experiance = search_experiance.split(",")

            joblist = joblist.filter((Q(
                job_main_category__id__icontains=search) | Q(
                job_main_category__id__in=search)) & Q(experiance_level__in=search_experiance) & Q(
                job_status__status__in=search_status) & Q(
                payment_type__type__in=search_type)).distinct()


        else:

            joblist = joblist
        return joblist

    def get_freelancer_search_query_list(self):
        """
        Get Invitation .
        :return: Invitation
        """

        key_words = []
        freelancers = Freelancer_Profile.objects.all()
        for skill in Skills.objects.all():
            if skill.name not in key_words:
                key_words.append(skill.name)
        for main_category in WorkCategories.objects.all():
            if main_category.category_title not in key_words:
                key_words.append(main_category.category_title)
        for sub_categories in WorkSubCategories.objects.all():
            if sub_categories.sub_category_title not in key_words:
                key_words.append(sub_categories.sub_category_title)
        for freelancer in freelancers:
            if freelancer.user.first_name not in key_words:
                key_words.append(freelancer.user.first_name)
            if freelancer.title not in key_words:
                key_words.append(freelancer.title)

        return key_words
    def get_job_search_query_list(self):
        """
        Get Invitation .
        :return: Invitation
        """

        key_words = []
        jobs = ClientJobs.objects.all()
        for skill in Skills.objects.all():
            if skill.name not in key_words:
                key_words.append(skill.name)
        for main_category in WorkCategories.objects.all():
            if main_category.category_title not in key_words:
                key_words.append(main_category.category_title)
        for sub_categories in WorkSubCategories.objects.all():
            if sub_categories.sub_category_title not in key_words:
                key_words.append(sub_categories.sub_category_title)
        for job in jobs:
            if job.job_title not in key_words:
                key_words.append(job.job_title)
            # if job.job_description not in key_words:
            #     key_words.append(job.job_description)

        return key_words

    def get_current_job(self, slug):

        try:
            job = ClientJobs.objects.get(id=slug)
        except:
            job = ClientJobs.objects.get(slug=slug)
        return job

    def if_job_applied(self, user, job_details):
        """
        Get ApplyJob
        :return: objects of ApplyJob
        """
        try:
            is_user = ApplyJob.objects.get(user=user, job=job_details)
        except:
            is_user = ''

        return is_user
    def if_job_posted(self, user, job_details):
        """
        Get ApplyJob
        :return: objects of ApplyJob
        """
        try:
            is_user = ClientJobs.objects.get(user=user, id=job_details)
        except:
            is_user = ''

        return is_user
    def is_job_invited(self, user, job_details):
        """
        Get ApplyJob
        :return: objects of ApplyJob
        """
        try:
            is_user = JobInvitation.objects.get(user=user, job=job_details)
        except:
            is_user = ''

        return is_user
    def update_status_cancel(self,user,job,status):
        """
        Get quotes cancel status.
        :return:
        """
        try:
           quotes_cancel = ApplyJob.objects.get(user=user,job=job)
           quotes_cancel.status=InvitationStatus.objects.get(status=status)
           quotes_cancel.save()
        except:
            quotes_cancel = None
        try:
           quotes_cancel = JobInvitation.objects.get(user=user,job=job)
           quotes_cancel.status=InvitationStatus.objects.get(status=status)
           quotes_cancel.save()
        except:
            quotes_cancel = None
        print quotes_cancel,"zzzzzzzzzzzzzzzzzz"
        return quotes_cancel
    def is_freelancer_profile(self,user):
        print user, "dfffffffffffffffffff"
        return Freelancer_Profile.objects.get(user=user)
    def is_client_profile(self,user):

        return Client_Profile.objects.get(user=user)
    def is_user(self,user):

        return Users.objects.get(email=user)
    def get_hired_details(self,slug):
        try:
            return HiredJobsContract.objects.get(slug=slug)
        except:
            return HiredJobsContract.objects.get(id=slug)
    def get_hire_status(self,id):
        return ContractStatus.objects.get(id=id)
    def get_milestone_status (self,id):
        try:
            return MileStoneStatus.objects.get(id=id)
        except:
            return MileStoneStatus.objects.get(status=id)

    def get_milstone_details(self,id):


        return JobsMilestone.objects.get(id=id)
    def get_milestone_list(self,id):
        hire = self.get_hired_details(id)
        return hire.milestone.all()
    def get_week_status(self,id):
        try:
            return TimesheetStatus.objects.get(id=id)
        except:
            return TimesheetStatus.objects.get(status=id)
    def get_all_hire(self,id,user):
        return HiredJobsContract.objects.filter(client_job=id,freelancer_profile__user=user)
    def get_all_hire_single(self,id):
            return HiredJobsContract.objects.filter(client_job=id)

    def get_all_notification(self, id,type):

        return Notifications.objects.filter(user__email=id,type__type=type)
    def get_unread_notification(self, id,type):

            return Notifications.objects.filter(user__email=id,status=0,type__type=type)

    def get_no_applied_job(self,freelancer,current_month):

        count = ApplyJob.objects.filter(created_date__month=current_month.month,user=freelancer,).exclude(status__status='Cancelled').count()
        #print count,"county"
        return count