from __future__ import unicode_literals

from django.apps import AppConfig


class FreelancerCompanyConfig(AppConfig):
    name = 'freelancer_company'
