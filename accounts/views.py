import StringIO
import email
import json

import logging
import threading
from urlparse import urlparse

import datetime

from PIL import Image
from django.contrib import messages
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.tokens import default_token_generator
from django.contrib.auth.views import deprecate_current_app
from django.contrib.sites.shortcuts import get_current_site
from django.core.exceptions import ValidationError
from django.core.files.base import ContentFile
from django.core.mail import send_mail
from django.core.validators import validate_email
from django.db.models.query_utils import Q
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.shortcuts import render, render_to_response, redirect, resolve_url
from django.template import Context
from django.template import RequestContext
from django.template import Template
from django.template import loader
from django.template.loader import render_to_string
from django.template.response import TemplateResponse
from django.utils.crypto import get_random_string
from django.utils.encoding import force_bytes
from django.utils.http import is_safe_url, urlsafe_base64_encode, urlsafe_base64_decode
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.debug import sensitive_post_parameters
from django.views.generic import FormView
from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from rolepermissions.decorators import has_role_decorator
from registration.backends.hmac.views import RegistrationView
from django.contrib.auth import (
    REDIRECT_FIELD_NAME, get_user_model, login as auth_login,
    logout as auth_logout, update_session_auth_hash, )
# Create your views here.
from accounts.forms import RegisterForm, LoginForm, ActivationForm, PasswordResetRequestForm, SetPasswordForm
from accounts.models import Users, User_Type
from client.mixins import ClientMixin
from client.models import Client_Profile, ClientJobs, PaymentType, Duration, JobStatus, ApplyJob
from freelancer.mixins import MainMixin
from freelancer.models import WorkCategories, WorkSubCategories, Skills, Freelancer_Profile, HourlyRate, LocationDetails, \
    EmploymentHistory, Certifications, FreelancerPortfolio
from hire.models import ContractStatus, JobsMilestone, MileStoneStatus, TimeWork, TimesheetStatus
from hire.models import HiredJobsContract
from newproject import settings
from newproject.settings import DEFAULT_FROM_EMAIL

logger = logging.getLogger(__name__)
from rolepermissions.roles import assign_role
from rolepermissions.roles import remove_role

from rolepermissions.mixins import HasPermissionsMixin
from rolepermissions.roles import get_user_roles
from django.core.cache import cache
import requests
from cities_light.models import Country, Region, City



def index(request):
    return HttpResponse("Hello, world. You're at the polls index.")
def next_weekday(d, weekday):
    days_ahead = weekday - d.weekday()
    # if days_ahead <= 0: # Target day already happened this week
    #     days_ahead += 7
    return d + datetime.timedelta(days_ahead)



class Home(MainMixin,TemplateView):
    template_name = "accounts/home.html"
    def next_weekday(d, weekday):
        days_ahead = weekday - d.weekday()
        # if days_ahead <= 0: # Target day already happened this week
        #     days_ahead += 7
        return d + datetime.timedelta(days_ahead)

    def get(self, request, *args, **kwargs):
        template_name = "accounts/home.html"
        users =self.get_users_list('0')
        online =self.get_users_list('1')
        jobs =self.get_jobs_list()
        print users.count(),"hjhjh"
        context = {'online': online.count(),'jobs':jobs.count(),'users':users.count(),'key_words_search':self.get_all_freelancer()}

        redirect_field_name = REDIRECT_FIELD_NAME,
        # workcategory = requests.get("http://52.15.241.137/atlancer_wp/api/?func=getWorkCategory").json()
        #
        # for work in workcategory:
        #
        #     workcategory, workcategorys = WorkCategories.objects.get_or_create(category_title= work['category_title'])
        #     workcategory.save()
        # worksubcategoryss = requests.get("http://52.15.241.137/atlancer_wp/api/?func=getWorkSubCategory").json()
        #
        # for sub in worksubcategoryss:
        #     workcategory =WorkCategories.objects.get(category_title=sub['category_title'])
        #     worksubcategory, worksubcategorys = WorkSubCategories.objects.get_or_create(sub_category_title=sub['sub_category_title'],work_category=workcategory)
        #     worksubcategory.save()
        #
        # workskills = requests.get("http://52.15.241.137/atlancer_wp/api/?func=getSkills").json()
        #
        # for skills in workskills:
        #     category =WorkCategories.objects.get(category_title=skills['category_title'])
        #     # print category,"jnmjjjjjjjjjjjjjjj"
        #     skillsobject, skillsss = Skills.objects.get_or_create(name=skills['skills'])
        #     skillsobject.category.add(category)
        #     skillsobject.save()
        # responsess = requests.get("http://52.15.241.137/atlancer_wp/api/?func=getUsers").json()
        #
        # for res in responsess:
        #     print res['user_type'], "typrtypr"
        #     # print res['created_date'],"ddd"
        #     if res['user_type'] == '1':
        #         type ="FREELANCER"
        #     else:
        #         type ="CLIENT"
        #     user_type = User_Type.objects.get(type=type)
        #     # print user_type, "typrtypr"
        #     invite_type = User_Type.objects.get(type='OWNER')
        #     store_corporate,store_corporates=Users.objects.get_or_create(email=res['email'])
        #     if store_corporates:
        #         store_corporate.token=get_random_string(length=32)
        #         store_corporate.first_name=res['first_name']
        #         store_corporate.last_name=res['last_name']
        #         store_corporate.active_account=user_type
        #         store_corporate.user_type=user_type
        #         store_corporate.invite_type=invite_type
        #         store_corporate.set_password('A!sgiwyGek#269')
        #         store_corporate.save()
        #     user = Users.objects.get(email=res['email'])
        #     if res['user_type'] == '1':
        #       url = "http://52.15.241.137/atlancer_wp/api?func=getFreelancerProfile&email="+res['email']
        #       # print url
        #       details = requests.get(url).json()
        #
        #
        #       user = Users.objects.get(email=res['email'])
        #       profile, profiles = Freelancer_Profile.objects.get_or_create(user=user)
        #       profile.title = details['title']
        #       if details['profile']:
        #           path = details['profile']
        #           domain, paths = split_url(path)
        #           filename = get_url_tail(paths)
        #           fobject = retrieve_image(path)
        #           pil_image = Image.open(fobject)
        #           django_file = pil_to_django(pil_image)
        #           profile.profile.save(filename, django_file)
        #
        #       profile.about = details['about']
        #       if details['categoryname'] :
        #           workcategory = WorkCategories.objects.get(category_title=details['categoryname'])
        #           profile.workcategory = workcategory
        #       profile.save()
        #       if details['skills'] :
        #           self.delete_existing_skill(user)
        #
        #
        #           for skills in Skills.objects.filter(name__in=[details['skills']],category=workcategory):
        #
        #               profile.skills.add(skills)
        #               profile.save()
        #       if details['subcategory'] :
        #           print details['subcategory'], "sdddddddd"
        #           print WorkSubCategories.objects.filter(sub_category_title__in=[details['subcategory']],work_category=workcategory), "sdddddddd"
        #           self.delete_existing_subcategory(user)
        #
        #           for subcategorys in WorkSubCategories.objects.filter(sub_category_title__in=[details['subcategory']],work_category=workcategory):
        #               print subcategorys,"dd"
        #               profile.subcategory.add(subcategorys)
        #               profile.save()
        #       url = "http://52.15.241.137/atlancer_wp/api?func=getHourlyrate&email=" + res['email']
        #
        #       details = requests.get(url).json()
        #       if details['hourly_rate']:
        #           hourly, hourlys = HourlyRate.objects.get_or_create(user=user)
        #           hourly.hourly_rate=details['hourly_rate']
        #           hourly.actual_rate=details['actual_rate']
        #           hourly.save()
        #       url = "http://52.15.241.137/atlancer_wp/api?func=getLocation&email=" + res['email']
        #
        #       details = requests.get(url).json()
        #       if details['address'] or details['country'] or details['zip_code']:
        #           location, locations = LocationDetails.objects.get_or_create(user=user)
        #           if details['country'] :
        #               location.country = Country.objects.get(name=details['country'])
        #           if details['state']:
        #               location.state = Region.objects.get(name=details['state'],country__name=details['country'])
        #           if details['city']:
        #               print details['city'],"details['city']details['city']details['city']"
        #               try:
        #                  location.city = City.objects.get(name=details['city'],region__name=details['state'])
        #               except:
        #                  location.city = City.objects.get(name=details['city'])
        #
        #           if details['address']:
        #               location.address = details['address']
        #
        #           if details['zip_code']:
        #               location.zip_code = details['zip_code']
        #           location.save()
        #       url = "http://52.15.241.137/atlancer_wp/api/?func=getExperienceLevel&email=" + res['email']
        #       details = requests.get(url).json()
        #       if details['exp_level_type']:
        #           exp,exps  = LocationDetails.objects.get_or_create(user=user)
        #           exp.exp_level_type = details['exp_level_type']
        #           exp.save()
        #       url = "http://52.15.241.137/atlancer_wp/api/?func=getEmpDetails&email=" + res['email']
        #       details = requests.get(url).json()
        #       user.employment_history.all().delete()
        #       for employment in details:
        #           if employment['company_name']:
        #
        #               getemploymentHistory = EmploymentHistory.objects.create(user=user,
        #                                                                       company_name=employment[
        #                                                                           'company_name'],
        #                                                                       role=employment['role'],
        #                                                                       description=employment[
        #                                                                           'description'])
        #
        #               if employment['start_date']:
        #                   start_dates = datetime.datetime.strptime(str(employment['start_date']), "%Y-%m-%d")
        #                   getemploymentHistory.start_date = start_dates
        #                   getemploymentHistory.save()
        #
        #               if employment['end_date']:
        #                   end_dates = datetime.datetime.strptime(str(employment['end_date']), "%Y-%m-%d")
        #
        #                   getemploymentHistory.end_date = end_dates
        #                   getemploymentHistory.save()
        #           url = "http://52.15.241.137/atlancer_wp/api/?func=getCertDetails&email=" + res['email']
        #           details = requests.get(url).json()
        #           user.cerifications.all().delete()
        #           for certification in details:
        #               if certification['certificate_name']:
        #
        #                   getcertification = Certifications.objects.create(user=user,
        #                                                                    certificate_name=certification[
        #                                                                        'certificate_name'],
        #                                                                    issued_by=certification['issued_by']
        #                                                                    )
        #                   if certification['valid_from'] != '':
        #                       start = datetime.datetime.strptime(str(certification['valid_from']), "%Y-%m-%d")
        #                       getcertification.valid_from = start
        #                       getcertification.save()
        #
        #                   if certification['end_to'] != '':
        #                       end_date = datetime.datetime.strptime(str(certification['end_to']), "%Y-%m-%d")
        #                       getcertification.end_to = end_date
        #                       getcertification.save()
        #           url = "http://52.15.241.137/atlancer_wp/api/?func=getPortfolioDetails&email=" + res['email']
        #           details = requests.get(url).json()
        #           user.portfolio.all().delete()
        #
        #           for portfolio in details:
        #               # print "dffdf",portfolio['filess']
        #               print portfolio
        #               print "ssssss",portfolio['project_name']
        #
        #               if portfolio['project_name']!= '':
        #
        #                   freelancer_object = FreelancerPortfolio.objects.create(user=user,
        #                                                                          project_name=portfolio['project_name'],
        #                                                                          description=portfolio['description']
        #                                                                          )
        #                   if portfolio['categoryname']:
        #
        #                       workcategory = WorkCategories.objects.get(category_title=portfolio['categoryname'])
        #                       freelancer_object.work_category = workcategory
        #                       freelancer_object.save()
        #                   if portfolio['project_url'] != '' or portfolio['project_url'] != '0':
        #                       freelancer_object.project_url = portfolio['project_url']
        #                       freelancer_object.save()
        #                   if portfolio['skills']:
        #                       print portfolio['skills']
        #                       print Skills.objects.filter(name__in=[portfolio['skills']],category=workcategory), "skillskillskillskillskillskill"
        #
        #                       for skill in Skills.objects.filter(name__in=portfolio['skills'],category=workcategory):
        #                           print skill,"skillskillskillskillskillskill"
        #                           freelancer_object.skills.add(skill)
        #                   if portfolio['subcategory']:
        #                       for sub_category in WorkSubCategories.objects.filter(
        #                               sub_category_title__in=[portfolio['subcategory']],work_category=workcategory):
        #                           freelancer_object.sub_category.add(sub_category)
        #
        #
        #
        #
        #
        #     else:
        #
        #         url ="http://52.15.241.137/atlancer_wp/api?func=getclientprofile&email="+res['email']
        #         res = requests.get(url).json()
        #
        #
        #         client_object, clientr_objects = Client_Profile.objects.get_or_create(user=user)
        #         if client_object:
        #             print res['profile'],"ccccccccccccccccccccccccc"
        #             if res['profile']:
        #                 path = res['profile']
        #                 domain, paths = split_url(path)
        #                 filename = get_url_tail(paths)
        #                 fobject = retrieve_image(path)
        #                 pil_image = Image.open(fobject)
        #                 django_file = pil_to_django(pil_image)
        #                 client_object.profile.save(filename, django_file)
        # url = "http://52.15.241.137/atlancer_wp/api/?func=getJobAssignedFreelancer&email=josuemann@gmail.com"
        # url = "http://52.15.241.137/atlancer_wp/api/?func=getJobAssignedFreelancer&email=kalebkyle@gmail.com"
        # res = requests.get(url).json()
        # for result in res:
        #     user = Users.objects.get(email=result['job_posted'])
        #     job_title = result['job_title']
        #     job_description = result['job_desc']
        #     hourly_rate = result['hourly_rate']
        #     #job_main_category = WorkCategories.objects.get(id=result['category_id'])
        #     job_main_category = WorkCategories.objects.get(id=11)
        #     payment_type =  PaymentType.objects.get(id=result['payment_id'])
        #     duration =  Duration.objects.get(id=result['duration_id'])
        #
        #
        #     job_object,job = ClientJobs.objects.get_or_create(user=user,job_title=job_title,job_main_category=job_main_category,payment_type=payment_type)
        #     if result['payment_id'] == '2':
        #         print "fdf",result['payment_id']
        #         job_object.budget=result['budget']
        #     if result['payment_id'] == '1':
        #         hourly = HourlyRate.objects.get(user__email=result['job_assigned'])
        #
        #         if hourly.hourly_rate:
        #             job_object.hourly_max=hourly.hourly_rate
        #             job_object.hourly_min=float(hourly.hourly_rate) - float(5)
        #     job_object.duration=duration
        #     job_object.job_description=job_description
        #     job_object.experiance_level=result['experience_level']
        #     #job_object.experiance_level='INTERMEDIATE'
        #     job_object.job_status=JobStatus.objects.get(status='Publish')
        #     #job_object.job_close_date=result['closing_date']
        #     job_object.job_close_date=datetime.datetime.strptime(str(result['closing_date']), "%Y-%m-%d %H:%M:%S")
        #     job_object.verification_status=0
        #     job_object.no_of_hires=result['no_of_hires']
        #     job_object.created_user=result['created_user']
        #     job_object.save();
        #     freelancer_user = Freelancer_Profile.objects.get(user__email=result['job_assigned'])
        #     freelancer_users = Users.objects.get(email=result['job_assigned'])
        #
        #     if job_object:
        #         ApplyJob.objects.get_or_create(user=freelancer_user,job=job_object,created_by=freelancer_users)
        #
        #     user = Users.objects.get(email=result['job_posted'])
        #     print user, "nnnnnnnnn"
        #     client_profile,ff =Client_Profile.objects.get_or_create(user=user)
        #     hire, hires = HiredJobsContract.objects.get_or_create(client_profile=client_profile,
        #                                                           freelancer_profile=freelancer_user, client_job=job_object)
        #     hire.job_title = job_object.job_title
        #     hire.job_description = job_object.job_description
        #     hire.status =  ContractStatus.objects.get(id=3)
        #     if job_object.payment_type.id == 1:
        #         hire.payment_type =payment_type
        #         # hire.hourly_max = jobdetails.hourly_max
        #         # hire.hourly_min = jobdetails.hourly_min
        #         hire.budget = hourly_rate
        #
        #     if job_object.payment_type.id == 2:
        #         hire.payment_type = payment_type
        #         hire.budget = job_object.budget
        #
        #     #hire.created_by = request.user
        #     # hire.status =''
        #     hire.approved_date=result['job_startdate']
        #     hire.save()
        #     if job_object.payment_type.id == 2:
        #         JobsMilestones,JobsMilestonee=JobsMilestone.objects.get_or_create(milestone_title=result['milestone_title'],milestone_description=result['milestone_desc'],
        #                                             start_date=datetime.datetime.strptime(str(result['job_startdate']), "%Y-%m-%d %H:%M:%S"),close_date=datetime.datetime.strptime(str(result['job_enddate']), "%Y-%m-%d %H:%M:%S"),status=MileStoneStatus.objects.get(status='Payout Requested'))
        #         hire.milestone.add(JobsMilestones)
        #     if job_object.payment_type.id == 1:
        #         start_date = datetime.datetime.strptime(str(result['job_startdate']), "%Y-%m-%d %H:%M:%S")
        #
        #         work_first_monday = start_date - datetime.timedelta(days=start_date.weekday())
        #         today = datetime.datetime.today()
        #         print today, "hjhjstart_datestart_date"
        #         work_last_monday = next_weekday(today, 0)
        #         print work_last_monday.date()
        #         print work_first_monday.date()
        #
        #         if work_last_monday.date() >= work_first_monday.date():
        #             # dates = week_range(work_first_monday)
        #             # print "***", dates
        #
        #             time_work, time_works = TimeWork.objects.get_or_create(contract=hire,
        #                                                                    start_date=work_first_monday,
        #                                                                    end_date=work_first_monday + datetime.timedelta(
        #                                                                        days=6))
        #
        #
        #             time_work.status = TimesheetStatus.objects.get(status='Payout Requested')
        #             # time_work.week_sum =str(result['total_hours'])+str("h ")+str("0")+str("m")
        #             time_work.time = result['total_hours']
        #             time_work.save()
        #             print "dfdfsucessss"
        # ////////////////////////////////////
        # workcategory = requests.get("http://52.15.241.137/atlancer_wp/api/?func=getWorkCategory").json()
        #
        # for work in workcategory:
        #
        #     workcategory, workcategorys = WorkCategories.objects.get_or_create(category_title= work['category_title'])
        #     workcategory.save()
        # worksubcategoryss = requests.get("http://52.15.241.137/atlancer_wp/api/?func=getWorkSubCategory").json()
        #
        # for sub in worksubcategoryss:
        #     workcategory =WorkCategories.objects.get(category_title=sub['category_title'])
        #     worksubcategory, worksubcategorys = WorkSubCategories.objects.get_or_create(sub_category_title=sub['sub_category_title'],work_category=workcategory)
        #     worksubcategory.save()
        #
        # workskills = requests.get("http://52.15.241.137/atlancer_wp/api/?func=getSkills").json()
        #
        # for skills in workskills:
        #     category =WorkCategories.objects.get(category_title=skills['category_title'])
        #     # print category,"jnmjjjjjjjjjjjjjjj"
        #     skillsobject, skillsss = Skills.objects.get_or_create(name=skills['skills'])
        #     skillsobject.category.add(category)
        #     skillsobject.save()
        #
        #
        #
        # responsess = requests.get("http://52.15.241.137/atlancer_wp/api/?func=getUsers").json()
        #
        # for res in responsess:
        #     print res['email'], "usersuser"
        #     # print res['created_date'],"ddd"
        #     if res['user_type'] == '1':
        #         type ="FREELANCER"
        #     else:
        #         type ="CLIENT"
        #     user_type = User_Type.objects.get(type=type)
        #     # print user_type, "typrtypr"
        #     invite_type = User_Type.objects.get(type='OWNER')
        #     store_corporate,store_corporates=Users.objects.get_or_create(email=res['email'])
        #     if store_corporates:
        #         store_corporate.token=get_random_string(length=32)
        #         store_corporate.first_name=res['first_name']
        #         store_corporate.last_name=res['last_name']
        #         store_corporate.active_account=user_type
        #         store_corporate.user_type=user_type
        #         store_corporate.invite_type=invite_type
        #         store_corporate.set_password('A!sgiwyGek#269')
        #         store_corporate.save()
        #     user = Users.objects.get(email=res['email'])
        #     if res['user_type'] == '1':
        #       url = "http://52.15.241.137/atlancer_wp/api?func=getFreelancerProfile&email="+res['email']
        #       # print url
        #       details = requests.get(url).json()
        #
        #
        #       user = Users.objects.get(email=res['email'])
        #       profile, profiles = Freelancer_Profile.objects.get_or_create(user=user)
        #       profile.title = details['title']
        #       if details['profile']:
        #           path = details['profile']
        #           domain, paths = split_url(path)
        #           filename = get_url_tail(paths)
        #           fobject = retrieve_image(path)
        #           pil_image = Image.open(fobject)
        #           django_file = pil_to_django(pil_image)
        #           profile.profile.save(filename, django_file)
        #
        #       profile.about = details['about']
        #       if details['categoryname'] :
        #           workcategory = WorkCategories.objects.get(category_title=details['categoryname'])
        #           profile.workcategory = workcategory
        #       profile.save()
        #       if details['skills'] :
        #           self.delete_existing_skill(user)
        #
        #
        #           for skills in Skills.objects.filter(name__in=[details['skills']],category=workcategory):
        #
        #               profile.skills.add(skills)
        #               profile.save()
        #       if details['subcategory'] :
        #           print details['subcategory'], "sdddddddd"
        #           print WorkSubCategories.objects.filter(sub_category_title__in=[details['subcategory']],work_category=workcategory), "sdddddddd"
        #           self.delete_existing_subcategory(user)
        #
        #           for subcategorys in WorkSubCategories.objects.filter(sub_category_title__in=[details['subcategory']],work_category=workcategory):
        #               print subcategorys,"dd"
        #               profile.subcategory.add(subcategorys)
        #               profile.save()
        #       url = "http://52.15.241.137/atlancer_wp/api?func=getHourlyrate&email=" + res['email']
        #
        #       details = requests.get(url).json()
        #       if details['hourly_rate']:
        #           hourly, hourlys = HourlyRate.objects.get_or_create(user=user)
        #           hourly.hourly_rate=details['hourly_rate']
        #           hourly.actual_rate=details['actual_rate']
        #           hourly.save()
        #       url = "http://52.15.241.137/atlancer_wp/api?func=getLocation&email=" + res['email']
        #
        #       details = requests.get(url).json()
        #       if details['address'] or details['country'] or details['zip_code']:
        #           location, locations = LocationDetails.objects.get_or_create(user=user)
        #           if details['country'] :
        #               try:
        #                   location.country = Country.objects.get(name=details['country'])
        #               except:
        #                   print "no country"
        #
        #           if details['state']:
        #               try:
        #                   location.state = Region.objects.get(name=details['state'], country__name=details['country'])
        #               except:
        #                   print "no country"
        #
        #           if details['city']:
        #               print details['city'],"details['city']details['city']details['city']"
        #               try:
        #                  location.city = City.objects.get(name=details['city'],region__name=details['state'])
        #               except:
        #                   try:
        #                     location.city = City.objects.get(name=details['city'])
        #                   except:
        #                       print "df"
        #
        #
        #           if details['address']:
        #               location.address = details['address']
        #
        #           if details['zip_code']:
        #               location.zip_code = details['zip_code']
        #           location.save()
        #       url = "http://52.15.241.137/atlancer_wp/api/?func=getExperienceLevel&email=" + res['email']
        #       details = requests.get(url).json()
        #       if details['exp_level_type']:
        #           exp,exps  = LocationDetails.objects.get_or_create(user=user)
        #           exp.exp_level_type = details['exp_level_type']
        #           exp.save()
        #       url = "http://52.15.241.137/atlancer_wp/api/?func=getEmpDetails&email=" + res['email']
        #       details = requests.get(url).json()
        #       user.employment_history.all().delete()
        #       for employment in details:
        #           if employment['company_name']:
        #
        #               getemploymentHistory = EmploymentHistory.objects.create(user=user,
        #                                                                       company_name=employment[
        #                                                                           'company_name'],
        #                                                                       role=employment['role'],
        #                                                                       description=employment[
        #                                                                           'description'])
        #
        #               if employment['start_date']:
        #                   start_dates = datetime.datetime.strptime(str(employment['start_date']), "%Y-%m-%d")
        #                   getemploymentHistory.start_date = start_dates
        #                   getemploymentHistory.save()
        #
        #               if employment['end_date']:
        #                   end_dates = datetime.datetime.strptime(str(employment['end_date']), "%Y-%m-%d")
        #
        #                   getemploymentHistory.end_date = end_dates
        #                   getemploymentHistory.save()
        #           url = "http://52.15.241.137/atlancer_wp/api/?func=getCertDetails&email=" + res['email']
        #           details = requests.get(url).json()
        #           user.cerifications.all().delete()
        #           for certification in details:
        #               if certification['certificate_name']:
        #
        #                   getcertification = Certifications.objects.create(user=user,
        #                                                                    certificate_name=certification[
        #                                                                        'certificate_name'],
        #                                                                    issued_by=certification['issued_by']
        #                                                                    )
        #                   if certification['valid_from'] != '':
        #                       start = datetime.datetime.strptime(str(certification['valid_from']), "%Y-%m-%d")
        #                       getcertification.valid_from = start
        #                       getcertification.save()
        #
        #                   if certification['end_to'] != '':
        #                       end_date = datetime.datetime.strptime(str(certification['end_to']), "%Y-%m-%d")
        #                       getcertification.end_to = end_date
        #                       getcertification.save()
        #           url = "http://52.15.241.137/atlancer_wp/api/?func=getPortfolioDetails&email=" + res['email']
        #           details = requests.get(url).json()
        #           user.portfolio.all().delete()
        #
        #           for portfolio in details:
        #               # print "dffdf",portfolio['filess']
        #               print portfolio
        #               print "ssssss",portfolio['project_name']
        #
        #               if portfolio['project_name']!= '':
        #
        #                   freelancer_object = FreelancerPortfolio.objects.create(user=user,
        #                                                                          project_name=portfolio['project_name'],
        #                                                                          description=portfolio['description']
        #                                                                          )
        #                   if portfolio['categoryname']:
        #
        #                       workcategory = WorkCategories.objects.get(category_title=portfolio['categoryname'])
        #                       freelancer_object.work_category = workcategory
        #                       freelancer_object.save()
        #                   if portfolio['project_url'] != '' or portfolio['project_url'] != '0':
        #                       freelancer_object.project_url = portfolio['project_url']
        #                       freelancer_object.save()
        #                   if portfolio['skills']:
        #                       print portfolio['skills']
        #                       print Skills.objects.filter(name__in=[portfolio['skills']],category=workcategory), "skillskillskillskillskillskill"
        #
        #                       for skill in Skills.objects.filter(name__in=portfolio['skills'],category=workcategory):
        #                           print skill,"skillskillskillskillskillskill"
        #                           freelancer_object.skills.add(skill)
        #                   if portfolio['subcategory']:
        #                       for sub_category in WorkSubCategories.objects.filter(
        #                               sub_category_title__in=[portfolio['subcategory']],work_category=workcategory):
        #                           freelancer_object.sub_category.add(sub_category)
        #
        #
        #
        #
        #
        #     else:
        #
        #         url ="http://52.15.241.137/atlancer_wp/api?func=getclientprofile&email="+res['email']
        #         res = requests.get(url).json()
        #
        #
        #         client_object, clientr_objects = Client_Profile.objects.get_or_create(user=user)
        #         if client_object:
        #             print res['profile'],"ccccccccccccccccccccccccc"
        #             if res['profile']:
        #                 path = res['profile']
        #                 domain, paths = split_url(path)
        #                 filename = get_url_tail(paths)
        #                 fobject = retrieve_image(path)
        #                 pil_image = Image.open(fobject)
        #                 django_file = pil_to_django(pil_image)
        #                 client_object.profile.save(filename, django_file)
        # url = "http://52.15.241.137/atlancer_wp/api/?func=getJobAssignedFreelancer&email=josuemann@gmail.com"
        # s = ClientJobs.objects.all().delete()
        # print s, "dfdf"
        # url = "http://52.15.241.137/atlancer_wp/api/?func=getPostedJobs"
        # res = requests.get(url).json()
        # for result in res:
        #     user = Users.objects.get(email=result['job_posted'])
        #     job_title = result['job_title']
        #     job_description = result['job_desc']
        #     hourly_rate = result['hourly_rate']
        #     job_main_category = WorkCategories.objects.get(category_title=result['category_id'])
        #     #job_main_category = WorkCategories.objects.get(id=11)
        #     payment_type =  PaymentType.objects.get(id=result['payment_id'])
        #     duration =  Duration.objects.get(id=result['duration_id'])
        #
        #
        #     job_object,job = ClientJobs.objects.get_or_create(user=user,job_title=job_title,job_main_category=job_main_category,payment_type=payment_type)
        #     if result['payment_id'] == '2':
        #         print "fdf",result['payment_id']
        #         job_object.budget=result['budget']
        #     if result['payment_id'] == '1':
        #         hourly = HourlyRate.objects.get(user__email=result['job_assigned'])
        #
        #         if hourly.hourly_rate:
        #             job_object.hourly_max=hourly.hourly_rate
        #             job_object.hourly_min=float(hourly.hourly_rate) - float(5)
        #     job_object.duration=duration
        #     job_object.job_description=job_description
        #     job_object.experiance_level=result['experience_level']
        #     #job_object.experiance_level='INTERMEDIATE'
        #     job_object.job_status=JobStatus.objects.get(status='Publish')
        #     #job_object.job_close_date=result['closing_date']
        #     job_object.job_close_date=datetime.datetime.strptime(str(result['closing_date']), "%Y-%m-%d %H:%M:%S")
        #     job_object.verification_status=0
        #     job_object.no_of_hires=result['no_of_hires']
        #     job_object.created_user=result['created_user']
        #     #job_object.slug=get_random_string(length=32)
        #     job_object.save();
        #     freelancer_user = Freelancer_Profile.objects.get(user__email=result['job_assigned'])
        #     freelancer_users = Users.objects.get(email=result['job_assigned'])
        #
        #     if job_object:
        #         ApplyJob.objects.get_or_create(user=freelancer_user,job=job_object,created_by=freelancer_users)
        #
        #     user = Users.objects.get(email=result['job_posted'])
        #     print user, "nnnnnnnnn"
        #     client_profile,ff =Client_Profile.objects.get_or_create(user=user)
        #     hire, hires = HiredJobsContract.objects.get_or_create(client_profile=client_profile,
        #                                                           freelancer_profile=freelancer_user, client_job=job_object)
        #     hire.job_title = job_object.job_title
        #     hire.job_description = job_object.job_description
        #     hire.status =  ContractStatus.objects.get(id=3)
        #     #hire.slug = get_random_string(length=32)
        #     if job_object.payment_type.id == 1:
        #         hire.payment_type =payment_type
        #         # hire.hourly_max = jobdetails.hourly_max
        #         # hire.hourly_min = jobdetails.hourly_min
        #         hire.budget = hourly_rate
        #
        #     if job_object.payment_type.id == 2:
        #         hire.payment_type = payment_type
        #         hire.budget = job_object.budget
        #
        #     #hire.created_by = request.user
        #     # hire.status =''
        #     hire.approved_date=result['job_startdate']
        #     hire.save()
        #     if job_object.payment_type.id == 2:
        #         JobsMilestones,JobsMilestonee=JobsMilestone.objects.get_or_create(milestone_title=result['milestone_title'],milestone_description=result['milestone_desc'],
        #                                             start_date=datetime.datetime.strptime(str(result['job_startdate']), "%Y-%m-%d %H:%M:%S"),close_date=datetime.datetime.strptime(str(result['job_enddate']), "%Y-%m-%d %H:%M:%S"),status=MileStoneStatus.objects.get(status='Payout Requested'))
        #         hire.milestone.add(JobsMilestones)
        #     if job_object.payment_type.id == 1:
        #         start_date = datetime.datetime.strptime(str(result['job_startdate']), "%Y-%m-%d %H:%M:%S")
        #
        #         work_first_monday = start_date - datetime.timedelta(days=start_date.weekday())
        #         today = datetime.datetime.today()
        #         print today, "hjhjstart_datestart_date"
        #         work_last_monday = next_weekday(today, 0)
        #         print work_last_monday.date()
        #         print work_first_monday.date()
        #
        #         if work_last_monday.date() >= work_first_monday.date():
        #             # dates = week_range(work_first_monday)
        #             # print "***", dates
        #
        #             time_work, time_works = TimeWork.objects.get_or_create(contract=hire,
        #                                                                    start_date=work_first_monday,
        #                                                                    end_date=work_first_monday + datetime.timedelta(
        #                                                                        days=6))
        #
        #
        #             time_work.status = TimesheetStatus.objects.get(status='Payout Requested')
        #             # time_work.week_sum =str(result['total_hours'])+str("h ")+str("0")+str("m")
        #             time_work.time = result['total_hours']
        #             time_work.save()
        #             print "dfdfsucessss"
        # ClientJobs.objects.all().update(job_close_date=datetime.datetime.strptime(str("10/12/2019"), "%m/%d/%Y"))








        if request.method == 'GET' and request.user.is_authenticated() and request.user.user_type != 'Site_admin' and not request.user.is_superuser:
            # return redirect('/dashboard')

            if request.user.active_account.type == 'CLIENT':

                return redirect('/client-profile')



            else:
                if request.user.last_login == None:

                    return redirect('/freelancer-profile')
                else:
                    return redirect('/freelancer-profile-view')

        redirect_to = request.POST.get(redirect_field_name,
                                       request.GET.get(redirect_field_name, ''))

        return TemplateResponse(request, template_name,context)

def retrieve_image(url):
    response = requests.get(url)
    return StringIO.StringIO(response.content)
def pil_to_django(image, format="JPEG"):
    # http://stackoverflow.com/questions/3723220/how-do-you-convert-a-pil-image-to-a-django-file
    fobject = StringIO.StringIO()
    if image.mode != "RGB":
        image = image.convert("RGB")
    image.save(fobject, format="JPEG")
    return ContentFile(fobject.getvalue())
def split_url(url):
    parse_object = urlparse(url)
    return parse_object.netloc, parse_object.path

def get_url_tail(url):
    return url.split('/')[-1]

class Dashboard(HasPermissionsMixin, TemplateView):
    required_permission = 'View_Dashboard_Client'
    template_name = 'accounts/dashboard.html'


def logout(request, next_page=None,
           template_name='registration/logged_out.html',
           redirect_field_name=REDIRECT_FIELD_NAME,
           current_app=None, extra_context=None):
    """
    Logs out the user and displays 'You are logged out' message.
    """

    if request.user.is_authenticated():
        user_obj = Users.objects.get(email=request.user.email)
        user_obj.is_online = False
        user_obj.save()

        auth_logout(request)

        if next_page is not None:
            next_page = resolve_url(next_page)

        if (redirect_field_name in request.POST or redirect_field_name in request.GET):
            next_page = request.POST.get(redirect_field_name,request.GET.get(redirect_field_name))
            # Security check -- don't allow redirection to a different host.
            if not is_safe_url(url=next_page, host=request.get_host()):
                next_page = request.path

        if next_page:
            # Redirect to this page until the session has been cleared.
            return HttpResponseRedirect(next_page)

        current_site = get_current_site(request)
        context = {
            'site': current_site,
            'site_name': current_site.name,
            'title': _('Logged out')
        }
        if extra_context is not None:
            context.update(extra_context)

        if current_app is not None:
            request.current_app = current_app

        return TemplateResponse(request, template_name, context)
    else:
        return redirect('/')


class Registers(RegistrationView):
    disallowed_url = 'registration_disallowed'
    template_name = 'accounts/registration.html'
    form_class = RegisterForm
    success_url = '/'

    def form_valid(self, form):
        email = form.cleaned_data['email']
        # user = form.save()

        self.create_inactive_user(form)
        if self.request.is_ajax():
            payload = {'status': 'success','message': 'Successfully resgistered!! Login your mail to Activate the account',
                       'data': 'g'}
            return HttpResponse(json.dumps(payload),content_type='application/json')
        else:
            return super(Registers, self).form_valid(form)

    def form_invalid(self, form, *args, **kwargs):
        """
        The Form is invalid
        """
        # form.save()

        self.message = "Validation failed."
        self.data = form.errors
        self.success = False

        payload = {'status': 'failed', 'message': self.message, 'data': self.data}

        if self.request.is_ajax():
            return HttpResponse(json.dumps(payload),content_type='application/json')
        else:
            return super(Registers, self).form_invalid(form, *args, **kwargs)

    def create_inactive_user(self, form):
        """
        Create the inactive user account and send an email containing
        activation instructions.
        """
        password = form.cleaned_data['password1']
        user_type = self.request.POST['user_type']
        username = form.cleaned_data['email']
        new_user = form.save(commit=False)
        new_user.set_password(password)
        new_user.set_password(password)
        #new_user.username = username
        #new_user.token = get_random_string(length=32)
        new_user.is_active = False
        new_user.user_type = User_Type.objects.get(type=user_type)
        new_user.invite_type = User_Type.objects.get(type='OWNER')
        # new_user.user_status = '1'
        new_user.save()
        user = Users.objects.get(email=username)
        if user_type == 'CLIENT':
            assign_role(user, 'client')
        else:
            assign_role(user, 'freelancer')

        # print new_user.username
        activation_key = self.get_activation_key(new_user)
        # print activation_key
        context = self.get_email_context(activation_key)

        context['names'] = activation_key
        context['firstname'] = new_user.first_name

        msg_plain = render_to_string('registration/activation_email.txt')
        msg_html = render_to_string('registration/activation_template.html', context)

        def Email():
            """

            :return:
            """
            send_mail(
                'Atlancer Account Activation',
                msg_plain,
                'atlancer@sender.com',
                [new_user.email],
                html_message=msg_html,
            )

        send_user_mail = threading.Thread(name='Email',target=Email)
        send_user_mail.start()



# def login(request):
#     if request.method == 'POST':
#
#         form = AuthenticationForm(request.POST)
#         if form.is_valid():
#             # user = Registration.objects.create(
#             # username=form.cleaned_data['username'],
#             # password=form.cleaned_data['password'],
#             # firstname=form.cleaned_data['firstname'],
#             # lastname=form.cleaned_data['lastname'],
#             # email=form.cleaned_data['email']
#             #  )
#             print form.errors
#            # form.save()
#         else:
#             #logger.debug("Login failed from " + request.META.get('REMOTE_ADDR'))
#             print "ds"
#
#
#     else:
#         form = AuthenticationForm
#
#     return render(request,'accounts/login.html',{'form': form})
@deprecate_current_app
@sensitive_post_parameters()
@csrf_protect
@never_cache
def login(request, template_name='accounts/login.html',
          redirect_field_name=REDIRECT_FIELD_NAME,
          authentication_form=LoginForm,
          extra_context=None):
    """    Displays the login form and handles the login action.    """

    if request.method == 'GET' and request.user.is_authenticated() and request.user.user_type != 'Site_admin' and not request.user.is_superuser:
        # return redirect('/dashboard')

        if request.user.user_type == 'CLIENT':

            return redirect('/client-profile')

        else:
            if request.user.last_login == None:

                return redirect('/freelancer-profile')
            else:
                return redirect('/freelancer-profile-view')

    redirect_to = request.POST.get(redirect_field_name,request.GET.get(redirect_field_name, ''))
    if request.method == "POST":
        form = authentication_form(request, data=request.POST)

        if form.is_valid():

            if not is_safe_url(url=redirect_to, host=request.get_host()):
                redirect_to = resolve_url(settings.LOGIN_REDIRECT_URL)
            user_obj = form.get_user()
            last_login = user_obj.last_login
            if user_obj.user_type != 'Site_admin' or not user_obj.is_superuser:
                auth_login(request, form.get_user())

            cache.set('user_details', user_obj.email)

            user = Users.objects.get(email=user_obj.email)
            if request.POST.get('rememberme') == 'on':
                settings.SESSION_SAVE_EVERY_REQUEST = False
                settings.SESSION_EXPIRE_AT_BROWSER_CLOSE = False

            if user_obj.user_type.type == 'CLIENT':

                user_obj.active_account = User_Type.objects.get(type='CLIENT')
                user_obj.token =get_random_string(length=32)
                user_obj.is_online=True
                user_obj.save()

                remove_role(user, 'client')
                assign_role(user, 'client')
                client_object, clientr_objects = Client_Profile.objects.get_or_create(user=request.user)
                return redirect('/client-profile')
            else:

                #print user_obj.active_account,"profillllllllllllllllllllllllllll"
                user_obj.active_account =  User_Type.objects.get(type='FREELANCER')
                user_obj.token = get_random_string(length=32)
                user_obj.is_online = True
                user_obj.save()
                remove_role(user, 'freelancer')
                assign_role(user, 'freelancer')

                if request.user.user_detail.all() or request.user.employment_history.all() or request.user.portfolio.all() or request.user.cerifications.all():

                    return redirect('/freelancer-profile-view')
                else:
                    return redirect('/freelancer-profile')

                    # return redirect('/dashboard')
        else:
            print form.errors

            logger.debug("Login failed from " + request.META.get('REMOTE_ADDR'))
    else:

        form = authentication_form(request)
    current_site = get_current_site(request)
    context = {'form': form, redirect_field_name: redirect_to, 'site': current_site,
               'site_name': current_site.name, }
    if extra_context is not None:
        context.update(extra_context)
    return TemplateResponse(request, template_name, context)




class CompanyActivate(TemplateView):


    def post(self, request, *args, **kwargs):
        method = request.POST['method']
        if method == 'Update_Client_Company_User':
            #if request.POST['first_name'] and request.POST['last_name'] and request.POST['password']:
                if Users.objects.filter(token=kwargs.get('token')):
                    client_object,client_objects = Users.objects.get_or_create(token=kwargs.get('token'))
                    # client_object.first_name = request.POST['first_name']
                    # client_object.last_name = request.POST['last_name']
                    # client_object.set_password(request.POST['password'])
                    # client_object.is_active = True
                    # client_object.token = get_random_string(length=32)
                    # client_object.save()
                    form_class = ActivationForm(request.POST,instance=client_object)
                    if form_class.is_valid():
                        print "sdsdsd"
                        form_class.save()
                        payload = {'status': 'success', 'message': 'Successfully Updated', 'data': 'g'}
                        return HttpResponse(json.dumps(payload), content_type='application/json', )
                    else :
                        print form_class.errors
                        payload = {'status': 'failed', 'message': 'failed', 'data': form_class.errors,'type':'validation'}
                        return HttpResponse(json.dumps(payload), content_type='application/json', )


                else:
                    payload = {'status': 'failed', 'message': 'failed','data': 'g','type':'g'}
                    return HttpResponse(json.dumps(payload), content_type='application/json', )


    def get(self, request, *args, **kwargs):
        template_name = 'registration/register_company_user.html'
        context = {'token': kwargs.get('token')}
        return TemplateResponse(request, template_name, context)

class Activated(TemplateView):
    def post(self, request, *args, **kwargs):
        method = request.POST['method']
        if method =="Activate_freelancer":
            if request.POST['role'] =='client':

                if request.POST['type'] == 'freelancer':

                    user_obj = Users.objects.get(email=request.user.email)
                    user_obj.active_account = User_Type.objects.get(type='FREELANCER')
                    user_obj.save()
                    #remove_role(request.user, 'extra_client')
                    assign_role(request.user, 'extra_client');

                    payload = {'status': 'sucess', 'message': "success" }
                    return HttpResponse(json.dumps(payload), content_type='application/json', )
                if request.POST['type'] == 'client':

                    user_obj = Users.objects.get(email=request.user.email)
                    user_obj.active_account = User_Type.objects.get(type='CLIENT')
                    user_obj.save()
                    remove_role(request.user, 'extra_client')
                    #assign_role(request.user, 'extra_client');

                    payload = {'status': 'sucess', 'message': "success"}
                    return HttpResponse(json.dumps(payload), content_type='application/json', )

            else:
                if request.POST['type'] == 'freelancer':
                    user_obj = Users.objects.get(email=request.user.email)
                    user_obj.active_account = User_Type.objects.get(type='FREELANCER')
                    user_obj.save()
                    remove_role(request.user, 'extra_freelancer')
                    #assign_role(request.user, 'extra_client');

                    payload = {'status': 'sucess', 'message': "success"}
                    return HttpResponse(json.dumps(payload), content_type='application/json', )
                if request.POST['type'] == 'client':

                    user_obj = Users.objects.get(email=request.user.email)
                    user_obj.active_account = User_Type.objects.get(type='CLIENT')
                    user_obj.save()
                    #remove_role(request.user, 'extra_client')
                    assign_role(request.user, 'extra_freelancer');

                    payload = {'status': 'sucess', 'message': "success"}
                    return HttpResponse(json.dumps(payload), content_type='application/json', )
class SuccessMessage(TemplateView):
    template_name = 'accounts/success_message.html'

def error_404(request):
        if request.user.is_authenticated():
            extended_template = 'inner_page_base.html'
        else:
            extended_template = 'base.html'
        data = {'extended_template': extended_template}
        return render(request, 'error/error_404.html', data)


def error_500(request):
    data = {}
    return render(request, 'error/error_404.html', data)
class ResetPasswordRequestView(FormView):
        template_name = "registration/password_reset.html"
        #code for template is given below the view's code
        success_url = '/reset_password'
        form_class = PasswordResetRequestForm

        @staticmethod
        def validate_email_address(email):


            try:
                validate_email(email)
                return True
            except ValidationError:
                return False

            #return status

        def post(self, request, *args, **kwargs):
            '''
            A normal post request which takes input from field "email_or_username" (in ResetPasswordRequestForm).
            '''
            form = self.form_class(request.POST)
            if form.is_valid():
                data = form.cleaned_data["email_or_username"]
                print data,"fff"

                if self.validate_email_address(data) is True:  # uses the method written above
                    '''
                    If the input is an valid email address, then the following code will lookup for users associated with that email address. If found then an email will be sent to the address, else an error message will be printed on the screen.
                    '''
                    associated_users = Users.objects.filter(Q(email=data) )
                    if associated_users.exists():
                        for user in associated_users:
                            c = {
                                'email': user.email,
                                'domain': request.META['HTTP_HOST'],
                                'site_name': 'your site',
                                'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                                'user': user,
                                'token': default_token_generator.make_token(user),
                                'protocol': 'http',
                            }
                            subject_template_name = 'registration/password_reset_subject.txt'
                            # copied from django/contrib/admin/templates/registration/password_reset_subject.txt to templates directory
                            email_template_name = 'registration/password_reset_email.html'
                            # copied from django/contrib/admin/templates/registration/password_reset_email.html to templates directory
                            subject = loader.render_to_string(subject_template_name, c)
                            # Email subject *must not* contain newlines
                            subject = ''.join(subject.splitlines())
                            email =render_to_string(email_template_name, c)

                            def Email():
                                """

                                :return:
                                """
                                send_mail(subject, '', DEFAULT_FROM_EMAIL, [user.email], fail_silently=False,
                                          html_message=email)

                            send_user_mail = threading.Thread(name='Email', target=Email)
                            send_user_mail.start()



                            #send_mail(subject, email, DEFAULT_FROM_EMAIL, [user.email], fail_silently=False)
                        result = self.form_valid(form)
                        # messages.success(request,
                        #                  'An email has been sent to ' + data + ". Please check its inbox to continue reseting password.")
                        #messages.success(request,"We have e-mailed your password reset link!")
                        if self.request.is_ajax():
                            payload = {'status': 'success', 'message': 'We have e-mailed your password reset link!',
                                       'data': ''}
                            return HttpResponse(json.dumps(payload), content_type='application/json')
                        return result
                    result = self.form_invalid(form)
                    #messages.error(request, 'No user is associated with this email address')
                    if self.request.is_ajax():
                        payload = {'status': 'failed', 'message': 'No user is associated with this email address',
                                   'data': ''}
                        return HttpResponse(json.dumps(payload), content_type='application/json')

                    return result
                else:
                    # '''
                    # If the input is an username, then the following code will lookup for users associated with that user. If found then an email will be sent to the user's address, else an error message will be printed on the screen.
                    # '''
                    # associated_users = Users.objects.filter(email=data)
                    # if associated_users.exists():
                    #     for user in associated_users:
                    #         c = {
                    #             'email': user.email,
                    #             'domain': 'example.com',  # or your domain
                    #             'site_name': 'example',
                    #             'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                    #             'user': user,
                    #             'token': default_token_generator.make_token(user),
                    #             'protocol': 'http',
                    #         }
                    #         subject_template_name = 'registration/password_reset_subject.txt'
                    #         email_template_name = 'registration/password_reset_email.html'
                    #         subject = loader.render_to_string(subject_template_name, c)
                    #         # Email subject *must not* contain newlines
                    #         subject = ''.join(subject.splitlines())
                    #         email = loader.render_to_string(email_template_name, c)
                    #
                    #
                    #         send_mail(subject, '', DEFAULT_FROM_EMAIL, [user.email], fail_silently=False,html_message=email)
                    #     result = self.form_valid(form)
                    #     if self.request.is_ajax():
                    #         payload = {'status': 'success', 'message': 'We have e-mailed your password reset link!', 'data': ''}
                    #         return HttpResponse(json.dumps(payload), content_type='application/json')
                    #     messages.success(request,
                    #                      'Email has been sent to ' + data + "'s email address. Please check its inbox to continue reseting password.")
                    #     return result
                    result = self.form_invalid(form)
                    messages.error(request, 'This Email does not exist in the system.')
                    return result
                messages.error(request, 'Invalid Input')
                return self.form_invalid(form)
            else:
               # messages.error(request, 'Invalid Input')
               self.message = "Validation failed."
               self.data = form.errors
               self.success = False

               payload = {'status': 'failed', 'message': self.message, 'data': self.data}

               if self.request.is_ajax():
                   return HttpResponse(json.dumps(payload), content_type='application/json')
               return self.form_invalid(form)
class PasswordResetConfirmView(FormView):
    template_name = "registration/password_reset_confirm.html"
    success_url = '.'
    form_class = SetPasswordForm

    def post(self, request, uidb64=None, token=None, *arg, **kwargs):
        """
        View that checks the hash in a password reset link and presents a
        form for entering a new password.
        """
        UserModel = get_user_model()
        form = self.form_class(request.POST)
        assert uidb64 is not None and token is not None  # checked by URLconf
        try:
            uid = urlsafe_base64_decode(uidb64)
            user = UserModel._default_manager.get(pk=uid)
        except (TypeError, ValueError, OverflowError, UserModel.DoesNotExist):
            user = None

        if user is not None and default_token_generator.check_token(user, token):
            if form.is_valid():
                new_password= form.cleaned_data['new_password2']
                user.set_password(new_password)
                user.save()
                if self.request.is_ajax():
                    payload = {'status': 'success', 'message': 'Password has been reset.',
                               'data': ''}
                    return HttpResponse(json.dumps(payload), content_type='application/json')

                messages.success(request, 'Password has been reset.')
                return self.form_valid(form)
            else:
                self.message = "Validation failed."
                self.data = form.errors
                self.success = False

                payload = {'status': 'failed', 'message': self.message, 'data': self.data}

                if self.request.is_ajax():
                    return HttpResponse(json.dumps(payload), content_type='application/json')
                messages.error(request, 'Password reset has not been unsuccessful.')
                return self.form_invalid(form)
        else:
            if self.request.is_ajax():
                payload = {'status': 'failed', 'message': 'The reset password link is no longer valid.',
                           'data': ''}
                return HttpResponse(json.dumps(payload), content_type='application/json')

            messages.error(request,'The reset password link is no longer valid.')
            return self.form_invalid(form)