from django import forms

from hire.models import JobsMilestone, HiredJobsContract, WeeklyTimeEntry


class MilstoneForm(forms.ModelForm):
    milestone_title = forms.RegexField(regex=r'^[a-zA-Z]+([0-9]|[_@. ,/#&+-])*',
                                 error_message=(
                                     "Only alphanumeric is allowed."))
    rate = forms.RegexField(widget=forms.TextInput(attrs={'placeholder': 'Rate'}),
                                   regex=r'^[0-9]\d*(\.\d+)?$',
                                   error_message=(
                                       "Enter a valid rate."))
    class Meta:
        model = JobsMilestone
        fields = ['milestone_title','milestone_description','start_date','close_date','rate','status']
    def clean_milestone_title(self):
        if self.cleaned_data["milestone_title"].strip() == '':
            raise forms.ValidationError("Milestone Title is required.")
        return self.cleaned_data["milestone_title"]

    def clean_milestone_description(self):
        if self.cleaned_data["milestone_description"].strip() == '':
            raise forms.ValidationError("Milestone Description is required.")
        return self.cleaned_data["milestone_description"]
    def clean_start_date(self):
        if self.cleaned_data["start_date"]== '':
            raise forms.ValidationError("Start date is required.")
        return self.cleaned_data["start_date"]
    def clean_close_date(self):
        if self.cleaned_data["close_date"] == '':
            raise forms.ValidationError("Close date is required.")
        return self.cleaned_data["close_date"]
    def clean_rate(self):
        if self.cleaned_data["rate"].strip() == '':
            raise forms.ValidationError("Rate is required.")
        return self.cleaned_data["rate"]

    def save(self,status, commit=True):
        milestone = super(MilstoneForm, self).save(commit=False)
        if commit:
            milestone.status = status
            milestone.save()
        return milestone
class HireForm(forms.ModelForm):
    class Meta:
        model = HiredJobsContract
        fields = ['job_title', 'job_description']

    def clean_job_title(self):
        if self.cleaned_data["job_title"].strip() == '':
            raise forms.ValidationError("Job Title is required.")
        return self.cleaned_data["job_title"]

    def clean_job_description(self):
        if self.cleaned_data["job_description"].strip() == '':
            raise forms.ValidationError("Job Description is required.")
        return self.cleaned_data["job_description"]
    def save(self, commit=True):
        hire = super(HireForm, self).save(commit=False)
        if commit:
            hire.save()
        return hire
class WeeklyTimeEntries(forms.ModelForm):
    """
    Form for hire
    """

    class Meta:
        model = WeeklyTimeEntry
        exclude = ['week', 'date_mon', 'date_tue', 'date_wed', 'date_thu', 'date_fri', 'date_sat', 'date_sun']

    def __init__(self, *args, **kwargs):
        super(WeeklyTimeEntries, self).__init__(*args, **kwargs)



    def save(self, week=None, commit=True):
        form = super(WeeklyTimeEntries, self).save(commit=False)
        if commit:
            form.week = week
            form.save()
        return form