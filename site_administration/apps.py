from __future__ import unicode_literals

from django.apps import AppConfig
from django.db.models.signals import post_migrate




def define_company_groups(sender, **kwargs):
    Store = sender.get_model("Permission")
    store_corporate = Store.objects.get_or_create(name='View_Dashboard_Client',display_name='View_Dashboard_Client', status='True',type='user')
    store_corporate = Store.objects.get_or_create(name='View_Dashboard_Freelancer', display_name='View_Dashboard_Freelancer', status='True',
                                                  type='user')
    #print store_corporate
    store_corporate = Store.objects.get_or_create(name='Admin_Role_list', display_name='Admin_Role_list', status='True',
                                                  type='admin')
    store_corporate = Store.objects.get_or_create(name='Admin_Role_Edit', display_name='Admin_Role_Edit', status='True',
                                                  type='admin')
    store_corporate = Store.objects.get_or_create(name='Admin_Client_List', display_name='Admin_Client_List', status='True',
                                                  type='admin')
    store_corporate = Store.objects.get_or_create(name='Admin_Freelancer_List', display_name='Admin_Freelancer_List', status='True',
                                                  type='admin')
    # print store_corporate
    # Rolepermission = sender.get_model("Role_Permission")
    # role = Rolepermission.objects.get_or_create(role='Client', permission='2')
    # role = Rolepermission.objects.get_or_create(role='Freelancer', permission='3')
    verify = sender.get_model("VerificationJobPost")
    store_corporate = verify.objects.get_or_create(verification_status='verification_off')



    #print sender

class SiteAdministrationConfig(AppConfig):
    name = 'site_administration'

    def ready(self):
        print "fgf"
        post_migrate.connect(define_company_groups, sender=self)