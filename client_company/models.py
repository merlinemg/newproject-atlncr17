from __future__ import unicode_literals

from django.db import models

# Create your models here.
from accounts.models import BaseTimestamp
from newproject import settings


class Client_Company_Profile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, related_name='client_company_user')
    logo = models.ImageField(upload_to='client/company', blank=True, null=True)
    company_name = models.CharField(max_length=500)
    description = models.TextField(blank=True, null=True)
    address = models.TextField(blank=True, null=True)
    phone = models.CharField(max_length=10, blank=True, null=True)

    def __unicode__(self):
        return self.user.email

    class Meta:
        verbose_name = "Client Company Profile"
        verbose_name_plural = "Client Company Profiles"


class Invited_Client_Company_User(BaseTimestamp):
    ACCOUNT_TYPES = (
        ('Admin', '1'),
        ('Staff', '0'),

    )
    user = models.OneToOneField(settings.AUTH_USER_MODEL, related_name='invited_client_company_user')
    role_type = models.CharField(max_length=10,
                                 choices=ACCOUNT_TYPES,
                                 default='admin', verbose_name="Role Type")
    client_company = models.ForeignKey(Client_Company_Profile, related_name='invited_client_company_id')
    # modified_date = models.DateTimeField(verbose_name='Modified Date', auto_now=True, blank=True, null=True)
    # created_date = models.DateTimeField(verbose_name='Create Date', auto_now_add=True, blank=True, null=True)

    def __unicode__(self):
        return self.user.email

    class Meta:
        verbose_name = "Invited Client Company User"
        verbose_name_plural = "Invited Client Company Users"
