from __future__ import unicode_literals

from django.db import models

# Create your models here.
from accounts.models import BaseTimestamp, User_Type
from client.models import ClientJobs
from newproject import settings


class Notifications(BaseTimestamp):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='notification_user',
                             verbose_name="user")
    title = models.CharField(max_length=200)
    description = models.TextField(max_length=500, blank=True, null=True)
    url = models.TextField(max_length=500, blank=True, null=True)
    job = models.ForeignKey(ClientJobs, blank=True, null=True)

    type = models.ForeignKey(User_Type,related_name='user_type_notification',null=True, blank=True,verbose_name="User Type")
    status = models.CharField(max_length=200)
    def __str__(self):
        return "{0}::{1}".format(self.user.first_name, self.title)