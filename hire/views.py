import json

import datetime

from django.db.models import Sum
from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.
from django.template.loader import render_to_string
from django.urls import reverse
from django.views.generic import ListView
from django.views.generic import TemplateView

from accounts.models import Users, User_Type
from client.mixins import ClientMixin
from client.models import ClientJobs
from hire.forms import MilstoneForm, HireForm, WeeklyTimeEntries
from hire.models import HiredJobsContract, MileStoneStatus, TimeWork, WeeklyTimeEntry
from notifications.models import Notifications
import requests

class HireView(ClientMixin,TemplateView):

    template_name = 'client/hire_view.html'
    def get_context_data(self, **kwargs):
        context = super(HireView, self).get_context_data(**kwargs)

        context['hiredetails'] = self.get_hired_details(kwargs.get('slug'))
        context['notificationcount'] = self.get_all_notification(self.request.user.email,
                                                                 self.request.user.active_account.type).count()
        return context


    def post(self,request, *args, **kwargs):
        if request.POST.get('method') == 'Send_contract':

            id =request.POST.get('id')
            if request.user.active_account.type == 'CLIENT':
                hire_details =self.get_hired_details(id)
                if hire_details.budget !=None and  hire_details.budget !=0:
                    if hire_details.status.status == 'Draft':
                       hire_details.status = self.get_hire_status('2')
                       hire_details.save()
                       payload_message = {'created_by': request.user.id, 'type': 'FREELANCER',
                                          'user_id': hire_details.freelancer_profile.user.id,
                                          'title': " has send contract a  job -" + hire_details.client_job.job_title,
                                          'description': "job title is " + hire_details.client_job.job_title,
                                          "url": reverse('freelancerjoblist')}
                       Notifications.objects.get_or_create(user=hire_details.freelancer_profile.user,
                                                           title='has send contract a   job ' + hire_details.client_job.job_title,
                                                           created_by=Users.objects.get(id=request.user.id),
                                                           status='0',
                                                           url=reverse('hireviews', kwargs={'slug': hire_details.slug}),
                                                           type=User_Type.objects.get(type='FREELANCER'))

                       payload = {'status': 'success','payload_message':payload_message,
                                   'type': '0', 'message': "Contract sent successfully", 'data': "k",'jobtype':hire_details.client_job.payment_type.id}
                       return HttpResponse(json.dumps(payload), content_type='application/json')
                    else:
                        payload = {'status': 'failed',
                                   'type': '0', 'message': "Couldn't send", 'data': "k"}
                        return HttpResponse(json.dumps(payload), content_type='application/json')
                else:
                    payload = {'status': 'failed',
                               'type': '0', 'message': "Pls add amount", 'data': "k"}
                    return HttpResponse(json.dumps(payload), content_type='application/json')

            else:
                payload = {'status': 'failed',
                           'type': '0', 'message': "Pls login as client", 'data': "k"}
                return HttpResponse(json.dumps(payload), content_type='application/json')
        elif request.POST.get('method') == 'Add_Milestone':
            form = MilstoneForm(request.POST)
            hire = self.get_hired_details(request.POST['id'])

            if form.is_valid():
                if request.user.active_account.type == 'CLIENT':
                    total_milestone = 0
                    sum = hire.milestone.all().aggregate(Sum('rate'))
                    print sum['rate__sum']
                    if sum['rate__sum'] != None:
                        total_milestone = sum['rate__sum']


                    sum_milestone_budget = (float(total_milestone )+ float(request.POST['rate']))
                    #print sum_milestone_budget,"sum_milestone_budgetsum_milestone_budgetsum_milestone_budget"
                    if hire.budget  >= sum_milestone_budget:
                        status =self.get_milestone_status('Draft')
                        milestone_id=form.save(status)
                        if hire:
                            hire.milestone.add(milestone_id)

                        payload = {'status': 'success', 'message': "Milestone Created Successfully", 'html': ''}
                        return HttpResponse(json.dumps(payload), content_type='application/json', )
                    else:
                        payload = {'status': 'failed',
                                   'type': '0', 'message': "The sum of milestone not meeting", 'data': "k"}
                        return HttpResponse(json.dumps(payload), content_type='application/json')

                else:
                    payload = {'status': 'failed',
                               'type': '0', 'message': "Pls login as client", 'data': "k"}
                    return HttpResponse(json.dumps(payload), content_type='application/json')
            else:
                self.message = "Validation failed."
                self.data = form.errors
                self.success = False
                payload = {'status': 'failed', 'type':'1','message': self.message, 'data': self.data}
                return HttpResponse(json.dumps(payload), content_type='application/json', )
        elif request.POST.get('method') == 'Edit_Milestone':
            form = MilstoneForm(request.POST,instance=self.get_milstone_details(request.POST['milestoneid']))
            hire = self.get_hired_details(request.POST['id'])
            if form.is_valid():
                if request.user.active_account.type == 'CLIENT':
                    total_milestone = 0
                    milestones = self.get_milstone_details(request.POST['milestoneid'])
                    sum = hire.milestone.exclude(id=milestones.id).aggregate(Sum('rate'))
                    print sum['rate__sum']
                    if sum['rate__sum'] != None:
                        total_milestone = sum['rate__sum']

                    sum_milestone_budget = (float(total_milestone) + float(request.POST['rate']))
                    if hire.budget >= sum_milestone_budget:
                        if milestones.status.status == 'Pending' or milestones.status.status == 'Changed':
                            status =self.get_milestone_status('Changed')
                            form.save(status)
                            payload = {'status': 'success',
                                         'type': '0', 'message': "Successfully Updated",'html':''}
                        elif milestones.status.status == 'Draft':
                            status = self.get_milestone_status('Draft')
                            form.save(status)
                            payload = {'status': 'success',
                                       'type': '0', 'message': "Successfully Updated",'html':''}
                        else:

                            payload = {'status': 'failed', 'type':'0' ,'message': "Could not updated", 'html': 'hourly_html'}


                        return HttpResponse(json.dumps(payload), content_type='application/json', )
                    else:
                        payload = {'status': 'failed',
                                   'type': '0', 'message': "The sum of milestone not meeting", 'data': "k"}
                        return HttpResponse(json.dumps(payload), content_type='application/json')
                else:
                    payload = {'status': 'failed',
                               'type': '0', 'message': "Pls login as client", 'data': "k"}
                    return HttpResponse(json.dumps(payload), content_type='application/json')
            else:
                self.message = "Validation failed."
                self.data = form.errors
                self.success = False
                payload = {'status': 'failed','type':'1', 'message': self.message, 'data': self.data}
                return HttpResponse(json.dumps(payload), content_type='application/json', )
        elif request.POST.get('method') == 'Get_milestone':
            milestone =self.get_milstone_details(request.POST['milestoneid'])
            if milestone.status.status == 'Draft' or milestone.status.status == 'Pending' or milestone.status.status == 'Changed':
                user_table = render_to_string('client/ajax/edit_milestone.html',
                                              {'csrf_token_value': request.COOKIES['csrftoken'],'queryset': milestone,'id':request.POST['id']})

                payload = {'status': 'success',
                           'type': '0', 'message': "successfully", 'html': user_table}
                return HttpResponse(json.dumps(payload), content_type='application/json')
            else:
                payload = {'status': 'failed',
                           'type': '0', 'message': "couldn't edit", 'html': ''}
                return HttpResponse(json.dumps(payload), content_type='application/json')
        elif request.POST.get('method') == 'Delete_milestone':
            milestone =self.get_milstone_details(request.POST['milestoneid'])
            if milestone.status.status == 'Draft' :
                milestone.status = self.get_milestone_status('Archive')
                milestone.save()
                payload = {'status': 'success',
                           'type': '0', 'message': "Deleted Successfully"}
                return HttpResponse(json.dumps(payload), content_type='application/json')
            else:
                payload = {'status': 'failed',
                           'type': '0', 'message': "Couldn't Delete", 'html': ''}
                return HttpResponse(json.dumps(payload), content_type='application/json')
        elif request.POST.get('method') == 'Send_milestone':
            if request.user.active_account.type == 'CLIENT':
                hire_details = self.get_hired_details(request.POST['id'])
                if hire_details.status.status == 'Approved' :

                    milestone =self.get_milstone_details(request.POST['milestoneid'])

                    if milestone.status.status == 'Draft':
                        milestone.status = self.get_milestone_status('Pending')
                        milestone.save()
                        Notifications.objects.get_or_create(user=hire_details.job.user,
                                                            title='has Milestone send a  job ' + hire_details.job.job_title,
                                                            created_by=Users.objects.get(id=request.user.id),
                                                            status='0',
                                                            url=reverse('freelancerjoblist'),
                                                            type=User_Type.objects.get(type='FREELANCER'))
                        payload = {'status': 'success','type':'0',
                                   'jobtype': hire_details.client_job.payment_type.id, 'message': "Milestone sent Successfully"}
                        return HttpResponse(json.dumps(payload), content_type='application/json')
                    else:
                        payload = {'status': 'failed',
                                   'type': '0', 'message': "Couldn't Send", 'html': ''}
                        return HttpResponse(json.dumps(payload), content_type='application/json')
                elif  hire_details.status.status == 'Draft':
                    payload = {'status': 'failed',
                               'type': '0', 'message': "Pls Send Contract ", 'data': "k"}
                    return HttpResponse(json.dumps(payload), content_type='application/json')
                else:
                    payload = {'status': 'failed',
                               'type': '0', 'message': "Contract could not approve ", 'data': "k"}
                    return HttpResponse(json.dumps(payload), content_type='application/json')
            else:
                payload = {'status': 'failed',
                           'type': '0', 'message': "Pls login as client", 'data': "k"}
                return HttpResponse(json.dumps(payload), content_type='application/json')
        elif request.POST.get('method') == 'Edit_hire_details':
            if request.user.active_account.type == 'CLIENT':
                form = HireForm(request.POST, instance=self.get_hired_details(request.POST['id']))

                if form.is_valid():
                    form.save()
                    html =render_to_string('client/ajax/hire_details.html',
                                      {'hiredetails': self.get_hired_details(request.POST['id'])})
                    payload = {'status': 'success',
                               'type': '0', 'message': "Successfully Updated",'html':html}
                else:
                    print "df"
                    self.message = "Validation failed."
                    self.data = form.errors
                    self.success = False
                    payload = {'status': 'failed', 'type': '1', 'message': self.message, 'data': self.data}
                return HttpResponse(json.dumps(payload), content_type='application/json', )

            else:
                payload = {'status': 'failed',
                           'type': '0', 'message': "Pls login as client", 'data': "k"}
                return HttpResponse(json.dumps(payload), content_type='application/json')
        elif request.POST.get('method') == 'Timesheet_approved':
            week = TimeWork.objects.get(id=request.POST.get('id'))

            if request.user.active_account.type == 'CLIENT':
                if week.status.status == 'Pending':
                    week.status = self.get_week_status('Approved')
                    week.save()
                    payload = {'status': 'success',
                               'type': '1', 'message': "Timesheet Approved Successfully", 'data': "k"}
                    return HttpResponse(json.dumps(payload), content_type='application/json')

                else:
                    payload = {'status': 'failed',
                               'type': '0', 'message': "Couldn't approve", 'data': "k"}
                    return HttpResponse(json.dumps(payload), content_type='application/json')


            else:
                payload = {'status': 'failed',
                           'type': '0', 'message': "Pls login as client", 'data': "k"}
                return HttpResponse(json.dumps(payload), content_type='application/json')
        elif request.POST.get('method') == 'Timesheet_adjust':
            week = TimeWork.objects.get(id=request.POST.get('id'))

            if request.user.active_account.type == 'CLIENT':
                if week.status.status == 'Pending' :
                    week.status = self.get_week_status('Adjust')
                    week.save()
                    payload = {'status': 'success',
                               'type': '1', 'message': "Timesheet Adjusted Successfully", 'data': "k"}
                    return HttpResponse(json.dumps(payload), content_type='application/json')

                else:
                    payload = {'status': 'failed',
                               'type': '0', 'message': "Couldn't approve", 'data': "k"}
                    return HttpResponse(json.dumps(payload), content_type='application/json')


            else:
                payload = {'status': 'failed',
                           'type': '0', 'message': "Pls login as Client", 'data': "k"}
                return HttpResponse(json.dumps(payload), content_type='application/json')
        elif request.POST.get('method') == 'Updated_Hire_budget':
            hire_details = self.get_hired_details(request.POST['id'])
            if request.user.active_account.type == 'CLIENT':
                if hire_details.budget == None or hire_details.budget == 0 or hire_details.status.status == 'Draft':
                    hire_details.budget = request.POST['budget']
                    hire_details.save()
                    payload = {'status': 'success',
                               'type': '1', 'message': "Budget Updated Successfully", 'data': "k"}
                    return HttpResponse(json.dumps(payload), content_type='application/json')

                else:
                    payload = {'status': 'failed',
                               'type': '0', 'message': "Couldn't updated", 'data': "k"}
                    return HttpResponse(json.dumps(payload), content_type='application/json')


            else:
                payload = {'status': 'failed',
                           'type': '0', 'message': "Pls login as Client", 'data': "k"}
                return HttpResponse(json.dumps(payload), content_type='application/json')
        elif request.POST.get('method') == 'Create_new_contract':
            job = self.get_job(request.POST.get('id'))
            print job,"jobjobjobjob"
            contract =self.get_hired_details(request.POST.get('contractid'))
            contract.repost_status=1
            contract.save()

            new_job = ClientJobs.objects.create(user=job.user,job_title=job.job_title,
                                               job_description=job.job_description,job_main_category=job.job_main_category,
                                               created_user=job.created_user,no_of_hires=job.no_of_hires,verification_status=job.verification_status
                                               ,job_close_date=job.job_close_date,repost_status='1',job_status=job.job_status,duration=job.duration
                                               )
            new_job.save()

            if job.payment_type.id == 1:
                new_job.budget = contract.budget
                new_job.payment_type = self.get_payment('2')
                new_job.save()
            elif job.payment_type.id == 2:
                new_job.budget = contract.budget
                new_job.payment_type = self.get_payment('1')
                new_job.save()

            if job.job_sub_category:
                for sub_category in job.job_sub_category.all():
                    new_job.job_sub_category.add(sub_category)


            if job.job_skills:
                for skilss in job.job_skills.all():
                   new_job.job_skills.add(skilss)

            hire= HiredJobsContract.objects.create(client_profile=contract.client_profile,
                                                                  freelancer_profile=contract.freelancer_profile, client_job= new_job,approved_date=contract.approved_date)
            hire.job_title = contract.job_title
            hire.job_description = contract.job_description
            hire.status = self.get_hire_status(3)
            hire.repost_status = 1


            if job.payment_type.id == 1:
                hire.budget = contract.budget
                hire.payment_type = self.get_payment('2')
            elif job.payment_type.id ==2:
                hire.budget = contract.budget
                hire.payment_type = self.get_payment('1')
            hire.save()
            payload = {'status': 'success',
                       'type': '1', 'message': "Budget Updated Successfully", 'data': reverse('hireviews', kwargs={'slug': hire.slug})}
            return HttpResponse(json.dumps(payload), content_type='application/json')
        elif request.POST.get('method') == 'Payout':
            print request.POST.get('id')
            import requests
            #auth = ('shibila.b@spericorn.com', '1366_1sRtZFt5ScAL8RclXRFu5qBITlEOMolH3h94UW4qjLhL7Uc7Njv262D3EZmtVgsm'),
            v = requests.get(
                'https://stgsecureapi.escrow.com/api/Transaction',
                auth=('shibila.b@spericorn.com', 'User@123'),
                )
            print v,"xxx"
            s = requests.post(
                'https://api.escrow-sandbox.com/2017-09-01/transaction',
                auth=('shibila.b@spericorn.com', '289_rkxkyUzzdAKrypMQ1zS1d1FTOIDZcBn1ZCyqHwEtLq8DjTwJMdZcF8qoi6dKffDB'),
                json={
                    "parties": [
                        {
                            "role": "buyer",
                            "customer": "me"
                        },
                        {
                            "role": "seller",
                            "customer": "keanu.reaves@escrow.com"
                        }
                    ],
                    "currency": "usd",
                    "description": "The sale of johnwick.com",
                    "items": [
                        {
                            "title": "johnwick.com",
                            "description": "johnwick.com",
                            "type": "domain_name",
                            "inspection_period": 259200,
                            "quantity": 1,
                            "schedule": [
                                {
                                    "amount": 1000.0,
                                    "payer_customer": "me",
                                    "beneficiary_customer": "keanu.reaves@escrow.com"
                                }
                            ]
                        }
                    ]
                },
            )
           #class_req_url = s.json()
            print s.json()
            payload = {'status': 'success',
                       'type': '1', 'message': "Budget Updated Successfully",
                       'data': ""}
            return HttpResponse(json.dumps(payload), content_type='application/json')








class FreelancerHireView(ClientMixin,TemplateView):
    template_name = 'freelancer/freelancer_hire_view.html'
    def get_context_data(self, **kwargs):
        context = super(FreelancerHireView, self).get_context_data(**kwargs)

        context['hiredetails'] = self.get_hired_details(kwargs.get('slug'))
        context['notificationcount'] = self.get_all_notification(self.request.user.email,self.request.user.active_account.type).count()
        return context

    def post(self, request, *args, **kwargs):

        if request.POST.get('method') == 'Contract_approved':

            id = request.POST.get('id')
            if request.user.active_account.type == 'FREELANCER':
                hire_details = HiredJobsContract.objects.get(id=id)
                if hire_details.status.status == 'Pending':
                    hire_details.status = self.get_hire_status('3')
                    hire_details.approved_date = datetime.datetime.today()
                    hire_details.save()
                    Notifications.objects.get_or_create(user=hire_details.client_job.user,
                                                        title='has send contract a   job ' + hire_details.client_job.job_title,
                                                        created_by=Users.objects.get(id=request.user.id),
                                                        status='0',
                                                        url= reverse('hireviews', kwargs={'slug': hire_details.slug}),
                                                        type=User_Type.objects.get(type='CLIENT'))

                    payload = {'status': 'success',
                               'type': '0', 'message': "Contract approved successfully", 'data': "k",'jobtype':hire_details.client_job.payment_type.id}
                    return HttpResponse(json.dumps(payload), content_type='application/json')
                else:
                    payload = {'status': 'failed',
                               'type': '0', 'message': "Couldn't approve", 'data': "k"}
                    return HttpResponse(json.dumps(payload), content_type='application/json')

            else:
                payload = {'status': 'failed',
                           'type': '0', 'message': "Pls login as freelancer", 'data': "k"}
                return HttpResponse(json.dumps(payload), content_type='application/json')
        elif request.POST.get('method') == 'Milestone_approved':
            if request.user.active_account.type == 'FREELANCER':
                milestone = self.get_milstone_details(request.POST['id'])
                if milestone.status.status == 'Pending' or milestone.status.status == 'Changed':
                    milestone.status = self.get_milestone_status('Approved')

                    milestone.save()
                    payload = {'status': 'success',
                               'type': '0', 'message': "Approved Successfully"}
                    return HttpResponse(json.dumps(payload), content_type='application/json')
                else:
                    payload = {'status': 'failed',
                              'type': '0', 'message': "You can't approve this milestone", 'data': "k"}
                    return HttpResponse(json.dumps(payload), content_type='application/json')

            else:
                payload = {'status': 'failed',
                           'type': '0', 'message': "Pls login as freelancer", 'data': "k"}
                return HttpResponse(json.dumps(payload), content_type='application/json')
        elif  request.POST.get('method') == 'Request_payout':
            if request.user.active_account.type == 'FREELANCER':
                if request.POST.get('type') == '1':
                    milestone = self.get_milstone_details(request.POST['id'])
                    print request.POST['id'],milestone.status.status,"ssss"
                    if milestone.status.status == 'Approved':

                        milestone.status = self.get_milestone_status('Payout Requested')
                        milestone.save()
                        payload = {'status': 'success',
                                   'type': '0', 'message': "Requested Successfully"}
                        return HttpResponse(json.dumps(payload), content_type='application/json')

                    else:
                        payload = {'status': 'failed',
                                   'type': '0', 'message': "You can't ", 'data': "k"}
                        return HttpResponse(json.dumps(payload), content_type='application/json')
                elif request.POST.get('type') == '0':
                    timesheet = TimeWork.objects.get(id=request.POST['id'])

                    if timesheet.status.status == 'Approved':
                        timesheet.status = self.get_week_status('Payout Requested')
                        timesheet.save()
                        payload = {'status': 'success',
                                   'type': '0', 'message': "Requested Successfully"}
                        return HttpResponse(json.dumps(payload), content_type='application/json')
                    else:
                        payload = {'status': 'failed',
                                   'type': '0', 'message': "You can't ", 'data': "k"}
                        return HttpResponse(json.dumps(payload), content_type='application/json')
                else:
                    payload = {'status': 'failed',
                               'type': '0', 'message': "Erorr ", 'data': "k"}
                    return HttpResponse(json.dumps(payload), content_type='application/json')

            else:
                payload = {'status': 'failed',
                           'type': '0', 'message': "Pls login as freelancer", 'data': "k"}
                return HttpResponse(json.dumps(payload), content_type='application/json')

        elif request.POST.get('method') == 'Creating_Timesheet':
            id = request.POST.get('id')
            contract = HiredJobsContract.objects.get(id=id)
            timeWork = TimeWork.objects.filter(contract=contract)
            if contract.status.status == 'Approved':
                start_date = contract.approved_date
                work_first_monday = start_date - datetime.timedelta(days=start_date.weekday())
                today = datetime.datetime.today()

                #today = todays + datetime.timedelta(days=9)
                #print "dfdf", today
                # 0 = Monday, 1=Tuesday, 2=Wednesday...
                work_last_monday = next_weekday(today, 0)

                while work_last_monday.date() >= work_first_monday.date():
                    # dates = week_range(work_first_monday)
                    # print "***", dates

                    time_work,time_works = TimeWork.objects.get_or_create(contract=contract, start_date=work_first_monday,
                                         end_date=work_first_monday + datetime.timedelta(days=6))

                    if time_works:
                        time_works.status =self.get_week_status('Draft')
                        time_work.save()


                    work_first_monday = work_first_monday + datetime.timedelta(days=7)
                weekly_contract = TimeWork.objects.filter(contract=contract)
                if request.user.active_account.type == 'CLIENT':
                    weekly_contract = weekly_contract.exclude(status='Draft')
                response_data={}
                response_data['status'] ='success'
                response_data['html_content'] = render_to_string('freelancer/ajax/list_timesheets.html',
                                                                 {'weekly_contract': weekly_contract},
                                                                )
            else:
                response_data = {'status': 'failed',
                           'type': '0', 'message': "You can't approve this milestone", 'data': "k"}

            return HttpResponse(json.dumps(response_data), content_type="application/json")
        elif request.POST.get('method') == 'Get_Timesheet':


            week = TimeWork.objects.get(id=request.POST.get('timesheetid'))
            try:
                week_entries = WeeklyTimeEntry.objects.get(week=week)
            except:
                week_entry = WeeklyTimeEntry(week=week, date_mon=week.start_date,
                                             date_tue=week.start_date + datetime.timedelta(days=1),
                                             date_wed=week.start_date + datetime.timedelta(days=2),
                                             date_thu=week.start_date + datetime.timedelta(days=3),
                                             date_fri=week.start_date + datetime.timedelta(days=4),
                                             date_sat=week.start_date + datetime.timedelta(days=5),
                                             date_sun=week.end_date)
                week_entries = week_entry.save()

            response_data = {}
            response_data['status'] = 'success'
            response_data['html_content'] = render_to_string('freelancer/ajax/pop_timesheet.html',
                                                             {'today': datetime.datetime.today(),'week_entries': week_entries,'csrf_token_value': request.COOKIES['csrftoken']},
                                                             )
            return HttpResponse(json.dumps(response_data), content_type="application/json")
        elif request.POST.get('method') =='Save_timesheet':
            week =TimeWork.objects.get(id=request.POST.get('week_id'))
            getweektime =WeeklyTimeEntry.objects.get(id=request.POST.get('id'))
            form =WeeklyTimeEntries(request.POST, instance=getweektime)

            # getweektime.time_mon = datetime.timedelta(days=0, hours=8)
            # getweektime.save()
            if request.user.active_account.type == 'FREELANCER':
                if week.status.status =='Draft' or week.status.status =='Pending' or week.status.status =='Adjust':
                    if form.is_valid():
                        form.save(week)
                        sheet_sum =datetime.timedelta()
                        if getweektime.time_mon:
                            (h, m, s) = str(getweektime.time_mon).split(':')
                            monday = datetime.timedelta(hours=int(h), minutes=int(m), seconds=int(s))

                            sheet_sum += monday
                        if getweektime.time_tue:
                            (h, m, s) = str(getweektime.time_tue).split(':')
                            tue = datetime.timedelta(hours=int(h), minutes=int(m), seconds=int(s))
                            sheet_sum += tue
                        if getweektime.time_wed:
                            (h, m, s) = str(getweektime.time_wed).split(':')
                            wed = datetime.timedelta(hours=int(h), minutes=int(m), seconds=int(s))
                            sheet_sum += wed
                        if getweektime.time_thu:

                            (h, m, s) = str(getweektime.time_thu).split(':')
                            thu = datetime.timedelta(hours=int(h), minutes=int(m), seconds=int(s))
                            sheet_sum += thu
                        if getweektime.time_fri:
                            (h, m, s) = str(getweektime.time_fri).split(':')
                            time_fri = datetime.timedelta(hours=int(h), minutes=int(m), seconds=int(s))
                            sheet_sum += time_fri
                        if getweektime.time_sat:
                            (h, m, s) = str(getweektime.time_sat).split(':')
                            time_sat = datetime.timedelta(hours=int(h), minutes=int(m), seconds=int(s))
                            sheet_sum += time_sat
                        if getweektime.time_sun:
                            (h, m, s) = str(getweektime.time_sun).split(':')
                            time_sun = datetime.timedelta(hours=int(h), minutes=int(m), seconds=int(s))
                            sheet_sum += time_sun

                        # sheet_sum += getweektime.time_wed
                        # sheet_sum += getweektime.time_thu
                        # sheet_sum += getweektime.time_fri
                        # sheet_sum += getweektime.time_sat
                        # sheet_sum += getweektime.time_sun
                        days, hours,minutess,s = sheet_sum.days, sheet_sum.seconds // 3600,(sheet_sum.seconds//60)%60,sheet_sum.seconds % 60

                        hourss = (days * 24) + hours
                        hours = hourss + float(minutess) / 60
                        hours ="%.2f" % hours

                        week.week_sum = str(hourss)+str("h ")+str(minutess)+str("m")
                        week.time=hours
                        week.save()




                    else:
                        print form.errors
                    payload = {'status': 'success',
                               'type': '0', 'message': "Timesheet Update Successfully"}
                    return HttpResponse(json.dumps(payload), content_type='application/json')
                else:
                    payload = {'status': 'failed',
                               'type': '0', 'message': "Could not Edit", 'data': "k"}
                    return HttpResponse(json.dumps(payload), content_type='application/json')

            else:
                payload = {'status': 'failed',
                           'type': '0', 'message': "Pls login as freelancer", 'data': "k"}
                return HttpResponse(json.dumps(payload), content_type='application/json')
        elif request.POST.get('method') == "Send_Timesheet":
            week = TimeWork.objects.get(id=request.POST.get('weekid'))
            if request.user.active_account.type == 'FREELANCER':
                if week.status.status=='Draft' or week.status.status=='Adjust':
                    week.status =self.get_week_status('Pending')
                    week.save()
                    payload = {'status': 'success',
                               'type': '1', 'message': "Timesheet Sent Successfully", 'data': "k"}
                    return HttpResponse(json.dumps(payload), content_type='application/json')
                else:
                    payload = {'status': 'failed',
                               'type': '0', 'message': "Couldn't Send", 'data': "k"}
                    return HttpResponse(json.dumps(payload), content_type='application/json')
            else:
                payload = {'status': 'failed',
                           'type': '0', 'message': "Pls login as freelancer", 'data': "k"}
                return HttpResponse(json.dumps(payload), content_type='application/json')







def next_weekday(d, weekday):
    days_ahead = weekday - d.weekday()
    # if days_ahead <= 0: # Target day already happened this week
    #     days_ahead += 7
    return d + datetime.timedelta(days_ahead)

class MilestoneListView(ClientMixin,ListView):
    paginate_by = 10

    def post(self, request, *args, **kwargs):
        response_data = {}
        if request.POST.get('method') == 'MileStone_listing':
            page_by = request.POST.get('page_by')

            milestonelist = self.get_milestone_list(request.POST.get('contractid'))
            milestonelist = milestonelist.exclude(status__status='Archive').order_by("-id")

            if request.user.active_account.type == 'FREELANCER':
                milestonelist = milestonelist.exclude(status__status='Draft')

            paginator_response = self.get_paginated_queryset_list(request, page_by, milestonelist)

            response_data['paginator_html'] = paginator_response['paginator_html']
            response_data['class'] = 'ListingMilestones'

            return HttpResponse(json.dumps(response_data), content_type="application/json")
        elif  request.POST.get('method') == 'Creating_Timesheet':
            page_by = request.POST.get('page_by')

            contract = HiredJobsContract.objects.get(id=request.POST.get('contractid'))
            timeWork = TimeWork.objects.filter(contract=contract)
            if contract.status.status == 'Approved':
                if contract.approved_date:
                    start_date = contract.approved_date
                    work_first_monday = start_date - datetime.timedelta(days=start_date.weekday())
                    today = datetime.datetime.today()

                    # today = todays + datetime.timedelta(days=9)
                    # print "dfdf", today
                    # 0 = Monday, 1=Tuesday, 2=Wednesday...
                    work_last_monday = next_weekday(today, 0)

                    while work_last_monday.date() >= work_first_monday.date():
                        # dates = week_range(work_first_monday)
                        # print "***", dates

                        time_work, time_works = TimeWork.objects.get_or_create(contract=contract,
                                                                               start_date=work_first_monday,
                                                                               end_date=work_first_monday + datetime.timedelta(
                                                                                   days=6))

                        if time_works == True:
                            time_work.status = self.get_week_status('Draft')
                            time_work.save()
                        # time_work.save()
                        work_first_monday = work_first_monday + datetime.timedelta(days=7)
                    weekly_contract = TimeWork.objects.filter(contract=contract)
                    if request.user.active_account.type == 'CLIENT':
                        weekly_contract = weekly_contract.exclude(status__status='Draft')

                    paginator_response = self.get_paginated_queryset_list_timesheet(request, page_by, weekly_contract)

                    response_data['paginator_html'] = paginator_response['paginator_html']
                    response_data['class'] ='weekly_contract'
                    return HttpResponse(json.dumps(response_data), content_type="application/json")
            else:
                payload = {'status': 'failed',
                           'type': '0', 'message': "Could not sent", 'data': "k"}
                return HttpResponse(json.dumps(payload), content_type='application/json')




    def get_paginated_queryset_list(self, request, page_by,  milestonelist):

        if page_by:
            page_by = int(page_by)
        else:
            page_by = 1
        page_size = self.get_paginate_by(milestonelist)
        size = self.paginate_queryset(milestonelist, page_size)

        # if search == '':
        #     page_size = self.get_paginate_by(freelancerlist)
        #     size = self.paginate_queryset(freelancerlist, page_size)
        # else:
        #     page_size = self.get_paginate_by(
        #         WorkSubCategories.objects.filter(sub_category_title__contains=search))
        #     size = self.paginate_queryset(
        #         WorkSubCategories.objects.filter(sub_category_title__contains=search), page_size)

        paginator, page, queryset, is_paginated = size
        try:
            queryset = paginator.page(page_by).object_list
        except:
            queryset = queryset

        user_table = render_to_string('client/ajax/milestone_list.html',
                                      {'queryset': queryset, 'paginator': paginator, 'page': page,
                                       'is_paginated': is_paginated, 'type': request.user.active_account, 'request': request,
                                       'page_obj': paginator.page(page_by)})

        # paginator_html = render_to_string('ajax/paginator_html.html',
        #                                   {'page_obj': paginator.page(page_by)})

        context = {'count': milestonelist.count(), 'paginator_html': user_table}
        return context
    def get_paginated_queryset_list_timesheet(self, request, page_by,  weekly_contract):
        if page_by:
            page_by = int(page_by)
        else:
            page_by = 1
        page_size = self.get_paginate_by(weekly_contract)
        size = self.paginate_queryset(weekly_contract, page_size)

        # if search == '':
        #     page_size = self.get_paginate_by(freelancerlist)
        #     size = self.paginate_queryset(freelancerlist, page_size)
        # else:
        #     page_size = self.get_paginate_by(
        #         WorkSubCategories.objects.filter(sub_category_title__contains=search))
        #     size = self.paginate_queryset(
        #         WorkSubCategories.objects.filter(sub_category_title__contains=search), page_size)

        paginator, page, queryset, is_paginated = size
        try:
            queryset = paginator.page(page_by).object_list
        except:
            queryset = queryset

        user_table = render_to_string('freelancer/ajax/list_timesheets.html',
                                      {'queryset': queryset, 'paginator': paginator, 'page': page,
                                       'is_paginated': is_paginated, 'type': request.user.active_account,
                                       'request': request,
                                       'page_obj': paginator.page(page_by)})

        # paginator_html = render_to_string('ajax/paginator_html.html',
        #                                   {'page_obj': paginator.page(page_by)})

        context = {'count': weekly_contract.count(), 'paginator_html': user_table}
        return context