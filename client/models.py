from __future__ import unicode_literals

import datetime

from django.core.exceptions import ValidationError
from django.core.validators import MinLengthValidator
from django.db import models

# Create your models here.
from django.utils.crypto import get_random_string
from django.utils.text import slugify

from accounts.models import BaseTimestamp
from freelancer.models import WorkCategories, WorkSubCategories, Skills, Freelancer_Profile
from newproject import settings
from django.utils.translation import ugettext_lazy as _


class Client_Profile(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='client_user_detail')
    name = models.CharField(max_length=255)
    profile = models.ImageField(upload_to='client/profile', blank=True, null=True)

    def __unicode__(self):
        return self.user.email

    class Meta:
        verbose_name = "Client Profile"
        verbose_name_plural = "Client Profiles"


class PaymentType(models.Model):
    type = models.CharField(max_length=255)

    def __unicode__(self):
        return self.type


class Duration(models.Model):
    date_range = models.CharField(max_length=255)

    def __unicode__(self):
        return self.date_range


class JobStatus(models.Model):
    status = models.CharField(max_length=200)

    def __unicode__(self):
        return self.status

    class Meta:
        verbose_name = "Job Status"
        verbose_name_plural = "Job Status"
class InvitationStatus(models.Model):
    status = models.CharField(max_length=250)

    def __unicode__(self):
        return self.status

    class Meta:
        verbose_name = "Invitation Status"
        verbose_name_plural = "Invitation Status"

class ClientJobs(BaseTimestamp):
    JOB_STATUS = (
        ('OPEN', 'Open'),
        ('CLOSE', 'Close'),
        ('DRAFT', 'Draft'), ('PENDING', 'Pending'),
    )
    EPERIANCE_LEVEL = (
        ('FRESHER', 'Fresher'),
        ('INTERMEDIATE', 'Intermediate'),
        ('EXPERT', 'Expert')
    )
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='client_jobs',
                             verbose_name="User")
    job_title = models.CharField(_('Job title'), max_length=255)
    job_number = models.IntegerField(_('Job Number'), default=0)
    job_description = models.TextField(_('Job Description'),blank=True, null=True)
    job_main_category = models.ForeignKey(WorkCategories, related_name='job_main_category',
                                          verbose_name="Job Category")

    job_sub_category = models.ManyToManyField(WorkSubCategories, related_name='job_sub_category',
                                              verbose_name="Job sub Category")

    job_skills = models.ManyToManyField(Skills, related_name='job_skills',
                                        verbose_name="Job Skill")
    payment_type = models.ForeignKey(PaymentType, related_name='payment_type',
                                     verbose_name="Payment Type",blank=True, null=True)
    budget = models.IntegerField(_('Budget'), default=0,blank=True, null=True)
    hourly_max = models.IntegerField(_('Hourly Max'), blank=True, null=True)
    hourly_min = models.IntegerField(_('Hourly Min'),  blank=True, null=True)
    duration = models.ForeignKey(Duration, related_name='duration', verbose_name="Duration",blank=True, null=True)

    # duration_in_hour = models.IntegerField(_('Duration In Hour'), default=0, blank=True, null=True)
    # duration_in_weak = models.IntegerField(_('Duration In Weak'), default=0, blank=True, null=True)
    # duration_in_month = models.IntegerField(_('Duration In Month'), default=0, blank=True, null=True)
    # # experiance_level = models.CharField(_('Desired Experience Level'), max_length=255, blank=True, null=True)
    experiance_level = models.CharField(max_length=15, choices=EPERIANCE_LEVEL, default='FRESHER',
                                        verbose_name="Desired Experience Level")
    no_of_hires = models.IntegerField(_('Number of Hires'), default=0,blank=True, null=True)
    job_status = models.ForeignKey(JobStatus, related_name='job_status', verbose_name="Job staus",blank=True, null=True)
    # job_status = models.CharField(max_length=6, choices=JOB_STATUS, default='OPEN', verbose_name="Job status")
    verification_status = models.CharField(max_length=6,  default='0', verbose_name="Verification status")
    repost_status = models.CharField(max_length=6,  default='0', verbose_name="Repost status")
    slug = models.SlugField(max_length=255, unique=True, auto_created=True, editable=False)
    job_close_date = models.DateField(_('Job close date'), max_length=255, blank=True, null=True)
    # created_date = models.CharField(_('Created By'), max_length=255, blank=True, null=True)
    last_view_date = models.DateTimeField(verbose_name='Last View Date', blank=True, null=True)
    created_user = models.CharField(_('Created User'), max_length=255, blank=True, null=True)

    # modified_date = models.DateTimeField(verbose_name='Modified Date',auto_now=True,blank=True, null=True)
    # created_date = models.DateTimeField(verbose_name='Create Date', auto_now_add=True, blank=True, null=True)

    def __str__(self):
        return "{0}::{1}".format(self.user.first_name, self.job_title)

    # def save(self, *args, **kwargs):
    #     super(ClientJobs, self).save(*args, **kwargs)
    #     print self.pk,"hdddddddddd"
    #     if self.pk is None:
    #         self.created_by = self.user
    #     else:
    #         self.modified_by = self.user
    #     super(ClientJobs, self).save()


    # def save(self):
    #     if self.id:
    #         print self.id,"self.idself.idself.idself.idself.id"
    #         self.modified_by = self.user
    #     else:
    #         self.created_by = self.user
    #     super(ClientJobs, self).save()
    # def save_model(self, request, obj, form, change):
    #     if change:
    #         obj.modified_by = request.user
    #         #obj.modified_date = datetime.now()
    #     else:
    #         obj.created_by = request.user
    #         #obj.created_date = datetime.now()
    #     obj.save()

    class Meta:
        verbose_name = "Client Job"
        verbose_name_plural = "Client Jobs"


class JobInvitation(BaseTimestamp):
    """
    Client Job invitation details.
    """

    user = models.ForeignKey(Freelancer_Profile, related_name='invited_user_name',
                             verbose_name="User", null=True)
    job = models.ForeignKey(ClientJobs, related_name='invited_jobs_name', verbose_name="Job")
    status =models.ForeignKey(InvitationStatus, related_name='invitation_status',
                                     verbose_name="Status",blank=True, null=True)
    # invite = models.BooleanField(default=False)
    # is_accepted = models.BooleanField(default=False)
    # is_rejected = models.BooleanField(default=False)
    # is_rejected_by_client = models.BooleanField(default=False)
    messege = models.TextField(blank=True, null=True)
    reject_messege = models.TextField(blank=True, null=True)

    # modified_date = models.DateTimeField(verbose_name='Modified Date', auto_now=True, blank=True, null=True)
    # created_date = models.DateTimeField(verbose_name='Create Date', auto_now_add=True, blank=True, null=True)

    def __str__(self):
        return self.user.user.email

    class Meta:
        verbose_name = "Job Invitation"
        verbose_name_plural = "Job Invitations"

    # def save(self, *args, **kwargs):
    #     super(JobInvitation, self).save(*args, **kwargs)
    #     if self.id:
    #         self.modified_by = self.user
    #         self.save()
    #     else:
    #         self.created_by = self.user
    #         self.save()


class ApplyJob(BaseTimestamp):
    """
    Client Apply Jobs model.
    """
    user = models.ForeignKey(Freelancer_Profile, related_name='applied_user',
                             verbose_name="User", blank=True, null=True)

    job = models.ForeignKey(ClientJobs, related_name='applied_jobs',
                            verbose_name="Job", blank=True, null=True)

    description = models.TextField(verbose_name='Description', blank=True, null=True)
    estimated_cost = models.FloatField(_('Estimated cost'), max_length=255, blank=True, null=True)
    document = models.FileField(upload_to='client/document_upload', max_length=100, blank=True, null=True)
    status = models.ForeignKey(InvitationStatus, related_name='applay_status',
                               verbose_name="Status", blank=True, null=True)
    def __str__(self):
        return "{0}::{1}".format(self.user.user.first_name, self.job.job_title)

    class Meta:
        verbose_name = "ApplyJob"
        verbose_name_plural = "ApplyJobs"

    # def save(self, *args, **kwargs):
    #     super(ApplyJob, self).save(*args, **kwargs)
    #     if self.id:
    #         self.modified_by = self.user
    #         self.save()
    #     else:
    #         self.created_by = self.user
    #         self.save()

class ApplyJobCancel(BaseTimestamp):
    """
    Cancel applied job
    """
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='cancel_applied_user',
                             verbose_name="User")

    job = models.ForeignKey(ClientJobs, related_name='cancel_applied_jobs',
                            verbose_name="Job")

    cancel_message = models.TextField(verbose_name='Cancel_message', blank=True, null=True)

    def __str__(self):
        return self.user.first_name

    class Meta:
        verbose_name = "ApplyJobCancel"
        verbose_name_plural = "ApplyJobCancels"
