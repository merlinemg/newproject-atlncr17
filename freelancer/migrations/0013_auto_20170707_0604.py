# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-07-07 06:04
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('freelancer', '0012_auto_20170707_0358'),
    ]

    operations = [
        migrations.AlterField(
            model_name='hourlyrate',
            name='actual_rate',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=7, null=True),
        ),
        migrations.AlterField(
            model_name='hourlyrate',
            name='hourly_rate',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=7, null=True),
        ),
        migrations.AlterField(
            model_name='locationdetails',
            name='address',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AlterField(
            model_name='locationdetails',
            name='city',
            field=models.ForeignKey(blank=True, max_length=255, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='city', to='cities_light.City'),
        ),
        migrations.AlterField(
            model_name='locationdetails',
            name='country',
            field=models.ForeignKey(blank=True, max_length=255, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='country', to='cities_light.Country'),
        ),
        migrations.AlterField(
            model_name='locationdetails',
            name='state',
            field=models.ForeignKey(blank=True, max_length=255, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='region', to='cities_light.Region'),
        ),
        migrations.AlterField(
            model_name='locationdetails',
            name='zip_code',
            field=models.CharField(blank=True, max_length=5, null=True),
        ),
    ]
