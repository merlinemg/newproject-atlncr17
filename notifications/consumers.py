import json
from django.contrib.auth.models import User
from channels import Group
from channels.sessions import channel_session

from accounts.models import Users, User_Type
from .models import Notifications


@channel_session
def ws_connect(message):
    message.reply_channel.send({"accept": True})
    Group('users').add(message.reply_channel)


@channel_session
def ws_receive(message):
    data = json.loads(message['text'])
    print data
    # try:
    #   user = Users.objects.get(id =data['user_id'])
    #
    # except:
    #     user = Users.objects.get(email=data['user_id'])
    #
    # try:
    #
    #   created_by = Users.objects.get(id =data['created_by'])
    # except:
    #
    #     created_by = Users.objects.get(email=data['created_by'])
    #
    # Notifications.objects.get_or_create(user=user,title=data['title'],created_by=created_by,status=0,description=data['description'],type=User_Type.objects.get(type=data['type']))

    Group('users').send({'text': json.dumps(data)})


@channel_session
def ws_disconnect(message):
    Group('users').discard(message.reply_channel)