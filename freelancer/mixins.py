from django.db.models import Q

from accounts.models import Users
from client.models import ClientJobs, ApplyJob, JobInvitation, ApplyJobCancel, InvitationStatus

from freelancer.models import Freelancer_Profile, Skills, WorkSubCategories, HourlyRate, LocationDetails, \
    FreelancerPortfolioimages,FreelancerPortfolio
from hire.models import HiredJobsContract


class MainMixin(object):
    def get_all_freelancer(self):
        key_words = []

        for skill in Freelancer_Profile.objects.filter(user__is_active=True):
            if skill.user.first_name not in key_words:
                key_words.append(skill.user.first_name )
        return key_words
    def get_users_list(self,type):
        if type == '0':
            data = Users.objects.filter(user_type__type__in=('CLIENTS','FREELANCER'),is_active=True)
        else:
            data = Users.objects.filter(user_type__type__in=('CLIENTS', 'FREELANCER'), is_active=True,is_online=True)

        return data
    def get_jobs_list(self):
        data = ClientJobs.objects.filter(job_status__status='Publish')
        return data
    def check_freelancer_profile(self, user):
        try:

            data = Freelancer_Profile.objects.get(user=user)

        except:
            data = None
        return data

    def check_hourly_rate(self, user):
        try:

            data = HourlyRate.objects.get(user=user)

        except:
            data = None
        return data

    def check_location(self, user):
        try:

            data = LocationDetails.objects.get(user=user)

        except:
            data = None
        return data

    def get_selected_skill(self, skills):
        """
        Get related skills.
        :return: skills object
        """
        skills_objs = Skills.objects.filter(pk__in=skills)

        return skills_objs

    def get_selected_subcategory(self, sub):
        """
        Get related WorkSubCategories.
        :return: WorkSubCategories object
        """
        sub_objs = WorkSubCategories.objects.filter(pk__in=sub)

        return sub_objs
    def get_Job_list(self,type):
        """
        Get related ApplyJob.
        :return: ApplyJob object
        """
        if type =='Applied':
            joblist = ApplyJob.objects.filter(user__user=self.request.user)
            print joblist, "dsdsdjoblistjoblistjoblist1"
        elif type == 'Invitation':
            joblist = JobInvitation.objects.filter(user__user=self.request.user)
            print joblist,"dsdsdjoblistjoblistjoblist2"

        else:
            joblist = HiredJobsContract.objects.filter(freelancer_profile__user=self.request.user,status__status__in=('Pending','Approved'))
            print joblist, "dsdsdjoblistjoblistjoblist3"
        return joblist

    def get_quotes_cancel_status(self):
        """
        Get quotes cancel status.
        :return:
        """

        quotes_cancel = ApplyJobCancel.objects.filter(user=self.request.user)
        return quotes_cancel

    def get_quotes_apply_status(self):
        """
        Get quotes cancel status.
        :return:
        """
        quotes_apply = ApplyJob.objects.filter(user__user=self.request.user)
        return quotes_apply
    def get_quote_cancel_array(self,quote_cancelled):
        """
        Get quotes cancel status.
        :return:

        """
        quote_cancel_array = []
        for quote_cancel in quote_cancelled:
            quote_cancel_array.append(int(quote_cancel.job.id))
        return quote_cancel_array
    def get_quote_apply_array(self,quote_applied):
        """
        Get quotes cancel status.
        :return:

        """
        quote_apply_array = []
        for quote_apply in quote_applied:
            quote_apply_array.append(int(quote_apply.job.id))
        return quote_apply_array

    def delete_existing_skill(self, user):
        """
        Get related Freelancer_Profile.
        :return: Freelancer_Profile object

        """
        user = Freelancer_Profile.objects.get(user=user)

        for skill_obj in Skills.objects.all():
            user.skills.remove(skill_obj)

    def delete_existing_subcategory(self, user):
        """
        Get related skills.
        :return: skills object

        """
        user = Freelancer_Profile.objects.get(user=user)
        for skill_obj in WorkSubCategories.objects.all():
            user.subcategory.remove(skill_obj)

    def get_sub_id(self, total):
        id = []
        print total

        if len(total) > 0:
            for datas in total:
                id.append(int(datas))
        else:
            id.append(total)

        return id
    def get_searchJob_list(self,type, serach_all,search_status, search, search_type, search_experiance, search_hourlymin,
                           search_hourlymax):
        joblist =  self.get_Job_list(type).order_by('-id')


        if serach_all  and search_status == '' and search=='' and search_type == '' and search_experiance == '' and search_hourlymin == '' and search_hourlymax == '':
            serach_all = serach_all.split(",")
            joblist = joblist.filter(Q(job__job_main_category__category_title__icontains=serach_all) | Q(
                job__job_main_category__category_title__in=serach_all)| Q(job__job_sub_category__sub_category_title__icontains=serach_all) | Q(
                job__job_sub_category__sub_category_title__in=serach_all) | Q(job__job_skills__name__icontains=serach_all) | Q(
                job__job_skills__name__in=serach_all) | Q(job__job_title__icontains=serach_all) | Q(
                job__job_title__in=serach_all)| Q(job__job_description__icontains=serach_all) | Q(
                job__job_description__in=serach_all)).distinct()




        #
        # if serach_all==''  and search_status == '' and search and search_type == '' and search_experiance == '' and search_hourlymin == '' and search_hourlymax == '':
        #     search = search.split(",")
        #     joblist = joblist.filter(Q(job_main_category__category_title__icontains=search) | Q(
        #         job_main_category__category_title__in=search)).distinct()
        #
        #


        elif serach_all == '' and search_status == '' and search and search_type == '' and search_experiance == '' and search_hourlymin == '' and search_hourlymax == '':
            search = filter(None, search.split(","))

            joblist = joblist.filter(Q(job__job_main_category__id__icontains=search) | Q(
               job__job_main_category__id__in=search)).distinct()




        elif serach_all==''  and search_status and search and search_type == '' and search_experiance == '' and search_hourlymin == '' and search_hourlymax == '':

            search = filter(None, search.split(","))
            search_status = search_status.split(",")

            joblist = joblist.filter((Q(
                job__job_main_category__id__icontains=search) | Q(
                job__job_main_category__id__in=search)) & Q(job__job_status__status__in=search_status)).distinct()

        elif serach_all==''  and search_status and search == '' and search_type == '' and search_experiance == '' and search_hourlymin == '' and search_hourlymax == '':

            search_status = search_status.split(",")

            joblist = joblist.filter(Q(job__job_status__status__in=search_status)).distinct()
        elif serach_all==''  and search_status == '' and search == '' and search_type and search_experiance == '' and search_hourlymin == '' and search_hourlymax == '':

            search_type = search_type.split(",")

            joblist = joblist.filter(
                Q(job__payment_type__type__icontains=search_type) | Q(job__payment_type__type__in=search_type)).distinct()
        elif serach_all==''  and search_status and search == '' and search_type and search_experiance == '' and search_hourlymin == '' and search_hourlymax == '':

            search_type = search_type.split(",")
            search_status = search_status.split(",")
            joblist = joblist.filter(
                Q(job__job_status__status__in=search_status) & Q(job__payment_type__type__in=search_type)).distinct()

        elif serach_all==''  and search_status == '' and search and search_type and search_experiance == '' and search_hourlymin == '' and search_hourlymax == '':

            search_type = search_type.split(",")
            search = filter(None, search.split(","))
            joblist = joblist.filter((Q(job__job_main_category__id__icontains=search) | Q(
                job__job_main_category__id__in=search)) & Q(job__payment_type__type__in=search_type)).distinct()

        elif serach_all==''  and search_status == '' and search == '' and search_type == '' and search_experiance and search_hourlymin == '' and search_hourlymax == '':

            search_experiance = search_experiance.split(",")
            joblist = joblist.filter(Q(job__experiance_level__in=search_experiance)).distinct()

        elif serach_all==''  and search_status and search == '' and search_type == '' and search_experiance and search_hourlymin == '' and search_hourlymax == '':
            search_status = search_status.split(",")
            search_experiance = search_experiance.split(",")
            joblist = joblist.filter(
                Q(job__experiance_level__in=search_experiance) & Q(job__job_status__in=search_status)).distinct()

        elif serach_all==''  and search_status == '' and search and search_type == '' and search_experiance and search_hourlymin == '' and search_hourlymax == '':
            search = filter(None, search.split(","))
            search_experiance = search_experiance.split(",")
            joblist = joblist.filter(Q(job__experiance_level__in=search_experiance) & (
                Q(job__job_main_category__id__icontains=search) | Q(
                    job__job_main_category__id__in=search))).distinct()


        elif serach_all==''  and search_status == '' and search == '' and search_type and search_experiance and search_hourlymin == '' and search_hourlymax == '':
            search_type = search_type.split(",")
            search_experiance = search_experiance.split(",")
            joblist = joblist.filter(
                Q(job__experiance_level__in=search_experiance) & Q(job__payment_type__type__in=search_type)).distinct()


        elif serach_all==''  and search_status and search and search_type == '' and search_experiance and search_hourlymin == '' and search_hourlymax == '':
            search_status = search_status.split(",")
            search = filter(None, search.split(","))
            search_experiance = search_experiance.split(",")
            joblist = joblist.filter((Q(job__job_main_category__id__icontains=search) | Q(
                job__job_main_category__id__in=search)) & Q(job__experiance_level__in=search_experiance) & Q(
                job__job_status__in=search_status)).distinct()


        elif serach_all==''  and search_status == '' and search and search_type and search_experiance and search_hourlymin == '' and search_hourlymax == '':
            search_type = search_type.split(",")
            search = filter(None, search.split(","))
            search_experiance = search_experiance.split(",")
            joblist = joblist.filter((Q(job__job_main_category__id__icontains=search) | Q(
                job__job_main_category__id__in=search)) & Q(job__experiance_level__in=search_experiance) & Q(
                job__payment_type__type__in=search_type)).distinct()


        elif serach_all==''  and search_status and search == '' and search_type and search_experiance and search_hourlymin == '' and search_hourlymax == '':
            search_type = search_type.split(",")
            search_status = search_status.split(",")
            search_experiance = search_experiance.split(",")
            joblist = joblist.filter(Q(job__experiance_level__in=search_experiance) & Q(
                job__payment_type__type__in=search_type) & Q(job__job_status__in=search_status)).distinct()


        elif serach_all==''  and search_status and search and search_type and search_experiance == '' and search_hourlymin == '' and search_hourlymax == '':
            search = filter(None, search.split(","))
            search_status = search_status.split(",")
            search_type = search_type.split(",")

            joblist = joblist.filter((Q(
                job__job_main_category__id__icontains=search) | Q(
                job__job_main_category__id__in=search)) & Q(job__job_status__in=search_status) & Q(
                job__payment_type__type__in=search_type)).distinct()


        elif serach_all==''  and search_status == '' and search == '' and search_type == '' and search_experiance == '' and search_hourlymin and search_hourlymax:

            joblist = joblist.filter(Q(job__budget__range=(search_hourlymin, search_hourlymax)) | (Q(job__hourly_min__gte=search_hourlymin) & Q(job__hourly_max__lte=search_hourlymax))).distinct()


        # elif search_status == '' and search == '' and search_type == '' and search_experiance == '' and search_hourlymin and search_hourlymax:
        #
        #     joblist = joblist.filter(Q(amount__range=(search_hourlymin, search_hourlymax))).distinct()

        elif serach_all==''  and search_status and search == '' and search_type == '' and search_experiance == '' and search_hourlymin and search_hourlymax:
            search_status = search_status.split(",")
            joblist = joblist.filter(
                Q(job__job_status__status__in=search_status) & (Q(job__budget__range=(search_hourlymin, search_hourlymax)) | (Q(job__hourly_min__gte=search_hourlymin) & Q(job__hourly_max__lte=search_hourlymax)))).distinct()
        elif serach_all==''  and search_status == '' and search and search_type == '' and search_experiance == '' and search_hourlymin and search_hourlymax:
            search = filter(None, search.split(","))
            joblist = joblist.filter((Q(
                job__job_main_category__id__icontains=search) | Q(
                job__job_main_category__id__in=search)) & (Q(job__budget__range=(search_hourlymin, search_hourlymax)) | (Q(job__hourly_min__gte=search_hourlymin) & Q(job__hourly_max__lte=search_hourlymax)))).distinct()
        elif serach_all==''  and search_status == '' and search == '' and search_type and search_experiance == '' and search_hourlymin and search_hourlymax:
            search_type = search_type.split(",")
            joblist = joblist.filter(Q(job__payment_type__type__in=search_type) & (Q(job__budget__range=(search_hourlymin, search_hourlymax)) | (Q(job__hourly_min__gte=search_hourlymin) & Q(job__hourly_max__lte=search_hourlymax)))).distinct()
        elif serach_all==''  and  search_status == '' and search == '' and search_type == '' and search_experiance and search_hourlymin and search_hourlymax:
            search_experiance = search_experiance.split(",")
            joblist = joblist.filter(Q(job__experiance_level__in=search_experiance) & (Q(job__budget__range=(search_hourlymin, search_hourlymax)) | (Q(job__hourly_min__gte=search_hourlymin) & Q(job__hourly_max__lte=search_hourlymax)))).distinct()

        elif serach_all==''  and search_status and search and search_type == '' and search_experiance == '' and search_hourlymin and search_hourlymax:
            search_status = search_status.split(",")
            search = filter(None, search.split(","))
            joblist = joblist.filter(Q(job__job_status__status__in=search_status) & (Q(
                job__job_main_category__id__icontains=search) | Q(
                job__job_main_category__id__in=search)) & (Q(job__budget__range=(search_hourlymin, search_hourlymax)) | (Q(job__hourly_min__gte=search_hourlymin) & Q(job__hourly_max__lte=search_hourlymax)))).distinct()

        elif serach_all==''  and search_status == '' and search and search_type and search_experiance == '' and search_hourlymin and search_hourlymax:
            search_type = search_type.split(",")
            search = filter(None, search.split(","))
            joblist = joblist.filter(Q(job__payment_type__type__in=search_type) & (Q(
                job__job_main_category__id__icontains=search) | Q(
                job__job_main_category__id__in=search)) & (Q(job__budget__range=(search_hourlymin, search_hourlymax)) | (Q(job__hourly_min__gte=search_hourlymin) & Q(job__hourly_max__lte=search_hourlymax)))).distinct()

        elif serach_all==''  and search_status == '' and search == '' and search_type and search_experiance and search_hourlymin and search_hourlymax:
            search_type = search_type.split(",")
            search_experiance = search_experiance.split(",")
            joblist = joblist.filter(
                Q(job__payment_type__type__in=search_type) & Q(job__experiance_level__in=search_experiance) & (Q(job__budget__range=(search_hourlymin, search_hourlymax)) | (Q(job__hourly_min__gte=search_hourlymin) & Q(job__hourly_max__lte=search_hourlymax)))).distinct()

        elif serach_all==''  and search_status and search == '' and search_type and search_experiance == '' and search_hourlymin and search_hourlymax:
            search_status = search_status.split(",")
            search_type = search_type.split(",")
            joblist = joblist.filter(Q(job__payment_type__type__in=search_type) & Q(job__job_status__status__in=search_status) & (Q(job__budget__range=(search_hourlymin, search_hourlymax)) | (Q(job__hourly_min__gte=search_hourlymin) & Q(job__hourly_max__lte=search_hourlymax)))).distinct()

        elif serach_all==''  and search_status == '' and search and search_type == '' and search_experiance and search_hourlymin and search_hourlymax:

            search = filter(None, search.split(","))
            search_experiance = search_experiance.split(",")
            joblist = joblist.filter(Q(job__experiance_level__in=search_experiance) & (Q(
                job__job_main_category__id__icontains=search) | Q(
                job__job_main_category__id__in=search)) & (Q(job__budget__range=(search_hourlymin, search_hourlymax)) | (Q(job__hourly_min__gte=search_hourlymin) & Q(job__hourly_max__lte=search_hourlymax)))).distinct()


        elif serach_all==''  and search_status == '' and search == '' and search_type == '' and search_experiance == '' and search_hourlymin and search_hourlymax == '':

            joblist = joblist.filter(Q(job__budget__gte=search_hourlymin) | Q(job__hourly_min__gte=search_hourlymin)).distinct()


        elif serach_all==''  and search_status and search == '' and search_type == '' and search_experiance == '' and search_hourlymin and search_hourlymax == '':
            search_status = search_status.split(",")
            joblist = joblist.filter(
                Q(job__job_status__status__in=search_status) & (Q(job__budget__gte=search_hourlymin) | Q(job__hourly_min__gte=search_hourlymin))).distinct()

        elif serach_all==''  and search_status == '' and search and search_type == '' and search_experiance == '' and search_hourlymin and search_hourlymax == '':
            search = filter(None, search.split(","))
            joblist = joblist.filter((Q(
                job__job_main_category__id__icontains=search) | Q(
                job__job_main_category__id__in=search)) & (Q(job__budget__gte=search_hourlymin) | Q(job__hourly_min__gte=search_hourlymin))).distinct()

        elif serach_all==''  and search_status == '' and search == '' and search_type and search_experiance == '' and search_hourlymin and search_hourlymax == '':
            search_type = search_type.split(",")
            joblist = joblist.filter(Q(job__payment_type__type__in=search_type) & (Q(job__budget__gte=search_hourlymin) | Q(job__hourly_min__gte=search_hourlymin))).distinct()

        elif serach_all==''  and search_status == '' and search == '' and search_type == '' and search_experiance and search_hourlymin and search_hourlymax == '':
            search_experiance = search_experiance.split(",")
            joblist = joblist.filter(
                Q(job__experiance_level__in=search_experiance) & (Q(job__budget__gte=search_hourlymin) | Q(job__hourly_min__gte=search_hourlymin))).distinct()

        elif serach_all==''  and search_status and search and search_type == '' and search_experiance == '' and search_hourlymin and search_hourlymax == '':
            search_status = search_status.split(",")
            search = filter(None, search.split(","))
            joblist = joblist.filter(Q(job__job_status__status__in=search_status) & (Q(
                job__job_main_category__id__icontains=search) | Q(
                job__job_main_category__id__in=search)) & (Q(job__budget__gte=search_hourlymin) | Q(job__hourly_min__gte=search_hourlymin))).distinct()


        elif serach_all==''  and search_status == '' and search and search_type and search_experiance == '' and search_hourlymin and search_hourlymax == '':
            search_type = search_type.split(",")
            search = filter(None, search.split(","))
            joblist = joblist.filter(Q(job__payment_type__type__in=search_type) & (Q(
                job__job_main_category__id__icontains=search) | Q(
                job__job_main_category__id__in=search)) & (Q(job__budget__gte=search_hourlymin) | Q(job__hourly_min__gte=search_hourlymin))).distinct()

        elif serach_all==''  and search_status == '' and search == '' and search_type and search_experiance and search_hourlymin and search_hourlymax == '':
            search_type = search_type.split(",")
            search_experiance = search_experiance.split(",")
            joblist = joblist.filter(
                Q(job__payment_type__type__in=search_type) & Q(job__experiance_level__in=search_experiance) & (Q(job__budget__gte=search_hourlymin) | Q(job__hourly_min__gte=search_hourlymin))).distinct()


        elif serach_all==''  and search_status and search == '' and search_type and search_experiance == '' and search_hourlymin and search_hourlymax == '':
            search_status = search_status.split(",")
            search_type = search_type.split(",")
            joblist = joblist.filter(Q(job__payment_type__type__in=search_type) & Q(job__job_status__status__in=search_status) & (Q(job__budget__gte=search_hourlymin) | Q(job__hourly_min__gte=search_hourlymin))).distinct()

        elif serach_all==''  and search_status == '' and search and search_type == '' and search_experiance and search_hourlymin and search_hourlymax == '':
            search = filter(None, search.split(","))
            search_experiance = search_experiance.split(",")
            joblist = joblist.filter(Q(job__experiance_level__in=search_experiance) & (Q(
                job__job_main_category__id__icontains=search) | Q(
                job__job_main_category__id__in=search)) & (Q(job__budget__gte=search_hourlymin) | Q(job__hourly_min__gte=search_hourlymin))).distinct()

        elif serach_all==''  and search_status and search and search_type == '' and search_experiance and search_hourlymin and search_hourlymax == '':
            search = filter(None, search.split(","))
            search_status = search_status.split(",")
            search_experiance = search_experiance.split(",")
            joblist = joblist.filter(Q(job__experiance_level__in=search_experiance) & (Q(
                job__job_main_category__id__icontains=search) | Q(
                job__job_main_category__id__in=search)) & Q(job__job_status__status__in=search_status) & (Q(job__budget__gte=search_hourlymin) | Q(job__hourly_min__gte=search_hourlymin))).distinct()

        elif serach_all==''  and search_status == '' and search and search_type and search_experiance and search_hourlymin and search_hourlymax == '':
            search = search.split(",")
            search_type = search_type.split(",")
            search_experiance = search_experiance.split(",")
            joblist = joblist.filter(Q(job__experiance_level__in=search_experiance) & (Q(
                job__job_main_category__id_icontains=search) | Q(
                job__job_main_category__id__in=search)) & Q(job__payment_type__type__in=search_type) &  (Q(job__budget__gte=search_hourlymin) | Q(job__hourly_min__gte=search_hourlymin))).distinct()




        elif serach_all==''  and search_status and search == '' and search_type and search_experiance and search_hourlymin and search_hourlymax == '':
            search_status = search_status.split(",")
            search_type = search_type.split(",")
            search_experiance = search_experiance.split(",")
            joblist = joblist.filter(
                Q(job__experiance_level__in=search_experiance) & Q(job__job_status__status__in=search_status) & Q(
                    job__payment_type__type__in=search_type) &  (Q(job__budget__gte=search_hourlymin) | Q(job__hourly_min__gte=search_hourlymin))).distinct()




        elif serach_all==''  and search_status and search and search_type and search_experiance == '' and search_hourlymin and search_hourlymax == '':
            search_status = search_status.split(",")
            search_type = search_type.split(",")
            search = filter(None, search.split(","))
            joblist = joblist.filter((Q(
                job__job_main_category__id__icontains=search) | Q(
                job__job_main_category__id__in=search)) & Q(job__job_status__status__in=search_status) & Q(
                job__payment_type__type__in=search_type) &  (Q(job__budget__gte=search_hourlymin) | Q(job__hourly_min__gte=search_hourlymin))).distinct()




        elif serach_all==''  and search_status and search and search_type and search_experiance and search_hourlymin and search_hourlymax == '':
            search_status = search_status.split(",")
            search_type = search_type.split(",")
            search = filter(None, search.split(","))
            search_experiance = search_experiance.split(",")
            joblist = joblist.filter((Q(
                job__job_main_category__id__icontains=search) | Q(
                job__job_main_category__id__in=search)) & Q(job__experiance_level__in=search_experiance) & Q(
                job__job_status__status__in=search_status) & Q(job__payment_type__type__in=search_type) & (Q(job__budget__gte=search_hourlymin) | Q(job__hourly_min__gte=search_hourlymin))).distinct()

        elif serach_all==''  and search_status == '' and search == '' and search_type == '' and search_experiance == '' and search_hourlymin == '' and search_hourlymax:


            joblist = joblist.filter( (Q(job__budget__lte=search_hourlymax) & Q(job__hourly_max__lte=search_hourlymax))).distinct()


        elif serach_all==''  and search_status and search == '' and search_type == '' and search_experiance == '' and search_hourlymin == '' and search_hourlymax:
            search_status = search_status.split(",")
            joblist = joblist.filter(
                Q(job__job_status__status__in=search_status) & (Q(job__budget__lte=search_hourlymax) & Q(job__hourly_max__lte=search_hourlymax))).distinct()

        elif serach_all==''  and search_status == '' and search and search_type == '' and search_experiance == '' and search_hourlymin == '' and search_hourlymax:
            search = filter(None, search.split(","))
            joblist = joblist.filter((Q(
                job__job_main_category__id__icontains=search) | Q(
                job__job_main_category__id__in=search)) & (Q(job__budget__lte=search_hourlymax) & Q(job__hourly_max__lte=search_hourlymax))).distinct()
            print(joblist), "hx";
        elif serach_all==''  and search_status == '' and search == '' and search_type and search_experiance == '' and search_hourlymin == '' and search_hourlymax:
            search_type = search_type.split(",")
            joblist = joblist.filter(
                Q(job__payment_type__type__in=search_type) & (Q(job__budget__lte=search_hourlymax) & Q(job__hourly_max__lte=search_hourlymax))).distinct()

        elif serach_all==''  and search_status == '' and search == '' and search_type == '' and search_experiance and search_hourlymin == '' and search_hourlymax:
            search_experiance = search_experiance.split(",")
            joblist = joblist.filter(
                Q(job__experiance_level__in=search_experiance) & (Q(job__budget__lte=search_hourlymax) & Q(job__hourly_max__lte=search_hourlymax))).distinct()

        elif serach_all==''  and search_status and search and search_type == '' and search_experiance == '' and search_hourlymin == '' and search_hourlymax:
            search_status = search_status.split(",")
            search = filter(None, search.split(","))
            joblist = joblist.filter(Q(job__job_status__status__in=search_status) & (Q(
                job__job_main_category__id__icontains=search) | Q(
                job__job_main_category__id__in=search)) & (Q(job__budget__lte=search_hourlymax) & Q(job__hourly_max__lte=search_hourlymax))).distinct()

        elif serach_all==''  and search_status == '' and search and search_type and search_experiance == '' and search_hourlymin == '' and search_hourlymax:

            search_type = search_type.split(",")
            search = filter(None, search.split(","))
            joblist = joblist.filter(Q(job__payment_type__type__in=search_type) & (Q(
                job__job_main_category__id__icontains=search) | Q(
                job__job_main_category__id__in=search)) & (Q(job__budget__lte=search_hourlymax) & Q(job__hourly_max__lte=search_hourlymax))).distinct()

        elif serach_all==''  and search_status == '' and search == '' and search_type and search_experiance and search_hourlymin == '' and search_hourlymax:
            search_type = search_type.split(",")
            search_experiance = search_experiance.split(",")
            joblist = joblist.filter(
                Q(job__payment_type__type__in=search_type) & Q(job__experiance_level__in=search_experiance) & (Q(job__budget__lte=search_hourlymax) & Q(job__hourly_max__lte=search_hourlymax))).distinct()

        elif serach_all==''  and search_status and search == '' and search_type and search_experiance == '' and search_hourlymin == '' and search_hourlymax:
            search_status = search_status.split(",")
            search_type = search_type.split(",")
            joblist = joblist.filter(Q(job__payment_type__type__in=search_type) & Q(job__job_status__status__in=search_status) & (Q(job__budget__lte=search_hourlymax) & Q(hourly_max__lte=search_hourlymax))).distinct()


        elif serach_all==''  and search_status == '' and search and search_type == '' and search_experiance and search_hourlymin == '' and search_hourlymax:
            search = filter(None, search.split(","))
            search_experiance = search_experiance.split(",")
            joblist = joblist.filter(Q(job__experiance_level__in=search_experiance) & (Q(
                job__job_main_category__id__icontains=search) | Q(
                job__job_main_category__id__in=search)) & (Q(job__budget__lte=search_hourlymax) & Q(job__hourly_max__lte=search_hourlymax))).distinct()



        elif serach_all==''  and search_status and search and search_type == '' and search_experiance and search_hourlymin == '' and search_hourlymax:
            search = filter(None, search.split(","))
            search_status = search_status.split(",")
            search_experiance = search_experiance.split(",")
            joblist = joblist.filter(Q(job__experiance_level__in=search_experiance) & (Q(
                job__job_main_category__id__icontains=search) | Q(
                job__job_main_category__id__in=search)) & Q(job__job_status__status__in=search_status) & (Q(job__budget__lte=search_hourlymax) & Q(job__hourly_max__lte=search_hourlymax))).distinct()




        elif serach_all==''  and search_status == '' and search and search_type and search_experiance and search_hourlymin == '' and search_hourlymax:
            search = filter(None, search.split(","))
            search_type = search_type.split(",")
            search_experiance = search_experiance.split(",")
            joblist = joblist.filter(Q(job__experiance_level__in=search_experiance) & (Q(
                job__job_main_category__id__icontains=search) | Q(
                job__job_main_category__id__in=search)) & Q(job__payment_type__type__in=search_type) & (Q(job__budget__lte=search_hourlymax) & Q(job__hourly_max__lte=search_hourlymax))).distinct()




        elif serach_all==''  and search_status and search == '' and search_type and search_experiance and search_hourlymin == '' and search_hourlymax:
            search_status = search_status.split(",")
            search_type = search_type.split(",")
            search_experiance = search_experiance.split(",")
            joblist = joblist.filter(Q(job__experiance_level__in=search_experiance) & Q(job__job_status__status__in=search_status) & Q(
                job__payment_type__type__in=search_type) & (Q(job__budget__lte=search_hourlymax) & Q(job__hourly_max__lte=search_hourlymax))).distinct()




        elif serach_all==''  and search_status and search and search_type and search_experiance == '' and search_hourlymin == '' and search_hourlymax:
            search_status = search_status.split(",")
            search_type = search_type.split(",")
            search = filter(None, search.split(","))
            joblist = joblist.filter((Q(
                job__job_main_category__id__icontains=search) | Q(
                job__job_main_category__id__in=search)) & Q(job__job_status__status__in=search_status) & Q(
                job__payment_type__type__in=search_type) & (Q(job__budget__lte=search_hourlymax) & Q(job__hourly_max__lte=search_hourlymax))).distinct()




        elif serach_all==''  and search_status and search and search_type and search_experiance and search_hourlymin == '' and search_hourlymax:
            search_status = search_status.split(",")
            search_type = search_type.split(",")
            search = filter(None, search.split(","))
            search_experiance = search_experiance.split(",")
            joblist = joblist.filter((Q(
                job__job_main_category__id__icontains=search) | Q(
                job__job_main_category__id__in=search)) & Q(job__experiance_level__in=search_experiance) & Q(
                job__job_status__status__in=search_status) & Q(job__payment_type__type__in=search_type) & (Q(job__budget__lte=search_hourlymax) & Q(job__hourly_max__lte=search_hourlymax))).distinct()
            print joblist, "fffffff"



        elif serach_all==''  and search_status and search == '' and search_type and search_experiance and search_hourlymin == '' and search_hourlymax:
            search = filter(None, search.split(","))
            search_experiance = search_experiance.split(",")
            joblist = joblist.filter(Q(job__experiance_level__in=search_experiance) & (Q(
                job__job_main_category__id__icontains=search) | Q(
                job__job_main_category__id__in=search)) & (Q(job__budget__lte=search_hourlymax) & Q(hourly_max__lte=search_hourlymax))).distinct()




        elif serach_all==''  and search_status and search and search_type and search_experiance and search_hourlymin and search_hourlymax:
            search = filter(None, search.split(","))
            search_status = search_status.split(",")
            search_type = search_type.split(",")
            search_experiance = search_experiance.split(",")

            joblist = joblist.filter((Q(
                job__job_main_category__id__icontains=search) | Q(
                job__job_main_category__id__in=search)) & Q(job__experiance_level__in=search_experiance) & Q(
                job__job_status__status__in=search_status) & Q(
                job__payment_type__type__in=search_type) & ((Q(job__budget__range=(search_hourlymin, search_hourlymax)))| (Q(job__hourly_max__lte=search_hourlymax) & Q(job__hourly_min__gte=search_hourlymin)))).distinct()
            print joblist, "fshibif"


        elif serach_all==''  and search_status and search and search_type and search_experiance and search_hourlymin == '' and search_hourlymax == '':
            search = filter(None, search.split(","))
            search_status = search_status.split(",")
            search_type = search_type.split(",")
            search_experiance = search_experiance.split(",")

            joblist = joblist.filter((Q(
                job__job_main_category__id__icontains=search) | Q(
                job__job_main_category__id__in=search)) & Q(job__experiance_level__in=search_experiance) & Q(
                job__job_status__status__in=search_status) & Q(
                job__payment_type__type__in=search_type)).distinct()


        else:

            joblist = joblist
        return joblist
    def get_freelancer_protfolio(self,id):
        return FreelancerPortfolio.objects.get(id=id)
    def get_all_portfolioimages(self,id):
        return FreelancerPortfolioimages.objects.filter(portfolio_id=id)
    def get_portfolioimages(self,id):
        return FreelancerPortfolioimages.objects.get(portfolio_id=id)
    def get_current_user(self,id):
        return Users.objects.get(token=id)

