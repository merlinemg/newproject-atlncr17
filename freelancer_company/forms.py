from django import forms

from freelancer_company.models import Freelancer_Company_Profile


class Freelancer_Company_ProfileForm(forms.ModelForm):
    class Meta:
        model = Freelancer_Company_Profile
        fields = ['company_name', 'description', 'workcategory', 'subcategory', 'skills', 'logo']

    def clean_company_name(self):
        if self.cleaned_data["company_name"].strip() == '':
            raise forms.ValidationError("Company Name is required.")
        return self.cleaned_data["company_name"]

    # def clean_description(self):
    #     if self.cleaned_data["description"].strip() == '':
    #         raise forms.ValidationError("Description is required.")
    #     return self.cleaned_data["description"]
    #
    # def clean_workcategory(self):
    #     if  self.cleaned_data["workcategory"] =='' :
    #         raise forms.ValidationError("Workcategory is required.")
    #     return self.cleaned_data["workcategory"]

    # def clean_subcategory(self):
    #     if not self.cleaned_data["subcategory"]:
    #         raise forms.ValidationError("Subcategory is required.")
    #     return self.cleaned_data["subcategory"]
    #
    # def clean_skills(self):
    #     if not self.cleaned_data["skills"]:
    #         raise forms.ValidationError("Skills is required.")
    #     return self.cleaned_data["skills"]

    def __init__(self, *args, **kwargs):
        super(Freelancer_Company_ProfileForm, self).__init__(*args, **kwargs)

        for field in self.fields:
            self.fields['company_name'].error_messages['required'] = 'Company Name is required'
            self.fields['workcategory'].error_messages['required'] = 'Workcategory  is required'
            #self.fields['workcategory'].empty_label = 'Select Category'

    def save(self, user, skills, subcategory,workcategory, commit=True):
        form = super(Freelancer_Company_ProfileForm, self).save(commit=False)

        # form.workcategory = values['workcategory']
        # form.subcategory = values['subcategory']


        if commit:
            form.user = user
            form.save()
            try:
                form.skills.add(skills)
                form.subcategory.add(subcategory)
                form.workcategory.add(workcategory)

            except:
                for skill in skills:
                    form.skills.add(skill)
                for subcategorys in subcategory:
                    # form.subcategory.remove(subcategorys)
                    form.subcategory.add(subcategorys)
                for workcategorys in workcategory:
                    # form.subcategory.remove(subcategorys)
                    form.workcategory.add(workcategorys)

                    # for skill in skills:
                    #
                    #     form.skills.add(skill)
                    # for subcategorys in subcategory:
                    #     form.subcategory.remove(subcategorys)
                    #     form.subcategory.add(subcategorys)

                    # form.skills = self.cleaned_data["skills"]
        return form
