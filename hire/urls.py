from django.conf.urls import url
from django.contrib.auth.decorators import login_required

from hire.views import HireView, FreelancerHireView, MilestoneListView

urlpatterns = [

    url(r'hire/(?P<slug>[-\w]+)/$', login_required(HireView.as_view()), name='hireviews'),
    url(r'hires/(?P<slug>[-\w]+)/$', login_required(FreelancerHireView.as_view()), name='hireviewsfreelancer'),
    url(r'listing-milestones$', login_required(MilestoneListView.as_view()), name='listmilestone'),

]