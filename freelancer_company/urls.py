from django.conf.urls import url
from django.contrib.auth.decorators import login_required

from freelancer_company.views import FreelancerCompany, FreelancerCompanyView, InvitedUserList

urlpatterns = [
    url(r'freelancer-company/$', login_required(FreelancerCompany.as_view()), name='freelancercompany'),
    url(r'freelancer-company-view/$', login_required(FreelancerCompanyView.as_view()), name='freelancercompanyview'),
    url(r'freelancer-pagination/$', login_required(InvitedUserList.as_view()), name='invitedlist'),
]