from django.conf.urls import url
from django.contrib.auth.decorators import login_required

from site_administration import views
from site_administration.views import AdminDashboard, AdmincreateList,AdmincreateRole, AdminClientList, logout, EditRole, \
      AdminFreelancerList

urlpatterns = [
      #url(r'^$', views.index, name='index'),admindashboard
      url(r'^$', views.siteAdminView, name='siteAdminView'),
      url(r'admindashboard/$', login_required(AdminDashboard.as_view()), name='admindashboard'),
      url(r'admincreaterolelist/$', login_required(AdmincreateList.as_view()), name='AdmincreateList'),
      url(r'admincreaterole/$', login_required(AdmincreateRole.as_view()), name='AdmincreateRole'),
      url(r'Adminclientlist/$', login_required(AdminClientList.as_view()), name='AdminClientList'),
      url(r'AdminFreelancerList/$', login_required(AdminFreelancerList.as_view()), name='AdminFreelancerList'),
      url(r'^adminlogout/$', logout, {"next_page": '/site_administration/'}, name="adminlogout"),
      url(r'^editrole/(?P<slug>[-\w]+)/$', login_required(EditRole.as_view()), name='editrole')

]