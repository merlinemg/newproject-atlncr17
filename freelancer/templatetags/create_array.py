import base64

from django import template
from django.contrib.auth.models import Group
from django.http import request

from accounts.models import Users
from hire.models import HiredJobsContract
from notifications.models import Notifications

register = template.Library()


@register.simple_tag(name='create_an_array')
def create_an_array(total):
    print total, "ffffffffffffffffffffffffffffffffid"
    id = []
    for datas in total:
        id.append(int(datas.pk))

    return id
@register.simple_tag(name='create_id')
def create_id(total):


    if total != 0:
        if total.workcategory :
          return total.workcategory.pk
    else:
        return 0

@register.simple_tag(name='get_image_path')
def get_image_path(total):


    if total != 0:
        if total.profile :
          return total.profile
    else:
        return 0

@register.simple_tag(name='create_range')
def create_range(total):
    print max(total.paginator.page_range), "ffffffffffffffffffffffffffffffffid"

    page =total.number
    total_page =max(total.paginator.page_range)
    #total_page =20
    if page not in [1,2,3]:
        cpage =min(page-1,page-2)
        get = min(cpage + 5, total_page)
        new = xrange(page-2, min(page+3,total_page))
        print len(new),"nn"
        if len(new)< 5:
            if len(new)/2 == 2:
                new = xrange(page - 3, min(page + 3, total_page))
            else:
                new = xrange(page - len(new)-1, min(page + 3, total_page))

        print len(new), "nnffffffffff"

    else:
        cpage = 0
        get = min(cpage+6,total_page)
        new =xrange(cpage+1,get)



    return new
@register.inclusion_tag('common/pagination.html', takes_context=True)
def get_pagination(context, first_last_amount=2, before_after_amount=4):
    page_obj = context['page_obj']
    paginator = context['paginator']
    is_paginated = context['is_paginated']
    page_numbers = []

    # Pages before current page
    if page_obj.number > first_last_amount + before_after_amount:
        for i in range(1, first_last_amount + 1):
            page_numbers.append(i)

        if first_last_amount + before_after_amount + 1 != paginator.num_pages:
            page_numbers.append(None)

        for i in range(page_obj.number - before_after_amount, page_obj.number):
            page_numbers.append(i)

    else:
        for i in range(1, page_obj.number):
            page_numbers.append(i)

    # Current page and pages after current page
    if page_obj.number + first_last_amount + before_after_amount < paginator.num_pages:
        for i in range(page_obj.number, page_obj.number + before_after_amount + 1):
            page_numbers.append(i)

        page_numbers.append(None)

        for i in range(paginator.num_pages - first_last_amount + 1, paginator.num_pages + 1):
            page_numbers.append(i)

    else:
        for i in range(page_obj.number, paginator.num_pages + 1):
            page_numbers.append(i)

    return {
        'paginator': paginator,
        'page_obj': page_obj,
        'page_numbers': page_numbers,
        'is_paginated': is_paginated,
    }
@register.simple_tag(name='check_hired_or_not')
def check_hired_or_not(jobid,freelancer):
    print jobid,"gggggggggg"
    try:
        hire = HiredJobsContract.objects.get(freelancer_profile__user__email=freelancer,client_job=jobid)

        return hire
    except:
        return None


@register.simple_tag(name='create_base64')
def create_base64(path):
    try:
        img = open(path.path, "rb")
        data = img.read()
        return "data:image/jpg;base64,%s" % data.encode('base64')

    except IOError:
        return path.url
@register.simple_tag(name='Get_notification')
def Get_notification(user):
     print user,"gggggggggg"
     return Notifications.objects.filter(user__id=user,status=0)
