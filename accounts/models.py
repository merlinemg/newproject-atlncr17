from __future__ import unicode_literals

from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.contrib.auth.models import AbstractUser, UserManager, PermissionsMixin
from django.db import models
from cities_light.models import Country
from django.utils.crypto import get_random_string
from django.utils.translation import ugettext_lazy as _


# Create your models here.


class Users_Status(models.Model):
    status = models.CharField(max_length=200)

    def __unicode__(self):
        return self.status
class User_Type(models.Model):
    type = models.CharField(max_length=200)

    def __unicode__(self):
        return self.type

class UserCustomManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self,  email, password, **extra_fields):
        """
        Creates and saves a User with the given username, email and password.
        """

        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)

        user.save(using=self._db)
        return user

    def create_user(self,  email, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self,  email, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)


class Users(AbstractBaseUser,PermissionsMixin):
    email = models.EmailField(unique=True)
    USERNAME_FIELD = 'email'
    ACCOUNT_TYPES = (
        ('CLIENT', 'client'),
        ('FREELANCER', 'freelancer'),
        ('Site_admin', 'admin'),
    )
    INVITE_TYPES = (
        ('CLIENT', 'client'),
        ('FREELANCER', 'freelancer'),
        ('BOTH', 'both'),
        ('OWNER', 'owner'),
    )
    USER_STATUS = (
        ('Active', 'active'),
        ('Suspended', 'suspended'),
        ('Deactivate', 'deactivate'),
    )
    id =models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')
    first_name = models.CharField(_('first name'), max_length=30)
    last_name = models.CharField(_('last name'), max_length=30)
    date_joined = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(default=True)
    is_superuser = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    is_online = models.BooleanField(default=False)

    user_type = models.ForeignKey(User_Type,related_name='user_type',null=True, blank=True,verbose_name="User Type")
    active_account = models.ForeignKey(User_Type, related_name='active_account',null=True, blank=True,verbose_name="Active Type")
    invite_type = models.ForeignKey(User_Type, related_name='invite_type',null=True, blank=True,verbose_name="Invite Type")

    # user_type = models.CharField(max_length=10,
    #                              choices=ACCOUNT_TYPES,
    #                              default='admin', verbose_name="User Type")
    # active_account = models.CharField(max_length=10,
    #                                   choices=ACCOUNT_TYPES,
    #                                   default='admin')
    # invite_type=models.CharField(max_length=10,
    #                                   choices=INVITE_TYPES,
    #                                   default='owner')

    token =models.CharField(max_length=255,unique=False,null=True,blank=True)

    objects = UserCustomManager()
    created_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)

    #country = models.CharField(Country, max_length=255)
    # user_status = models.CharField(max_length=10,
    # choices=USER_STATUS,
    # default='active', verbose_name="User Status")
   # user_status = models.ForeignKey(Users_Status, on_delete=models.CASCADE)
    # USERNAME_FIELD = 'email'
    # REQUIRED_FIELDS = ['username']
    #
    # def get_username(self):
    #     return self.email

    def save(self, *args, **kwargs):
        super(Users, self).save(*args, **kwargs)
        if not self.token:
            unique_id = get_random_string(length=32)
            self.token = unique_id
            self.save()
    def get_client_company_intance(self):
        try:
            from client_company.models import Client_Company_Profile
            instance = Client_Company_Profile.objects.get(user__pk=self.pk)
        except:
            from client_company.models import Invited_Client_Company_User

            instance = Invited_Client_Company_User.objects.get(user__pk=self.pk).client_company


            pass

        return instance

    def get_freelancer_company_intance(self):
        try:

            from freelancer_company.models import Freelancer_Company_Profile

            instance = Freelancer_Company_Profile.objects.get(user__pk=self.pk)

        except:

                from freelancer_company.models import Invited_Freelancer_Company_User
                instance = Invited_Freelancer_Company_User.objects.get(user__pk=self.pk).freelancer_company
        pass

        return instance
    def get_full_name(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        "Returns the short name for the user."
        return self.first_name


class BaseTimestamp(models.Model):
    created_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(Users, null=True, blank=True,related_name='+' )
    modified_by = models.ForeignKey(Users, null=True, blank=True,related_name='+')

    class Meta:
        abstract = True
