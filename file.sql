-- MySQL dump 10.13  Distrib 5.5.54, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: new_project_atlancer
-- ------------------------------------------------------
-- Server version	5.5.54-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accounts_users`
--

DROP TABLE IF EXISTS `accounts_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts_users` (
  `password` varchar(128) NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime NOT NULL,
  `email` varchar(254) NOT NULL,
  `user_type` varchar(10) NOT NULL,
  PRIMARY KEY (`email`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts_users`
--

LOCK TABLES `accounts_users` WRITE;
/*!40000 ALTER TABLE `accounts_users` DISABLE KEYS */;
INSERT INTO `accounts_users` VALUES ('pbkdf2_sha256$30000$t8qlnZkkFtWN$geid3DxVxnf30C+Xgr56mVsLPj1Cos4SRYKWQAnQIYw=',NULL,1,'admin@gmail.com','admin','admin',0,1,'2017-06-29 04:33:37','admin@gmail.com','Site_admin'),('pbkdf2_sha256$30000$PfMMhTM3Cqal$2M9gSMt6PS4zy0nOe405MujdQYgRZptr3ZgB44KO3no=','2017-06-29 04:40:50',0,'shibila.b@spericorn.com','client02','client02',0,1,'2017-06-29 04:40:15','shibila.b@spericorn.com','FREELANCER'),('pbkdf2_sha256$30000$DEeCEgX7tL0G$kmC1whUy+KoBBpAJ0dVB3zBsQ0x5GCVc6+bWMrlNYCw=',NULL,0,'superadmin@gmail.com','superadmin','superadmin',0,1,'2017-06-29 04:33:37','superadmin@gmail.com','Site_admin'),('pbkdf2_sha256$30000$bVj0vvocoPRw$1jIJFPPOtFwVwDFpCoZ16lKk0/IhiKZX47m3q9Zd49Y=','2017-06-29 04:35:17',1,'test','','',1,1,'2017-06-29 04:35:10','test@gmail.com','admin');
/*!40000 ALTER TABLE `accounts_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accounts_users_groups`
--

DROP TABLE IF EXISTS `accounts_users_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts_users_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `users_id` varchar(254) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `accounts_users_groups_users_id_8dfb39d5_uniq` (`users_id`,`group_id`),
  KEY `accounts_users_groups_group_id_371be490_fk_auth_group_id` (`group_id`),
  CONSTRAINT `accounts_users_groups_group_id_371be490_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `accounts_users_groups_users_id_c8303f87_fk_accounts_users_email` FOREIGN KEY (`users_id`) REFERENCES `accounts_users` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts_users_groups`
--

LOCK TABLES `accounts_users_groups` WRITE;
/*!40000 ALTER TABLE `accounts_users_groups` DISABLE KEYS */;
INSERT INTO `accounts_users_groups` VALUES (2,'shibila.b@spericorn.com',1);
/*!40000 ALTER TABLE `accounts_users_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accounts_users_status`
--

DROP TABLE IF EXISTS `accounts_users_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts_users_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts_users_status`
--

LOCK TABLES `accounts_users_status` WRITE;
/*!40000 ALTER TABLE `accounts_users_status` DISABLE KEYS */;
/*!40000 ALTER TABLE `accounts_users_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accounts_users_user_permissions`
--

DROP TABLE IF EXISTS `accounts_users_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts_users_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `users_id` varchar(254) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `accounts_users_user_permissions_users_id_866b235f_uniq` (`users_id`,`permission_id`),
  KEY `accounts_users_user_permission_id_5f3d0fff_fk_auth_permission_id` (`permission_id`),
  CONSTRAINT `accounts_users_user_permission_id_5f3d0fff_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `accounts_users_user_pe_users_id_62a47b5b_fk_accounts_users_email` FOREIGN KEY (`users_id`) REFERENCES `accounts_users` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts_users_user_permissions`
--

LOCK TABLES `accounts_users_user_permissions` WRITE;
/*!40000 ALTER TABLE `accounts_users_user_permissions` DISABLE KEYS */;
INSERT INTO `accounts_users_user_permissions` VALUES (2,'shibila.b@spericorn.com',52);
/*!40000 ALTER TABLE `accounts_users_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
INSERT INTO `auth_group` VALUES (1,'freelancer');
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissi_permission_id_84c5c92e_fk_auth_permission_id` (`permission_id`),
  CONSTRAINT `auth_group_permissi_permission_id_84c5c92e_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permissi_content_type_id_2f476e4b_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add log entry',1,'add_logentry'),(2,'Can change log entry',1,'change_logentry'),(3,'Can delete log entry',1,'delete_logentry'),(4,'Can add permission',2,'add_permission'),(5,'Can change permission',2,'change_permission'),(6,'Can delete permission',2,'delete_permission'),(7,'Can add group',3,'add_group'),(8,'Can change group',3,'change_group'),(9,'Can delete group',3,'delete_group'),(10,'Can add content type',4,'add_contenttype'),(11,'Can change content type',4,'change_contenttype'),(12,'Can delete content type',4,'delete_contenttype'),(13,'Can add session',5,'add_session'),(14,'Can change session',5,'change_session'),(15,'Can delete session',5,'delete_session'),(16,'Can add role_ permission',6,'add_role_permission'),(17,'Can change role_ permission',6,'change_role_permission'),(18,'Can delete role_ permission',6,'delete_role_permission'),(19,'Can add permission',7,'add_permission'),(20,'Can change permission',7,'change_permission'),(21,'Can delete permission',7,'delete_permission'),(22,'Can add users_ status',8,'add_users_status'),(23,'Can change users_ status',8,'change_users_status'),(24,'Can delete users_ status',8,'delete_users_status'),(25,'Can add user',9,'add_users'),(26,'Can change user',9,'change_users'),(27,'Can delete user',9,'delete_users'),(28,'Can add skills',10,'add_skills'),(29,'Can change skills',10,'change_skills'),(30,'Can delete skills',10,'delete_skills'),(31,'Can add Work Sub Category',11,'add_worksubcategories'),(32,'Can change Work Sub Category',11,'change_worksubcategories'),(33,'Can delete Work Sub Category',11,'delete_worksubcategories'),(34,'Can add Work Category',12,'add_workcategories'),(35,'Can change Work Category',12,'change_workcategories'),(36,'Can delete Work Category',12,'delete_workcategories'),(37,'Can add freelancer_ profile',13,'add_freelancer_profile'),(38,'Can change freelancer_ profile',13,'change_freelancer_profile'),(39,'Can delete freelancer_ profile',13,'delete_freelancer_profile'),(40,'Can add country',14,'add_country'),(41,'Can change country',14,'change_country'),(42,'Can delete country',14,'delete_country'),(43,'Can add city',15,'add_city'),(44,'Can change city',15,'change_city'),(45,'Can delete city',15,'delete_city'),(46,'Can add region/state',16,'add_region'),(47,'Can change region/state',16,'change_region'),(48,'Can delete region/state',16,'delete_region'),(49,'Can add registration profile',17,'add_registrationprofile'),(50,'Can change registration profile',17,'change_registrationprofile'),(51,'Can delete registration profile',17,'delete_registrationprofile'),(52,'',9,'View_Dashboard_Freelancer');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cities_light_city`
--

DROP TABLE IF EXISTS `cities_light_city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cities_light_city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_ascii` varchar(200) NOT NULL,
  `slug` varchar(50) NOT NULL,
  `geoname_id` int(11) DEFAULT NULL,
  `alternate_names` longtext,
  `name` varchar(200) NOT NULL,
  `display_name` varchar(200) NOT NULL,
  `search_names` longtext NOT NULL,
  `latitude` decimal(8,5) DEFAULT NULL,
  `longitude` decimal(8,5) DEFAULT NULL,
  `region_id` int(11) DEFAULT NULL,
  `country_id` int(11) NOT NULL,
  `population` bigint(20) DEFAULT NULL,
  `feature_code` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `geoname_id` (`geoname_id`),
  UNIQUE KEY `cities_light_city_region_id_29b81cd4_uniq` (`region_id`,`name`),
  UNIQUE KEY `cities_light_city_region_id_dc18c213_uniq` (`region_id`,`slug`),
  KEY `cities_light_city_country_id_cf310fd2_fk_cities_light_country_id` (`country_id`),
  KEY `cities_light_city_d7397f31` (`name_ascii`),
  KEY `cities_light_city_2dbcba41` (`slug`),
  KEY `cities_light_city_b068931c` (`name`),
  KEY `cities_light_city_248081ec` (`population`),
  KEY `cities_light_city_3f98884f` (`feature_code`),
  CONSTRAINT `cities_light_city_country_id_cf310fd2_fk_cities_light_country_id` FOREIGN KEY (`country_id`) REFERENCES `cities_light_country` (`id`),
  CONSTRAINT `cities_light_city_region_id_f7ab977b_fk_cities_light_region_id` FOREIGN KEY (`region_id`) REFERENCES `cities_light_region` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cities_light_city`
--

LOCK TABLES `cities_light_city` WRITE;
/*!40000 ALTER TABLE `cities_light_city` DISABLE KEYS */;
INSERT INTO `cities_light_city` VALUES (1,'xc','xc',NULL,'xc','xc','xc, state2, country','xccountry xcstate2country xcstatecountry',NULL,NULL,1,1,9,NULL);
/*!40000 ALTER TABLE `cities_light_city` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cities_light_country`
--

DROP TABLE IF EXISTS `cities_light_country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cities_light_country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_ascii` varchar(200) NOT NULL,
  `slug` varchar(50) NOT NULL,
  `geoname_id` int(11) DEFAULT NULL,
  `alternate_names` longtext,
  `name` varchar(200) NOT NULL,
  `code2` varchar(2) DEFAULT NULL,
  `code3` varchar(3) DEFAULT NULL,
  `continent` varchar(2) NOT NULL,
  `tld` varchar(5) NOT NULL,
  `phone` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `geoname_id` (`geoname_id`),
  UNIQUE KEY `code2` (`code2`),
  UNIQUE KEY `code3` (`code3`),
  KEY `cities_light_country_d7397f31` (`name_ascii`),
  KEY `cities_light_country_2dbcba41` (`slug`),
  KEY `cities_light_country_0e20e84e` (`continent`),
  KEY `cities_light_country_a311df50` (`tld`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cities_light_country`
--

LOCK TABLES `cities_light_country` WRITE;
/*!40000 ALTER TABLE `cities_light_country` DISABLE KEYS */;
INSERT INTO `cities_light_country` VALUES (1,'country','country',NULL,'country','country','op','eee','OC','ss','545');
/*!40000 ALTER TABLE `cities_light_country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cities_light_region`
--

DROP TABLE IF EXISTS `cities_light_region`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cities_light_region` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_ascii` varchar(200) NOT NULL,
  `slug` varchar(50) NOT NULL,
  `geoname_id` int(11) DEFAULT NULL,
  `alternate_names` longtext,
  `name` varchar(200) NOT NULL,
  `display_name` varchar(200) NOT NULL,
  `geoname_code` varchar(50) DEFAULT NULL,
  `country_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cities_light_region_country_id_6e5b3799_uniq` (`country_id`,`name`),
  UNIQUE KEY `cities_light_region_country_id_3dd02103_uniq` (`country_id`,`slug`),
  UNIQUE KEY `geoname_id` (`geoname_id`),
  KEY `cities_light_region_d7397f31` (`name_ascii`),
  KEY `cities_light_region_2dbcba41` (`slug`),
  KEY `cities_light_region_b068931c` (`name`),
  KEY `cities_light_region_eb02a700` (`geoname_code`),
  CONSTRAINT `cities_light_regi_country_id_b2782d49_fk_cities_light_country_id` FOREIGN KEY (`country_id`) REFERENCES `cities_light_country` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cities_light_region`
--

LOCK TABLES `cities_light_region` WRITE;
/*!40000 ALTER TABLE `cities_light_region` DISABLE KEYS */;
INSERT INTO `cities_light_region` VALUES (1,'dsdsd','dsdsd',NULL,'state','state2','state2, country',NULL,1);
/*!40000 ALTER TABLE `cities_light_region` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` varchar(254) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin__content_type_id_c4bce8eb_fk_django_content_type_id` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_accounts_users_email` (`user_id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_accounts_users_email` FOREIGN KEY (`user_id`) REFERENCES `accounts_users` (`email`),
  CONSTRAINT `django_admin__content_type_id_c4bce8eb_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
INSERT INTO `django_admin_log` VALUES (1,'2017-06-29 05:35:48','1','country',1,'[{\"added\": {}}]',14,'test@gmail.com'),(2,'2017-06-29 05:36:07','1','country',2,'[{\"changed\": {\"fields\": [\"code2\"]}}]',14,'test@gmail.com'),(3,'2017-06-29 05:37:34','1','dsdsd, country',1,'[{\"added\": {}}]',16,'test@gmail.com'),(4,'2017-06-29 05:37:47','1','state2, country',2,'[{\"changed\": {\"fields\": [\"alternate_names\", \"name\"]}}]',16,'test@gmail.com'),(5,'2017-06-29 08:44:03','1','Skills object',1,'[{\"added\": {}}]',10,'test@gmail.com'),(6,'2017-06-29 08:52:33','2','php',1,'[{\"added\": {}}]',10,'test@gmail.com'),(7,'2017-06-29 09:22:00','1','category1',1,'[{\"added\": {}}]',12,'test@gmail.com'),(8,'2017-06-29 09:24:46','1','xxxxx',1,'[{\"added\": {}}]',11,'test@gmail.com'),(9,'2017-06-29 12:49:26','2','shibila.b@spericorn.com',3,'',13,'test@gmail.com'),(10,'2017-06-29 12:49:26','1','shibila.b@spericorn.com',3,'',13,'test@gmail.com'),(11,'2017-06-29 12:54:35','3','shibila.b@spericorn.com',3,'',13,'test@gmail.com'),(12,'2017-06-29 13:02:26','4','shibila.b@spericorn.com',3,'',13,'test@gmail.com'),(13,'2017-06-29 13:03:54','5','shibila.b@spericorn.com',3,'',13,'test@gmail.com'),(14,'2017-06-29 13:05:11','6','shibila.b@spericorn.com',3,'',13,'test@gmail.com'),(15,'2017-06-29 13:06:30','7','shibila.b@spericorn.com',3,'',13,'test@gmail.com'),(16,'2017-06-30 04:59:17','9','shibila.b@spericorn.com',3,'',13,'test@gmail.com'),(17,'2017-06-30 05:00:22','8','shibila.b@spericorn.com',3,'',13,'test@gmail.com'),(18,'2017-06-30 05:17:46','10','shibila.b@spericorn.com',3,'',13,'test@gmail.com'),(19,'2017-06-30 05:21:05','11','shibila.b@spericorn.com',3,'',13,'test@gmail.com'),(20,'2017-06-30 05:34:48','12','shibila.b@spericorn.com',3,'',13,'test@gmail.com'),(21,'2017-06-30 05:52:35','13','shibila.b@spericorn.com',3,'',13,'test@gmail.com'),(22,'2017-06-30 05:54:40','14','shibila.b@spericorn.com',3,'',13,'test@gmail.com'),(23,'2017-06-30 05:55:55','15','shibila.b@spericorn.com',3,'',13,'test@gmail.com'),(24,'2017-06-30 06:29:14','17','shibila.b@spericorn.com',3,'',13,'test@gmail.com'),(25,'2017-06-30 06:29:14','16','shibila.b@spericorn.com',3,'',13,'test@gmail.com'),(26,'2017-06-30 07:03:45','18','shibila.b@spericorn.com',2,'[{\"changed\": {\"fields\": [\"profile\"]}}]',13,'test@gmail.com'),(27,'2017-06-30 08:11:49','18','shibila.b@spericorn.com',3,'',13,'test@gmail.com'),(28,'2017-06-30 08:36:31','19','shibila.b@spericorn.com',3,'',13,'test@gmail.com'),(29,'2017-06-30 09:47:58','20','shibila.b@spericorn.com',3,'',13,'test@gmail.com'),(30,'2017-06-30 13:06:33','1','client02 client02',3,'',18,'test@gmail.com'),(31,'2017-06-30 13:51:48','1','xc, state2, country',1,'[{\"added\": {}}]',15,'test@gmail.com'),(32,'2017-06-30 14:15:26','1','client02 client02',3,'',19,'test@gmail.com'),(33,'2017-06-30 14:17:38','2','shibila.b@spericorn.com',3,'',19,'test@gmail.com'),(34,'2017-07-01 03:48:05','3','shibila.b@spericorn.com',3,'',19,'test@gmail.com'),(35,'2017-07-01 03:48:27','2','shibila.b@spericorn.com',3,'',18,'test@gmail.com'),(36,'2017-07-01 03:52:03','4','shibila.b@spericorn.com',3,'',19,'test@gmail.com'),(37,'2017-07-01 03:52:14','4','shibila.b@spericorn.com',3,'',18,'test@gmail.com'),(38,'2017-07-03 12:38:10','1','client02 client02',3,'',21,'test@gmail.com'),(39,'2017-07-03 12:39:51','2','client02 client02',3,'',21,'test@gmail.com'),(40,'2017-07-04 04:13:25','1','Mid level',3,'',22,'test@gmail.com'),(41,'2017-07-04 04:15:53','3','Mid level',3,'',22,'test@gmail.com'),(42,'2017-07-04 04:32:48','7','Mid level',3,'',22,'test@gmail.com'),(43,'2017-07-04 04:39:28','8','Mid level',3,'',22,'test@gmail.com'),(44,'2017-07-04 05:08:30','126','client02 client02',2,'[{\"changed\": {\"fields\": [\"start_date\", \"end_date\", \"description\"]}}]',20,'test@gmail.com'),(45,'2017-07-04 05:32:54','29','client02 client02',3,'',23,'test@gmail.com'),(46,'2017-07-04 05:32:55','28','client02 client02',3,'',23,'test@gmail.com'),(47,'2017-07-04 05:32:55','27','client02 client02',3,'',23,'test@gmail.com'),(48,'2017-07-04 05:32:55','26','client02 client02',3,'',23,'test@gmail.com'),(49,'2017-07-04 05:32:55','25','client02 client02',3,'',23,'test@gmail.com'),(50,'2017-07-04 05:32:55','24','client02 client02',3,'',23,'test@gmail.com'),(51,'2017-07-04 05:32:55','23','client02 client02',3,'',23,'test@gmail.com'),(52,'2017-07-04 05:32:55','22','client02 client02',3,'',23,'test@gmail.com'),(53,'2017-07-04 05:32:55','21','client02 client02',3,'',23,'test@gmail.com'),(54,'2017-07-04 05:32:55','20','client02 client02',3,'',23,'test@gmail.com'),(55,'2017-07-04 05:32:55','19','client02 client02',3,'',23,'test@gmail.com'),(56,'2017-07-04 05:32:55','18','client02 client02',3,'',23,'test@gmail.com'),(57,'2017-07-04 05:32:55','17','client02 client02',3,'',23,'test@gmail.com'),(58,'2017-07-04 05:32:55','16','client02 client02',3,'',23,'test@gmail.com'),(59,'2017-07-04 05:32:55','15','client02 client02',3,'',23,'test@gmail.com'),(60,'2017-07-04 05:32:55','14','client02 client02',3,'',23,'test@gmail.com'),(61,'2017-07-04 05:32:55','13','client02 client02',3,'',23,'test@gmail.com'),(62,'2017-07-04 05:32:55','12','client02 client02',3,'',23,'test@gmail.com'),(63,'2017-07-04 05:32:55','11','client02 client02',3,'',23,'test@gmail.com'),(64,'2017-07-04 05:32:55','10','client02 client02',3,'',23,'test@gmail.com'),(65,'2017-07-04 05:32:55','9','client02 client02',3,'',23,'test@gmail.com'),(66,'2017-07-04 05:32:55','8','client02 client02',3,'',23,'test@gmail.com'),(67,'2017-07-04 05:32:55','7','client02 client02',3,'',23,'test@gmail.com'),(68,'2017-07-04 05:32:55','6','client02 client02',3,'',23,'test@gmail.com'),(69,'2017-07-04 05:32:55','5','client02 client02',3,'',23,'test@gmail.com'),(70,'2017-07-04 05:32:56','4','client02 client02',3,'',23,'test@gmail.com'),(71,'2017-07-04 05:32:56','3','client02 client02',3,'',23,'test@gmail.com'),(72,'2017-07-04 05:32:56','2','client02 client02',3,'',23,'test@gmail.com'),(73,'2017-07-04 05:32:56','1','client02 client02',3,'',23,'test@gmail.com'),(74,'2017-07-04 05:46:37','2','category2',1,'[{\"added\": {}}]',12,'test@gmail.com'),(75,'2017-07-04 05:59:16','44','client02 client02',3,'',23,'test@gmail.com'),(76,'2017-07-04 05:59:16','43','client02 client02',3,'',23,'test@gmail.com'),(77,'2017-07-04 05:59:16','42','client02 client02',3,'',23,'test@gmail.com'),(78,'2017-07-04 05:59:16','41','client02 client02',3,'',23,'test@gmail.com'),(79,'2017-07-04 05:59:16','40','client02 client02',3,'',23,'test@gmail.com'),(80,'2017-07-04 05:59:16','39','client02 client02',3,'',23,'test@gmail.com'),(81,'2017-07-04 05:59:16','38','client02 client02',3,'',23,'test@gmail.com'),(82,'2017-07-04 05:59:16','37','client02 client02',3,'',23,'test@gmail.com'),(83,'2017-07-04 05:59:16','36','client02 client02',3,'',23,'test@gmail.com'),(84,'2017-07-04 05:59:16','35','client02 client02',3,'',23,'test@gmail.com'),(85,'2017-07-04 05:59:16','34','client02 client02',3,'',23,'test@gmail.com'),(86,'2017-07-04 05:59:16','33','client02 client02',3,'',23,'test@gmail.com'),(87,'2017-07-04 05:59:17','32','client02 client02',3,'',23,'test@gmail.com'),(88,'2017-07-04 05:59:17','31','client02 client02',3,'',23,'test@gmail.com'),(89,'2017-07-04 05:59:17','30','client02 client02',3,'',23,'test@gmail.com');
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (9,'accounts','users'),(8,'accounts','users_status'),(1,'admin','logentry'),(3,'auth','group'),(2,'auth','permission'),(15,'cities_light','city'),(14,'cities_light','country'),(16,'cities_light','region'),(4,'contenttypes','contenttype'),(23,'freelancer','certifications'),(20,'freelancer','employmenthistory'),(22,'freelancer','experiencelevel'),(21,'freelancer','freelancerportfolio'),(13,'freelancer','freelancer_profile'),(18,'freelancer','hourlyrate'),(19,'freelancer','locationdetails'),(10,'freelancer','skills'),(12,'freelancer','workcategories'),(11,'freelancer','worksubcategories'),(17,'registration','registrationprofile'),(5,'sessions','session'),(7,'site_administration','permission'),(6,'site_administration','role_permission');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2017-06-29 04:33:25'),(2,'contenttypes','0002_remove_content_type_name','2017-06-29 04:33:26'),(3,'auth','0001_initial','2017-06-29 04:33:27'),(4,'auth','0002_alter_permission_name_max_length','2017-06-29 04:33:27'),(5,'auth','0003_alter_user_email_max_length','2017-06-29 04:33:27'),(6,'auth','0004_alter_user_username_opts','2017-06-29 04:33:27'),(7,'auth','0005_alter_user_last_login_null','2017-06-29 04:33:27'),(8,'auth','0006_require_contenttypes_0002','2017-06-29 04:33:27'),(9,'auth','0007_alter_validators_add_error_messages','2017-06-29 04:33:27'),(10,'auth','0008_alter_user_username_max_length','2017-06-29 04:33:27'),(11,'accounts','0001_initial','2017-06-29 04:33:29'),(12,'admin','0001_initial','2017-06-29 04:33:29'),(13,'admin','0002_logentry_remove_auto_add','2017-06-29 04:33:29'),(14,'cities_light','0001_initial','2017-06-29 04:33:31'),(15,'cities_light','0002_city','2017-06-29 04:33:33'),(16,'cities_light','0003_auto_20141120_0342','2017-06-29 04:33:33'),(17,'cities_light','0004_autoslug_update','2017-06-29 04:33:33'),(18,'cities_light','0005_blank_phone','2017-06-29 04:33:33'),(19,'cities_light','0006_compensate_for_0003_bytestring_bug','2017-06-29 04:33:33'),(20,'freelancer','0001_initial','2017-06-29 04:33:35'),(21,'registration','0001_initial','2017-06-29 04:33:35'),(22,'sessions','0001_initial','2017-06-29 04:33:35'),(23,'site_administration','0001_initial','2017-06-29 04:33:37'),(24,'freelancer','0002_freelancer_profile_skills','2017-06-29 04:38:12'),(25,'freelancer','0003_certifications_employmenthistory_experiencelevel_freelancerportfolio_hourlyrate_locationdetails','2017-06-29 05:57:51'),(26,'freelancer','0004_auto_20170629_0601','2017-06-29 06:01:04'),(27,'freelancer','0005_auto_20170703_1302','2017-07-03 13:02:12'),(28,'freelancer','0006_skills_subcategory','2017-07-04 06:08:38');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_de54fa62` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('bsx76d872yrry8osy4rbtbihfpjzyb2d','MjliMGE1NmFhMzliMDE5MjUyYjlhM2IxYmQ3MjAzYzhlNzZlNDFjYzp7Il9hdXRoX3VzZXJfaGFzaCI6IjczNGZjNWI0NTY3MzQxNDJkYzY1YzhiYmMzMGUyMGEyMjgyMTllOTMiLCJfYXV0aF91c2VyX2lkIjoic2hpYmlsYS5iQHNwZXJpY29ybi5jb20iLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9','2017-07-18 06:11:50'),('ggp68oxrs0ogtteq593wwt550sccs71q','YjYxYmQ4MTZiZGY2YjJhYmQzMGRhZWU4Mzc1ODc2ZWJmNDE0MmJmYzp7Il9hdXRoX3VzZXJfaGFzaCI6IjNmNzhlMjkzMTUyNTQ3ZmYyMmQ4MGJjY2JhMTY2NDk3ZWZhOWZhYjkiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiJ0ZXN0QGdtYWlsLmNvbSJ9','2017-07-18 05:59:17');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `freelancer_certifications`
--

DROP TABLE IF EXISTS `freelancer_certifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `freelancer_certifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `certificate_name` varchar(255) NOT NULL,
  `issued_by` varchar(255) DEFAULT NULL,
  `valid_from` date DEFAULT NULL,
  `end_to` date DEFAULT NULL,
  `user_id` varchar(254) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `freelancer_certificatio_user_id_f8b5ee4e_fk_accounts_users_email` (`user_id`),
  CONSTRAINT `freelancer_certificatio_user_id_f8b5ee4e_fk_accounts_users_email` FOREIGN KEY (`user_id`) REFERENCES `accounts_users` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `freelancer_certifications`
--

LOCK TABLES `freelancer_certifications` WRITE;
/*!40000 ALTER TABLE `freelancer_certifications` DISABLE KEYS */;
INSERT INTO `freelancer_certifications` VALUES (48,'c','',NULL,NULL,'shibila.b@spericorn.com');
/*!40000 ALTER TABLE `freelancer_certifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `freelancer_employmenthistory`
--

DROP TABLE IF EXISTS `freelancer_employmenthistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `freelancer_employmenthistory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(500) NOT NULL,
  `role` varchar(255) NOT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `description` longtext NOT NULL,
  `user_id` varchar(254) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `freelancer_employmenthi_user_id_222b910e_fk_accounts_users_email` (`user_id`),
  CONSTRAINT `freelancer_employmenthi_user_id_222b910e_fk_accounts_users_email` FOREIGN KEY (`user_id`) REFERENCES `accounts_users` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=137 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `freelancer_employmenthistory`
--

LOCK TABLES `freelancer_employmenthistory` WRITE;
/*!40000 ALTER TABLE `freelancer_employmenthistory` DISABLE KEYS */;
INSERT INTO `freelancer_employmenthistory` VALUES (136,'x','x','2017-07-11','2017-07-21','    klklk','shibila.b@spericorn.com');
/*!40000 ALTER TABLE `freelancer_employmenthistory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `freelancer_experiencelevel`
--

DROP TABLE IF EXISTS `freelancer_experiencelevel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `freelancer_experiencelevel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exp_level_type` varchar(255) NOT NULL,
  `user_id` varchar(254) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`),
  CONSTRAINT `freelancer_experiencele_user_id_8a11496b_fk_accounts_users_email` FOREIGN KEY (`user_id`) REFERENCES `accounts_users` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `freelancer_experiencelevel`
--

LOCK TABLES `freelancer_experiencelevel` WRITE;
/*!40000 ALTER TABLE `freelancer_experiencelevel` DISABLE KEYS */;
INSERT INTO `freelancer_experiencelevel` VALUES (10,'Fresher','shibila.b@spericorn.com');
/*!40000 ALTER TABLE `freelancer_experiencelevel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `freelancer_freelancer_profile`
--

DROP TABLE IF EXISTS `freelancer_freelancer_profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `freelancer_freelancer_profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `profile` varchar(100) NOT NULL,
  `title` varchar(500) NOT NULL,
  `about` longtext,
  `user_id` varchar(254) NOT NULL,
  `workcategory_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `freelancer_freelancer_p_user_id_176e795e_fk_accounts_users_email` (`user_id`),
  KEY `freelan_workcategory_id_d71c8035_fk_freelancer_workcategories_id` (`workcategory_id`),
  CONSTRAINT `freelan_workcategory_id_d71c8035_fk_freelancer_workcategories_id` FOREIGN KEY (`workcategory_id`) REFERENCES `freelancer_workcategories` (`id`),
  CONSTRAINT `freelancer_freelancer_p_user_id_176e795e_fk_accounts_users_email` FOREIGN KEY (`user_id`) REFERENCES `accounts_users` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `freelancer_freelancer_profile`
--

LOCK TABLES `freelancer_freelancer_profile` WRITE;
/*!40000 ALTER TABLE `freelancer_freelancer_profile` DISABLE KEYS */;
INSERT INTO `freelancer_freelancer_profile` VALUES (21,'','h','h','shibila.b@spericorn.com',1);
/*!40000 ALTER TABLE `freelancer_freelancer_profile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `freelancer_freelancer_profile_skills`
--

DROP TABLE IF EXISTS `freelancer_freelancer_profile_skills`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `freelancer_freelancer_profile_skills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `freelancer_profile_id` int(11) NOT NULL,
  `skills_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `freelancer_freelancer_profil_freelancer_profile_id_887aa85d_uniq` (`freelancer_profile_id`,`skills_id`),
  KEY `freelancer_freelancer_skills_id_542f1c16_fk_freelancer_skills_id` (`skills_id`),
  CONSTRAINT `D9ad2e0e1e58ea42a8fdbcde99337acf` FOREIGN KEY (`freelancer_profile_id`) REFERENCES `freelancer_freelancer_profile` (`id`),
  CONSTRAINT `freelancer_freelancer_skills_id_542f1c16_fk_freelancer_skills_id` FOREIGN KEY (`skills_id`) REFERENCES `freelancer_skills` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=193 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `freelancer_freelancer_profile_skills`
--

LOCK TABLES `freelancer_freelancer_profile_skills` WRITE;
/*!40000 ALTER TABLE `freelancer_freelancer_profile_skills` DISABLE KEYS */;
INSERT INTO `freelancer_freelancer_profile_skills` VALUES (192,21,1);
/*!40000 ALTER TABLE `freelancer_freelancer_profile_skills` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `freelancer_freelancer_profile_subcategory`
--

DROP TABLE IF EXISTS `freelancer_freelancer_profile_subcategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `freelancer_freelancer_profile_subcategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `freelancer_profile_id` int(11) NOT NULL,
  `worksubcategories_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `freelancer_freelancer_profil_freelancer_profile_id_ba7484e2_uniq` (`freelancer_profile_id`,`worksubcategories_id`),
  KEY `D07e7f0dd13f4c3afb6fd54082d38cfd` (`worksubcategories_id`),
  CONSTRAINT `D1b050ceda866d8cb695055b4b9b6047` FOREIGN KEY (`freelancer_profile_id`) REFERENCES `freelancer_freelancer_profile` (`id`),
  CONSTRAINT `D07e7f0dd13f4c3afb6fd54082d38cfd` FOREIGN KEY (`worksubcategories_id`) REFERENCES `freelancer_worksubcategories` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=120 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `freelancer_freelancer_profile_subcategory`
--

LOCK TABLES `freelancer_freelancer_profile_subcategory` WRITE;
/*!40000 ALTER TABLE `freelancer_freelancer_profile_subcategory` DISABLE KEYS */;
INSERT INTO `freelancer_freelancer_profile_subcategory` VALUES (119,21,1);
/*!40000 ALTER TABLE `freelancer_freelancer_profile_subcategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `freelancer_freelancerportfolio`
--

DROP TABLE IF EXISTS `freelancer_freelancerportfolio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `freelancer_freelancerportfolio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_name` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `project_url` varchar(200) NOT NULL,
  `user_id` varchar(254) NOT NULL,
  `work_category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `freelancer_freelancerpo_user_id_0f189c5c_fk_accounts_users_email` (`user_id`),
  KEY `freela_work_category_id_a3fbb5bf_fk_freelancer_workcategories_id` (`work_category_id`),
  CONSTRAINT `freela_work_category_id_a3fbb5bf_fk_freelancer_workcategories_id` FOREIGN KEY (`work_category_id`) REFERENCES `freelancer_workcategories` (`id`),
  CONSTRAINT `freelancer_freelancerpo_user_id_0f189c5c_fk_accounts_users_email` FOREIGN KEY (`user_id`) REFERENCES `accounts_users` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `freelancer_freelancerportfolio`
--

LOCK TABLES `freelancer_freelancerportfolio` WRITE;
/*!40000 ALTER TABLE `freelancer_freelancerportfolio` DISABLE KEYS */;
INSERT INTO `freelancer_freelancerportfolio` VALUES (54,'clllvvvvvvvvvvvvvv','clllvvvvvvvvvvvvvv','https://www.google.co.in/search?q=creat+e+ana+array+an+array+containg+multiple+array+using+jquery&oq=creat+e+ana+array+an+array+containg+multiple+array+using+jquery&aqs=chrome..69i57.21295j0j9&sourcei','shibila.b@spericorn.com',1),(55,'clll','clll','https://www.google.co.in/search?q=creat+e+ana+array+an+array+containg+multiple+array+using+jquery&oq=creat+e+ana+array+an+array+containg+multiple+array+using+jquery&aqs=chrome..69i57.21295j0j9&sourcei','shibila.b@spericorn.com',2);
/*!40000 ALTER TABLE `freelancer_freelancerportfolio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `freelancer_freelancerportfolio_skills`
--

DROP TABLE IF EXISTS `freelancer_freelancerportfolio_skills`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `freelancer_freelancerportfolio_skills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `freelancerportfolio_id` int(11) NOT NULL,
  `skills_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `freelancer_freelancerportfo_freelancerportfolio_id_299c177f_uniq` (`freelancerportfolio_id`,`skills_id`),
  KEY `freelancer_freelancer_skills_id_8067cb73_fk_freelancer_skills_id` (`skills_id`),
  CONSTRAINT `freelancer_freelancer_skills_id_8067cb73_fk_freelancer_skills_id` FOREIGN KEY (`skills_id`) REFERENCES `freelancer_skills` (`id`),
  CONSTRAINT `ee0aac5e68ca51764a557a20a4964aa1` FOREIGN KEY (`freelancerportfolio_id`) REFERENCES `freelancer_freelancerportfolio` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `freelancer_freelancerportfolio_skills`
--

LOCK TABLES `freelancer_freelancerportfolio_skills` WRITE;
/*!40000 ALTER TABLE `freelancer_freelancerportfolio_skills` DISABLE KEYS */;
INSERT INTO `freelancer_freelancerportfolio_skills` VALUES (14,54,1);
/*!40000 ALTER TABLE `freelancer_freelancerportfolio_skills` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `freelancer_freelancerportfolio_sub_category`
--

DROP TABLE IF EXISTS `freelancer_freelancerportfolio_sub_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `freelancer_freelancerportfolio_sub_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `freelancerportfolio_id` int(11) NOT NULL,
  `worksubcategories_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `freelancer_freelancerportfo_freelancerportfolio_id_022c327c_uniq` (`freelancerportfolio_id`,`worksubcategories_id`),
  KEY `D0d8fa8d743e1cf4ca77f5522ad91584` (`worksubcategories_id`),
  CONSTRAINT `D0d8fa8d743e1cf4ca77f5522ad91584` FOREIGN KEY (`worksubcategories_id`) REFERENCES `freelancer_worksubcategories` (`id`),
  CONSTRAINT `D6a008dfdf2f730a7a48a1c82e03e273` FOREIGN KEY (`freelancerportfolio_id`) REFERENCES `freelancer_freelancerportfolio` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `freelancer_freelancerportfolio_sub_category`
--

LOCK TABLES `freelancer_freelancerportfolio_sub_category` WRITE;
/*!40000 ALTER TABLE `freelancer_freelancerportfolio_sub_category` DISABLE KEYS */;
INSERT INTO `freelancer_freelancerportfolio_sub_category` VALUES (14,54,1);
/*!40000 ALTER TABLE `freelancer_freelancerportfolio_sub_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `freelancer_hourlyrate`
--

DROP TABLE IF EXISTS `freelancer_hourlyrate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `freelancer_hourlyrate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hourly_rate` decimal(7,2) NOT NULL,
  `actual_rate` decimal(7,2) NOT NULL,
  `user_id` varchar(254) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `freelancer_hourlyrate_user_id_51ad7fbc_fk_accounts_users_email` (`user_id`),
  CONSTRAINT `freelancer_hourlyrate_user_id_51ad7fbc_fk_accounts_users_email` FOREIGN KEY (`user_id`) REFERENCES `accounts_users` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `freelancer_hourlyrate`
--

LOCK TABLES `freelancer_hourlyrate` WRITE;
/*!40000 ALTER TABLE `freelancer_hourlyrate` DISABLE KEYS */;
INSERT INTO `freelancer_hourlyrate` VALUES (5,55.00,55.00,'shibila.b@spericorn.com');
/*!40000 ALTER TABLE `freelancer_hourlyrate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `freelancer_locationdetails`
--

DROP TABLE IF EXISTS `freelancer_locationdetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `freelancer_locationdetails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(100) NOT NULL,
  `zip_code` varchar(5) NOT NULL,
  `city_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  `user_id` varchar(254) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `freelancer_locationdeta_city_id_be6de951_fk_cities_light_city_id` (`city_id`),
  KEY `freelancer_locati_country_id_926daa3f_fk_cities_light_country_id` (`country_id`),
  KEY `freelancer_locationd_state_id_085f5912_fk_cities_light_region_id` (`state_id`),
  KEY `freelancer_locationdeta_user_id_2ed3e127_fk_accounts_users_email` (`user_id`),
  CONSTRAINT `freelancer_locationdeta_user_id_2ed3e127_fk_accounts_users_email` FOREIGN KEY (`user_id`) REFERENCES `accounts_users` (`email`),
  CONSTRAINT `freelancer_locationdeta_city_id_be6de951_fk_cities_light_city_id` FOREIGN KEY (`city_id`) REFERENCES `cities_light_city` (`id`),
  CONSTRAINT `freelancer_locationd_state_id_085f5912_fk_cities_light_region_id` FOREIGN KEY (`state_id`) REFERENCES `cities_light_region` (`id`),
  CONSTRAINT `freelancer_locati_country_id_926daa3f_fk_cities_light_country_id` FOREIGN KEY (`country_id`) REFERENCES `cities_light_country` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `freelancer_locationdetails`
--

LOCK TABLES `freelancer_locationdetails` WRITE;
/*!40000 ALTER TABLE `freelancer_locationdetails` DISABLE KEYS */;
INSERT INTO `freelancer_locationdetails` VALUES (5,'tyty','555',1,1,1,'shibila.b@spericorn.com');
/*!40000 ALTER TABLE `freelancer_locationdetails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `freelancer_skills`
--

DROP TABLE IF EXISTS `freelancer_skills`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `freelancer_skills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `freelancer_skills`
--

LOCK TABLES `freelancer_skills` WRITE;
/*!40000 ALTER TABLE `freelancer_skills` DISABLE KEYS */;
INSERT INTO `freelancer_skills` VALUES (1,'Python'),(2,'php');
/*!40000 ALTER TABLE `freelancer_skills` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `freelancer_skills_subcategory`
--

DROP TABLE IF EXISTS `freelancer_skills_subcategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `freelancer_skills_subcategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `skills_id` int(11) NOT NULL,
  `worksubcategories_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `freelancer_skills_subcategory_skills_id_496aef00_uniq` (`skills_id`,`worksubcategories_id`),
  KEY `D67650b60b5954ba6072d8fc33374728` (`worksubcategories_id`),
  CONSTRAINT `D67650b60b5954ba6072d8fc33374728` FOREIGN KEY (`worksubcategories_id`) REFERENCES `freelancer_worksubcategories` (`id`),
  CONSTRAINT `freelancer_skills_sub_skills_id_372461b7_fk_freelancer_skills_id` FOREIGN KEY (`skills_id`) REFERENCES `freelancer_skills` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `freelancer_skills_subcategory`
--

LOCK TABLES `freelancer_skills_subcategory` WRITE;
/*!40000 ALTER TABLE `freelancer_skills_subcategory` DISABLE KEYS */;
/*!40000 ALTER TABLE `freelancer_skills_subcategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `freelancer_workcategories`
--

DROP TABLE IF EXISTS `freelancer_workcategories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `freelancer_workcategories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `freelancer_workcategories`
--

LOCK TABLES `freelancer_workcategories` WRITE;
/*!40000 ALTER TABLE `freelancer_workcategories` DISABLE KEYS */;
INSERT INTO `freelancer_workcategories` VALUES (1,'category1'),(2,'category2');
/*!40000 ALTER TABLE `freelancer_workcategories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `freelancer_worksubcategories`
--

DROP TABLE IF EXISTS `freelancer_worksubcategories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `freelancer_worksubcategories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sub_category_title` varchar(255) NOT NULL,
  `work_category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `freela_work_category_id_b60ad8d7_fk_freelancer_workcategories_id` (`work_category_id`),
  CONSTRAINT `freela_work_category_id_b60ad8d7_fk_freelancer_workcategories_id` FOREIGN KEY (`work_category_id`) REFERENCES `freelancer_workcategories` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `freelancer_worksubcategories`
--

LOCK TABLES `freelancer_worksubcategories` WRITE;
/*!40000 ALTER TABLE `freelancer_worksubcategories` DISABLE KEYS */;
INSERT INTO `freelancer_worksubcategories` VALUES (1,'xxxxx',1);
/*!40000 ALTER TABLE `freelancer_worksubcategories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `registration_registrationprofile`
--

DROP TABLE IF EXISTS `registration_registrationprofile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `registration_registrationprofile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `activation_key` varchar(40) NOT NULL,
  `user_id` varchar(254) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`),
  CONSTRAINT `registration_registrati_user_id_5fcbf725_fk_accounts_users_email` FOREIGN KEY (`user_id`) REFERENCES `accounts_users` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `registration_registrationprofile`
--

LOCK TABLES `registration_registrationprofile` WRITE;
/*!40000 ALTER TABLE `registration_registrationprofile` DISABLE KEYS */;
/*!40000 ALTER TABLE `registration_registrationprofile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_administration_permission`
--

DROP TABLE IF EXISTS `site_administration_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `site_administration_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `display_name` varchar(200) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `type` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_administration_permission`
--

LOCK TABLES `site_administration_permission` WRITE;
/*!40000 ALTER TABLE `site_administration_permission` DISABLE KEYS */;
INSERT INTO `site_administration_permission` VALUES (1,'View_Dashboard_Client','View_Dashboard_Client',1,'user'),(2,'View_Dashboard_Freelancer','View_Dashboard_Freelancer',1,'user'),(3,'Admin_Role_list','Admin_Role_list',1,'admin'),(4,'Admin_Role_Edit','Admin_Role_Edit',1,'admin'),(5,'Admin_Client_List','Admin_Client_List',1,'admin'),(6,'Admin_Freelancer_List','Admin_Freelancer_List',1,'admin');
/*!40000 ALTER TABLE `site_administration_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_administration_role_permission`
--

DROP TABLE IF EXISTS `site_administration_role_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `site_administration_role_permission` (
  `slug` varchar(255) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(300) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_administration_role_permission`
--

LOCK TABLES `site_administration_role_permission` WRITE;
/*!40000 ALTER TABLE `site_administration_role_permission` DISABLE KEYS */;
/*!40000 ALTER TABLE `site_administration_role_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_administration_role_permission_permission`
--

DROP TABLE IF EXISTS `site_administration_role_permission_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `site_administration_role_permission_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_permission_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `site_administration_role_permis_role_permission_id_058743da_uniq` (`role_permission_id`,`permission_id`),
  KEY `site_permission_id_fcb91e63_fk_site_administration_permission_id` (`permission_id`),
  CONSTRAINT `site_permission_id_fcb91e63_fk_site_administration_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `site_administration_permission` (`id`),
  CONSTRAINT `ca5c844a67e0149635b7d0e4f5f4ecd4` FOREIGN KEY (`role_permission_id`) REFERENCES `site_administration_role_permission` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_administration_role_permission_permission`
--

LOCK TABLES `site_administration_role_permission_permission` WRITE;
/*!40000 ALTER TABLE `site_administration_role_permission_permission` DISABLE KEYS */;
/*!40000 ALTER TABLE `site_administration_role_permission_permission` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-07-04 11:49:12
