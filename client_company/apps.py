from __future__ import unicode_literals

from django.apps import AppConfig


class ClientCompanyConfig(AppConfig):
    name = 'client_company'
