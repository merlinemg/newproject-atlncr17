import re
from datetime import datetime

from django import forms

from accounts.models import Users
from client.models import ClientJobs, JobInvitation, ApplyJob, ApplyJobCancel


class ClientForm(forms.Form):
    first_name = forms.RegexField(regex=r'[a-zA-Z]',
                                  error_message=(
                                      "Only alphabets is allowed."),
                                  widget=forms.TextInput(attrs={'placeholder': 'First Name'}),
                                  error_messages={'required': 'First Name is required'})
    last_name = forms.RegexField(regex=r'[a-zA-Z]',
                                 error_message=(
                                     "Only alphabets is allowed."),
                                 widget=forms.TextInput(attrs={'placeholder': 'Last Name'}),
                                 error_messages={'required': 'Last Name is required'})

    class Meta:
        model = Users
        fields = ['first_name', 'last_name']

    def clean_first_name(self):
        if self.cleaned_data["first_name"].strip() == '':
            raise forms.ValidationError("First name is required.")
        return self.cleaned_data["first_name"]

    def clean_last_name(self):
        if self.cleaned_data["last_name"].strip() == '':
            raise forms.ValidationError("Last name is required.")
        return self.cleaned_data["last_name"]

    def save(self, user, first_name, last_name, commit=True):
        form = super(ClientForm, self).save(commit=False)
        if commit:
            form.user = user
            form.first_name = first_name
            form.last_name = last_name
            form.save()


class PostJobForm(forms.ModelForm):
    """
    Form to display the post job page details.
    """

    # qualifications = forms.RegexField(regex=r'^[a-zA-Z]+([0-9]|[_@. ,/#&+-])*',
    #                                   error_message=(
    #                                       "Only alphanumeric is allowed."))

    job_title = forms.RegexField(regex=r'^[a-zA-Z]+([0-9]|[_@. ,/#&+-])*',
                                 error_message=(
                                     "Only alphanumeric is allowed."))

    # job_description = forms.RegexField(regex=r'^[a-zA-Z]+([0-9]|[_@. ,/#&+-])*',
    #                                    error_message=(
    #                                        "Only alphanumeric is allowed."))

    class Meta:
        model = ClientJobs
        exclude = ('user', 'job_skills', 'job_sub_category',)
        fields = ('user', 'job_title', 'job_main_category', 'job_description', 'experiance_level', 'no_of_hires',
                  'payment_type', 'hourly_max', 'hourly_min', 'duration', 'job_close_date', 'job_status')

    def __init__(self, *args, **kwargs):
        super(PostJobForm, self).__init__(*args, **kwargs)
        self.fields['job_close_date'].widget.attrs.update({'class': 'datepicker'})

        for field in self.fields:
            self.fields['job_title'].error_messages['required'] = 'Enter  Job Title'
            self.fields['job_main_category'].error_messages['required'] = 'Select Job Category'
            self.fields['job_description'].error_messages[
                'required'] = 'This Description is required minimum 300 character'

    def clean_job_description(self):
        job_descriptionc = self.cleaned_data.get("job_description")
        job_descriptionc = job_descriptionc.strip()

        if len(job_descriptionc) < 300:
            raise forms.ValidationError("This Description is required minimum 300 character.")
        return job_descriptionc

    def clean_job_skills(self):
        job_skills = self.cleaned_data.get("job_skills")
        job_skills = job_skills.strip()
        if job_skills == "":
            raise forms.ValidationError("This field is required.")
        return job_skills

    def clean_job_sub_category(self):
        job_sub_category = self.cleaned_data.get("job_sub_category")
        job_sub_category = job_sub_category.strip()
        if job_sub_category == "":
            raise forms.ValidationError("This field is required.")
        return job_sub_category

    def clean_no_of_hires(self):
        no_of_hires = self.cleaned_data.get("no_of_hires")
        if no_of_hires:
            if no_of_hires <= 0 or no_of_hires == 0:
                raise forms.ValidationError("invalid number of hire")
        return no_of_hires

    def clean_hourly_max(self):
        max = self.cleaned_data.get("hourly_max")
        if max:
            if max <= 0 or max == 0:
                raise forms.ValidationError("invalid number of hourly max")

        return max

    def clean_hourly_min(self):
        min = self.cleaned_data.get("hourly_min")
        if min:
            if min <= 0 or min == 0:
                raise forms.ValidationError("invalid number of hourly min")





        return min

    def clean_budget(self):
        budget = self.cleaned_data.get("budget")
        if budget:
            if budget <= 0 or budget == 0:
                raise forms.ValidationError("invalid number of budget")
        return budget

    # def clean_amount(self):
    #     # clean_duration_in_weak
    #
    #     payment = self.cleaned_data.get("amount")
    #
    #     if payment <= 0 or payment == 0:
    #         raise forms.ValidationError("invalid amount")
    #     return payment

    # def clean_job_close_date(self):
    #     # print datetime.today().date()
    #     my_date_time = datetime.today().date()
    #     close_date = self.cleaned_data.get('job_close_date')
    #     print type(close_date)
    #     if my_date_time >= close_date:
    #         raise forms.ValidationError('Enter valid end date')
    #     return close_date



    def clean_job_main_category(self):
        job_main_category = self.cleaned_data.get("job_main_category")
        if not job_main_category:
            raise forms.ValidationError("This field is required.")
        return job_main_category

    # def clean(self):
    #     duration_in_hour = self.cleaned_data.get("duration_in_hour")
    #     duration_in_weak = self.cleaned_data.get("duration_in_weak")
    #     duration_in_month = self.cleaned_data.get("duration_in_month")
    #     if duration_in_hour is None and duration_in_weak is None and duration_in_month is None:
    #         raise forms.ValidationError("Any field is required in duration.")

    def clean_job_title(self):
        job_title = self.cleaned_data.get("job_title")
        job_title = job_title.strip()
        if job_title == "":
            raise forms.ValidationError("This field is required.")
        return job_title

    def save(self, user, duration, payment_type, budget, hourly_max, hourly_min, job_status, created_by,
             job_main_category, job_sub_category, skills,date,status,commit=True):
        form = super(PostJobForm, self).save(commit=False)

        if commit:
            form.user = user
            form.job_close_date = date
            form.verification_status = status
            form.repost_status = 0
            form.created_user = created_by
            if duration:
                form.duration = duration
            if payment_type:
                form.payment_type = payment_type

            if budget:
                form.budget = budget


            if hourly_max:

                form.hourly_max = hourly_max
            if hourly_min:

                form.hourly_min = hourly_min


            if payment_type:
                form.payment_type = payment_type
            form.job_status = job_status
            form.job_main_category = job_main_category
            form.save()
            try:
                if skills:
                  form.job_skills.add(skills)
                if job_sub_category:
                  form.job_sub_category.add(job_sub_category)

            except:
                if job_sub_category != []:
                    for sub_category in job_sub_category:
                        form.job_sub_category.add(sub_category)
                if skills:
                    for skill in skills:
                        print skill, "ddddddkkkddddd"
                        form.job_skills.add(skill)

        return form


class InviteForm(forms.ModelForm):
    class Meta:
        model = JobInvitation
        fields = ('job', 'messege', 'reject_messege')

    def __init__(self, *args, **kwargs):
        super(InviteForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields['job'].error_messages['required'] = 'Please Select a Job'

    def save(self, user, job, message, commit=True):
        form = super(InviteForm, self).save(commit=False)

        if commit:
            form.user = user
            form.job = job
            form.message = message
            form.invite = True
            form.save()
        return form


class ApplyJobForm(forms.ModelForm):
    """
    Form to apply jobs

    """

    class Meta:
        model = ApplyJob
        exclude = ['user', 'job']
        fields = ['description', 'document', 'estimated_cost']

    def __init__(self, *args, **kwargs):
        super(ApplyJobForm, self).__init__(*args, **kwargs)
        # for field in self.fields:
        #     self.fields['estimated_cost'].widget.attrs['class'] = 'estimated_cost'
        #     self.fields['estimated_cost'].widget.attrs['min'] = '0'

    # def clean_estimated_cost(self):
    #     estimated_cost = self.cleaned_data.get("estimated_cost")
    #     #estimated_cost = estimated_cost.strip()
    #     if estimated_cost:
    #         reg = re.compile('^[0-9]{4,7}(?![0-9])')
    #
    #         if not reg.match(estimated_cost):
    #             raise forms.ValidationError("Only digits.")
    #     return estimated_cost
    def clean_document(self):
        document = self.cleaned_data['document']
        # document = document.strip()

        # if document:
        #     reg = re.compile('^.*txt|xlsx$')
        #     if not reg.match(document):
        #         raise forms.ValidationError("Select valid format.")
        # return document
        #print document,"hhhhhhhhhhhhhhhhhhhhhh"
        if document:
            file_type = document.content_type.split('/')[0]
            #print file_type, "hhhhhhhhhhhhhhhhhhhhhh"

            if file_type not in ['pdf','application/pdf','application/docx','application/doc']:
                raise forms.ValidationError('File type is not supported')
        return document

    def save(self, user, job, is_resume=None, commit=True, ):
        job_form = super(ApplyJobForm, self).save(commit=False)
        if commit:
            job_form.user = user
            if is_resume == 'False':
                job_form.document = None
            job_form.job = job
            job_form.save()
        return job_form


class ApplyJobCancelForm(forms.ModelForm):
    """
    Form to apply jobs
    """

    class Meta:
        model = ApplyJobCancel
        exclude = ['user', 'job']
        fields = ['cancel_message']

    def clean_cancel_message(self):
        cancel_message = self.cleaned_data.get("cancel_message")
        cancel_message = cancel_message.strip()
        if cancel_message == "":
            raise forms.ValidationError("Cancel Message is required.")
        return cancel_message

    def save(self, user, job, commit=True):
        job_cancel_form = super(ApplyJobCancelForm, self).save(commit=False)
        if commit:
            job_cancel_form.user = user
            job_cancel_form.job = job
            job_cancel_form.save()
        return job_cancel_form
