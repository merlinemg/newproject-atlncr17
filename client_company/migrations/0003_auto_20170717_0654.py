# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-07-17 06:54
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('client_company', '0002_auto_20170711_1052'),
    ]

    operations = [
        migrations.AlterField(
            model_name='client_company_profile',
            name='user',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='client_company_user', to=settings.AUTH_USER_MODEL),
        ),
    ]
