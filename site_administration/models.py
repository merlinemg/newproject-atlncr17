from __future__ import unicode_literals

from django.db import migrations
from django.db import models

# Create your models here.
from django.db.models.signals import post_migrate
from django.utils.crypto import get_random_string
from django.utils.text import slugify


class VerificationJobPost(models.Model):
    TYPES = (
        ('verification_off', '1'),
        ('verification_on', '0')
    )
    verification_status = models.CharField(max_length=20,
                                           choices=TYPES,
                                           default='verification_off', verbose_name="Verifiction Status")
    def __unicode__(self):
        return self.verification_status



class Permission(models.Model):
    PERMISSION_TYPE = (
        ('admin', 'admin'),
        ('user', 'user')
    )
    name = models.CharField(max_length=200)
    display_name = models.CharField(max_length=200)
    status = models.BooleanField(default=False)
    type = models.CharField(max_length=10,
                            choices=PERMISSION_TYPE, verbose_name="Permission")

    def __unicode__(self):
        return self.name


# class Role(models.Model):
#     roles_name = models.CharField(max_length=300)
#
#
#
#     def __unicode__(self):
#         #post_migrate.connect(define_company_groups, sender=self)
#
#         return self.roles_name



class Role_Permission(models.Model):
    role = models.CharField(max_length=300)
    # permission = models.ForeignKey(Permission, verbose_name='Permissions', related_name='permissions', null=True,
    # blank=True)
    permission = models.ManyToManyField(Permission)
    slug = models.SlugField(max_length=255, unique=True, auto_created=True, editable=False)

    def __unicode__(self):
        return self.role

    def save(self, *args, **kwargs):
        super(Role_Permission, self).save(*args, **kwargs)
        if not self.slug:
            unique_id = get_random_string(length=15)
            self.slug = slugify(self.role) + "-" + unique_id
            self.save()
