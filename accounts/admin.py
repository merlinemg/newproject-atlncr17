from django.contrib import admin

# Register your models here.
from accounts.models import Users, Users_Status, User_Type

admin.site.register(Users)
admin.site.register(Users_Status)
admin.site.register(User_Type)