from django.conf.urls import url
from django.contrib.auth.decorators import login_required

from client.views import ClientProfile, ClientJobPost, SerachFreelancer, PaginationView, FreelancerPaginationView, \
    MyJobs, JoblistPaginationView, JobDetails, JobInvitedPaginationView, FindJobs, EditJob

urlpatterns = [
    url(r'client-profile/$', login_required(ClientProfile.as_view()), name='clientprofile'),
    url(r'client-job-posting/$', login_required(ClientJobPost.as_view()), name='clientjobpost'),
    url(r'client-job-posting/(?P<slug>[-\w]+)/$', login_required(ClientJobPost.as_view()), name='clientjobpost'),
    url(r'edit-job/(?P<slug>[-\w]+)/$', login_required(EditJob.as_view()), name='editjobpost'),
    url(r'search-freelancer/$', login_required(SerachFreelancer.as_view()), name='searchfreelancer'),
    url(r'paginations-search/$', PaginationView.as_view(), name='paginationsearch'),
    url(r'jobs/$', MyJobs.as_view(), name='clientjobs'),

    url(r'find_jobs/$', FindJobs.as_view(), name='findjobs'),
    url(r'paginations-freelsearch/$', login_required(FreelancerPaginationView.as_view()), name='freelancerpagination'),
    url(r'paginations-jobss/$', JoblistPaginationView.as_view(), name='jobspagination'),
    url(r'paginations-jobsdetails/$', login_required(JobInvitedPaginationView.as_view()), name='jobdetails-pagination'),
    #url(r'freelancer-profile-view/$', login_required(FreelancerProfileView.as_view()), name='freelancerprofileview'),
    url(r'^client_job_details/(?P<slug>[-\w]+)/$', login_required(JobDetails.as_view()), name='jobdetails'),
]