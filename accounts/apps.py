from __future__ import unicode_literals

from django.apps import AppConfig
from django.db.models.signals import post_migrate
from django.utils.crypto import get_random_string


def define_user(sender, **kwargs):

    type = sender.get_model("User_Type")
    types, typess = type.objects.get_or_create(type='CLIENT')
    types, typess = type.objects.get_or_create(type='FREELANCER')
    Site_admin, typess = type.objects.get_or_create(type='Site_admin')
    BOTH, typess = type.objects.get_or_create(type='BOTH')
    OWNER, typess = type.objects.get_or_create(type='OWNER')
    Store = sender.get_model("Users")

    store_corporate,create = Store.objects.get_or_create(first_name='admin', last_name='admin',token = get_random_string(length=32),is_superuser='True',password='admin',email='admin@gmail.com')

    store_corporates, creates = Store.objects.get_or_create(first_name='superadmin', last_name='superadmin',
                                                          password='admin',
                                                          email='superadmin@gmail.com', token = get_random_string(length=32))

    store_corporate.set_password('admin')
    store_corporate.save()
    store_corporates.set_password('admin')
    store_corporates.save()


class AccountsConfig(AppConfig):
    name = 'accounts'
    def ready(self):

        post_migrate.connect(define_user, sender=self)
