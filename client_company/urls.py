from django.conf.urls import url
from django.contrib.auth.decorators import login_required

from client_company.views import ClientCompany, ClientCompanyView, InvitedUserList

urlpatterns = [
    url(r'client-company/$', login_required(ClientCompany.as_view()), name='clientcompany'),
    url(r'client-company-view/$', login_required(ClientCompanyView.as_view()), name='clientcompanyview'),
    url(r'client-pagination/$', login_required(InvitedUserList.as_view()), name='invitedlist'),
]