from rolepermissions.roles import AbstractUserRole


class test(AbstractUserRole):
	available_permission = {
		'View_Dashboard_Freelancer': True,
		'Admin_Role_list': True,

	}

class test3(AbstractUserRole):
	available_permission = {
		'View_Dashboard_Client': True,
		'View_Dashboard_Freelancer': True,
		'Admin_Role_list': True,
		'Admin_Role_Edit': True,
		'Admin_Client_List': True,
		'Admin_Freelancer_List': True,

	}

