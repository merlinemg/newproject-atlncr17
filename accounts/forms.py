import re

from django import forms
from django.contrib.auth import get_user_model, authenticate
from django.contrib.auth.forms import AuthenticationForm, UsernameField
from django.utils.crypto import get_random_string
from django.utils.text import capfirst

from accounts.models import Users, User_Type
from registration.forms import RegistrationForm


class LoginForm(forms.Form):
    """
    Base class for authenticating users. Extend this to get a form that accepts
    username/password logins.
    """
    username = forms.CharField(label="username", max_length=30,
                               widget=forms.TextInput(
                                   attrs={'placeholder': 'Email', 'class': 'form-control', 'name': 'username'}),
                               error_messages={'required': 'Email is required'})
    password = forms.CharField(label="Password", max_length=30,
                               widget=forms.PasswordInput(
                                   attrs={'placeholder': 'Password', 'class': 'form-control', 'name': 'password'}),
                               error_messages={'required': 'Password is required'})

    error_messages = {
        'invalid_login': (
            "Oops! The login credentials you entered were incorrect."

        ),
        'inactive': ("This account is inactive."),
    }



    def __init__(self, request=None, *args, **kwargs):
        """
        The 'request' parameter is set for custom auth use by subclasses.
        The form data comes in via the standard 'data' kwarg.
        """
        self.request = request
        self.user_cache = None
        super(LoginForm, self).__init__(*args, **kwargs)

        # Set the label for the "username" field.
        UserModel = get_user_model()
        self.username_field = UserModel._meta.get_field(UserModel.USERNAME_FIELD)
        if self.fields['username'].label is None:
            self.fields['username'].label = capfirst(self.username_field.verbose_name)

    def clean(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')

        if username and password:
            self.user_cache = authenticate(username=username, password=password)
            if self.user_cache is None:
                raise forms.ValidationError(
                    self.error_messages['invalid_login'],
                    code='invalid_login',
                    params={'username': self.username_field.verbose_name},
                )
            else:
                self.confirm_login_allowed(self.user_cache)

        return self.cleaned_data

    def confirm_login_allowed(self, user):
        """
        Controls whether the given User may log in. This is a policy setting,
        independent of end-user authentication. This default behavior is to
        allow login by active users, and reject login by inactive users.

        If the given user cannot log in, this method should raise a
        ``forms.ValidationError``.

        If the given user may log in, this method should return None.
        """
        if not user.is_active:
            raise forms.ValidationError(
                self.error_messages['inactive'],
                code='inactive',
            )

    def get_user_id(self):
        if self.user_cache:
            return self.user_cache.id
        return None

    def get_user(self):
        return self.user_cache


class LoginForms(AuthenticationForm):
    username = forms.CharField(label="username", max_length=30,
                               widget=forms.TextInput(
                                   attrs={'placeholder': 'Email', 'class': 'form-control', 'name': 'username'}),
                               error_messages={'required': 'Email is required'})
    password = forms.CharField(label="Password", max_length=30,
                               widget=forms.PasswordInput(
                                   attrs={'placeholder': 'Password', 'class': 'form-control', 'name': 'password'}),
                               error_messages={'required': 'Password is required'})


class RegisterForm(RegistrationForm):
    password1 = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': 'Password',}),
                                error_messages={'required': 'Password is required'})
    password2 = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': 'Confirm Password'}),
                                error_messages={'required': 'Confirm Password is required'})
    email = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Your Email Id'}),
                            error_messages={'required': 'Email Id is required'})
    first_name = forms.RegexField(regex=r'[a-zA-Z]',
                                  error_message=(
                                      "Only alphabets is allowed."),
                                  widget=forms.TextInput(attrs={'placeholder': 'First Name'}),
                                  error_messages={'required': 'First Name is required'})
    last_name = forms.RegexField(regex=r'[a-zA-Z]',
                                 error_message=(
                                     "Only alphabets is allowed."),
                                 widget=forms.TextInput(attrs={'placeholder': 'Last Name'}),
                                 error_messages={'required': 'Last Name is required'})

    INVITE_TYPES = (
        ('CLIENT', 'client'),
        ('FREELANCER', 'freelancer'),
        ('BOTH', 'both'),
        ('OWNER', 'owner'),
    )
    user_type=forms.ChoiceField(INVITE_TYPES, widget=forms.RadioSelect())


    class Meta:
        model = Users
        fields = ['user_type','password1', 'password2', 'first_name', 'last_name', 'email']

    def __init__(self, *args, **kwargs):
        super(RegisterForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields['user_type'].error_messages['required'] = 'Please Make a Selection'

    def clean_first_name(self):
        if self.cleaned_data["first_name"].strip() == '':
            raise forms.ValidationError("First name is required.")
        return self.cleaned_data["first_name"]

    def clean_last_name(self):
        if self.cleaned_data["last_name"].strip() == '':
            raise forms.ValidationError("Last name is required.")
        return self.cleaned_data["last_name"]

    def clean_email(self):
        email = self.cleaned_data.get('email')

        if email and Users.objects.filter(email=email).count():
            raise forms.ValidationError('Email address already Exists.')

        if self.cleaned_data.get('email').strip() == '':
            raise forms.ValidationError("Email is required")
        return email

    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')

        if password1 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")

        return password1
    def clean_password1(self):
        password1 = self.cleaned_data.get('password1')
        #if not re.match(r'[A-Za-z0-9@#$%^&+=]{8,}', password1):
        if not re.match(r'((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$*&!%]).{6,20})', password1):
            raise forms.ValidationError("Password should be a lowercase characters and one uppercase characters and Numbers and special characters and atleast 6...")
        return password1
    def clean_user_type(self):
        user_type = self.cleaned_data.get('user_type')
        if not user_type:
            raise forms.ValidationError("Please Make a Selection")
        else:
            user_type = User_Type.objects.get(type=user_type)
        return user_type

    def save(self, commit=True):
        form = super(RegisterForm, self).save(commit=False)

        if commit:
            form.set_password(self.cleaned_data["password1"])
            form.set_password(self.cleaned_data["password1"])
            form.is_active = False
            #form.user_type = self.cleaned_data["user_type"]
            #form.username = self.cleaned_data["email"]
            form.save()

        return form
class ActivationForm(forms.ModelForm):
    password1 = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': 'Password', }),
                                error_messages={'required': 'Password is required'})
    password2 = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': 'Confirm Password'}),
                                error_messages={'required': 'Confirm Password is required'})

    first_name = forms.RegexField(regex=r'[a-zA-Z]',
                                  error_message=(
                                      "Only alphabets is allowed."),
                                  widget=forms.TextInput(attrs={'placeholder': 'First Name'}),
                                  error_messages={'required': 'First Name is required'})
    last_name = forms.RegexField(regex=r'[a-zA-Z]',
                                 error_message=(
                                     "Only alphabets is allowed."),
                                 widget=forms.TextInput(attrs={'placeholder': 'Last Name'}),
                                 error_messages={'required': 'Last Name is required'})
    class Meta:
        model = Users
        fields = ['password1', 'password2', 'first_name', 'last_name']
    def clean_first_name(self):
        if self.cleaned_data["first_name"].strip() == '':
            raise forms.ValidationError("First name is required.")
        return self.cleaned_data["first_name"]

    def clean_last_name(self):
        if self.cleaned_data["last_name"].strip() == '':
            raise forms.ValidationError("Last name is required.")
        return self.cleaned_data["last_name"]


    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')

        if password1 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")

        return password1

    def clean_password1(self):
        password1 = self.cleaned_data.get('password1')
        # if not re.match(r'[A-Za-z0-9@#$%^&+=]{8,}', password1):
        if not re.match(r'((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20})', password1):
            raise forms.ValidationError(
                "Password should be a lowercase characters and one uppercase characters and Numbers and special characters and atleast 6...")
        return password1
    def save(self, commit=True):
        form = super(ActivationForm, self).save(commit=False)

        if commit:
            form.set_password(self.cleaned_data["password1"])
            form.set_password(self.cleaned_data["password1"])
            form.is_active = True
            form.token = get_random_string(length=32)
            #form.user_type = self.cleaned_data["user_type"]
            #form.username = self.cleaned_data["email"]
            form.save()
class PasswordResetRequestForm(forms.Form):
    email_or_username = forms.CharField(label="Email", max_length=254,widget=forms.TextInput(attrs={'placeholder': 'Email'}))

    def __init__(self, *args, **kwargs):
        super(PasswordResetRequestForm, self).__init__(*args, **kwargs)
        self.fields['email_or_username'].required = True

class SetPasswordForm(forms.Form):
    """
    A form that lets a user change set their password without entering the old
    password
    """
    error_messages = {
        'password_mismatch': ("The two password fields didn't match."),
        }
    new_password1 = forms.CharField(label=("New password"),
                                    widget=forms.PasswordInput(attrs={'placeholder': 'New Password'}))
    new_password2 = forms.CharField(label=("New password confirmation"),
                                    widget=forms.PasswordInput(attrs={'placeholder': 'Confirm Password'}))

    def clean_new_password2(self):
        password1 = self.cleaned_data.get('new_password1')
        password2 = self.cleaned_data.get('new_password2')
        if password1 and password2:
            if password1 != password2:
                raise forms.ValidationError(
                    self.error_messages['password_mismatch'],
                    code='password_mismatch',
                    )
        return password2
