# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-06-29 04:38
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('freelancer', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='freelancer_profile',
            name='skills',
            field=models.ManyToManyField(to='freelancer.Skills'),
        ),
    ]
