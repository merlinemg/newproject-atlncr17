from django.conf.urls import url
from django.contrib.auth.decorators import login_required
# from django.contrib.auth import views as auth_views

from accounts import views
from accounts.views import Home, Registers, Dashboard, logout, CompanyActivate, Activated, SuccessMessage, \
    ResetPasswordRequestView, PasswordResetConfirmView

urlpatterns = [
    url(r'^test/$', views.index, name='home'),
    url(r'^$', Home.as_view(), name='home'),
    url(r'register/$', Registers.as_view(), name='register'),
    url(r'activate/$', Activated.as_view(), name='activateaccount'),
    url(r'companyactivate/(?P<token>[\w-]+)$', CompanyActivate.as_view(), name='companyactivate'),
    url(r'clientdashboard/$', login_required(Dashboard.as_view()), name='dashboard'),
    url(r'registration-success/$', SuccessMessage.as_view(), name='registersuccess'),
    url(r'signin/$', views.login, name='login'),
    url(r'^logout/$', logout, {"next_page": '/'}, name="logout"),
    url(r'^reset_password', ResetPasswordRequestView.as_view(), name="reset_password"),
    url(r'^account/reset_password_confirm/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$', PasswordResetConfirmView.as_view(),
        name='confirmpaass'),

    # url(r'^password_reset/$', auth_views.password_reset, name='password_reset'),
    # url(r'^password_reset/done/$', auth_views.password_reset_done, name='password_reset_done'),
    # url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
    #     auth_views.password_reset_confirm, name='password_reset_confirm'),
    # url(r'^reset/done/$', auth_views.password_reset_complete, name='password_reset_complete'),
    # url(r'^accounts/activate/(?P<activation_key>\w+)/$',
    #     Home.as_view(),
    #     {'backend': 'registration.backends.default.DefaultBackend'},
    #     name='registration_activate'),
]