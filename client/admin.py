from django.contrib import admin

# Register your models here.
from client.models import Client_Profile, ClientJobs, PaymentType, JobInvitation, ApplyJob, JobStatus, Duration, \
    ApplyJobCancel, InvitationStatus

admin.site.register(Client_Profile)
admin.site.register(ClientJobs)
admin.site.register(PaymentType)
admin.site.register(JobInvitation)
admin.site.register(InvitationStatus)
admin.site.register(ApplyJob)
admin.site.register(JobStatus)
admin.site.register(Duration)
admin.site.register(ApplyJobCancel)

