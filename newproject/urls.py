"""newproject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.conf.urls.i18n import i18n_patterns
from django.conf.urls.static import static
from django.contrib import admin



import accounts
from newproject import settings

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^polls/', include('accounts.urls')),
    url(r'^freelancer/', include('freelancer.urls')),
    url(r'^client/', include('client.urls')),
    url(r'^freelancer-company/', include('freelancer_company.urls')),
    url(r'^client-company/', include('client_company.urls')),
    url(r'^site_administration/', include('site_administration.urls',namespace='siteadmin')),

    url(r'^i18n/', include('django.conf.urls.i18n')),
    url(r'^accounts/', include('registration.backends.hmac.urls')),
    url(r'^hire/', include('hire.urls')),
    url(r'^notifications/', include('notifications.urls')),
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += i18n_patterns(
    url(r'^', include('accounts.urls', namespace='index')),
    url(r'^polls/', include('accounts.urls', namespace='login')),
    url(r'^', include('freelancer.urls', namespace='freelancer')),
    url(r'^', include('client.urls', namespace='client')),
    url(r'^', include('hire.urls', namespace='hire')),
    url(r'^', include('freelancer_company.urls', namespace='freelancer-company')),
    url(r'^', include('client_company.urls', namespace='client-company')),
    url(r'^', include('notifications.urls', namespace='notifications')),
    url(r'^accounts/', include('registration.backends.hmac.urls'))
)

handler404 = accounts.views.error_404
handler500 = accounts.views.error_500