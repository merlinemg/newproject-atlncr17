from django import forms

from client_company.models import Client_Company_Profile, Invited_Client_Company_User


class Client_Company_ProfileForm(forms.ModelForm):
    class Meta:
        model = Client_Company_Profile
        fields = ['company_name', 'description', 'address', 'phone']

    def __init__(self, *args, **kwargs):
        super(Client_Company_ProfileForm, self).__init__(*args, **kwargs)

        for field in self.fields:
            self.fields['company_name'].error_messages['required'] = 'Company Name is required'

    def clean_company_name(self):
        if self.cleaned_data["company_name"].strip() == '':
            raise forms.ValidationError("Company Name is required.")
        return self.cleaned_data["company_name"]

    def save(self, user, commit=True):
        form = super(Client_Company_ProfileForm, self).save(commit=False)

        # form.workcategory = values['workcategory']
        # form.subcategory = values['subcategory']


        if commit:
            form.user = user
            form.save()
        return form


