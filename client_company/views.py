import base64
import datetime
import json
import threading

from django.core.files.base import ContentFile
from django.core.mail import send_mail
from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.
from django.template.loader import render_to_string
from django.utils.crypto import get_random_string
from django.views.generic import ListView
from django.views.generic import TemplateView
from rolepermissions.mixins import HasPermissionsMixin

from accounts.models import Users
from client_company.forms import Client_Company_ProfileForm
from client_company.mixins import ClientCompanyMixin
from client_company.models import Client_Company_Profile, Invited_Client_Company_User
from freelancer_company.models import Invited_Freelancer_Company_User


class ClientCompany(HasPermissionsMixin, ClientCompanyMixin, TemplateView):
    required_permission = 'add_client_company'
    template_name = 'client/client_company.html'

    def post(self, request, *args, **kwargs):
        method = request.POST['method']
        if method == 'client_company_image_save':
            if request.user.invite_type == 'OWNER':
                self.check_exist_client_company(request.user)
            # print request.FILES['freelancer_image'],"cccccccccccccccccccccccccccccch"
            client_object, clientr_objects = Client_Company_Profile.objects.get_or_create(
                user=self.Get_user_client())
            if client_object:
                format, imgstr = request.POST['freelancer_image'].split(';base64,')
                ext = format.split('/')[-1]
                date_str = datetime.datetime.utcnow()
                data = ContentFile(base64.b64decode(imgstr), name=str(request.user.first_name) + date_str.strftime(
                    "%Y_%m_%d_%H_%M_%S_%f") + '.' + ext)
                client_object.logo = data
                client_object.save()
                option_html = render_to_string('client/ajax/client_company_logo.html',
                                               {'object': client_object.logo.url,
                                                'csrf_token_value': request.COOKIES['csrftoken'], request: request}
                                               )

            payload = {'status': 'sucess', 'message': "success", 'data': client_object.logo.url, 'html': option_html}
            return HttpResponse(json.dumps(payload),
                                content_type='application/json',
                                )
        if method == 'Save_client_company_profile':
            if request.user.invite_type == 'OWNER':
                self.check_exist_client_company(request.user)
            try:
               user = self.check_client_company_profile(request.user.get_client_company_intance().user)
            except:
                user = self.check_client_company_profile(request.user)

            form = Client_Company_ProfileForm(request.POST, instance=user)
            payload = ''
            if form.is_valid():
                form.save(self.request.user)

                payload = {'status': 'success',
                           'message': 'Successfully Updated',
                           'data': 'g'}
                return HttpResponse(json.dumps(payload), content_type='application/json', )
            else:

                self.message = "Validation failed."
                self.data = form.errors
                self.success = False
                payload = {'status': 'failed', 'message': self.message, 'data': self.data}
                return HttpResponse(json.dumps(payload), content_type='application/json', )


class ClientCompanyView(HasPermissionsMixin, ClientCompanyMixin, TemplateView):
    template_name = 'client/client_company_view.html'
    required_permission = 'View_Client_Company'

    def post(self, request, *args, **kwargs):
        method = request.POST['method']

        if method == 'invite_user':

            user = self.check_user_exist(request.POST['email'])
            print user, "useruser"
            if user is None:
                print user, "userusernew"
                client_object, clientr_objects = Users.objects.get_or_create(
                                                                             email=request.POST['email'],
                                                                             is_active=False,
                                                                             token=get_random_string(length=32),
                                                                             user_type=self.get_user_type('CLIENT'), invite_type=self.get_user_type('CLIENT'))
                if client_object:
                    self.activate_users_accounts(client_object, request.META['HTTP_HOST'], 'new')
                    print client_object.pk, "jjj"
                    invite_object, invite_objects = Invited_Client_Company_User.objects.get_or_create(
                        user=client_object,
                        role_type=request.POST['role_type'],
                        client_company=request.user.get_client_company_intance())
                payload = {'status': 'success', 'message': "success", 'data': "jkjk"}
                return HttpResponse(json.dumps(payload), content_type='application/json', )
            else:
                users = self.check_user_exist_invited_company(request.POST['email'])
                freelancer_user = self.freelancer_user(request.POST['email'])

                client_object, clientr_objects = Users.objects.get_or_create(email=request.POST['email'])
                # invite_objects = Invited_Freelancer_Company_User.objects.get(
                #     user=client_object
                #     )

                if users is None and not user.is_superuser and user.invite_type.type != 'OWNER':
                    if freelancer_user.freelancer_company.user.email == request.user.get_client_company_intance().user.email:
                        invite_object, invite_objects = Invited_Client_Company_User.objects.get_or_create(
                            user=client_object,
                            role_type=request.POST['role_type'],
                            client_company=request.user.get_client_company_intance())

                        if client_object.invite_type == self.get_user_type('FREELANCER'):
                            client_object.invite_type = self.get_user_type('BOTH')
                            client_object.save()
                        else:
                            client_object.invite_type = self.get_user_type('CLIENT')
                            client_object.save()

                            self.activate_users_accounts(client_object, request.META['HTTP_HOST'], 'existing')
                            # print client_object.pk, "jjj"
                            # invite_object, invite_objects = Invited_Client_Company_User.objects.get_or_create(
                            #     user=client_object,
                            #     role_type=request.POST['role_type'],
                            #     client_company=request.user.get_client_company_intance())
                        payload = {'status': 'success', 'message': "success", 'data': "jkjk"}
                        return HttpResponse(json.dumps(payload), content_type='application/json', )
                    else:
                        payload = {'status': 'failed', 'message': "This user can't be added as a team member",
                                   'data': "jkjk"}
                        return HttpResponse(json.dumps(payload), content_type='application/json', )
                else:
                    payload = {'status': 'failed', 'message': "This user can't be added as a team member",
                               'data': "jkjk"}
                    return HttpResponse(json.dumps(payload), content_type='application/json', )
        if method == 'ChangeRole':
            object =Invited_Client_Company_User.objects.get(id=request.POST['roleid'])

            msg_html = render_to_string('freelancer/ajax/changeRole.html',
                                        {'csrf_token_value': request.COOKIES['csrftoken'],'object':object})
            payload = {'status': 'success', 'message': "success", 'html': msg_html}
            return HttpResponse(json.dumps(payload), content_type='application/json', )
        if method == 'ChangeDectivation':
            object, objects  =Users.objects.get_or_create(email=request.POST['roleid'])
            print object
            if request.POST['rolestatus'] == 'True' or request.POST['rolestatus'] == 'true':
                status = False
                type = "Activate"
            else:
                status = True
                type = "Deactivate"
            print status
            object.is_active = status
            object.save()



            payload = {'status': 'success', 'message': "success",'data':status,'type':type}
            return HttpResponse(json.dumps(payload), content_type='application/json', )
        if method == 'update_invite_user':
            object =Invited_Client_Company_User.objects.get(id=request.POST['roleid'])
            object.role_type =request.POST['role_type']
            object.save()


            payload = {'status': 'success', 'message': "success", }
            return HttpResponse(json.dumps(payload), content_type='application/json', )

    def activate_users_accounts(self, client_object, site, type):
        token = client_object.token
        msg_plain = "Atlancer Client Company Account Activation"
        print type, "invite@mailinator.cominvite@mailinator.cominvite@mailinator.com"

        if type == 'existing':
            print type, "typetypetypetypetypetypetypetypetype"
            msg_html = render_to_string('registration/activate_company_existing_user.html',
                                        {'site': site, 'token': token, 'firstname': client_object.email})
        else:
            msg_html = render_to_string('registration/activate_company_user.html',
                                        {'site': site, 'token': token, 'firstname': client_object.email,
                                         'account': 'Client'})

        def Email():
            send_mail(
                msg_plain,
                msg_plain,
                'atlancer@sender.com',
                [client_object.email],
                html_message=msg_html,
            )

        send_user_mail = threading.Thread(name='Email', target=Email)
        send_user_mail.start()


class InvitedUserList(ListView):
    paginate_by = 6

    def post(self, request, *args, **kwargs):
        response_data = {}
        # option_html = render_to_string('freelancer/ajax/listing_invited_user.html')

        page_by = request.POST.get('page_by')
        search = request.POST.get('search')

        paginator_response = self.get_paginated_queryset(request, page_by, search)
        response_data['paginator_html'] = paginator_response['paginator_html']
        # response_data['count'] = self.request.user.get_client_company_intance().invited_client_company_id.filter(
        #     user__is_active=True).count()
        response_data['count'] = self.request.user.get_client_company_intance().invited_client_company_id.count()
        return HttpResponse(json.dumps(response_data), content_type="application/json")

    def get_paginated_queryset(self, request, page_by, search):
        if page_by:
            page_by = int(page_by)
        else:
            page_by = 1
        #print search, "hsearchsearchsearch"
        if search == '':
            page_size = self.get_paginate_by(
                request.user.get_client_company_intance().invited_client_company_id.all())
            size = self.paginate_queryset(
                request.user.get_client_company_intance().invited_client_company_id.all(),
                page_size)
        else:
            page_size = self.get_paginate_by(
                request.user.get_client_company_intance().invited_client_company_id.filter(
                    Q(user__first_name__contains=search) | Q(user__last_name__contains=search) | Q(
                        user__email__contains=search)))
            size = self.paginate_queryset(
                request.user.get_client_company_intance().invited_client_company_id.filter(
                    Q(user__first_name__contains=search) | Q(user__last_name__contains=search) | Q(
                        user__email__contains=search)), page_size)

        paginator, page, queryset, is_paginated = size
        try:
            queryset = paginator.page(page_by).object_list
        except:
            queryset = queryset

        user_table = render_to_string('freelancer/ajax/listing_invited_user.html',
                                      {'all_invited': queryset, 'paginator': paginator, 'page': page,
                                       'is_paginated': is_paginated, 'type': 'Client', 'request': request,
                                       'page_obj': paginator.page(page_by)})

        # paginator_html = render_to_string('ajax/paginator_html.html',
        #                                   {'page_obj': paginator.page(page_by)})

        context = {'user_table': user_table, 'paginator_html': user_table}
        return context
