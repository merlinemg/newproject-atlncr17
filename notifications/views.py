import json

from django.http import HttpResponse

# Create your views here.
from django.template.loader import render_to_string
from django.template.response import TemplateResponse
from django.views.generic import ListView
from django.views.generic import TemplateView

from accounts.models import Users, User_Type
from client.mixins import ClientMixin
from notifications.models import Notifications


class NotificationCount(ClientMixin,TemplateView):
    template_name = 'freelancer/freelancer_hire_view.html'

    def post(self, request, *args, **kwargs):
        if request.POST.get('method') == 'Check_count':
            notification =Notifications.objects.filter(user=request.user,status=0)
            #notification.status = 1
            notification.update(status=1)
            payload = {'status': 'success',
                       'type': '0', 'message': "Successfully", 'data': "k",
                     }
            return HttpResponse(json.dumps(payload), content_type='application/json')
        elif request.POST.get('method') == 'Save_notification':
            print request.POST.get('title')

            try:
                user = Users.objects.get(id=request.POST.get('user_id'))

            except:
                user = Users.objects.get(email=request.POST.get('user_id'))

            try:

                created_by = Users.objects.get(id=request.POST.get('created_by'))
            except:

                created_by = Users.objects.get(email=request.POST.get('created_by'))

            Notifications.objects.get_or_create(user=user, title=request.POST.get('title'), created_by=created_by, status=0,
                                                description=request.POST.get('description'),url=request.POST.get('url'),
                                                type=User_Type.objects.get(type=request.POST.get('type'),))

            payload = {'status': 'success',
                       'type': '0', 'message': "Successfully", 'data': "k",'classs':'contents_count'+request.POST.get('type')+request.POST.get('user_id')
                     }
            return HttpResponse(json.dumps(payload), content_type='application/json')
class NotificationView(ClientMixin,TemplateView):
    template_name = 'common/notification.html'
    def get(self, request,**kwargs):
        list =self.get_unread_notification(request.user.email,request.user.active_account).update(status=1)


        template_name = 'common/notification.html'
        notification = self.get_all_notification(request.user.email,request.user.active_account)



        context = {'notification': notification}
        return TemplateResponse(request, template_name, context)
class NotificationsListView(ClientMixin,ListView):
    paginate_by = 5

    def post(self, request, *args, **kwargs):
        response_data = {}
        if request.POST.get('method') == 'List_notifications':
            page_by = request.POST.get('page_by')
            list = self.get_all_notification(request.user.email,request.user.active_account).order_by('-id')
            paginator_response = self.get_paginated_notification_list(request, page_by, list)

            response_data['paginator_html'] = paginator_response['paginator_html']
            response_data['count'] = list.count()


            return HttpResponse(json.dumps(response_data), content_type="application/json")

    def get_paginated_notification_list(self,request, page_by, list):
        if page_by:
            page_by = int(page_by)
        else:
            page_by = 1


        page_size = self.get_paginate_by(list)
        size = self.paginate_queryset(list, page_size)
        paginator, page, queryset, is_paginated = size
        try:
            queryset = paginator.page(page_by).object_list
        except:
            queryset = queryset

        user_table = render_to_string('common/ajax/notification_list.html',
                                      {'queryset': queryset, 'paginator': paginator, 'page': page,
                                       'is_paginated': is_paginated, 'type': request.user.active_account,

                                       'page_obj': paginator.page(page_by)})

        # paginator_html = render_to_string('ajax/paginator_html.html',
        #                                   {'page_obj': paginator.page(page_by)})

        context = {'count': list.count(), 'paginator_html': user_table}
        return context