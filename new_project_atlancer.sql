-- MySQL dump 10.13  Distrib 5.5.54, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: new_project_atlancer
-- ------------------------------------------------------
-- Server version	5.5.54-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accounts_users`
--

DROP TABLE IF EXISTS `accounts_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts_users` (
  `password` varchar(128) NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime NOT NULL,
  `email` varchar(254) NOT NULL,
  `user_type` varchar(10) NOT NULL,
  PRIMARY KEY (`email`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts_users`
--

LOCK TABLES `accounts_users` WRITE;
/*!40000 ALTER TABLE `accounts_users` DISABLE KEYS */;
INSERT INTO `accounts_users` VALUES ('pbkdf2_sha256$30000$rBeN0eUVMUiK$VRak4KlBZq0vdAESxAetovLqeEFoK1MbYdXOP8Rc9AM=','2017-06-21 06:33:30',1,'admin@gmail.com','admin','admin',0,1,'2017-06-16 12:10:32','admin@gmail.com','Si'),('pbkdf2_sha256$30000$CXkL2DXQy9ss$ZGq2PIWCXxWk9iVIet+s6NO3YEb9z9XNnQDZ+RCzqsY=','2017-06-28 10:25:01',0,'shibila.b@spericorn.com','fgfgfg','test',0,1,'2017-06-23 09:48:36','shibila.b@spericorn.com','FREELANCER'),('pbkdf2_sha256$30000$nxeDFyX8nMEe$VAU8lZnkjcR3PvKb1yGQmMkuQQW4f0/T7vElQXcp2J0=','2017-06-28 11:51:06',1,'shibila@gmail.com','','',1,1,'2017-06-16 12:08:23','shibila@gmail.com','ad'),('pbkdf2_sha256$30000$SPiIc9nH9n8J$cFHzGSmQ4neOv4irdWPaMkYzlSG2FFrZKf+Twk3HAcU=',NULL,0,'superadmin@gmail.com','superadmin','superadmin',0,1,'2017-06-16 12:10:32','superadmin@gmail.com','Si'),('pbkdf2_sha256$30000$LY58xMXb7491$yp722Llnk7R2P0mjs5qWNdj4GPbeZ4sVVKXUg+yElOY=',NULL,0,'test@gmail.coms','client02','client02',0,0,'2017-06-23 07:29:27','test@gmail.coms','FREELANCER');
/*!40000 ALTER TABLE `accounts_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accounts_users_groups`
--

DROP TABLE IF EXISTS `accounts_users_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts_users_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `users_id` varchar(254) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `accounts_users_groups_users_id_8dfb39d5_uniq` (`users_id`,`group_id`),
  KEY `accounts_users_groups_group_id_371be490_fk_auth_group_id` (`group_id`),
  CONSTRAINT `accounts_users_groups_group_id_371be490_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `accounts_users_groups_users_id_c8303f87_fk_accounts_users_email` FOREIGN KEY (`users_id`) REFERENCES `accounts_users` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts_users_groups`
--

LOCK TABLES `accounts_users_groups` WRITE;
/*!40000 ALTER TABLE `accounts_users_groups` DISABLE KEYS */;
INSERT INTO `accounts_users_groups` VALUES (2,'admin@gmail.com',1),(13,'admin@gmail.com',2),(35,'shibila.b@spericorn.com',4),(26,'test@gmail.coms',4);
/*!40000 ALTER TABLE `accounts_users_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accounts_users_status`
--

DROP TABLE IF EXISTS `accounts_users_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts_users_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts_users_status`
--

LOCK TABLES `accounts_users_status` WRITE;
/*!40000 ALTER TABLE `accounts_users_status` DISABLE KEYS */;
/*!40000 ALTER TABLE `accounts_users_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accounts_users_user_permissions`
--

DROP TABLE IF EXISTS `accounts_users_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts_users_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `users_id` varchar(254) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `accounts_users_user_permissions_users_id_866b235f_uniq` (`users_id`,`permission_id`),
  KEY `accounts_users_user_permission_id_5f3d0fff_fk_auth_permission_id` (`permission_id`),
  CONSTRAINT `accounts_users_user_permission_id_5f3d0fff_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `accounts_users_user_pe_users_id_62a47b5b_fk_accounts_users_email` FOREIGN KEY (`users_id`) REFERENCES `accounts_users` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts_users_user_permissions`
--

LOCK TABLES `accounts_users_user_permissions` WRITE;
/*!40000 ALTER TABLE `accounts_users_user_permissions` DISABLE KEYS */;
INSERT INTO `accounts_users_user_permissions` VALUES (5,'admin@gmail.com',40),(6,'admin@gmail.com',41),(7,'admin@gmail.com',42),(8,'admin@gmail.com',43),(33,'shibila.b@spericorn.com',45),(24,'test@gmail.coms',45);
/*!40000 ALTER TABLE `accounts_users_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissi_permission_id_84c5c92e_fk_auth_permission_id` (`permission_id`),
  CONSTRAINT `auth_group_permissi_permission_id_84c5c92e_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permissi_content_type_id_2f476e4b_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cities_light_city`
--

DROP TABLE IF EXISTS `cities_light_city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cities_light_city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_ascii` varchar(200) NOT NULL,
  `slug` varchar(50) NOT NULL,
  `geoname_id` int(11) DEFAULT NULL,
  `alternate_names` longtext,
  `name` varchar(200) NOT NULL,
  `display_name` varchar(200) NOT NULL,
  `search_names` longtext NOT NULL,
  `latitude` decimal(8,5) DEFAULT NULL,
  `longitude` decimal(8,5) DEFAULT NULL,
  `region_id` int(11) DEFAULT NULL,
  `country_id` int(11) NOT NULL,
  `population` bigint(20) DEFAULT NULL,
  `feature_code` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `geoname_id` (`geoname_id`),
  UNIQUE KEY `cities_light_city_region_id_29b81cd4_uniq` (`region_id`,`name`),
  UNIQUE KEY `cities_light_city_region_id_dc18c213_uniq` (`region_id`,`slug`),
  KEY `cities_light_city_country_id_cf310fd2_fk_cities_light_country_id` (`country_id`),
  KEY `cities_light_city_d7397f31` (`name_ascii`),
  KEY `cities_light_city_2dbcba41` (`slug`),
  KEY `cities_light_city_b068931c` (`name`),
  KEY `cities_light_city_248081ec` (`population`),
  KEY `cities_light_city_3f98884f` (`feature_code`),
  CONSTRAINT `cities_light_city_country_id_cf310fd2_fk_cities_light_country_id` FOREIGN KEY (`country_id`) REFERENCES `cities_light_country` (`id`),
  CONSTRAINT `cities_light_city_region_id_f7ab977b_fk_cities_light_region_id` FOREIGN KEY (`region_id`) REFERENCES `cities_light_region` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cities_light_city`
--

LOCK TABLES `cities_light_city` WRITE;
/*!40000 ALTER TABLE `cities_light_city` DISABLE KEYS */;
/*!40000 ALTER TABLE `cities_light_city` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cities_light_country`
--

DROP TABLE IF EXISTS `cities_light_country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cities_light_country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_ascii` varchar(200) NOT NULL,
  `slug` varchar(50) NOT NULL,
  `geoname_id` int(11) DEFAULT NULL,
  `alternate_names` longtext,
  `name` varchar(200) NOT NULL,
  `code2` varchar(2) DEFAULT NULL,
  `code3` varchar(3) DEFAULT NULL,
  `continent` varchar(2) NOT NULL,
  `tld` varchar(5) NOT NULL,
  `phone` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `geoname_id` (`geoname_id`),
  UNIQUE KEY `code2` (`code2`),
  UNIQUE KEY `code3` (`code3`),
  KEY `cities_light_country_d7397f31` (`name_ascii`),
  KEY `cities_light_country_2dbcba41` (`slug`),
  KEY `cities_light_country_0e20e84e` (`continent`),
  KEY `cities_light_country_a311df50` (`tld`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cities_light_country`
--

LOCK TABLES `cities_light_country` WRITE;
/*!40000 ALTER TABLE `cities_light_country` DISABLE KEYS */;
/*!40000 ALTER TABLE `cities_light_country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cities_light_region`
--

DROP TABLE IF EXISTS `cities_light_region`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cities_light_region` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_ascii` varchar(200) NOT NULL,
  `slug` varchar(50) NOT NULL,
  `geoname_id` int(11) DEFAULT NULL,
  `alternate_names` longtext,
  `name` varchar(200) NOT NULL,
  `display_name` varchar(200) NOT NULL,
  `geoname_code` varchar(50) DEFAULT NULL,
  `country_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cities_light_region_country_id_6e5b3799_uniq` (`country_id`,`name`),
  UNIQUE KEY `cities_light_region_country_id_3dd02103_uniq` (`country_id`,`slug`),
  UNIQUE KEY `geoname_id` (`geoname_id`),
  KEY `cities_light_region_d7397f31` (`name_ascii`),
  KEY `cities_light_region_2dbcba41` (`slug`),
  KEY `cities_light_region_b068931c` (`name`),
  KEY `cities_light_region_eb02a700` (`geoname_code`),
  CONSTRAINT `cities_light_regi_country_id_b2782d49_fk_cities_light_country_id` FOREIGN KEY (`country_id`) REFERENCES `cities_light_country` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cities_light_region`
--

LOCK TABLES `cities_light_region` WRITE;
/*!40000 ALTER TABLE `cities_light_region` DISABLE KEYS */;
/*!40000 ALTER TABLE `cities_light_region` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` varchar(254) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin__content_type_id_c4bce8eb_fk_django_content_type_id` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_accounts_users_email` (`user_id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_accounts_users_email` FOREIGN KEY (`user_id`) REFERENCES `accounts_users` (`email`),
  CONSTRAINT `django_admin__content_type_id_c4bce8eb_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
INSERT INTO `django_admin_log` VALUES (1,'2017-06-16 12:15:52','1','Client',1,'[{\"added\": {}}]',6,'shibila@gmail.com'),(2,'2017-06-16 12:16:02','2','Freelancer',1,'[{\"added\": {}}]',6,'shibila@gmail.com'),(3,'2017-06-16 12:16:14','3','admin',1,'[{\"added\": {}}]',6,'shibila@gmail.com'),(4,'2017-06-16 12:16:27','4','Superadmin',1,'[{\"added\": {}}]',6,'shibila@gmail.com'),(5,'2017-06-16 12:46:11','4','Superadmin',3,'',6,'shibila@gmail.com'),(6,'2017-06-16 12:46:11','3','admin',3,'',6,'shibila@gmail.com'),(7,'2017-06-16 12:46:11','2','Freelancer',3,'',6,'shibila@gmail.com'),(8,'2017-06-16 12:46:11','1','Client',3,'',6,'shibila@gmail.com'),(9,'2017-06-19 06:14:12','12','Client',3,'',6,'shibila@gmail.com'),(10,'2017-06-19 06:25:18','15','Client',3,'',6,'shibila@gmail.com'),(11,'2017-06-19 06:25:18','13','Client',3,'',6,'shibila@gmail.com'),(12,'2017-06-19 06:25:24','11','xcxcxcxc',3,'',6,'shibila@gmail.com'),(13,'2017-06-19 06:25:24','9','xcxcxcxc',3,'',6,'shibila@gmail.com'),(14,'2017-06-19 06:27:21','16','Client',3,'',6,'shibila@gmail.com'),(15,'2017-06-19 07:14:02','18','xxxxxxxxxxxxxxxxx',3,'',6,'shibila@gmail.com'),(16,'2017-06-19 07:14:02','17','jhkjk',3,'',6,'shibila@gmail.com'),(17,'2017-06-19 07:17:12','21','xcxcxcxc',3,'',6,'shibila@gmail.com'),(18,'2017-06-19 07:17:12','20','xcxcxcxc',3,'',6,'shibila@gmail.com'),(19,'2017-06-19 07:17:17','19','xxxxxxxxxxx',3,'',6,'shibila@gmail.com'),(20,'2017-06-19 07:23:16','22','xcxcxcxc',3,'',6,'shibila@gmail.com'),(21,'2017-06-21 10:40:44','shibila.b@spericorn.com','shibila.b@spericorn.com',3,'',9,'shibila@gmail.com'),(22,'2017-06-21 10:42:24','shibila.b@spericorn.com','shibila.b@spericorn.com',3,'',9,'shibila@gmail.com'),(23,'2017-06-21 10:46:26','shibila.b@spericorn.com','shibila.b@spericorn.com',3,'',9,'shibila@gmail.com'),(24,'2017-06-21 12:02:46','shibila.b@spericorn.com','shibila.b@spericorn.com',3,'',9,'shibila@gmail.com'),(25,'2017-06-21 12:04:07','shibila.b@spericorn.com','shibila.b@spericorn.com',3,'',9,'shibila@gmail.com'),(26,'2017-06-21 12:07:20','shibila.b@spericorn.com','shibila.b@spericorn.com',3,'',9,'shibila@gmail.com'),(27,'2017-06-21 12:10:03','shibila.b@spericorn.com','shibila.b@spericorn.com',3,'',9,'shibila@gmail.com'),(28,'2017-06-21 12:11:41','shibila.b@spericorn.com','shibila.b@spericorn.com',3,'',9,'shibila@gmail.com'),(29,'2017-06-21 12:17:31','shibila.b@spericorn.com','shibila.b@spericorn.com',3,'',9,'shibila@gmail.com'),(30,'2017-06-21 12:24:16','shibila.b@spericorn.com','shibila.b@spericorn.com',3,'',9,'shibila@gmail.com'),(31,'2017-06-21 12:25:13','shibila.b@spericorn.com','shibila.b@spericorn.com',3,'',9,'shibila@gmail.com'),(32,'2017-06-23 09:48:18','shibila.b@spericorn.com','shibila.b@spericorn.com',3,'',9,'shibila@gmail.com');
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2017-06-28 12:28:52'),(2,'contenttypes','0002_remove_content_type_name','2017-06-28 12:28:53'),(3,'auth','0001_initial','2017-06-28 12:32:49'),(4,'auth','0002_alter_permission_name_max_length','2017-06-28 12:32:49'),(5,'auth','0003_alter_user_email_max_length','2017-06-28 12:32:50'),(6,'auth','0004_alter_user_username_opts','2017-06-28 12:32:50'),(7,'auth','0005_alter_user_last_login_null','2017-06-28 12:32:50'),(8,'auth','0006_require_contenttypes_0002','2017-06-28 12:32:50'),(9,'auth','0007_alter_validators_add_error_messages','2017-06-28 12:32:50'),(10,'auth','0008_alter_user_username_max_length','2017-06-28 12:32:50');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_de54fa62` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('5772swcpjsmfhwo9p1tyigx8b960yzw3','YjlhMjljOWZhOTllZmI0MDdkMGU5NDY3ZDQxNzhiYzNhNzI3Mzc5Nzp7Il9hdXRoX3VzZXJfaGFzaCI6IjYwZjY5OTg1MWEzZTUwOGJiNzBlOTJlYWZiY2EzZmVmOGY4OGNhNWMiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiJzaGliaWxhQGdtYWlsLmNvbSJ9','2017-07-12 11:40:13'),('6jaukdvrd5dmfildw3wo0ytai046fecp','NDE5NzM5Y2YxNWZhYjQzYTk0MDM4OTMxNGY0ZjgxOTQ1MTI1MGYzNTp7Il9hdXRoX3VzZXJfaGFzaCI6IjlmNDBlOTBlZjcyNTFiOWU4Njk4OTUzNzM5NmFlZWUzNGUyODMzMzkiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiJzaGliaWxhLmJAc3Blcmljb3JuLmNvbSJ9','2017-07-12 11:41:35'),('begk64xalxwwswlemiao7ll425n8apz7','YjlhMjljOWZhOTllZmI0MDdkMGU5NDY3ZDQxNzhiYzNhNzI3Mzc5Nzp7Il9hdXRoX3VzZXJfaGFzaCI6IjYwZjY5OTg1MWEzZTUwOGJiNzBlOTJlYWZiY2EzZmVmOGY4OGNhNWMiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiJzaGliaWxhQGdtYWlsLmNvbSJ9','2017-06-30 12:15:24'),('ddtjet2a2wjew2vwywiw4vlisgdivqc5','NmM5MjRiZGY4ODFmZjQzNTIzMjY3NzE0YjUwM2NlYTU5MzYwODJjMjp7fQ==','2017-07-07 10:12:46'),('i7kl48if9ljjnmffhvafil6r8t6jcy7v','NTI5YmQ1YzM0ZjM5MGY4MWZjNzFhYWRmNjZkOTVjY2IwNThjZTE5ZDp7Il9hdXRoX3VzZXJfaGFzaCI6IjYwZjY5OTg1MWEzZTUwOGJiNzBlOTJlYWZiY2EzZmVmOGY4OGNhNWMiLCJfYXV0aF91c2VyX2lkIjoic2hpYmlsYUBnbWFpbC5jb20iLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9','2017-07-07 04:19:41'),('jf46lb53bv4n6qn417culq1sdkg7bmtt','YjlhMjljOWZhOTllZmI0MDdkMGU5NDY3ZDQxNzhiYzNhNzI3Mzc5Nzp7Il9hdXRoX3VzZXJfaGFzaCI6IjYwZjY5OTg1MWEzZTUwOGJiNzBlOTJlYWZiY2EzZmVmOGY4OGNhNWMiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiJzaGliaWxhQGdtYWlsLmNvbSJ9','2017-07-07 09:16:15'),('mafpzio6ydg5ck90yp5hzteioj9ha0n7','ZmEzYmI4NDc5ZTEzYjQ0ZDc1ZjRkZmM0YzNkYzdjYTY3ZjI2ODQ1ODp7Il9hdXRoX3VzZXJfaGFzaCI6ImMxMDcwN2I2MTA0OTEzOGZkZDJkODJkZWRiMWQzODE1Yzg5NjAyZDIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiJhZG1pbkBnbWFpbC5jb20ifQ==','2017-07-03 09:10:36'),('psv8tk2esaj3uvrpnb9vgq0yh50cgthi','NTI5YmQ1YzM0ZjM5MGY4MWZjNzFhYWRmNjZkOTVjY2IwNThjZTE5ZDp7Il9hdXRoX3VzZXJfaGFzaCI6IjYwZjY5OTg1MWEzZTUwOGJiNzBlOTJlYWZiY2EzZmVmOGY4OGNhNWMiLCJfYXV0aF91c2VyX2lkIjoic2hpYmlsYUBnbWFpbC5jb20iLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9','2017-07-12 11:02:30'),('xd7tyiv09tnsyl825i2ygv88wl4m7wqo','NTI5YmQ1YzM0ZjM5MGY4MWZjNzFhYWRmNjZkOTVjY2IwNThjZTE5ZDp7Il9hdXRoX3VzZXJfaGFzaCI6IjYwZjY5OTg1MWEzZTUwOGJiNzBlOTJlYWZiY2EzZmVmOGY4OGNhNWMiLCJfYXV0aF91c2VyX2lkIjoic2hpYmlsYUBnbWFpbC5jb20iLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9','2017-07-12 12:33:48'),('y9jw2sznkzogga1otzaq3o68jc6b79ke','NmM5MjRiZGY4ODFmZjQzNTIzMjY3NzE0YjUwM2NlYTU5MzYwODJjMjp7fQ==','2017-07-03 09:02:49');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `registration_registrationprofile`
--

DROP TABLE IF EXISTS `registration_registrationprofile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `registration_registrationprofile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `activation_key` varchar(40) NOT NULL,
  `user_id` varchar(254) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`),
  CONSTRAINT `registration_registrati_user_id_5fcbf725_fk_accounts_users_email` FOREIGN KEY (`user_id`) REFERENCES `accounts_users` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `registration_registrationprofile`
--

LOCK TABLES `registration_registrationprofile` WRITE;
/*!40000 ALTER TABLE `registration_registrationprofile` DISABLE KEYS */;
/*!40000 ALTER TABLE `registration_registrationprofile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_administration_permission`
--

DROP TABLE IF EXISTS `site_administration_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `site_administration_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `display_name` varchar(200) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `type` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_administration_permission`
--

LOCK TABLES `site_administration_permission` WRITE;
/*!40000 ALTER TABLE `site_administration_permission` DISABLE KEYS */;
INSERT INTO `site_administration_permission` VALUES (1,'View_Dashboard_Client','View_Dashboard_Client',1,'user'),(2,'View_Dashboard_Freelancer','View_Dashboard_Freelancer',1,'user'),(3,'Admin_Role_list','Admin_Role_list',1,'admin'),(4,'Admin_Role_Edit','Admin_Role_Edit',1,'admin'),(5,'Admin_Client_List','Admin_Client_List',1,'admin'),(6,'Admin_Freelancer_List','Admin_Freelancer_List',1,'admin');
/*!40000 ALTER TABLE `site_administration_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_administration_role_permission`
--

DROP TABLE IF EXISTS `site_administration_role_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `site_administration_role_permission` (
  `slug` varchar(255) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(300) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_administration_role_permission`
--

LOCK TABLES `site_administration_role_permission` WRITE;
/*!40000 ALTER TABLE `site_administration_role_permission` DISABLE KEYS */;
INSERT INTO `site_administration_role_permission` VALUES ('client-5gimoVu1dAhFCeQ',5,'Client'),('freelancer-e2RgBTurAxXCXCD',6,'Freelancer'),('superadmin-Sa9zghH7QOlZMHj',7,'Superadmin'),('admin-ZCkHYupiQ9q9s5P',8,'Admin'),('xcxcxcxc-03UKBy0FLbUrnNW',23,'xcxcxcxc');
/*!40000 ALTER TABLE `site_administration_role_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `site_administration_role_permission_permission`
--

DROP TABLE IF EXISTS `site_administration_role_permission_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `site_administration_role_permission_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_permission_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `site_administration_role_permis_role_permission_id_058743da_uniq` (`role_permission_id`,`permission_id`),
  KEY `site_permission_id_fcb91e63_fk_site_administration_permission_id` (`permission_id`),
  CONSTRAINT `ca5c844a67e0149635b7d0e4f5f4ecd4` FOREIGN KEY (`role_permission_id`) REFERENCES `site_administration_role_permission` (`id`),
  CONSTRAINT `site_permission_id_fcb91e63_fk_site_administration_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `site_administration_permission` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `site_administration_role_permission_permission`
--

LOCK TABLES `site_administration_role_permission_permission` WRITE;
/*!40000 ALTER TABLE `site_administration_role_permission_permission` DISABLE KEYS */;
INSERT INTO `site_administration_role_permission_permission` VALUES (9,5,1),(49,5,2),(51,5,3),(10,6,2),(11,7,5),(12,7,6),(13,8,3),(14,8,4),(15,8,5),(16,8,6),(46,23,1),(47,23,2),(45,23,3),(48,23,4),(52,23,5);
/*!40000 ALTER TABLE `site_administration_role_permission_permission` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-06-28 18:16:12
