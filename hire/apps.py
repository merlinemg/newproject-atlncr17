from __future__ import unicode_literals

from django.apps import AppConfig
from django.db.models.signals import post_migrate


def define_status_contract(sender, **kwargs):
    Status = sender.get_model("ContractStatus")
    type, types = Status.objects.get_or_create(status='Draft')
    type, types = Status.objects.get_or_create(status='Pending')
    type, types = Status.objects.get_or_create(status='Approved')
def define_status_milestone(sender, **kwargs):
    milestonestatus = sender.get_model("MileStoneStatus")
    type, types = milestonestatus.objects.get_or_create(status='Draft')
    type, types = milestonestatus.objects.get_or_create(status='Pending')
    type, types = milestonestatus.objects.get_or_create(status='Approved')
    type, types = milestonestatus.objects.get_or_create(status='Completed')
    type, types = milestonestatus.objects.get_or_create(status='Changed')
    type, types = milestonestatus.objects.get_or_create(status='Archive')
    type, types = milestonestatus.objects.get_or_create(status='Payout Requested')

def define_status_timesheetstatus(sender, **kwargs):
    timesheetstatus = sender.get_model("TimesheetStatus")
    type, types = timesheetstatus.objects.get_or_create(status='Draft')
    type, types = timesheetstatus.objects.get_or_create(status='Pending')
    type, types = timesheetstatus.objects.get_or_create(status='Approved')
    type, types = timesheetstatus.objects.get_or_create(status='Adjust')
    type, types = timesheetstatus.objects.get_or_create(status='Payout Requested')

class HireConfig(AppConfig):
    name = 'hire'
    def ready(self):
        from client import signals
        post_migrate.connect(define_status_contract, sender=self)
        post_migrate.connect(define_status_milestone, sender=self)
        post_migrate.connect(define_status_timesheetstatus, sender=self)
