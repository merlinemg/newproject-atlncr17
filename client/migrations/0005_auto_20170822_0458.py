# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-08-22 04:58
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0004_merge_20170821_1219'),
    ]

    operations = [
        migrations.AddField(
            model_name='clientjobs',
            name='slug',
            field=models.SlugField(auto_created=True, default=1, editable=False, max_length=255, unique=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='clientjobs',
            name='job_status',
            field=models.CharField(choices=[('OPEN', 'Open'), ('CLOSE', 'Close'), ('DRAFT', 'Draft'), ('PENDING', 'Pending')], default='OPEN', max_length=6, verbose_name='Job status'),
        ),
    ]
