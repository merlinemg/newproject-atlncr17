from accounts.models import Users, User_Type
from client_company.models import Invited_Client_Company_User
from freelancer.models import Skills, WorkSubCategories, WorkCategories
from freelancer_company.models import Freelancer_Company_Profile, Invited_Freelancer_Company_User


class CompanyMixin(object):
    def check_freelancer_company_profile(self):
        user = self.Get_user()

        try:

            data = Freelancer_Company_Profile.objects.get(user=user)

        except:
            data = None
        return data

    def check_exist_freelancer_company(self, user):
        try:

            data = Freelancer_Company_Profile.objects.get_or_create(user=user)

        except:
            data = None
        return data

    def Get_user(self):
        try:

            user = self.request.user.get_freelancer_company_intance().user

        except:
            user = self.request.user
        return user
    def get_user_type(self,type):
        """
        get selected skills.
        :return:
        """
        types =User_Type.objects.get(type=type)
        return types

    def delete_existing_skill(self):
        """
        Get related skills.
        :return: skills object

        """
        user = self.Get_user()

        user = Freelancer_Company_Profile.objects.get(user=user)

        for skill_obj in Skills.objects.all():
            user.skills.remove(skill_obj)

    def delete_existing_subcategory(self):
        """
        Get related skills.
        :return: skills object

        """
        user = self.Get_user()
        users = Freelancer_Company_Profile.objects.get(user=user)
        for skill_obj in WorkSubCategories.objects.all():
            users.subcategory.remove(skill_obj)

    def delete_existing_workcategory(self ):
        """
        Get related skills.
        :return: skills object

        """
        user = self.Get_user()
        users = Freelancer_Company_Profile.objects.get(user=user)
        for skill_obj in WorkCategories.objects.all():
            users.workcategory.remove(skill_obj)

    def get_selected_skill(self, skills):
        """
        Get related skills.
        :return: skills object
        """
        skills_objs = Skills.objects.filter(pk__in=skills)

        return skills_objs

    def get_selected_subcategory(self, sub):
        """
        Get related skills.
        :return: skills object
        """
        sub_objs = WorkSubCategories.objects.filter(pk__in=sub)

        return sub_objs

    def get_selected_workcategory(self, sub):
        """
        Get related skills.
        :return: skills object
        """
        sub_objs = WorkCategories.objects.filter(pk__in=sub)

        return sub_objs

    def check_user_exist(self, user):
        try:

            data = Users.objects.get(email=user)

        except:
            data = None
        return data

    def check_user_exist_invited_company(self, user):
        try:

            data = Invited_Freelancer_Company_User.objects.get(user__email=user)

        except:
            data = None
        return data

    def client_user(self, user):
        try:

            data = Invited_Client_Company_User.objects.get(user__email=user)

        except:
            data = None
        return data
