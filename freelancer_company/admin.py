from django.contrib import admin

# Register your models here.
from freelancer_company.models import Freelancer_Company_Profile, Freelancer_company_Portfolio, \
    Freelancer_company_Certifications, Freelancer_company_HourlyRate, Freelancer_company_LocationDetails, \
    Invited_Freelancer_Company_User, FreelancerCompanyPortfolioimages

admin.site.register(Freelancer_Company_Profile)
admin.site.register(Freelancer_company_Portfolio)
admin.site.register(Freelancer_company_Certifications)
admin.site.register(Freelancer_company_HourlyRate)
admin.site.register(Freelancer_company_LocationDetails)
admin.site.register(Invited_Freelancer_Company_User)
admin.site.register(FreelancerCompanyPortfolioimages)