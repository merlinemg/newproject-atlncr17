from __future__ import unicode_literals

import datetime
from django.core.validators import MinLengthValidator
from django.db import models

# Create your models here.
from django.template.defaultfilters import slugify
import random
from django.utils.crypto import get_random_string

from accounts.models import BaseTimestamp
from client.models import ClientJobs, Client_Profile, PaymentType
from freelancer.models import Freelancer_Profile
from django.utils.translation import ugettext_lazy as _

class ContractStatus(BaseTimestamp):
    status = models.CharField(max_length=250)

    def __unicode__(self):
        return self.status
    class Meta:
        verbose_name = "Contract Status"
        verbose_name_plural = "Contract Status"

class MileStoneStatus(BaseTimestamp):
    status = models.CharField(max_length=250)

    def __unicode__(self):
        return self.status

    class Meta:
        verbose_name = "MileStone Status"
        verbose_name_plural = "MileStone Status"
class JobsMilestone(models.Model):
    """
       Job Milestone model.
    """
    MILESTONE_STATUS = (
        ('DRAFT', 'Draft'),
        ('PENDING', 'Pending'),
        ('CHANGE', 'Change'),
        ('APPROVED', 'Approved'),
        ('COMPLETED', 'Completed'),
    )
    milestone_title = models.CharField(_('Milestone Title'), max_length=50)
    milestone_description = models.TextField(_('Milestone Description'), validators=[MinLengthValidator(00)])
    start_date = models.DateField(_('Milestone start date'), max_length=255, default=datetime.date.today)
    close_date = models.DateField(_('Milestone close date'), max_length=255, default=datetime.date.today)
    rate = models.IntegerField(_('Hire Payment'), default=0)

    status = models.ForeignKey(MileStoneStatus, related_name='milestone_status', verbose_name="Milestone Status",
                               blank=True,
                               null=True)
    # history = models.ManyToManyField('MilestoneHistory', related_name='hire_milestone_history',
    #                                  verbose_name="Contract Milestone History", blank=True)
    # add_status = models.BooleanField(default=False, verbose_name="Added After approve contract")
    # change_rate_flag = models.BooleanField(default=False)

    def __str__(self):
        return self.milestone_title

    class Meta:
        verbose_name = "Milestone"
        verbose_name_plural = "Milestones"
        default_permissions = ('add', 'change', 'delete', 'view')

class HiredJobsContract(BaseTimestamp):
    """
    Hire Jobs model.
    """
    STATUS = (
        ('DRAFT', 'Draft'),
        ('PENDING', 'Pending'),
        ('HOLD', 'Hold'),
        ('APPROVED', 'Approved'),
        ('CANCEL', 'Cancel'),
    )
    PAYMENT_TYPE = (
        ('Fixed', 'Fixed'),
        ('Hourly', 'Hourly'),
    )
    client_job = models.ForeignKey(ClientJobs, related_name='hire_jobs', verbose_name="Client Job")
    freelancer_profile = models.ForeignKey(Freelancer_Profile, related_name='hire_freelancer',
                                           verbose_name="Hired Freelancer")
    client_profile = models.ForeignKey(Client_Profile, related_name='hire_client', verbose_name="Hired Client")
    job_title = models.CharField(_('Hire Job title'), max_length=255)
    contract_number = models.CharField(_('Job Contract Number'), max_length=20, default=0)
    # job_main_category = models.ForeignKey(WorkCategories, related_name='hire_job_main_category',
    #                                       verbose_name="Hire Job Category")
    job_description = models.TextField(_('Hire Job Description'), validators=[MinLengthValidator(00)])
    payment_type =models.ForeignKey(PaymentType, related_name='hire_payment_type',
                                     verbose_name="Payment Type",blank=True, null=True)
    #amount = models.IntegerField(_('Hire Amount'), default=0)
    budget = models.IntegerField(_('Budget'), default=0, blank=True, null=True)
    hourly_max = models.IntegerField(_('Hourly Max'),  blank=True, null=True)
    hourly_min = models.IntegerField(_('Hourly Min'),  blank=True, null=True)

    #company = models.CharField(_('Company Name'), null=True, max_length=255, default='')

    milestone = models.ManyToManyField(JobsMilestone, related_name='hire_job_milestones',
                                       verbose_name="Jobs Milestone", blank=True)
    # history = models.ManyToManyField('HiredJobHistory', related_name='hire_history',
    #                                    verbose_name="Hired Contract History", blank=True)

    slug = models.SlugField(max_length=255, unique=True,auto_created=True,editable=False)
    # status = models.CharField(max_length=10, choices=STATUS, default='DRAFT',
    #                           verbose_name="contract status")
    status = models.ForeignKey(ContractStatus, related_name='contract_status', verbose_name="Contract Status", blank=True,
                                   null=True)

    # change_made_client = models.BooleanField(default=False)
    # change_rate_flag = models.BooleanField(default=False)
    # approved = models.BooleanField(default=False, verbose_name="Is approved once")
    repost_status = models.CharField(max_length=6, default='0', verbose_name="Repost status")
    approved_date = models.DateTimeField(verbose_name='Date', auto_now_add=False, null=True, blank=True)

    def __str__(self):
        return "{0}::{1}".format(self.client_job.user.first_name, self.freelancer_profile.user.last_name)

    def save(self, *args, **kwargs):
        super(HiredJobsContract, self).save(*args, **kwargs)
        if not self.slug:
            unique_id = get_random_string(length=15)
            self.slug = slugify(self.job_title) + "-" + unique_id
            lower = 100 * 1000
            upper = lower * 1000
            random_num = random.randint(lower, upper)
            contract_num = 'CR_'+str(random_num)
            self.contract_number = contract_num
            self.save()

    class Meta:
        verbose_name = "HireJob"
        verbose_name_plural = "HireJobs"
class TimesheetStatus(BaseTimestamp):
    status = models.CharField(max_length=250)

    def __unicode__(self):
        return self.status

    class Meta:
        verbose_name = "Timesheet Status"
        verbose_name_plural = "Timesheet Status"

class TimeWork(BaseTimestamp):
    """contract_select_box
       individual work time entry
    """

    STATUS = (
        ('DRAFT', 'Draft'),
        ('SEND', 'Send'),
        ('REJECTED', 'Rejected'),
        ('APPROVED', 'Approved'),
        ('ADJUST', 'Adjust'),
    )
    contract = models.ForeignKey(HiredJobsContract, related_name='contract', verbose_name="Contract")
    start_date = models.DateField(_('Week Start Date'), null=True, blank=True)
    end_date = models.DateField(_('Week End Date'), null=True, blank=True)
    #week_sum = models.TimeField(_('Time Sheet Sum'), blank=True, null=True, default=datetime.time(00, 00, 00))
    week_sum = models.CharField(_('Time Sheet Sum'), max_length=250 ,blank=True, null=True)
    status = models.ForeignKey(TimesheetStatus, related_name='timesheet_status', verbose_name="Timesheet Status",
                               blank=True,null=True)
    #time = models.TimeField(_('Time'), null=True, blank=True)
    time =models.CharField(_('Time'), max_length=250,blank=True, null=True)

    def __str__(self):
        return "{0}::{1}::{2}".format(self.contract.job_title, self.start_date, self.end_date)

    class Meta:
        verbose_name = "TimeWork"
        verbose_name_plural = "TimeWorks"
        default_permissions = ('add', 'change', 'delete', 'view')
class WeeklyTimeEntry(BaseTimestamp):
    """contract_select_box
       individual work time entry
    """
    week = models.OneToOneField(TimeWork, related_name='weekly_time', verbose_name="weekly_time")
    date_mon = models.DateField(_('Date monday'), blank=True)
    date_tue = models.DateField(_('Date tuesday'), blank=True)
    date_wed = models.DateField(_('Date wednesday'), blank=True)
    date_thu = models.DateField(_('Date thursday'), blank=True)
    date_fri = models.DateField(_('Date friday'), blank=True)
    date_sat = models.DateField(_('Date saturday'), blank=True)
    date_sun = models.DateField(_('Date sunday'), blank=True)
    time_mon = models.DurationField(null=True, blank=True)
    time_tue = models.DurationField(null=True, blank=True)
    time_wed = models.DurationField(null=True, blank=True)
    time_thu = models.DurationField(null=True, blank=True)
    time_fri = models.DurationField(null=True, blank=True)
    time_sat = models.DurationField(null=True, blank=True)
    time_sun = models.DurationField(null=True, blank=True)

    def __str__(self):
        return "{0}::{1}".format(self.date_mon, self.time_sun)

    class Meta:
        verbose_name = "Weekly Time Entry"
        verbose_name_plural = "Weekly Time Entrys"

